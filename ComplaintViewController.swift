//
//  ComplaintViewController.swift
//  purZ
//
//  Created by Vertace on 23/11/17.
//  Copyright © 2017 Vertace. All rights reserved.
//



import UIKit

class ComplaintViewController: UIViewController, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource, UITabBarControllerDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var remitter_lbl: UILabel!
    @IBOutlet weak var beneficiary_lbl: UILabel!
    @IBOutlet weak var amount_lbl: UILabel!
    @IBOutlet weak var date_lbl: UILabel!
    @IBOutlet weak var transactionType_lbl: UILabel!
    @IBOutlet weak var transactionID_lbl: UILabel!
    @IBOutlet weak var remarks_lbl: UILabel!
    @IBOutlet weak var status_lbl: UILabel!
    @IBOutlet weak var reasonbtn: UIButton!
    
    @IBOutlet weak var complaint_textView: UITextView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var remitter = ""
    var beneficiary = ""
    var amount = ""
    var date = ""
    var transactionType = ""
    var transactionID = ""
    var remarks = ""
    var status = ""
    var complaintRefNo = ""
    var complaintStatus = ""
    var tap: UITapGestureRecognizer! = UITapGestureRecognizer()
    
    var complaintReasonCode = [String]()
    var ReasonCode = [String]()
    var complaint_reason_code = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialize()
    }
    
    func initialize() {
        tabBarController?.delegate = self
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        self.tableView.removeGestureRecognizer(tap)
        reasonbtn.contentHorizontalAlignment = .Left
        complaint_textView.layer.cornerRadius = 5
        remitter_lbl.text = remitter
        beneficiary_lbl.text = beneficiary
        amount_lbl.text = amount
        date_lbl.text = date
        transactionType_lbl.text = transactionType
        transactionID_lbl.text = transactionID
        remarks_lbl.text = remarks
        status_lbl.text = status
        self.tableView.scrollEnabled = false
        
        self.tableView.layer.backgroundColor = CGColorCreate(CGColorSpaceCreateDeviceRGB(), [1.0, 1.0, 1.0, 1.0])
        self.tableView.layer.masksToBounds = false
        self.tableView.layer.cornerRadius = 2.0
        self.tableView.layer.shadowOffset = CGSizeMake(-1, 1)
        self.tableView.layer.shadowOpacity = 0.5
        
        if status == "REJECT" {
            status_lbl.textColor = UIColor.redColor()
        }
        tableView.hidden = true
        reasonbtn.addTarget(self, action: #selector(ComplaintViewController.resonBtnAction(_:)), forControlEvents: .TouchUpInside)
        ApiClient().getComplaintReasonCode { (success, error, response) in
            if success {
                var jsonData: NSData?
                
                do
                    
                {
                    
                    jsonData = try NSJSONSerialization.dataWithJSONObject(response, options:NSJSONWritingOptions.PrettyPrinted)
                    let json = JSON(data: jsonData!)
                    for item in json.arrayValue {
                        self.complaintReasonCode.append(item["description"].stringValue)
                        self.ReasonCode.append(item["reasonCode"].stringValue)
                    }
                }
                catch {
                    print("error")
                }
                
            }
            if self.complaintReasonCode.count > 0 {
                self.reasonbtn.setTitle(self.complaintReasonCode[0], forState: UIControlState.Normal)
                self.complaint_reason_code = self.ReasonCode[0]
            }
            self.tableViewHeight.constant = 45 * CGFloat(self.complaintReasonCode.count)
            self.tableView.reloadData()
        }
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        self.view.endEditing(true)
        var p = touch.locationInView(view)
        p.y = p.y - 80
        print(tableView.frame)
        print(p)
        if CGRectContainsPoint(tableView.frame, p) {
            return false
        }
        self.tableView.hidden = true
        return true
    }
    
    
    
    
    func resonBtnAction(sender: UIButton) {
        self.view.endEditing(true)
        tableView.hidden = false
        
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            self.view.endEditing(true)
        }
        return true
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        let reason_lbl = cell.viewWithTag(1) as! UILabel
        reason_lbl.text = self.complaintReasonCode[indexPath.row]
        cell.selectionStyle = .None
        cell.removeGestureRecognizer(tap)
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.complaint_reason_code = self.ReasonCode[indexPath.row]
        dispatch_async(dispatch_get_main_queue()) {
            self.reasonbtn.setTitle(self.complaintReasonCode[indexPath.row], forState: UIControlState.Normal)
        }
        tableView.hidden = true
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return complaintReasonCode.count
    }
    
    @IBAction func backBtnAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func submitBtnAction(sender: AnyObject) {
        
        let reason_str: String = (self.reasonbtn.titleLabel?.text!)! as String
        
        if complaint_textView.text!.isEmpty {
            self.presentViewController(Alert().alert("Please enter valid comment", message: ""), animated: true, completion: nil)
        }
        else {
            self.activityIndicator.startAnimating()
            ApiClient().registerComplaint(self.complaint_reason_code, comment: self.complaint_textView.text!, rrn: transactionID, txnType: transactionType, txnDate: date) { (success, error, response) in
                self.activityIndicator.stopAnimating()
                if success {
                    
                    let response_data = response.valueForKey("MobileAppData")
                    var alertController:UIAlertController?
                    alertController?.view.tintColor = UIColor.blackColor()
                    alertController = UIAlertController(title: "SUCCESS",message: String(response_data!) ,preferredStyle: .Alert)
                    let action1 = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                        // have to go transaction history page
                        self!.performSegueWithIdentifier("Back_to_TransactionHistory", sender: self)
                        })
                    alertController?.addAction(action1)
                    self.presentViewController(alertController!, animated: true, completion: nil)
                }
                else{
                    
                    let response_data = response.valueForKey("message")
                    var alertController:UIAlertController?
                    alertController?.view.tintColor = UIColor.blackColor()
                    alertController = UIAlertController(title: "FAILED",message: String(response_data!) ,preferredStyle: .Alert)
                    let action1 = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                        // have to go transaction history page
                        self!.performSegueWithIdentifier("Back_to_TransactionHistory", sender: self)
                        })
                    alertController?.addAction(action1)
                    self.presentViewController(alertController!, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
