//
//  SigninViewController.swift
//  Cippy
//
//  Created by apple on 16/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit
class SigninViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet var showhidebtn: UIButton!
    @IBOutlet weak var logoimage: UIImageView!
    
    @IBOutlet weak var signupbtn: UIButton!
    @IBOutlet weak var mobilenolbl: UITextField!
    @IBOutlet weak var passwordlbl: UITextField!
    
    @IBOutlet var loginbtn: UIButton!
    
    @IBOutlet weak var forgotbtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var termsandconditionBtn: UIButton!
    var characterCountLimit = 10
    var fromsignup = false
    var mobileno =  ""
    var showpwd = false
    
    var fromOtp = false
    var fadeButton = UIButton()
    var alertController:UIAlertController?
    
    var IdType = ""
    var idNumber = ""
    
    var attrs = [
        NSFontAttributeName : UIFont.systemFontOfSize(19.0),
        NSForegroundColorAttributeName : UIColor.redColor(),
        NSUnderlineStyleAttributeName : 1]
    
    
    
    var temp_lastDigitCharacterLength = 0
    
    override func  viewWillAppear(animated: Bool)
    {
         dispatch_async(dispatch_get_main_queue()) {
        if Appconstant.From_Signin
        {
           Appconstant.From_Signin = false
           
                self.performSegueWithIdentifier("signin_home", sender: self)
            }
        }
        
        self.tabBarController?.hidesBottomBarWhenPushed = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        showhidebtn.hidden = true
        self.tabBarController?.hidesBottomBarWhenPushed = true
        //        if(mobileno.characters.count != 10){
        //                       forgotbtn.userInteractionEnabled = false
        //        }
        // mobilenolbl.setLeftPaddingPoints(10)
        let paddingForFirst = UIView(frame: CGRectMake(0, 0, 10, self.mobilenolbl.frame.size.height))
        //Adding the padding to the second textField
        mobilenolbl.leftView = paddingForFirst
        mobilenolbl.leftViewMode = UITextFieldViewMode .Always
        let paddingForFirst1 = UIView(frame: CGRectMake(0, 0, 10, self.passwordlbl.frame.size.height))
        
        passwordlbl.leftView = paddingForFirst1
        passwordlbl.leftViewMode = UITextFieldViewMode .Always
        
        
        mobilenolbl.delegate = self
        passwordlbl.delegate = self
        navigationController?.navigationBarHidden = true
        
        // Do any additional setup after loading the view, typically from a nib.
        logoimage.layer.borderWidth = 5
        logoimage.layer.masksToBounds = false
        logoimage.layer.borderColor = UIColor(red:230/255.0, green:175/255.0, blue:7/255.0, alpha:1.0).CGColor;          logoimage.layer.cornerRadius =  logoimage.frame.size.height/2
        logoimage.clipsToBounds = true
        loginbtn.layer.cornerRadius = 5
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        signupbtn.titleLabel?.attributedText = NSAttributedString(string: "Sign up", attributes: underlineAttribute)
        forgotbtn.titleLabel?.attributedText = NSAttributedString(string: "Forgot Password?", attributes: underlineAttribute)
        termsandconditionBtn.titleLabel?.attributedText = NSAttributedString(string: "Terms & Conditions", attributes: underlineAttribute)
        termsandconditionBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(12)
        settextfield()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SigninViewController.dismissKeyboard))
        scrollView.addGestureRecognizer(tap)
        mobilenolbl.delegate = self
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 560)
        
        if fromOtp
        {
            mobilenolbl.text = Appconstant.mobileno
        }
        
        if(mobileno != ""){
            mobilenolbl.text = mobileno
            self.passwordlbl.becomeFirstResponder()
        }
        
        
        self.navigationController?.navigationBarHidden = true
        
    }
    @IBAction func termscondition(sender: AnyObject) {
        
        UIApplication.sharedApplication().openURL(NSURL(string: "https://yappay.in/app-static/eqwallet/tnc.html")!)
        
    }
    
    func settextfield(){
        mobilenolbl.font = UIFont(name: "calibri", size: 15.0)
        passwordlbl.font = UIFont(name: "calibri", size: 15.0)
        mobilenolbl.attributedPlaceholder = NSAttributedString(string:"Mobile Number",
                                                               attributes:[NSForegroundColorAttributeName: UIColor.grayColor()])
        //        passwordlbl.layer.borderColor = UIColor( red: 153/255, green: 153/255, blue:0/255, alpha: 1.0 ).CGColor
        //        passwordlbl.layer.borderWidth = 2.0let bottomBorder = CALayer()
        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRectMake(0.0, passwordlbl.frame.height - 1 , passwordlbl.frame.width, 1.0)
        bottomLine4.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        passwordlbl.borderStyle = UITextBorderStyle.None
        passwordlbl.layer.addSublayer(bottomLine4)
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, mobilenolbl.frame.size.height - 1 , mobilenolbl.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                mobilenolbl.borderStyle = UITextBorderStyle.None
        mobilenolbl.layer.addSublayer(bottomLine)
        passwordlbl.attributedPlaceholder = NSAttributedString(string:"Password",
                                                               attributes:[NSForegroundColorAttributeName: UIColor.grayColor()])
    }
    
    
    @IBAction func showhideBtnAction(sender: AnyObject) {
        if(!showpwd){
            showpwd = true
            showhidebtn.setTitle("hide", forState: UIControlState.Normal)
            passwordlbl.secureTextEntry = false
        }
        else{
            showpwd = false
            showhidebtn.setTitle("show", forState: UIControlState.Normal)
            passwordlbl.secureTextEntry = true
        }
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 800)
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        if textField == self.mobilenolbl {
            self.passwordlbl.becomeFirstResponder()
            
        }
        return true
    }
    
    

    
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        
        
        if(textFieldToChange == mobilenolbl){
            characterCountLimit = 10
            
        }
       
        
        let userEnteredString = mobilenolbl.text
        
        let newString = (userEnteredString! as NSString).stringByReplacingCharactersInRange(range, withString: string) as NSString
        
        if(newString.length == temp_lastDigitCharacterLength)
        {
            
        }
            
        else if(textFieldToChange == mobilenolbl)
        {
            temp_lastDigitCharacterLength = (newString.length)
            
            if(mobilenolbl.text!.characters.count == 9 && string != "")
            {
                
                if(mobilenolbl.text!+string  == "0000000000" || mobilenolbl.text!+string == "9999999999" || mobilenolbl.text!+string == "8888888888" || mobilenolbl.text!+string == "7777777777" || mobilenolbl.text!+string == "6666666666" || mobilenolbl.text!+string == "5555555555" || mobilenolbl.text!+string == "4444444444" || mobilenolbl.text!+string == "3333333333" || mobilenolbl.text!+string == "2222222222" || mobilenolbl.text!+string == "1111111111")
                {
                    self.presentViewController(Alert().alert("Please enter the valid mobile number!", message: ""),animated: true,completion: nil)
                    mobilenolbl.text = ""
                }
                else
                {
                    forgotbtn.userInteractionEnabled = true
                    let number = self.mobilenolbl.text! + string
                    sendrequesttoserver(Appconstant.BASE_URL+Appconstant.MANAGER_BUSINESS_ENTITY+Appconstant.URL_CHECK_CUSTOMER+number)
                }
            }
            else
            {
                
            }
            
        }
        
        
        
        if(textFieldToChange == passwordlbl){
            characterCountLimit = 4
            if(passwordlbl.text!.characters.count == 4)
            {
                showhidebtn.hidden = false
            }
            
        }
        if textFieldToChange.placeholder == "Enter the Number"
        {
            characterCountLimit = 16
        }
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        print(characterCountLimit)
        return newLength <= characterCountLimit
    }
    
    func sendrequesttoserver(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Bad Gateway", message: ""),animated: true,completion: nil)
                }
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            let json = JSON(data: data!)
            
            let item = json["result"]
            print(item)
            if((item["registered"].stringValue == "true")&&(item["isPassCodeSet"].stringValue == "true")){
                dispatch_async(dispatch_get_main_queue()) {
                    self.passwordlbl.becomeFirstResponder()
                }
            }
            else if((item["registered"].stringValue != "true")&&(item["isPassCodeSet"].stringValue != "true") ){
                
                dispatch_async(dispatch_get_main_queue()) {
                    //                        self.disableTouchesOnView(self.view)
                    //                        var alertController:UIAlertController?
                    self.alertController?.view.tintColor = UIColor.blackColor()
                    self.alertController = UIAlertController(title: "Purz",
                                                             message: "Have we met before?Looks like you haven't registered with us.Please Register!",
                                                             preferredStyle: .Alert)
                    
                    let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                        self!.performSegueWithIdentifier("signin_signup", sender: self) })
                    self.alertController?.addAction(action)
                    //                    alertController?.addAction(action1)
                    self.presentViewController(self.alertController!, animated: true, completion: nil)
                    
                    
                }
            }
                
            else {
                dispatch_async(dispatch_get_main_queue()) {
                    var alertController1:UIAlertController?
                    alertController1?.view.tintColor = UIColor.blackColor()
                    let action1 = UIAlertAction(title: "Invalid Credentials", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                        })
                    alertController1?.addAction(action1)
                    
                }
            }
            
            
            
        }
        
        task.resume()
        
    }
    
    func enableTouchesOnView(view : UIView)
    {
        view.viewWithTag(42)?.removeFromSuperview()
    }
    
    
    @IBAction func loginbtnAction(sender: AnyObject) {
        
        
        let mobileno = self.mobilenolbl.text!
        Appconstant.mobileno = mobileno
        Appconstant.pwd = self.passwordlbl.text!
        if((mobileno == "")){
            self.presentViewController(Alert().alert("Oops! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
            
        else if(mobileno.characters.count != 10 ){
            self.presentViewController(Alert().alert("Oops! Something is wrong, please check the number and try again.", message: ""),animated: true,completion: nil)
        }
            
        else if((Appconstant.pwd == "")){
            self.presentViewController(Alert().alert("Oops! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
            
        else if(Appconstant.pwd.characters.count != 4){
            self.presentViewController(Alert().alert("Punch in a 4 digit password", message: ""),animated: true,completion: nil)
        }
            
        else{
            activityIndicator.startAnimating()
            let mobilenumber = "+91" + mobileno
            var passcode = self.passwordlbl.text!
            let checkpwd: Int = Int(passcode.substringWithRange(passcode.startIndex.advancedBy(0)..<passcode.startIndex.advancedBy(1)))!
            if checkpwd == 0 {
                passcode = "1" + passcode
            }
            
            let signinviewmodel = SigninViewModel.init(yapcode: passcode, businessId: mobilenumber, business: "EQWALLET", mobileNumber: mobilenumber, passcode: passcode, appGuid: Appconstant.gcmid)!
            let serializedjson  = JSONSerializer.toJson(signinviewmodel)
            sendrequesttoserverForSignin(Appconstant.BASE_URL+Appconstant.URL_VERIFY_EXIST_CUSTOMER, values: serializedjson)
            
        }
        
    }
    func sendrequesttoserverForSignin(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        //        request.setValue("\(values.characters.count)", forHTTPHeaderField: "Content-Length")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                }
                
                
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            let json = JSON(data: data!)
            
            let item = json["result"]
            if json["result"] != nil{
                if(item["entityId"].stringValue != ""){
                    Appconstant.customerid = item["entityId"].stringValue
                    dispatch_async(dispatch_get_main_queue()) {
                        self.sendrequesttoserverForGetCustomerDetail(Appconstant.BASE_URL+Appconstant.URL_FETCH_CUSTOMER_DETAILS_BY_MOBILENO+Appconstant.mobileno)
                    }
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Incorrect Password", message: ""),animated: true,completion: nil)
                        
                    }
                }
            }
            else {
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Incorrect Password", message: ""),animated: true,completion: nil)
                    
                }
            }
            
        }
        
        task.resume()
        
    }
    
    func sendrequesttoserverForGetCustomerDetail(url : String)
    {
        print(url)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Bad Gateway", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseStringcustomerDetails = \(responseString)")
                let json = JSON(data: data!)
//                dispatch_async(dispatch_get_main_queue()) {
//                    self.activityIndicator.stopAnimating()
//                }
                let item = json["result"]
                if(item["customerId"].stringValue != ""){
                    DBHelper().purzDB()
                    let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
                    let databasePath = databaseURL.absoluteString
                    let purzDB = FMDatabase(path: databasePath as String)
                    if purzDB.open() {
                        let email = "empty"
                        Appconstant.CustomerType = item["customerType"].stringValue
                        Appconstant.KitNo = item["kitNo"].stringValue
                        Appconstant.dateOfBirth = item["dob"].stringValue
                        Appconstant.expiredate = item["expiryDateList"][0].stringValue
                        Appconstant.IdType = item["idType"].stringValue
                        Appconstant.IdNumber = item["idNumber"].stringValue
                        print("IdType\(Appconstant.IdType)")
                         print("IdNumber\(Appconstant.IdNumber)")
                        
////                        let dateFormatter = NSDateFormatter()
////                        dateFormatter.dateFormat = "dd MMM yyyy tt:mm:ss GMT"
////                        dateFormatter.timeZone = NSTimeZone(name: "UTC")
////                        let dateString = dateFormatter.stringFromDate(date)
//                        
//                        
//                        let dateFormatter = NSDateFormatter()
//                        dateFormatter.dateFormat = "dd MMM yyyy tt:mm:ss"
////                        dateFormatter.timeZone = NSTimeZone(name: "UTC")
//                        let dateString = dateFormatter.stringFromDate(date)
////                        (item["expiryDate"].stringValue)
//                         print(dateString)
//
////                        let dateFormatter1 = NSDateFormatter()
////                        dateFormatter1.dateFormat = "yyMM"
////                        let dateString1 = dateFormatter1.stringFromDate(dateString!)
////                        (dateString!)
//                        
//                                 print(dateString)
////                        print(dateString1)
//                     
                       
                        print(Appconstant.expiredate)
                        if(item["firstName"].stringValue == item["lastName"].stringValue){
                            Appconstant.customername = item["firstName"].stringValue
                        }
                        else if item["lastName"].stringValue == "" {
                            Appconstant.customername = item["firstName"].stringValue
                        }
                        else{
                            Appconstant.customername = item["firstName"].stringValue
                        }
                        Appconstant.cardnumber = item["cardNo"].stringValue
                     
                        print("6"+Appconstant.customername+"name")
                        print(item["dob"].stringValue)
                        let insert = "INSERT INTO PROFILE_INFO (CUSTOMER_ID,YAP_CODE,CUSTOMER_NAME,MOBILE_NUMBER,EMAIL,CUSTOMER_TYPE,CARDNUMBER,EXPIREDATE,KITNO,DATEOFBIRTH,NIKI) VALUES ('\(item["customerId"].stringValue)','\(Appconstant.pwd)','\(Appconstant.customername)','\(Appconstant.mobileno)','\(email)','\( Appconstant.CustomerType)','\(item["cardNo"].stringValue)','\(Appconstant.expiredate)','\(Appconstant.KitNo)','\(Appconstant.dateOfBirth)','\("")')"
                        let result = purzDB.executeUpdate(insert,
                                                          withArgumentsInArray: nil)
                        var question = "null"
                        var answer = "null"
                        print(item["description"].stringValue)
                        if(item["description"].stringValue != "") && (item["description"].stringValue != "null") {
                            let description = item["description"].stringValue.componentsSeparatedByString(":")
                            for(var i = 0; i<description.count; i++){
                                if(i == 0){
                                    question = description[0]
                                }
                                else if(i == 1){
                                    answer = description[1]
                                }
                            }
                        }
                        let imgurl = ""
                        let insertsql = "INSERT INTO CUSTOMERDETAIL (CUSTOMER_ID,DATE_OF_BIRTH,CUSTOMER_BANK,IMAGE_PATH,ADDRESS_LINE_1,ADDRESS_LINE_2,CITY,STATE,PIN,SECURITY_QUESTION,SECURITY_ANSWER,ID_TYPE,ID_NUMBER) VALUES ('\(Appconstant.customerid)','\(item["dob"].stringValue)','\("DCB")','\(imgurl)','\(item["address"].stringValue)','\(item["address2"].stringValue)','\(item["city"].stringValue)','\("Tamil nadu")','\(item["pincode"].stringValue)','\(question)','\(answer)','\(item["idType"].stringValue)','\(item["idNumber"].stringValue)')"
                        
                        let result2 = purzDB.executeUpdate(insertsql,
                                                           withArgumentsInArray: nil)
                        
                        if !result && !result2 {
                            //   status.text = "Failed to add contact"
                            print("Error: \(purzDB.lastErrorMessage())")
                            dispatch_async(dispatch_get_main_queue()) {
                                self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                                
                            }
                        }
                            else if Appconstant.IdNumber == ""
                            && Appconstant.IdType == ""
                         
                        {
                            dispatch_async(dispatch_get_main_queue()) {
                                Appconstant.From_Signin = true
                              self.performSegueWithIdentifier("To_GetId", sender: self)
                            }
                        }
                        else{
                            dispatch_async(dispatch_get_main_queue()) {
                                print("UPDATE successfully")

                                self.performSegueWithIdentifier("signin_home", sender: self)
                            }
                        }
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        
                    }
                }
            }
        }
        
        task.resume()
        
    }
//    func typeGetAlert()
//    {
//        let alertController = UIAlertController(title: "Please select your Card Type to continue app",
//                                                message: "",
//                                                preferredStyle: .Alert)
//        
//        let action = UIAlertAction(title: "PAN Card", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//            dispatch_async(dispatch_get_main_queue()) {
//                Appconstant.IdType = "PAN Card"
//                self!.afterGetType()
//            }p
//            
//            })
//        let action1 = UIAlertAction(title: "Passport", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//            dispatch_async(dispatch_get_main_queue()) {
//                 Appconstant.IdType = "Passport"
//                self!.afterGetType()
//            }
//            
//            })
//        let action2 = UIAlertAction(title: "Driving License", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//            dispatch_async(dispatch_get_main_queue()) {
//                 Appconstant.IdType = "Driving License"
//                self!.afterGetType()
//            }
//            
//            })
//        let action3 = UIAlertAction(title: "Voter's ID", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//            dispatch_async(dispatch_get_main_queue()) {
//                Appconstant.IdType = "Voter's ID"
//                self!.afterGetType()
//            }
//            
//            })
//        let action4 = UIAlertAction(title: "Aadhaar No", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//            dispatch_async(dispatch_get_main_queue()) {
//                 Appconstant.IdType = "Aadhaar No"
//                self!.afterGetType()
//            }
//            
//            })
//      
//        alertController.addAction(action)
//         alertController.addAction(action1)
//        alertController.addAction(action2)
//        alertController.addAction(action3)
//        alertController.addAction(action4)
//       
//        self.presentViewController(alertController, animated: true, completion: nil)
//    }
//    func idGetAlert()
//    {
//        let alertController = UIAlertController(title: "Please select your Card Type",
//                                                message: "",
//                                                preferredStyle: .Alert)
//        
//        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//            dispatch_async(dispatch_get_main_queue()) {
//               self!.typeGetAlert()
//            }
//            
//            })
//               alertController.addAction(action)
//        
//        self.presentViewController(alertController, animated: true, completion: nil)
//    }
//    func afterGetType()
//    {
//        
//            var alertController:UIAlertController?
//            alertController?.view.tintColor = UIColor.blackColor()
//            alertController = UIAlertController(title: "Please enter the " +   (Appconstant.IdType) + "'s number to continue app",
//                                                message: "",
//                                                preferredStyle: .Alert)
//            alertController!.addTextFieldWithConfigurationHandler(
//                {(textField: UITextField!) in
//                    
//                    textField.placeholder = "Enter the Number"
//                    textField.delegate = self
//                    textField.secureTextEntry  = true
//                    textField.keyboardType = UIKeyboardType.NumberPad
//                    
//            })
//            let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//                if let textFields = alertController?.textFields{
//                    let theTextFields = textFields as [UITextField]
//                    let CardNumber = theTextFields[0].text!
//                    if(CardNumber == ""){
//                        print(CardNumber)
//                        
//                        self!.alert()
//                    }
//                    else{
//                         Appconstant.IdNumber = theTextFields[0].text!
//                        self!.updateCustomerDetails()
//                    }
//                    
//                }
//                })
//        
//            let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//                
//                })
//            
//            alertController?.addAction(action)
//       
//            alertController?.addAction(action2)
//            self.presentViewController(alertController!, animated: true, completion: nil)
//        
//    }
    
    
    @IBAction func useotpbtnAction(sender: AnyObject) {
        
        print("tapped")
        let mobileno = self.mobilenolbl.text!
        if(mobileno == ""){
            self.presentViewController(Alert().alert("Oops! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            
        }
        else if(mobileno.characters.count != 10){
            self.presentViewController(Alert().alert("Oops! Something is wrong,please check the number and try again.", message: ""),animated: true,completion: nil)
        }
            
        else{
            self.activityIndicator.startAnimating()
            Appconstant.mobileno = self.mobilenolbl.text!
            print("NO=="+Appconstant.mobileno)
            print(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+self.mobilenolbl.text!)
            forgotbtn.userInteractionEnabled = false
            self.sendrequesttoserverForGenerateOTP(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+self.mobilenolbl.text!)
            
            
            
        }
    }
    func sendrequesttoserverForGenerateOTP(url : String)
    {
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                dispatch_async(dispatch_get_main_queue()) {
                    self.forgotbtn.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
                }
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    
                    self.forgotbtn.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
                    self.presentViewController(Alert().alert("Bad Gateway", message: ""),animated: true,completion: nil)
                }
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            
            let item = json["result"]
            if(item["success"].stringValue == "true"){
                dispatch_async(dispatch_get_main_queue()) {
                    Appconstant.otp = item["otp"].stringValue
                    Appconstant.customerid = item["customerId"].stringValue
                    Appconstant.customername =  item["customerName"].stringValue
                    print("Cus===="+Appconstant.customername)
                    self.activityIndicator.stopAnimating()
                    self.performSegueWithIdentifier("signin_otp", sender: self)
                }
            }
                
            else{
                dispatch_async(dispatch_get_main_queue()) {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Verifying", message: "Buffering..Blah Blah!!"),animated: true,completion: nil)
                    }
                }
            }
        }
        
        task.resume()
        
    }
    
    
    @IBAction func CallBtnAction(sender: AnyObject) {
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "Purz",
                                            message: "Are you sure  you want to call our support?",
                                            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            })
        let action1 = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            let url:NSURL = NSURL(string: "tel://180030001222")!
            UIApplication.sharedApplication().openURL(url)
            
            })
        
        alertController?.addAction(action)
        alertController?.addAction(action1)
        self.presentViewController(alertController!, animated: true, completion: nil)
        
    }
    
    
    @IBAction func signupBtnAction(sender: AnyObject) {
        mobilenolbl.text = ""
        passwordlbl.text = ""
        self.performSegueWithIdentifier("signin_signup", sender: self)
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "signin_otp") {
            let nextview = segue.destinationViewController as! OTPViewController
            nextview.fromsignin = true
        }
        if(segue.identifier == "signin_signup")
        {
            let nextview = segue.destinationViewController as! SignupViewController
            nextview.mobileNo = self.mobilenolbl.text!
            nextview.fromsignin = true
            
        }
        
    }
    
    func dismissKeyboard(){
        self.view.endEditing(true)
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 650)
    }
    
}
