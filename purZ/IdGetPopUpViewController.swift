//
//  IdGetPopUpViewController.swift
//  purZ
//
//  Created by Vertace on 29/12/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class IdGetPopUpViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var headerTxtFiels: UILabel!
    @IBOutlet weak var bottomSpace_View: NSLayoutConstraint!
    @IBOutlet weak var popUpHeight: NSLayoutConstraint!
   
    @IBOutlet weak var panCardView: UIView!
    @IBOutlet weak var panCardCheckBox: UIButton!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordCheckBox: UIButton!
    @IBOutlet weak var LicenseView: UIView!
    @IBOutlet weak var LicenseCheckBox: UIButton!
    @IBOutlet weak var voterIDView: UIView!
    @IBOutlet weak var voterIdCheckBox: UIButton!
    @IBOutlet weak var AadharnoView: UIView!
    @IBOutlet weak var AadharNoCheckBox: UIButton!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var PopUpView: UIView!
    var characterCountLimit = 0
    @IBOutlet weak var id_view_Space: NSLayoutConstraint!
    @IBOutlet weak var id_cardViewSpace: NSLayoutConstraint!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var IdTxtField: UITextField!
    var isValidNumber = false
    override func viewDidLoad() {
        super.viewDidLoad()
        initialFunc()
 
if Appconstant.FromProfile_IDType
{
    popUpHeight.constant = 300
    IdTxtField.hidden = true
 }
else{
    IdTxtField.hidden = false
    popUpHeight.constant = 320
        }
        // Do any additional setup after loading the view.
    }
    func setShadow(view: UIView) {
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowOffset = CGSizeZero
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 3
        view.layer.cornerRadius = 5
    }

func initialFunc()
{
//   headerTxtFiels.text = "Please fill the following details it's necessary \n to continue your service!"
    view.userInteractionEnabled = true
   headerTxtFiels.userInteractionEnabled = false
    IdTxtField.delegate = self
    
    let bottomLine = CALayer()
    bottomLine.frame = CGRectMake(0, headerTxtFiels.frame.size.height - 2 , headerTxtFiels.frame.size.width, 1.0)
    bottomLine.backgroundColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha:1.0).CGColor;
       headerTxtFiels.layer.addSublayer(bottomLine)
    PopUpView.layer.borderColor = UIColor.whiteColor().CGColor
    PopUpView.layer.borderWidth = 1
        PopUpView.layer.cornerRadius = 9
    setShadow(PopUpView)

    
    cardView.layer.borderColor = UIColor.lightGrayColor().CGColor
    cardView.layer.borderWidth = 1
    cardView.layer.cornerRadius = 9
//okBtn.layer.borderColor = UIColor.blackColor().CGColor
//     okBtn.layer.borderWidth = 1
//    cancelBtn.layer.borderColor = UIColor.blackColor().CGColor
//    cancelBtn.layer.borderWidth = 1
    panCardView.userInteractionEnabled = true
   
    let tap11: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(IdGetPopUpViewController.dismissKeyboard))
    view.addGestureRecognizer(tap11)
    
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(IdGetPopUpViewController.panCardViewTapped))
    panCardView.addGestureRecognizer(tap)
     panCardView.userInteractionEnabled = true
      panCardCheckBox.addTarget(self, action: #selector(panCardViewTapped), forControlEvents: .TouchUpInside)
    
    passwordView.userInteractionEnabled = true
    let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "passwordViewTapped")
    passwordView.addGestureRecognizer(tap1)
    passwordCheckBox.addTarget(self, action: #selector(passwordViewTapped), forControlEvents: .TouchUpInside)

     LicenseView.userInteractionEnabled = true
    let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "LicenseViewTapped")
    LicenseView.addGestureRecognizer(tap2)
    LicenseCheckBox.addTarget(self, action: #selector(LicenseViewTapped), forControlEvents: .TouchUpInside)

    voterIDView.userInteractionEnabled = true
    let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "voterIDViewTapped")
    voterIDView.addGestureRecognizer(tap3)
    voterIdCheckBox.addTarget(self, action: #selector(voterIDViewTapped), forControlEvents: .TouchUpInside)

    AadharnoView.userInteractionEnabled = true
    let tap4: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "AadharnoViewTapped")
    AadharnoView.addGestureRecognizer(tap4)
    AadharNoCheckBox.addTarget(self, action: #selector(AadharnoViewTapped), forControlEvents: .TouchUpInside)

    }
    @IBAction func skipBtnAction(sender: AnyObject) {
        exit(0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func dismissKeyboard(){
        self.view.endEditing(true)
        
    }
    func panCardViewTapped()
    {
        panCardCheckBox.setImage(UIImage(named: "radio-on-button-1.png"), forState: .Normal)
         passwordCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
         LicenseCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
         voterIdCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
         AadharNoCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        Appconstant.IdType = "PAN Card"
        
    }
    func passwordViewTapped()
    {
        panCardCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        passwordCheckBox.setImage(UIImage(named: "radio-on-button-1.png"), forState: .Normal)
        LicenseCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        voterIdCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        AadharNoCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        Appconstant.IdType = "Passport"
        
    }
    
    func LicenseViewTapped()
    {
        panCardCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        passwordCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        LicenseCheckBox.setImage(UIImage(named: "radio-on-button-1.png"), forState: .Normal)
        voterIdCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        AadharNoCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        Appconstant.IdType = "Driving License"
        
    }
    func voterIDViewTapped()
    {
        panCardCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        passwordCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        LicenseCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        voterIdCheckBox.setImage(UIImage(named: "radio-on-button-1.png"), forState: .Normal)
        AadharNoCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        Appconstant.IdType = "Voter ID"
        
    }
    func AadharnoViewTapped()
    {
        panCardCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        passwordCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        LicenseCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        voterIdCheckBox.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        AadharNoCheckBox.setImage(UIImage(named: "radio-on-button-1.png"), forState: .Normal)
        Appconstant.IdType = "Aadhaar No"
        
    }
    
    func ValidIDNumber() {
        var isValid = ""
        
        if Appconstant.IdType == "Aadhaar No" {
            isValid = checkValidExpression(self.IdTxtField.text!, myPattern: "[0-9]{12}")
        }
        else if Appconstant.IdType == "PAN Card" {
            isValid = checkValidExpression(self.IdTxtField.text!, myPattern: "[A-Za-z]{5}\\d{4}[A-Za-z]{1}")
        }
        else if Appconstant.IdType == "Passport" {
            isValid = checkValidExpression(self.IdTxtField.text!, myPattern: "^(?!^0+$)[a-zA-Z0-9]{3,20}$")
        }
        else if Appconstant.IdType == "Driving License" {
            isValid = checkValidExpression(self.IdTxtField.text!, myPattern: "[A-Za-z]{2}\\d{13}")
        }
        else if Appconstant.IdType == "Voter ID" {
            isValid = checkValidExpression(self.IdTxtField.text!, myPattern: "[A-Za-z]{3}\\d{7}")
        }
        
        if isValid == "valid" {
            updateCustomerIDDetails()
        }
        else {
            if Appconstant.IdType == "Aadhaar No" {
                
                    self.presentViewController(Alert().alert("Please enter the valid Aadhaar number", message: ""),animated: true,completion: nil)
          
            }
            else if Appconstant.IdType == "PAN Card" {
               dispatch_async( dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Please enter the valid PAN card number", message: ""),animated: true,completion: nil)
                }

            }
            else if Appconstant.IdType == "Passport" {
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Please enter the valid passport number", message: ""),animated: true,completion: nil)
                }
            }
            else if Appconstant.IdType == "Driving License" {
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Please enter the valid driving license number", message: ""),animated: true,completion: nil)
                }
                
            }
            else if Appconstant.IdType == "Voter ID" {
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Please enter the valid voter id", message: ""),animated: true,completion: nil)
                }
            }
            }
        }

    
    func checkValidExpression(numberText:String, myPattern:String)-> String
    {
        
        let myString = "\(numberText)"
        let regex = try! NSRegularExpression(pattern: myPattern, options: NSRegularExpressionOptions.CaseInsensitive)
        let range = NSMakeRange(0, myString.characters.count)
//        let CountString = myString.characters.count
        let modString = regex.stringByReplacingMatchesInString(myString, options: [], range: range, withTemplate: "valid")
        
        return modString
    }
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textFieldToChange == IdTxtField {
            characterCountLimit = 20
        }

//            if Appconstant.IdType == "Aadhaar No" {
//                characterCountLimit = 12
//                isValid = checkValidExpression(self.IdTxtField.text!, myPattern: "[0-9]{12,12}")
//            }
//            else if Appconstant.IdType == "PAN Card" {
//                characterCountLimit = 11
//                isValid = checkValidExpression(self.IdTxtField.text!, myPattern: "[A-Za-z]{5}\\d{4}[A-Za-z]{1}")
//            }
//            else if Appconstant.IdType == "Passport" {
//                characterCountLimit = 20
//                isValid = checkValidExpression(self.IdTxtField.text!, myPattern: "^(?!^0+$)[a-zA-Z0-9]{3,20}$")
//            }
//            else if Appconstant.IdType == "Driving License" {
//                characterCountLimit = 16
//                isValid = checkValidExpression(self.IdTxtField.text!, myPattern: "[A-Za-z]{2}\\d{13}")
//            }
//            else if Appconstant.IdType == "Voter ID" {
               //                isValid = checkValidExpression(self.IdTxtField.text!, myPattern: "[A-Za-z]{3}\\d{7}")
//            }
//            
//        }
        
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
        
    }
    
    
    
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if(textField == IdTxtField) {
            animateViewMoving(true, moveValue: 200)
        }
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if(textField == IdTxtField) {
            animateViewMoving(false, moveValue: 200)
        }
    }
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.2
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
        
    }
    @IBAction func OkBtnAction(sender: AnyObject) {
        //        self.performSegueWithIdentifier("To_Home", sender: self)
        view.endEditing(true)
        Appconstant.IdNumber = IdTxtField.text!
        if Appconstant.IdType == ""
        {
            self.presentViewController(Alert().alert("Please Select the ID type", message: ""),animated: true,completion: nil)
            
        }
        else if Appconstant.IdNumber == ""
        {
            self.presentViewController(Alert().alert("Please enter the ID number", message: ""),animated: true,completion: nil)
        }   
        else{
            ValidIDNumber()
        }
        
    }
    
    func alert(){
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(Alert().alert("Please enter a valid id", message: ""),animated: true,completion: nil)
        }
    }
    
    func updateCustomerIDDetails(){
        let currentdate = NSDate()
       let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
  let registeredDate = dateFormatter.stringFromDate(currentdate)
        
        let profileviewmodel = UpdateUserCardIdViewModel.init(entityId: Appconstant.customerid, idType:  Appconstant.IdType, idNumber:  Appconstant.IdNumber, kycType: "MIN", registeredDate: registeredDate, description: "")!
        let serializedjson  = JSONSerializer.toJson(profileviewmodel)
    
        self.sendrequesttoserverForUpdateEntity(Appconstant.BASE_URL+Appconstant.URL_UPDATE_KYCDETAILS, values: serializedjson)
        
    }
    func sendrequesttoserverForUpdateEntity(url : String, values: String)
    {
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                dispatch_async(dispatch_get_main_queue()) {
                    //                self.activityIndicator.stopAnimating()
                }
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {
                dispatch_async(dispatch_get_main_queue()) {
                    //                self.activityIndicator.stopAnimating()
                }
//                print("statusCode should be 200, but is \(httpStatus.statusCode)")  // check for http errors
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                let json = JSON(data: data!)
                print("responseString: \(responseString)")
                if(json["result"].stringValue == "true"){
                    dispatch_async(dispatch_get_main_queue()) {
                        
                       self.saveintoIDDB()
                        let alert = UIAlertController(title: "Your ID is updated successfully!",message: "", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.Default, handler: self.navigateFunc))
                        self.presentViewController(alert, animated: true, completion: nil)
                       
                        
                        
                        
                    }
                    
                }
                
            }
        }
        
        task.resume()
        
    }
  
    func saveintoIDDB(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        
        if purzDB.open() {
            let selectSQL = "SELECT * FROM CUSTOMERDETAIL"
            
            let results:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                           withArgumentsInArray: nil)
            if (results.next()){
                let update11 = "UPDATE CUSTOMERDETAIL SET ID_TYPE='"+Appconstant.IdType+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update12 = "UPDATE CUSTOMERDETAIL SET ID_NUMBER='"+Appconstant.IdNumber+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                
                
                let result11 = purzDB.executeUpdate(update11,
                                                    withArgumentsInArray: nil)
                let result12 = purzDB.executeUpdate(update12,
                                                    withArgumentsInArray: nil)
                
                if (!result11 || !result12){
                    
                    print("Error: \(purzDB.lastErrorMessage())")
                    //                    dispatch_async(dispatch_get_main_queue()) {
                    //                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    //
                    //                    }
                }
            }
            
        }
        purzDB.close()
    }
    func navigateFunc(action: UIAlertAction) {
        
        if Appconstant.From_Signin{
            self.performSegueWithIdentifier("To_Home", sender: self)
        }
        if Appconstant.FromFourDigit_IDType{
            self.performSegueWithIdentifier("To_Home", sender: self)
        }
        if Appconstant.FromOTP_IDType{
            self.performSegueWithIdentifier("To_Home", sender: self)
        }
        

        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
