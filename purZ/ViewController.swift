//
//  ViewController.swift
//  Cippy
//
//  Created by apple on 15/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate {
    
    //        var poseDuration: Float = 1.0
    //    var indexProgressBar: Float = 0.0
    //    var index = 0
    //    var final = 10
    //    var timer = NSTimer()
    
    
    @IBOutlet weak var logoimage: UIImageView!
    @IBOutlet weak var funbankingimage: UIImageView!
    //    @IBOutlet weak var logoimage: UIImageView!
    let img:UIImageView = UIImageView()
    var today_date = ""
    var timer = NSTimer()
    var a: CGFloat = 1.0
    
    
    func addLogo() {
        
        img.frame = CGRectMake( self.view.frame.size.width/2 - (50 + a/2) , self.view.frame.size.height/2 - (50 + a/2) , 100 + a, 100 + a )
        img.layer.cornerRadius = img.frame.size.height/2
        a += 0.5
        
        if(a == 150.0){
            timer.invalidate()
            
        }
        
    }
    
    
    //    func someSelector()
    //    {
    //        funbankingimage.hidden = false
    //    }
    
    func someSelector1(){
        
        let imgfun:UIImageView = UIImageView()
        let imgwidth = img.frame.origin.x+img.frame.size.width-60
        let imgheight = img.frame.origin.y+img.frame.size.height-60
        imgfun.frame = CGRectMake( imgwidth , imgheight , 60, 60 )
        imgfun.layer.cornerRadius =  imgfun.frame.size.height/2
        imgfun.layer.borderWidth = 5
        imgfun.layer.masksToBounds = false
        imgfun.layer.borderColor = UIColor(red:229/255.0, green:177/255.0, blue:22/255.0, alpha:1.0).CGColor;
        imgfun.layer.cornerRadius =  imgfun.frame.size.height/2
        imgfun.clipsToBounds = true
        imgfun.backgroundColor = UIColor(red:255/255.0, green:203/255.0, blue:8/255.0, alpha:1.0)
        self.view.backgroundColor = UIColor.whiteColor()
        
        imgfun.image = UIImage(named: "Fun-Banking.png")
        self.view.addSubview(imgfun)
        
        let bounds = imgfun.bounds
        imgfun.layer.cornerRadius =  imgfun.frame.size.height/2
        
        UIView.animateWithDuration(1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5, options: UIViewAnimationOptions.TransitionNone, animations: {
            imgfun.bounds = CGRect(x: imgwidth - 20, y: imgheight - 20, width: bounds.size.width + 40, height: bounds.size.height + 40)
            
            imgfun.layer.cornerRadius =  imgfun.frame.size.height/2
            imgfun.clipsToBounds = true
            
            }, completion: nil)
        
    }
    func someSelector()
    {
        let imgfun:UIImageView = UIImageView()
        imgfun.frame = CGRectMake( 200 , 315 , 100, 100 )
        
        imgfun.layer.borderWidth = 5
        imgfun.layer.masksToBounds = false
        imgfun.layer.borderColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor; imgfun.layer.cornerRadius =  imgfun.frame.size.height/2
        imgfun.clipsToBounds = true
        imgfun.backgroundColor = UIColor(red:255/255.0, green:203/255.0, blue:8/255.0, alpha:1.0)
        self.view.backgroundColor = UIColor.whiteColor()
        
        imgfun.image = UIImage(named: "Fun-Banking.png")
        self.view.addSubview(imgfun)
        
        
        //IMGFUN PULSE ANIMATION
        let pulseAnimation = CABasicAnimation(keyPath: "opacity")
        pulseAnimation.duration = 0.5
        pulseAnimation.fromValue = 0
        
        pulseAnimation.toValue = 25
        pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        pulseAnimation.autoreverses = true
        pulseAnimation.repeatCount = 2
        imgfun.layer.addAnimation(pulseAnimation, forKey: "animateOpacity")
        imgfun.layer.addAnimation(pulseAnimation, forKey: "opacity")
        
    }
    override func viewDidLoad() {
        
//        CheckJailBreak()
        funbankingimage.hidden = true
        var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("someSelector1"), userInfo: nil, repeats: false)
        
        img.layer.masksToBounds = false
        img.layer.borderColor = UIColor(red:229/255.0, green:177/255.0, blue:22/255.0, alpha:1.0).CGColor;        img.layer.borderWidth = 5
        img.layer.cornerRadius =  75
        
        img.clipsToBounds = true
        img.image = UIImage(named: "Purz-Logo.png")
        super.viewDidLoad()
        navigationController?.navigationBarHidden = true
        
        //FUNBANK IMAGE
        //        img.image = UIImage(named: "Fun-Banking.png")
        
        funbankingimage.layer.borderWidth = 3
        funbankingimage.layer.masksToBounds = false
        funbankingimage.layer.borderColor = UIColor(red:230/255.0, green:175/255.0, blue:7/255.0, alpha:1.0).CGColor;        //funbankingimage.layer.backgroundColor = UIColor(red:230/255.0, green:175/255.0, blue:7/255.0, alpha:1.0).CGColor;
        funbankingimage.layer.cornerRadius =  funbankingimage.frame.size.height/2
        funbankingimage.clipsToBounds = true
        self.view.backgroundColor = UIColor.whiteColor()
        
        
        //TIMER FOR LOGO IMAGE
        
        self.timer = NSTimer.scheduledTimerWithTimeInterval(
            0.001, target: self, selector: "addLogo", userInfo: nil, repeats: true
        )
        self.view.addSubview(img)
        
        //FUNBANK PULSE ANIMATION
        let pulseAnimation = CABasicAnimation(keyPath: "opacity")
        pulseAnimation.duration = 0.5
        pulseAnimation.fromValue = 0
        pulseAnimation.toValue = 5
        pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        pulseAnimation.autoreverses = true
        pulseAnimation.repeatCount = 2
        
        self.funbankingimage.layer.addAnimation(pulseAnimation, forKey: "animateOpacity")
        funbankingimage.layer.addAnimation(pulseAnimation, forKey: "opacity")
        
        
        var timer1 = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: #selector(ViewController.databaseCall), userInfo: nil, repeats: false)
        navigationController?.navigationBar.hidden = true
        
        
    }
    
//    func CheckJailBreak() {
//        let filePath = "/Applications/Cydia.app"
//        if NSFileManager.defaultManager().fileExistsAtPath(filePath) {
//            let alertController = UIAlertController(title: "Warning!",
//                                                message: "This app is running in JailBreak" ,
//                                                preferredStyle: .Alert)
//            
//            let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//                dispatch_async(dispatch_get_main_queue()) {
//                    exit(0)
//                }
//                
//                })
//            
//            alertController.addAction(action)
//            self.presentViewController(alertController, animated: true, completion: nil)
//        }
//
//    }
    
    func databaseCall()
    {
        getversionfromServer(Appconstant.VERSION_CHECK)
        //        self.calldatabase()
    }
    
//    func URLSession(session: NSURLSession, didReceiveChallenge challenge: NSURLAuthenticationChallenge, completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?) -> Void) {
//    let serverTrust = challenge.protectionSpace.serverTrust
//    let certificate = SecTrustGetCertificateAtIndex(serverTrust!, 0)
//
//    // Set SSL policies for domain name check
//    let policies = NSMutableArray();
//    policies.addObject(SecPolicyCreateSSL(true, (challenge.protectionSpace.host)))
//    SecTrustSetPolicies(serverTrust!, policies);
//
//    // Evaluate server certificate
//    var result: SecTrustResultType = 0
//    SecTrustEvaluate(serverTrust!, &result)
//    let isServerTrusted:Bool = (Int(result) == kSecTrustResultUnspecified || Int(result) == kSecTrustResultProceed)
//
//    // Get local and remote cert data
//    let remoteCertificateData:NSData = SecCertificateCopyData(certificate!)
//    let pathToCert = NSBundle.mainBundle().pathForResource(githubCert, ofType: "cer")
//    let localCertificate:NSData = NSData(contentsOfFile: pathToCert!)!
//
//    if (isServerTrusted && remoteCertificateData.isEqualToData(localCertificate)) {
//        let credential:NSURLCredential = NSURLCredential(forTrust: serverTrust!)
//        completionHandler(.UseCredential, credential)
//    } else {
//        completionHandler(.CancelAuthenticationChallenge, nil)
//    }
//}




    
    func getversionfromServer(url: String){
        let defaults = NSUserDefaults.standardUserDefaults()
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        
//        let pathToCert = NSBundle.mainBundle().pathForResource("yappay.in", ofType: "cer")
//        let localCertificate:NSData = NSData(contentsOfFile: pathToCert!)!
//        
//        let serverTrustPolicy = ServerTrustPolicy.PinCertificates(
//            // Getting the certificate from the certificate data
//            certificates: [SecCertificateCreateWithData(nil, localCertificate)!],
//            // Choose to validate the complete certificate chain, not only the certificate itself
//            validateCertificateChain: true,
//            // Check that the certificate mathes the host who provided it
//            validateHost: true
//        )
//        
//        let serverTrustPolicies = [
//            "github.com": serverTrustPolicy
//        ]
//        
//        let afManager = Manager(
//            configuration: NSURLSessionConfiguration.defaultSessionConfiguration(),
//            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
//        )
        

        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else
            {
                  // check for fundamental networking error
                
                self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                
                //                    }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
//                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{

                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                let json = JSON(data: data!)
                let item = json["result"]
                let nsObject = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as! String
                let appversion = nsObject.stringByReplacingOccurrencesOfString(".", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                if Int(appversion) < item["minimumRequiredVersion"].intValue{
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        var alertController:UIAlertController?
                        alertController?.view.tintColor = UIColor.blackColor()
                        alertController = UIAlertController(title: "We had something very important to fix and we have done it. Please update now to continue experiencing Purz",
                                                            message: "",
                                                            preferredStyle: .Alert)
                        
                        let action = UIAlertAction(title: "Update", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                            UIApplication.sharedApplication().openURL(NSURL(string: "itms-apps://itunes.apple.com/app/bars/id1223418461")!)
                            
                            })
                        let action1 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                            exit(0)
                            
                            })
                        
                        alertController?.addAction(action)
                        alertController?.addAction(action1)
                        self.presentViewController(alertController!, animated: true, completion: nil)
                    }
                }
                else if Int(appversion) < item["currentVersion"].intValue{
                    
                    var updatetime = defaults.stringForKey("purzupdatetime")
                    var temp_date = NSDate()
                    if(updatetime != nil){
                        let currentdate = NSDate()
                        let calendar = NSCalendar.currentCalendar()
                        let components = calendar.components([.Day , .Month , .Year], fromDate: currentdate)
                        let year =  components.year
                        let month = components.month
                        let day = components.day
                        //                            let time = components.hour
                        
                        let dateFormatter = NSDateFormatter()
                        let dateFormatter1 = NSDateFormatter()
                        let dateFormatter2 = NSDateFormatter()
                        let dateFormatter3 = NSDateFormatter()
                        let dateFormatter4 = NSDateFormatter()
                        //                            let dateFormatter5 = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
                        dateFormatter.timeZone = NSTimeZone(name: "UTC")
                        dateFormatter1.dateFormat = "dd/MM/yy"
                        dateFormatter1.timeZone = NSTimeZone(name: "UTC")
                        dateFormatter2.dateFormat = "yyyy"
                        dateFormatter2.timeZone = NSTimeZone(name: "UTC")
                        dateFormatter3.dateFormat = "MM"
                        dateFormatter3.timeZone = NSTimeZone(name: "UTC")
                        dateFormatter4.dateFormat = "dd"
                        dateFormatter4.timeZone = NSTimeZone(name: "UTC")
                        
                        temp_date = dateFormatter.dateFromString(updatetime!)!
                        let temp_year = Int(dateFormatter2.stringFromDate(temp_date))!
                        let temp_month = Int(dateFormatter3.stringFromDate(temp_date))!
                        let temp_day = Int(dateFormatter4.stringFromDate(temp_date))!
                        //                            let temp_hour = Int(dateFormatter5.stringFromDate(temp_date))!
                        
                        if(Int(year)<=temp_year && Int(month)<=temp_month && Int(day)<temp_day){
                            self.showalert("A new version of Purz is available")
                            self.calldatabase()
                        }
                        else{
                            self.calldatabase()
                        }
                        
                    }
                    else{
                        self.calldatabase()
                    }
                    
                    defaults.setObject(self.today_date, forKey: "purzupdatetime")
                    
                }
                else{
                    self.calldatabase()
                }
            }
        }
        
        task.resume()
        
        
    }
    
    func showalert(str: String){
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: str,
                                            message: "",
                                            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "Update", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            dispatch_async(dispatch_get_main_queue()) {
                UIApplication.sharedApplication().openURL(NSURL(string: "itms-apps://itunes.apple.com/app/bars/id1223418461")!)
            }
            
            })
        let action1 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            self!.calldatabase()
            
            })
        
        alertController?.addAction(action)
        alertController?.addAction(action1)
        
        
        self.presentViewController(alertController!, animated: true, completion: nil)
    }
    
    
    
    
    
    /*
     
     override func viewDidLoad() {
     super.viewDidLoad()
     navigationController?.navigationBarHidden = true
     var pulseAnimation:CABasicAnimation = CABasicAnimation(keyPath: "opacity")
     pulseAnimation.duration = 5.0
     pulseAnimation.fromValue = NSNumber(float: 0.0)
     pulseAnimation.toValue = NSNumber(float: 4.0)
     pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
     pulseAnimation.autoreverses = true
     pulseAnimation.repeatCount = Float.infinity
     let pulseAnimation = CABasicAnimation(keyPath: "opacity")
     pulseAnimation.duration = 2
     pulseAnimation.fromValue = 0
     pulseAnimation.toValue = 5
     pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
     pulseAnimation.autoreverses = true
     pulseAnimation.repeatCount = FLT_MAX
     self.funbankingimage.layer.addAnimation(pulseAnimation, forKey: "animateOpacity")
     funbankingimage.layer.addAnimation(pulseAnimation, forKey: "opacity")
     a = 1.0
     calldatabase()
     //        self.timer = NSTimer.scheduledTimerWithTimeInterval(
     //            0.01, target: self, selector: #selector(addLogo), userInfo: nil, repeats: true)
     
     //        self.logobackscrollview.minimumZoomScale = 1.0
     //        self.logobackscrollview.maximumZoomScale = 6.0
     //         NSRunLoop.currentRunLoop().runUntilDate(NSDate(timeIntervalSinceNow: 1))
     //        // Do any additional setup after loading the view, typically from a nib.
     //        progressView.layer.cornerRadius = 10
     //        progressView.progress = 0.0
     //        self.navigationController?.navigationBarHidden = true
     //        timer = NSTimer.scheduledTimerWithTimeInterval(
     //            0.1, target: self, selector: Selector("setProgressBar"), userInfo: nil, repeats: true)
     //        logoimage.layer.borderWidth = 2
     //        logoimage.layer.masksToBounds = false
     logoimage.layer.borderColor = UIColor(red:230/255.0, green:175/255.0, blue:7/255.0, alpha:1.0).CGColor;
     logoimage.layer.borderWidth = 3
     logoimage.layer.cornerRadius =  logoimage.frame.size.height/2
     logoimage.clipsToBounds = true
     funbankingimage.layer.borderWidth = 3
     funbankingimage.layer.masksToBounds = false
     funbankingimage.layer.borderColor = UIColor(red:230/255.0, green:175/255.0, blue:7/255.0, alpha:1.0).CGColor;
     funbankingimage.layer.cornerRadius =  funbankingimage.frame.size.height/2
     funbankingimage.clipsToBounds = true
     // logoimage.hidden = true
     self.view.backgroundColor = UIColor.whiteColor()
     
     //        addBackgroundImage()
     //        addLogo()
     navigationController?.navigationBar.hidden = true
     }
     
     */
    
    // Do any additional setup after loading the view, typically from a nib.
    
    
    
    
    //        func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView?
    //        {
    //            return self.logoimage
    //        }
    //          func addLogo() {
    
    //    func addLogo() {
    //    let img = UIImageView(image: UIImage(named: "Launchar.png"))
    //
    //    img.frame = CGRectMake( self.view.frame.size.width/2 - (50 + a/2) , 145 , 100 + a, 100 + a )
    //    a += 0.5
    //
    //    if(a == 40.0){
    //    timer.invalidate()
    //        calldatabase()
    //       //            sendrequesttoserver(Appconstant.WEB_API+Appconstant.SIGNUP_URL)
    //    }
    //    self.view.addSubview(img)
    //}
    //
    //    func addLogo() {
    //          var img = UIImageView(image: UIImage(named: "Launcher.png"))
    //        img.layer.borderWidth = 2
    //        img.layer.masksToBounds = false
    //        img.layer.cornerRadius = img.frame.size.height/2
    //        img.layer.borderColor = UIColor(red:230/255.0, green:175/255.0, blue:7/255.0, alpha:1.0).CGColor;
    //        img.clipsToBounds = true
    //
    // img = UIImageView(frame: CGRectMake(0, 0, 100, 100))
    //       img.frame = CGRectMake( self.view.frame.size.width/2 - (50 + a/2) , 145 , 120 + a, 123 + a )
    //      a += 0.5
    //              img.layer.borderWidth = 2
    //        img.layer.cornerRadius = img.frame.size.height/2
    //        img.layer.borderColor = UIColor(red:230/255.0, green:175/255.0, blue:7/255.0, alpha:1.0).CGColor;
    //        if(a == 40.0){
    //            timer.invalidate()
    //            calldatabase()
    //
    //        }
    //        self.view.addSubview(img)
    //
    //    }
    override func prefersStatusBarHidden() ->Bool {
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func calldatabase(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            
            let selectSQL = "SELECT * FROM PROFILE_INFO"
            
            let result:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                          withArgumentsInArray: nil)
            if (result.next()){
                Appconstant.customerid = result.stringForColumn("CUSTOMER_ID")
                Appconstant.pwd = result.stringForColumn("YAP_CODE")
                Appconstant.customername = result.stringForColumn("CUSTOMER_NAME")
                Appconstant.mobileno = result.stringForColumn("MOBILE_NUMBER")
                Appconstant.cardnumber = result.stringForColumn("CARDNUMBER")
                Appconstant.expiredate = result.stringForColumn("EXPIREDATE")
                 Appconstant.KitNo = result.stringForColumn("KITNO")
                 Appconstant.dateOfBirth = result.stringForColumn("DATEOFBIRTH")
                Appconstant.CustomerType = result.stringForColumn("CUSTOMER_TYPE")
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("startviewtoFourdigitview", sender: self)
                }
            }
            else{
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("initial_to_signin", sender: self)
                }
            }
            
        }
        purzDB.close()
    }
    
}

class Alert: UIViewController {
    func alert(title: String,message: String)-> UIAlertController
    {
        let alert = UIAlertController(title: title,message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.Default, handler: nil))
        return alert
    }
}

