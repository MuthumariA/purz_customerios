//
//  operatorViewController.swift
//  Purz
//
//  Created by Vertace on 27/02/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import UIKit

class operatorViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITabBarControllerDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    

    
    var operator_circle1 = [String]()
    var operator_circle1_1 = [String]()

    var IsPostpaid = ""
    var amount = ""
    var mobile = ""
    var provider = ""
    var serviceType = ""
    var special = ""
    var selectedindex = 0
    var operatorname = ""
    var operatorcode = ""
    var isradioviewhide = true
    var isradiospecial = false
    var browsebtn1_hide = false
    var browsebtn3_hide = false
var segmentno = ""
    var operatorNameFromRecharge = ""
    var isClickedPrepaid = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
         tabBarController?.delegate = self
         operator_circle1 = ["Kolkata","Mumbai","Delhi","Chennai","Maharashtra","Andhra Pradesh","Karnataka","Tamilnadu","Kerala","Punjab","Haryana","Uttar Pradesh (West)","Uttar Pradesh (East)","Rajasthan","Madhya Pradesh ","WestBengal & AN Island","Himachal Pradesh","Bihar & Jharkhand","Orissa","Assam","NorthEast","J&K","Uttaranchal"]
        
         operator_circle1_1 = ["KOL","MUM","DEL","CHE","MAH","GUJ","KK","TN","KER","PUN","HAR","UPW","UPE","RAJ","MP","WB","HP","BIH","ORI","ASM","NE","JK","UPW"]
        
        
        //  transactionlbl.text = "No transactions yet!\nBut you can change that in a jiffy...\nTry out our easy Recharge & Bill payments, Or do you fancy some other features?"
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         tabBarController?.delegate = self
//        if Appconstant.pushhome{
//            Appconstant.pushhome = false
//            dispatch_async(dispatch_get_main_queue()) {
//                self.performSegueWithIdentifier("To_Home", sender: self)
//            }
//        }
        
    }
 
 
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
                   let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as UITableViewCell!
        
         print(indexPath.row)
        
        let operator_circlelbl = cell.viewWithTag(1) as! UILabel

        operator_circlelbl.text = operator_circle1[indexPath.row]
        
        return cell
        
        }

func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return operator_circle1.count
}

func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    let selectionColor = UIView() as UIView
//    selectionColor.layer.borderWidth = 1
    selectionColor.layer.borderColor = UIColor.clearColor().CGColor
    selectionColor.backgroundColor = UIColor.clearColor()
    cell.selectedBackgroundView = selectionColor
}

func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    self.tableView.reloadRowsAtIndexPaths([tableView.indexPathForSelectedRow!], withRowAnimation: .Fade)
    print(indexPath.row)
    activityIndicator.startAnimating()
    operatorname = operator_circle1[indexPath.row]
    operatorcode = operator_circle1_1[indexPath.row]
    self.performSegueWithIdentifier("tableSelect_Recharge", sender: self)
//    activityIndicator.stopAnimating()
}
//
//    @IBAction func backAction(sender: UIButton)
//    {
//        self.performSegueWithIdentifier("back_recharge", sender: self)
//    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "tableSelect_Recharge") {
            let nextview = segue.destinationViewController as! RechargeorPayBills
            nextview.IsPostpaid = IsPostpaid
            nextview.selectedindex = selectedindex
            nextview.operator_amt = amount
            nextview.operator_mobile = mobile
            nextview.operator_provider = provider
            nextview.operatorname = operatorname
            nextview.operator_operatorcode = operatorcode
            nextview.fromoperatorview = true
            nextview.isradioviewhide = isradioviewhide
            nextview.isradiospecial = isradiospecial
            
            if isClickedPrepaid
            {
            
            nextview.fromoperatorview_Prepaid = true
            }
            else
            {
               nextview.fromoperatorview_Dth = true
            }
            
            print(IsPostpaid)
            print(selectedindex)
            print(amount)
            print(mobile)
            print(provider)
            print(operatorname)
            print(operatorcode)
            print(isradioviewhide)
            
            
        }
        if(segue.identifier == "back_recharge") {
            let nextview = segue.destinationViewController as! RechargeorPayBills
            nextview.IsPostpaid = IsPostpaid
            nextview.operator_mobile = mobile
            nextview.operator_provider = provider
            nextview.fromoperatorview = true
            nextview.operatorname = operatorNameFromRecharge
            nextview.isradioviewhide = isradioviewhide
            nextview.isradiospecial = isradiospecial
            nextview.selectedindex = self.selectedindex
            if isClickedPrepaid
            {
                
                nextview.fromoperatorview_Prepaid = true
            }
            else
            {
                nextview.fromoperatorview_Dth = true
            }
            
           
            
            print(selectedindex)
            print(IsPostpaid)
            print(mobile)
            print(provider)
            print(isradioviewhide)
            print(operatorname)
            
            
        }

        
        
        
    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
//        else if tabBarIndex == 2{
//            Appconstant.fromlogout = false
//            let alert = UIAlertController(title: "Are you sure want to logout?", message: "", preferredStyle: UIAlertControllerStyle.Alert)
//            alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: { alertAction in
//                exit (0)
//            }))
//            
//            alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { alertAction in
//            }))
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }
        else if tabBarIndex == 2 {
            print("Selected item 2 ")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }

    }


}



