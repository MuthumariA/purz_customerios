//
//  NikiWebViewController.swift
//  purZ
//
//  Created by Vertace on 22/12/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class NikiWebViewController: UIViewController,UIWebViewDelegate, UITabBarControllerDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    var nikiurl = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tabBarController?.delegate = self
        self.navigationController?.navigationBarHidden = true
        webView.delegate = self
        let urlStr : NSString = nikiurl.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        
        let url = NSURL(string: urlStr as String)!
        let requestObj = NSURLRequest(URL: url);
        webView.loadRequest(requestObj)
        
    }
    
    func webViewDidStartLoad(webView: UIWebView)
    {
    }
    func webViewDidFinishLoad(webView: UIWebView) {
    }
    
    
    @IBAction func backBtnAction(sender: AnyObject) {
        self.performSegueWithIdentifier("To_Home", sender: self)
    }
    
   
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
