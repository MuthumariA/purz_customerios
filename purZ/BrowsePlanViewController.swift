//
//  BrowsePlanViewController.swift
//  Purz
//
//  Created by Vertace on 01/03/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import UIKit

class BrowsePlanViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITabBarControllerDelegate {
    
    var operator_circle1 = [String]()
    var operator_circle1_1 = [String]()
    var space = 0
    var servicename = ""
    var providername = ""
    var locationname = ""
    var segmentno = 0
    var plan_mobileno = ""
    var plan_provider = ""
    var plan_operator = ""
    var plan_amt = ""
    var plan_radioview = false
    var plan_selectedspecial = false
    var plan_selectedindex = 0
    var fromplan = false
    var isradioviewhide = false
    var isradiospecial = false
    
    @IBOutlet weak var T0_HomeBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var activityController: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var segmentController: UISegmentedControl!
    
    var amount = [[String]]()
    var plannames = [String]()
    var validity = [[String]]()
    var talktime = [[String]]()
    var descriptions = [[String]]()
    
    var temp_amount = [String]()
    var temp_validity = [String]()
    var temp_talktime = [String]()
    var temp_descriptions = [String]()
var labelarray = [UILabel]()
    
    
    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
print(segmentno)
         tabBarController?.delegate = self
        // Do any additional setup after loading the view.
        activityController.startAnimating()
        segmentController.hidden = true
        self.segmentController.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.darkGrayColor()], forState: UIControlState.Normal)
        self.segmentController.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Selected)
        self.segmentController.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        T0_HomeBtn.hidden = true
//        if Appconstant.pushhome{
//           // Appconstant.pushhome = false
//            dispatch_async(dispatch_get_main_queue()) {
//                self.performSegueWithIdentifier("To_Home", sender: self)
//            }
    //    }
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        

        operator_circle1 = ["Kolkata","Mumbai","Delhi","Chennai","Maharashtra","Andhra Pradesh","Karnataka","Tamilnadu","Kerala","Punjab","Haryana","Uttar Pradesh (West)","Uttar Pradesh (East)","Rajasthan","Madhya Pradesh ","WestBengal & AN Island","Himachal Pradesh","Bihar & Jharkhand","Orissa","Assam","NorthEast","J&K","Uttaranchal"]
        
        operator_circle1_1 = ["KOL","MUM","DEL","CHE","MAH","GUJ","KK","TN","KER","PUN","HAR","UPW","UPE","RAJ","MP","WB","HP","BIH","ORI","ASM","NE","JK","UPW"]
        let placename = locationname
        for(var i = 0; i<operator_circle1.count; i++){
            if operator_circle1[i] == placename{
                locationname = operator_circle1_1[i]
                break
            }
        }
        
        let planviewmodel = PlanViewModel.init(serviceName: servicename, serviceProviderName: providername, locationName: locationname)!
        let serializedjson  = JSONSerializer.toJson(planviewmodel)
        print(serializedjson)
        print(Appconstant.BASE_URL+Appconstant.BROWSE_PLANS)
        self.sendrequesttoserverForBrowsePlan(Appconstant.BASE_URL+Appconstant.BROWSE_PLANS, values: serializedjson)
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell!
        print(indexPath.row)
        
        let amountbtn = cell.viewWithTag(4) as! UIButton
        let validitylbl = cell.viewWithTag(1) as! UILabel
        let talktimelbl = cell.viewWithTag(2) as! UILabel
        let des_lbl = cell.viewWithTag(3) as! UILabel
        
        let amt = "₹ " + amount[segmentController.selectedSegmentIndex][indexPath.row]
        amountbtn.setTitle(amt, forState: .Normal)
        validitylbl.text = "Validity: " + validity[segmentController.selectedSegmentIndex][indexPath.row]
        talktimelbl.text = "Talk time: " + talktime[segmentController.selectedSegmentIndex][indexPath.row]
        des_lbl.text = descriptions[segmentController.selectedSegmentIndex][indexPath.row]
        
        
        amountbtn.userInteractionEnabled = false
        amountbtn.layer.borderWidth = 2
        amountbtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        amountbtn.layer.cornerRadius = 15
        if(indexPath.row == 0)
        {
            cell.layer.backgroundColor = UIColor.clearColor().CGColor
        }
       else
        {
        cell.layer.borderColor = UIColor.lightGrayColor().CGColor
        cell.layer.borderWidth = 1
        cell.selectionStyle = .None
        
        }
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.plannames.count == 0{
            return 0
        }
        print(self.validity[segmentController.selectedSegmentIndex].count)
        return self.validity[segmentController.selectedSegmentIndex].count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
              let selectionColor = UIView() as UIView
        selectionColor.layer.borderWidth = 1
        selectionColor.layer.borderColor = UIColor.clearColor().CGColor
        selectionColor.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = selectionColor
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        print(segmentno)
        
        
       print(self.amount[segmentController.selectedSegmentIndex][indexPath.row])
        self.plan_amt = self.amount[segmentController.selectedSegmentIndex][indexPath.row]
        self.performSegueWithIdentifier("plan_to_recharge", sender: self)
    }
    
    func sendrequesttoserverForBrowsePlan(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("Basic NzY2NzE4OTI5MToxMjM0", forHTTPHeaderField: "Authorization")
        request.addValue("EQWALLET", forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                self.activityController.stopAnimating()
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                self.activityController.stopAnimating()
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
                
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                
                let json = JSON(data: data!)
                
                if json["result"].isEmpty{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                else{
                    for item in json["result"].arrayValue{
                        
                        var isplanexist = false
                        for(var i=0; i<self.plannames.count; i++){
                            if self.plannames[i] == item["PlanName"].stringValue{
                                print("PlanNames:\(self.plannames)")
                                isplanexist = true
                            }
                        }
                        if !isplanexist{
                            if self.plannames.count != 0{
                                
                                self.amount.append(self.temp_amount)
                                self.validity.append(self.temp_validity)
                                self.talktime.append(self.temp_talktime)
                                self.descriptions.append(self.temp_descriptions)
                                
                                self.temp_amount.removeAll()
                                self.temp_validity.removeAll()
                                self.temp_talktime.removeAll()
                                self.temp_descriptions.removeAll()
                            }
                            self.plannames.append(item["PlanName"].stringValue)
                            self.temp_amount.append(item["Amount"].stringValue)
                            self.temp_validity.append(item["Validity"].stringValue)
                            self.temp_talktime.append(item["Talktime"].stringValue)
                            self.temp_descriptions.append(item["Description"].stringValue)
                        }
                        else{
                            self.temp_amount.append(item["Amount"].stringValue)
                            self.temp_validity.append(item["Validity"].stringValue)
                            self.temp_talktime.append(item["Talktime"].stringValue)
                            self.temp_descriptions.append(item["Description"].stringValue)
                        }
                        
                    }
                    
                    self.amount.append(self.temp_amount)
                    self.validity.append(self.temp_validity)
                    self.talktime.append(self.temp_talktime)
                    self.descriptions.append(self.temp_descriptions)
                    
                    self.temp_amount.removeAll()
                    self.temp_validity.removeAll()
                    self.temp_talktime.removeAll()
                    self.temp_descriptions.removeAll()
                }
                
                self.segmentController.removeAllSegments()
                dispatch_async(dispatch_get_main_queue()) {
                    print("Plan Name list:_____")
                     print(self.plannames)
                for(var i = 0; i<self.plannames.count; i++){
//                    if i == 0 || i == 1{
//                        self.segmentedControl.setTitle(self.plannames[i], forSegmentAtIndex: i)
//                        self.segmentedControl.setWidth(120, forSegmentAtIndex: i)
//                    }
//                    else{
                   
                    self.segmentController.insertSegmentWithTitle(self.plannames[i], atIndex: i, animated: true)
                    self.segmentController.setWidth(120, forSegmentAtIndex: i)
//                    }
                    self.segmentController.subviews[i].backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
                    self.segmentController.subviews[i].tintColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
                }
                    self.segmentController.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
                    self.segmentController.tintColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
                self.segmentController.selectedSegmentIndex = 0
                
                    self.segmentController.hidden = false
                    self.activityController.stopAnimating()
                    self.tableView.reloadData()
                }
                if(self.plannames.count != 0)
                {
                for(var i = 0; i<self.plannames.count; i++){
                    let label = UILabel()
                    self.labelarray.append(label)
                    self.labelarray[i].frame = CGRect(x: CGFloat(self.space)
                        ,y:28,width: 120,height:1.5);
                    self.labelarray[i].layer.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1).CGColor
                  self.space = self.space + 120
                    self.scrollView.addSubview(self.labelarray[i])
                    
                    //****
                    self.scrollView.contentSize = CGSizeMake(CGFloat(self.space), self.scrollView.frame.size.height)
 
                }
                    self.labelarray[0].layer.backgroundColor = UIColor.whiteColor().CGColor
                }
            }
        }
        
        task.resume()
        
    }
   

    @IBAction func segmentAction(sender: AnyObject) {
       
        var labelvalue = 0
        labelvalue = segmentController.selectedSegmentIndex
        for(var i = 0; i < self.plannames.count; i += 1)
        {
            if(labelarray[i] == labelarray[labelvalue])
            {
               //self.view.viewWithTag(labelvalue)?.backgroundColor
        self.labelarray[labelvalue].layer.backgroundColor = UIColor.whiteColor().CGColor
            }
            else
            {
               self.labelarray[i].layer.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1).CGColor
            }
        }
       self.tableView.reloadData()
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "plan_to_recharge") {
            let nextview = segue.destinationViewController as! RechargeorPayBills
            nextview.segmentNumberSelectedInRecharge = segmentno
            nextview.plan_mobileno = plan_mobileno
            nextview.plan_provider = plan_provider
            nextview.plan_operator = plan_operator
            nextview.plan_amt = plan_amt
            nextview.plan_radioview = plan_radioview
            nextview.plan_selectedindex = plan_selectedindex
            nextview.plan_selectedspecial = plan_selectedspecial
            nextview.fromplan = true
            nextview.selectedindex = segmentno
            nextview.isradioviewhide = isradioviewhide
            nextview.isradiospecial = isradiospecial
        }}
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    

}







