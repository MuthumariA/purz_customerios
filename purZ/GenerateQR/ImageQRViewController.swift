


//
//  ImageQRViewController.swift
//  purZ
//
//  Created by Vertace on 11/08/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit
import MessageUI
class ImageQRViewController: UIViewController, MFMailComposeViewControllerDelegate
 {

    
    @IBOutlet weak var QRimg: UIImageView!
    @IBOutlet weak var popupView: UIView!
    
    var img:UIImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.userInteractionEnabled = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissView")
        view.addGestureRecognizer(tap)
        popupView.userInteractionEnabled = true
        let tapBack: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "popupViewAction")
        popupView.addGestureRecognizer(tapBack)
        QRimg.image = img

        // Do any additional setup after loading the view.
    }
    func popupViewAction()
    {
    }
    func dismissView()
    {
        dismissViewControllerAnimated(true, completion: nil)
        //        performSegueWithIdentifier("filter_transaction", sender: self)
    }
    
    @IBAction func shareBtnAction(sender: AnyObject) {
        let activityItem: [UIImage] = [img as UIImage]
//        let wsactivity = WhatsAppActivity
        let objActivityViewController = UIActivityViewController(activityItems: activityItem as [UIImage], applicationActivities: nil)
        objActivityViewController.excludedActivityTypes =  [UIActivityTypePostToFacebook, UIActivityTypePostToTwitter, UIActivityTypePostToWeibo, UIActivityTypePrint, UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr, UIActivityTypePostToVimeo,UIActivityTypePostToTencentWeibo,UIActivityTypeAirDrop, UIActivityTypeOpenInIBooks, UIActivityTypeAddToReadingList]
        
         objActivityViewController.popoverPresentationController?.sourceView = sender as! UIView
        self.presentViewController(objActivityViewController, animated: true, completion: {
            objActivityViewController.completionWithItemsHandler = { activity, success, items, error in
                print(activity)
                print(items)
                print(error)
                if !success { print("cancelled")
                    return
                }
                
                if activity == UIActivityTypeMail {
                    print("mail")
                    if MFMailComposeViewController.canSendMail() {
                        let mail = MFMailComposeViewController()
                        mail.mailComposeDelegate = self;
                        mail.setSubject("")
                        mail.setMessageBody("", isHTML: false)
                        
                        let image : UIImage = self.img as UIImage
                        
                        //            let img1 = UIImagePNGRepresentation(image)
                        //           let image = UIImage(named:"Collect_Success.png")
                        let imageData:NSData = UIImagePNGRepresentation(image)!
                        print(imageData)
                        //            let imageData: NSData = UIImagePNGRepresentation(img)!
                        //            (img, 1)!
                        mail.addAttachmentData(imageData, mimeType: "image/png", fileName: "")
                        self.presentViewController(mail, animated: true, completion: nil)
                    }

                }
                else if activity == UIActivityTypeMessage {
                    print("message")
                }
                else if activity == UIActivityTypeSaveToCameraRoll {
                    print("camera")
                }
            }
        })
        
            }
        func mailComposeController(controller: MFMailComposeViewController,
                                   didFinishWithResult result: MFMailComposeResult, error: NSError?) {
            controller.dismissViewControllerAnimated(true, completion: nil)
        }
    
  
      
        
//        let activityViewController = UIActivityViewController(activityItems: activityItem, applicationActivities: nil)
//        activityViewController.excludedActivityTypes =  [ UIActivityTypePostToFacebook, UIActivityTypePostToTwitter, UIActivityTypePostToWeibo, UIActivityTypePrint, UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr, UIActivityTypePostToVimeo,UIActivityTypePostToTencentWeibo,UIActivityTypeAirDrop, UIActivityTypeOpenInIBooks, UIActivityTypeAddToReadingList]
//        activityViewController.popoverPresentationController?.sourceView = self.view
//        
//        presentViewController(activityViewController, animated: true, completion: nil)
//        let mailComposeViewController = configuredMailComposeViewController()
//        if MFMailComposeViewController.canSendMail() {
//            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
//        } else {
//            self.showSendMailErrorAlert()
//        }
//    }
//    
//    func configuredMailComposeViewController() -> MFMailComposeViewController {
//        let mailComposerVC = MFMailComposeViewController()
//        mailComposerVC.mailComposeDelegate = self
//       
//            mailComposerVC.setSubject("Your messagge")
//            mailComposerVC.setMessageBody("Message body", isHTML: false)
//            let imageData: NSData = UIImagePNGRepresentation(QRimg.image!)!
//            mailComposerVC.addAttachmentData(imageData, mimeType: "image/png", fileName: "imageName")
//            self.presentViewController(mailComposerVC, animated: true, completion: nil)
//        
//       
//        
//        return mailComposerVC
//    }
//    
//    func showSendMailErrorAlert() {
//        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
//        sendMailErrorAlert.show()
//    }
//
       
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
