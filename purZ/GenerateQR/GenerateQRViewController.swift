//
//  GenerateQRViewController.swift
//  purZ
//
//  Created by Vertace on 25/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class GenerateQRViewController: UIViewController, UITabBarControllerDelegate, UITextFieldDelegate{

    @IBOutlet weak var createBtn: UIButton!
    @IBOutlet weak var QRListTxtField: UITextField!
    @IBOutlet weak var remarkTxtfiled: UITextField!
    @IBOutlet weak var amountTxtfield: UITextField!
    @IBOutlet var payeeNameTxtField: UITextField!
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var characterCountLimit = 6
    var alertView = UIAlertView()
    var vpaname = [String]()
    var vpaaccountdetail = [String]()
    var qrimage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let qrdata = "upi://pay?tn=tgwg&tr=6;7%I}&mc=0000&pn=icici1@purz&pa=prabjjsa@purz&cu=INR&am=55.00"
//        qrimage = QRCode.generateImage(qrdata, avatarImage: UIImage(named: "images.png") , avatarScale: 0.25)!
       

        initialFunc()
               // Do any additional setup after loading the view.
        self.alertView.delegate = self
        self.alertView.title = "Select VPA"

            initi()

        
    }
    func initialFunc()
    {
        view.contentMode = UIViewContentMode.ScaleAspectFill
        tabBarController?.delegate = self
        createBtn.layer.borderWidth = 2
        createBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        createBtn.layer.cornerRadius = createBtn.frame.size.height/2
        amountTxtfield.delegate = self
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, payeeNameTxtField.frame.size.height - 1 , payeeNameTxtField.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                payeeNameTxtField.borderStyle = UITextBorderStyle.None
        payeeNameTxtField.layer.addSublayer(bottomLine1)
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, amountTxtfield.frame.size.height - 1 , amountTxtfield.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                amountTxtfield.borderStyle = UITextBorderStyle.None
        amountTxtfield.layer.addSublayer(bottomLine)
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, remarkTxtfiled.frame.size.height - 1 , remarkTxtfiled.frame.size.width, 1.0)
        bottomLine2.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                remarkTxtfiled.borderStyle = UITextBorderStyle.None
        remarkTxtfiled.layer.addSublayer(bottomLine2)
        let bottomLine3 = CALayer()
        bottomLine3.frame = CGRectMake(0.0, QRListTxtField.frame.size.height - 1 , QRListTxtField.frame.size.width, 1.0)
        bottomLine3.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                QRListTxtField.borderStyle = UITextBorderStyle.None
        QRListTxtField.layer.addSublayer(bottomLine3)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GenerateQRViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

    }
    func dismissKeyboard(){
        self.view.endEditing(true)
        
    }
//    func alertClose(gesture: UITapGestureRecognizer) {
//        self.dismissViewControllerAnimated(true, completion: nil)
//    }
    func initi() {
        self.activityIndicator.startAnimating()
        ApiClient().getMappedVirtualAddresses { (success, error, response) in
            self.activityIndicator.stopAnimating()
            if success {
                if response != nil {
                    print(response)
                    do{
                        // do whatever with jsonResult
                        let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(response, options: NSJSONWritingOptions.PrettyPrinted)
                        let json = JSON(data: jsonData)
                        var vpaaccountdetail = [String]()
                        var vpaprovidernames = [String]()
//                        for item in json.arrayValue {
//                            print(String(item))
//                            vpaname.append(item["va"].stringValue)
//                            vpaaccountdetail.append(String(item))
//                            vpaprovidernames.append(item["account-provider-name"].stringValue)
//                            self.alertView.addButtonWithTitle(item["va"].stringValue)
//                        }
                    
                        
                        for item1 in json.arrayValue {
                            print(item1)
                            let item2 = item1["VirtualAddress"]
                            let items = item2["accounts"]
                            print(items)
                            self.vpaname.append(items["handle"].stringValue)
                            self.alertView.addButtonWithTitle(items["handle"].stringValue)
                            for item in items["CustomerAccount"].arrayValue {
                                if item["default-credit"].stringValue == "D"{
                                    vpaprovidernames.append(item["account-provider-name"].stringValue)
                                    vpaaccountdetail.append(String(item))
                                }
                            }
                        }
                        self.alertView.addButtonWithTitle("Cancel")
                    }
                    catch {
                        print("error")
                    }
                    
                }
                else{
                    if response != nil {
                        print(response)
                    }
                }
            }
            
        }
    }

    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
       
        if self.vpaname.count != buttonIndex {
        QRListTxtField.text = self.vpaname[buttonIndex]
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func createBtnAction(sender: AnyObject) {
      
        
        if payeeNameTxtField.text! == "" {
            self.presentViewController(Alert().alert("Name can't be empty",message: ""), animated: true, completion: nil)
        }
        else if amountTxtfield.text == "" {
            self.presentViewController(Alert().alert("Amount can't be empty",message: ""), animated: true, completion: nil)
        }
        else if Double(amountTxtfield.text!)! < 1
        {
            self.presentViewController(Alert().alert("Amount can't be zero",message: ""), animated: true, completion: nil)
        }
        else if Double(amountTxtfield.text!) > 100000.0 {
            self.presentViewController(Alert().alert("Invalid Amount",message: ""), animated: true, completion: nil)
        }
        else if QRListTxtField.text! == "" {
            self.presentViewController(Alert().alert("Please select the VPA",message: ""), animated: true, completion: nil)
        }
        else{
            var remark = remarkTxtfiled.text!
            if remark == "" {
                remark = "NA"
            }
         let linkable_trans_details = LinkableTransaction.init(payeeName: self.QRListTxtField.text!, payeeName: self.payeeNameTxtField.text!, mode: 0)
    
//            let linkable_trans_details = LinkableTransaction.init(self.QRListTxtField.text!, payeeName: self.payeeNameTxtField.text!)
            linkable_trans_details.setRemarks(remark)
            linkable_trans_details.setAmount(NSDecimalNumber(string: amountTxtfield.text!))
            linkable_trans_details.setCurrencyCode("INR")
            let trans_ref_ID = NSUUID().UUIDString
            let trans_ID = NSUUID().UUIDString
            linkable_trans_details.setTransactionReferenceID(trans_ref_ID)
            linkable_trans_details.setTransactionID(trans_ID)
        

            let qrdata = LinkableTransaction().toIntentData(linkable_trans_details)
            print(qrdata)
             qrimage = QRCode.generateImage(qrdata, avatarImage: UIImage(named: "images.png") , avatarScale: 0.25)!
//            let khjdh = UIImageJPEGRepresentation(qrimage, 0.2)

    performSegueWithIdentifier("To_QRImage", sender: self)
     }
    }
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
         if (textFieldToChange == amountTxtfield){
            characterCountLimit = 6
            if((amountTxtfield.text?.rangeOfString(".")) != nil){
                characterCountLimit = 9
                let strcount = amountTxtfield.text! + string
                let strarray = strcount.componentsSeparatedByString(".")
                for(var i = 0; i<strarray.count; i++){
                    if i == 1{
                        if strarray[1].isEmpty{
                            
                        }
                        else{
                            if strarray[1].characters.count == 3{
                                return false
                            }
                            else{
                                return true
                            }
                        }
                    }
                }
            }
            else if string == "." && amountTxtfield.text?.characters.count == 6{
                return true
            }

        }
            
        else{
            characterCountLimit = 25
        }
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    }
    
    
    @IBAction func creditVPAbtnAction(sender: AnyObject) {
        self.view.endEditing(true)
        if self.vpaname.count != 0 {
            self.alertView.show()
        }
    }
    
 
    
  
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
 */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "To_QRImage") {
            let nextview = segue.destinationViewController as! ImageQRViewController
            nextview.img = qrimage
        }
    }
 
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
}
