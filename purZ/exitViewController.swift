//
//  exitViewController.swift
//  purZ
//
//  Created by Vertace on 21/02/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class exitViewController: UIViewController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
       

    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBarHidden = true
        Appconstant.fromlogout = true
      //  guard let tabBarController = tabBarController else { return }
        tabBarController!.selectedIndex = 0
        dispatch_async(dispatch_get_main_queue()) {
        self.performSegueWithIdentifier("from_logout", sender: self)
      
    
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
