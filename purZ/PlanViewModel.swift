//
//  PlanViewModel.swift
//  Purz
//
//  Created by Vertace on 01/03/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import Foundation

class PlanViewModel {

    var serviceName: String
    var serviceProviderName: String
    var locationName: String
    init?(serviceName: String, serviceProviderName: String, locationName: String){
        self.serviceName = serviceName
        self.serviceProviderName = serviceProviderName
        self.locationName = locationName
    }
}
class PieChart {
    
    var fromDate: String
    var toDate: String
    var txnType: String
    init?(fromDate: String, toDate: String, txnType: String){
        self.fromDate = fromDate
        self.toDate = toDate
        self.txnType = txnType
    }
}
class GenerateCVV {
    
    var entityId: String
    var kitNo: String
    var expiryDate: String
     var dob: String
    init?(entityId: String, kitNo: String, expiryDate: String, dob: String){
        self.entityId = entityId
        self.kitNo = kitNo
        self.expiryDate = expiryDate
        self.dob = dob
    }
}
class SetPin {
    
    var entityId: String
    var pin: String
    var kitNo: String
    var expiryDate: String
    var dob: String
    init?(entityId: String, pin: String, kitNo: String, expiryDate: String, dob: String){
        self.entityId = entityId
        self.pin = pin
        self.kitNo = kitNo
        self.expiryDate = expiryDate
        self.dob = dob
    }
}

