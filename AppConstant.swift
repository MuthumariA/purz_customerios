//
//  AppConstant.swift
//  payableApp
//
//  Created by apple on 15/09/16.
//  Copyright © 2016 apple. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration


struct Appconstant
{
    
    static let WEB_URL = "https://walletnew.yappay.in/Yappay" //prod
    static let BASE_URL = "https://walletnew.yappay.in/Yappay";   //prod check new

        static let TOENTITYID = "JRIRECHARGE"     // prod
        static let BILLDESKTOENTITYID = "BILLDESKBILLPAY" //prod
        static let VERSION_CHECK = "https://yappay.in/prod-app-info/EQWALLETIOS"
    
    
    
//    static let WEB_URL =  "https://sit-secure.yappay.in/Yappay";         //test check new
//    static let BASE_URL = "https://sit-secure.yappay.in/Yappay";
//    //test check new
//    static let TOENTITYID = "50405"     // test
//    static let BILLDESKTOENTITYID = "50405" //test
//    static let VERSION_CHECK = "https://yappay.in/test-app-info/EQWALLETIOS"  // test URL
    
    
    static let TENANT = "EQWALLET"
    
    //check customer already register
    static let SET_PIN =  MANAGER_BUSINESS_ENTITY +  "setPin"
   static let GENERATE_CVV =  MANAGER_BUSINESS_ENTITY + "generateCVV"
    static let MANAGER_BUSINESS_ENTITY = "/business-entity-manager/";
    static let MANAGER_OTP = "/otp-manager/";
    static let MANAGER_BILL = "/bill-manager/";
    static let TRANSACTIONHISTORY_PIECHART = "/txn-manager/fetchTnxBetweenDates"
    
    
    static let URL_CHECK_CUSTOMER = "otp/+91";
    static let URL_GENERATE_OTP = MANAGER_OTP + "generate/+91";
    
    static let MANAGER_REGISTRATION = "/registration-manager/";
    static let URL_REGISTER = MANAGER_REGISTRATION + "register";
    
    static let MANAGER_CUSTOMER = "/customer-manager/";
    static let URL_FETCH_CUSTOMER_DETAILS_BY_MOBILENO = MANAGER_CUSTOMER + "fetch/mobileno/+91";
    
    // update customer details
    static let URL_UPDATE_ENTITY = MANAGER_BUSINESS_ENTITY + "updateentity";
       static let URL_UPDATE_KYCDETAILS =  MANAGER_BUSINESS_ENTITY + "addKycDetails";
    // forgot password/change password
    static let URL_FORGOT_PASSCODE = MANAGER_CUSTOMER + "yapcode/forgot";
    static let URL_CHANGE_PASSCODE = MANAGER_CUSTOMER + "yapcode/change";
    // signin url
    static let URL_VERIFY_EXIST_CUSTOMER = MANAGER_REGISTRATION + "existingCustomer";
    
    // get balance
    static let URL_FETCH_MULTI_BALANCE_INFO = MANAGER_BUSINESS_ENTITY + "fetchbalance/";
    
    
    //fetch bill
    static let URL_FETCH_BILL = MANAGER_BILL + "billFetch/";
    //Pay Bill
    
    static let URL_PAY_BILL = MANAGER_BILL + "billPay/";
    
    
    // transaction details
    static let MANAGER_TRANSACTION = "/txn-manager/";
    static let URL_FETCH_RECENT_TRANSACTIONS = MANAGER_TRANSACTION + "fetch/success/entity/";
    static let URL_REQUEST_FUNDS = MANAGER_TRANSACTION + "requestfunds";
    
    // check customer for ask money
    static let URL_CHECK_CUSTOMER_REGISTRATION = MANAGER_REGISTRATION + "checkCustomer";
    
    // send money
    static let URL_PAY_MERCHANT = MANAGER_TRANSACTION + "create";
    
    // for get plans
    static let URL_GETOPEARTORS = "/recharge-manager/operators";
    
    static let URL_RECHARGE = "/recharge-manager/recharge";
    
    // pay @ store
    static let URL_PAY_STORE = "/payment-manager/payment";
    
    //notification
    static let URL_FUNDREQUEST = MANAGER_TRANSACTION + "fetch/fundsrequest/";
    
    static let URL_FETCH_MERCHANT_DETAILS = MANAGER_BUSINESS_ENTITY + "fetchbyentityid/";
    static let URL_APPROVE_REQUEST_FUNDS = MANAGER_TRANSACTION + "authorize/requestfunds";
    
    // recharge or pay bill
    static let URL_RECHARGE_PAY_BILL = "/recharge-manager/jriRecharge";
    
    
    
    // Add Money
    static let URL_BILL_DESK_INTEGRATION = "/pg/billdesk/paymentreqid";
    //BROWSE PLAN
    static let BROWSE_PLANS = "/recharge-manager/jriPopularRecharges"
    
    
    // split bill
    static let URL_SPLIT_TRANSACTION = MANAGER_TRANSACTION + "split/";
    
    //Get mobile number for transaction history
    static let MOBILENUMBER_FOR_TRANSACTIONHISTORY = "/recharge-manager/corporatestatus/"
    
    
    //Add recent transaction
//    static let BASE = "http://52.66.4.239:8080/Yappay"
    static let ADD_RECENT_TRANSACTION = "/txn-manager/addRecentTxn"
    
    //Get recent transactions
    static let GET_RECENT_TRANSACTIONS = "/txn-manager/fetchRecentTxn?pageSize=2"
    
    //
    static var SideMenu = 0;
    static var KitNo = ""
    static var dateOfBirth = ""
    static var CustomerType = ""
   static var expiryDate = ""
static var IdType = ""
    static var IdNumber = ""
    static var From_Signin = false
     static var From_Signup_IDType = false
    static var FromFourDigit_IDType = false
     static var FromProfile_IDType = false
    static var FromOTP_IDType = false
    
    
    //        static let WEB_URL = "https://wallet.yappay.in/Yappay" //prod
    //        static let BASE_URL = "https://wallet.yappay.in/Yappay";   //prod check new
    // static let yellowcolor = UIColor(red: 248.0/255.0, green: 215.0/255.0, blue: 18.0/255.0, alpha: 1)
    
    //     static let BASIC_AUTHCODE = "Basic YWRtaW46YWRtaW4=";
    //
    
    //     static let URL_SET_ENTITY_DETAILS = MANAGER_REGISTRATION + "entityDetails";
    
    //     static let URL_RESEND_OTP = "/resendOtp";
    
    //     static let MANAGER_CARD = "/ysci-manager/";
    //
    //     static let URL_CHANGE_PASSCODE = MANAGER_CUSTOMER + "yapcode/change";
    
    //     static let URL_FETCH_PENDING_TRANSACTIONS = MANAGER_TRANSACTION + "fetch/stage/entity/{customerId}?pageNo=1&pageSize=10";
    //      let URL_FETCH_COMPLETED_TRANSACTIONS = MANAGER_TRANSACTION + "fetch/success/entity/{customerId}?pageNo=1&pageSize=10";
    
    
    
    
    //     static let URL_GENERATE_OTP_CARD = MANAGER_OTP + "generate/+91{mobileNumber}";
    
    
    
    
    
    
    //     static let URL_PAYMENT_INTEGRATION = "/pg/paymentreqid";
    //     static let URL_PAYMENT_INTEGRATION_RETURN_URL = "/pg/processor/payment";
    //     static let URL_VERSION_UPDATE = "/api-manager?appName=" + "EQWALLET";
    //     static let GetPlanList = "/Test/getPlan";
    //     static let Recharge = "/Test/Recharge";
    //     static let PlanServerUrl = "http://oynk.yappay.in/getplans/prepaid/";
    
    
    
    //     static let URL_PAY_MERCHANT_DIRECT = MANAGER_TRANSACTION + "create/direct";
    
    //     static let PayForACauseUrl = "http://yappay.in/app-static/EQWALLET/";
    //     static let URL_REQUEST_CARD = "/cardrequest/newcard";
    //     static let URL_GET_CARD_DETAILS = "/getcarddetails";
    //     static let URL_GET_CARD_STATUS = "/cardrequest/cardstatus";
    //     static let URL_LINK_CARD = MANAGER_BUSINESS_ENTITY + "card/link";
    
    
    //     static let URL_DIRECT_REGISTER = MANAGER_REGISTRATION + "register";
    
    static var sessionToken = ""
    
    static var mobileno = ""
    static var pwd = ""
    static var firstname = ""
    static var lastname = ""
    static var email = ""
    static var otp = ""
    static var customerid = ""
    static var customername = ""
    static var cardnumber: String = ""
    static var expiredate = ""
    
    static var dummycustomer = false
    static var newcustomer = false
    static var mainbalance = "0"
    static var notificationcount = 0
    static var gcmid = ""
    static var Url = ""
    static var updatenotification = false
    static var fromlogout = false
    static var fromsidemenu = false
    static var unreadcount = 0
    
    //For FORGOT PIN WHILE TRANSACTION
    
    static var segmentedIndex = 0
    static var fromsegue = false
    static var r_mobileNo = ""
    static var r_provider = ""
    static var r_operator = ""
    static var r_amount = ""
    static var r_dob = ""
    static var r_customerID = ""
    static var r_customerNo = ""
    static var r_customeraccNo = ""
    static var r_policyno = ""
    static var r_phoneNo = ""
    static var r_accountNo = ""
    
    static var segmentValueWhileClickingForgot = 0
    
    static  var fromweb = false
    static var alertmsg = ""
    // static var pushhome = false
    static var fromcropimage = false
    
    static var takephoto = false
    static var cropimage = false
    
    static var filter = false
    static var successCheckBox = false
    static var rejectCheckBox = false
    static var creditCheckBox = false
    static var debitCheckBox = false
    static var filter_EndDate = ""
    static var filter_StartDate = ""
    
    static var collect_Money = false
    static var send_Money = false
    static var sendMoney_Account = false
    static var sendMoney_Aadhar = false
    static var sendMoney_MMID = false
    
    static var OTP = 0
}

public class Reachability: UIViewController {
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}



struct Operators{
    static var pre_operatorName = [String]()
    static var pre_operatorCode = [String]()
    static var pre_operatorType = [String]()
    static var pre_special = [Bool]()
    static var pre_MinBillAmount = [String]()
    static var pre_Authenticator1 = [String]()
    static var pre_Authenticator2 = [String]()
    static var pre_Authenticator3 = [String]()
    
    
    static var post_operatorName = [String]()
    static var post_operatorCode = [String]()
    static var post_operatorType = [String]()
    static var post_special = [Bool]()
    static var post_MinBillAmount = [String]()
    static var post_Authenticator1 = [String]()
    static var post_Authenticator2 = [String]()
    static var post_Authenticator3 = [String]()
    
    
    static var dth_operatorName = [String]()
    static var dth_operatorCode = [String]()
    static var dth_operatorType = [String]()
    static var dth_special = [Bool]()
    static var dth_MinBillAmount = [String]()
    static var dth_Authenticator1 = [String]()
    static var dth_Authenticator2 = [String]()
    static var dth_Authenticator3 = [String]()
    
    
    
    static var elect_operatorName = [String]()
    static var elect_operatorCode = [String]()
    static var elect_operatorType = [String]()
    static var elect_special = [Bool]()
    static var elect_MinBillAmount = [String]()
    static var elect_Authenticator1 = [String]()
    static var elect_Authenticator2 = [String]()
    static var elect_Authenticator3 = [String]()
    
    
    
    static var gas_operatorName = [String]()
    static var gas_operatorCode = [String]()
    static var gas_operatorType = [String]()
    static var gas_special = [Bool]()
    static var gas_MinBillAmount = [String]()
    static var gas_Authenticator1 = [String]()
    static var gas_Authenticator2 = [String]()
    static var gas_Authenticator3 = [String]()
    
    
    
    static var ins_operatorName = [String]()
    static var ins_operatorCode = [String]()
    static var ins_operatorType = [String]()
    static var ins_special = [Bool]()
    static var ins_MinBillAmount = [String]()
    static var ins_Authenticator1 = [String]()
    static var ins_Authenticator2 = [String]()
    static var ins_Authenticator3 = [String]()
    
    
    
    static var lan_operatorName = [String]()
    static var lan_operatorCode = [String]()
    static var lan_operatorType = [String]()
    static var lan_special = [Bool]()
    static var lan_MinBillAmount = [String]()
    static var lan_Authenticator1 = [String]()
    static var lan_Authenticator2 = [String]()
    static var lan_Authenticator3 = [String]()
    
}



struct Mobilelist
{
    
    static var mbl_Srno = [String]()
    static var mbl_Mobile = [String]()
    static var mbl_Operator = [String]()
    static var mbl_Location = [String]()
    
    
}
