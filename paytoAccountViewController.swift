//
//  paytoAccountViewController.swift
//  purZ
//
//  Created by Vertace on 21/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class paytoAccountViewController: UIViewController,UITabBarControllerDelegate,UITextFieldDelegate {
    @IBOutlet weak var Registername: UITextField!
    
    @IBOutlet weak var VPAListTxtField: UITextField!
    @IBOutlet weak var remarkTxtfield: UITextField!
    @IBOutlet weak var amountTxtField: UITextField!
    @IBOutlet weak var confirmAccountTxtField: UITextField!
    @IBOutlet weak var accountTxtField: UITextField!
    @IBOutlet weak var IFSCtxtField: UITextField!
    
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var refreshBtn: UIButton!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var contact_PayeeName = ""
    var contact_IFSC = ""
    var contact_AccountNo = ""
    var status = ""
    var descriptiontxt = ""
    var transactionid = ""
    
    
    var vpa = [String]()
    var VPAResponse = [String]()
    var payerVPA=NSDictionary()
    
    var characterCountLimit = 0
    var result = false
    let alertView: UIAlertView = UIAlertView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        initialFunction()
        pick_Contact()
        VPAList()
        
    }
    @IBAction func refreshBtnAction(sender: AnyObject) {
        UIView.animateWithDuration(0.5, animations: {
            let angle: CGFloat? = CGFloat((self.refreshBtn.valueForKeyPath("layer.transform.rotation.z") as? NSNumber)!)
            let transform = CGAffineTransformMakeRotation(angle! + CGFloat(M_PI))
            self.refreshBtn.transform = transform
        })
        
        Registername.text = ""
        IFSCtxtField.text = ""
        accountTxtField.text = ""
        confirmAccountTxtField.text = ""
        amountTxtField.text = ""
        remarkTxtfield.text = ""
        VPAListTxtField.text = ""
        
    }
    
    @IBAction func contactBtnAction(sender: AnyObject) {
         Appconstant.sendMoney_Account = true
        self.performSegueWithIdentifier("To_IFSCContact", sender: self)
    }
    
    @IBAction func VPAListBtnAction(sender: AnyObject) {
        self.view.endEditing(true)
        if self.vpa.count != 0
        {
            alertView.show()
        }
        
    }
    
    func initialFunction()
    {
        Registername.userInteractionEnabled = true
        IFSCtxtField.userInteractionEnabled = true
        accountTxtField.userInteractionEnabled = true
        confirmAccountTxtField.userInteractionEnabled = true
        amountTxtField.delegate = self
        remarkTxtfield.delegate = self
        tabBarController?.delegate = self
        sendBtn.layer.borderWidth = 2
        sendBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        sendBtn.layer.cornerRadius = sendBtn.frame.size.height/2
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, Registername.frame.size.height - 1 , Registername.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                Registername.borderStyle = UITextBorderStyle.None
        Registername.layer.addSublayer(bottomLine)
        
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, IFSCtxtField.frame.size.height - 1 , IFSCtxtField.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                IFSCtxtField.borderStyle = UITextBorderStyle.None
        IFSCtxtField.layer.addSublayer(bottomLine1)
        
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, accountTxtField.frame.size.height - 1 , accountTxtField.frame.size.width, 1.0)
        bottomLine2.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                accountTxtField.borderStyle = UITextBorderStyle.None
        accountTxtField.layer.addSublayer(bottomLine2)
        accountTxtField.secureTextEntry = true
        let bottomLine3 = CALayer()
        bottomLine3.frame = CGRectMake(0.0, confirmAccountTxtField.frame.size.height - 1 , confirmAccountTxtField.frame.size.width, 1.0)
        bottomLine3.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                confirmAccountTxtField.borderStyle = UITextBorderStyle.None
        confirmAccountTxtField.layer.addSublayer(bottomLine3)
        
        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRectMake(0.0, amountTxtField.frame.size.height - 1 , amountTxtField.frame.size.width, 1.0)
        bottomLine4.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                amountTxtField.borderStyle = UITextBorderStyle.None
        amountTxtField.layer.addSublayer(bottomLine4)
        
        let bottomLine5 = CALayer()
        bottomLine5.frame = CGRectMake(0.0, remarkTxtfield.frame.size.height - 1 , remarkTxtfield.frame.size.width, 1.0)
        bottomLine5.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                remarkTxtfield.borderStyle = UITextBorderStyle.None
        remarkTxtfield.layer.addSublayer(bottomLine5)
        
        let bottomLine6 = CALayer()
        bottomLine6.frame = CGRectMake(0.0, VPAListTxtField.frame.size.height - 1 , VPAListTxtField.frame.size.width, 1.0)
        bottomLine6.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                VPAListTxtField.borderStyle = UITextBorderStyle.None
        VPAListTxtField.layer.addSublayer(bottomLine6)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GenerateQRViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
       
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    func VPAList()
    {
        self.activityIndicator.startAnimating()
        var error: NSError?
        ApiClient().getMappedVirtualAddresses
            { (success, error, response) in
                self.activityIndicator.stopAnimating()
                if success
                {
                    
                    if response != nil
                    {
                        
                        var jsonData: NSData?
                        
                        do
                            
                        {
                            jsonData = try NSJSONSerialization.dataWithJSONObject(response, options:NSJSONWritingOptions.PrettyPrinted)
                            let json = JSON(data: jsonData!)
//                            for item in json.arrayValue {
//                                self.vpa.append(item["va"].stringValue)
//                                self.VPAResponse.append(String(item))
//                                print("VPA:\(self.vpa)")
//                                
//                            }
                            
                            for item1 in json.arrayValue {
                                
                                let item2 = item1["VirtualAddress"]
                                let items = item2["accounts"]
                                
                                self.vpa.append(items["handle"].stringValue)
                                for item in items["CustomerAccount"].arrayValue {
                                    if item["default-debit"].stringValue == "D"{
                                        self.VPAResponse.append(String(item))
                                    }
                                }
                            }
                            
                        }
                        catch
                        {
                            jsonData = nil
                            
                        }
                        let jsonDataLength = "\(jsonData!.length)"
                    }
                    self.alertView.delegate = self
                    self.alertView.title = "Debit VPA"
                    for(var i = 0; i<self.vpa.count; i += 1){
                        self.alertView.addButtonWithTitle(self.vpa[i])
                    }
                    self.alertView.addButtonWithTitle("Cancel")
                    
                }
                
        }
    }
    
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        if buttonIndex != VPAResponse.count
        {
            payerVPA = self.convertStringToDictionary(VPAResponse[buttonIndex])! as NSDictionary
            VPAListTxtField.text = vpa[buttonIndex]
            
        }
    }
    func convertStringToDictionary(text: String) -> [NSObject:AnyObject]?
    {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding)
        {
            do
            {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [NSObject:AnyObject]
            }
            catch let error as NSError
            {
                print(error)
            }
        }
        return nil
    }
    func pick_Contact()
    {
        
        Registername.text = contact_PayeeName
        IFSCtxtField.text = contact_IFSC
        accountTxtField.text = contact_AccountNo
        confirmAccountTxtField.text = contact_AccountNo
        if Registername.text! != "" {
        Registername.userInteractionEnabled = false
        }
        if IFSCtxtField.text! != "" {
        IFSCtxtField.userInteractionEnabled = false
        }
        if accountTxtField.text! != "" {
        accountTxtField.userInteractionEnabled = false
        }
        if confirmAccountTxtField.text! != "" {
        confirmAccountTxtField.userInteractionEnabled = false
        }
        
    }
    
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendBtnAction(sender: AnyObject) {
        let ifsc = IFSCtxtField.text!
        let ifscValidation = validateIFSC(IFSCtxtField.text!)
        
        if Registername.text == ""
        {
            self.presentViewController(Alert().alert("Registered Name can't be empty", message: ""), animated: true, completion: nil)
            
        }
        else if IFSCtxtField.text == ""
        {
            self.presentViewController(Alert().alert("IFSC can't be empty", message: ""), animated: true, completion: nil)
        }
        else if(ifsc.characters.count != 11 || !ifscValidation )
        {
            self.presentViewController(Alert().alert("Enter Valid IFSC code", message: ""),animated: true,completion: nil)
            
        }
        else if accountTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Account number can't be empty", message: ""), animated: true, completion: nil)
            
            
        }
        else if confirmAccountTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Confirm account number can't be empty", message: ""), animated: true, completion: nil)
            
        }
        else if accountTxtField.text! != confirmAccountTxtField.text!
        {
            self.presentViewController(Alert().alert("Account number can't be different", message: ""), animated: true, completion: nil)
            
        }
        else if amountTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Amount can't be empty", message: ""), animated: true, completion: nil)
        }
        else if Double(amountTxtField.text!)! < 1
        {
            self.presentViewController(Alert().alert("Amount can't be zero",message: ""), animated: true, completion: nil)
        }
        else if VPAListTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Please select the VPA", message: ""), animated: true, completion: nil)
        }
        else
        {
            
            let payerAccount = VirtualAddress.init(payerVPA as! [NSObject : AnyObject], handle: VPAListTxtField.text!)
            let payeeAccount = PayeeParam.init(account: Registername.text!, ifsc: IFSCtxtField.text!, account: confirmAccountTxtField.text!)
            
            
            let transaction = TransactionParameters.init(transaction: payerAccount!, payee: payeeAccount!, amount: NSDecimalNumber(string: amountTxtField.text!), remark: remarkTxtfield.text!)
            self.activityIndicator.startAnimating()
            self.sendBtn.userInteractionEnabled = false
            ApiClient().pay(transaction!, view: self, withCompletion: { (success, error, response) in
                if success
                {
                    
                    let response_success = response.valueForKey("success")
                    if String(response_success!) == "1" {
                        self.status = "Success"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("sendmoneyAccount_to_success", sender: self)
                    }
                    else {
                        self.status = "Failed"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("sendmoneyAccount_to_success", sender: self)
                    }
                }
                else
                {
                    if response != nil
                    {
                        self.status = "Failed"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("sendmoneyAccount_to_success", sender: self)
                    }
                }
                self.activityIndicator.stopAnimating()
                self.sendBtn.userInteractionEnabled = true
            })
            
            
        }
    }
    func validateIFSC(ifsc:String)-> Bool
    {
        
        let passwordRegex = "^[A-Z]{4}0.{6}$"
        
        let myString = "\(ifsc)"
        let regex = try! NSRegularExpression(pattern: passwordRegex, options: [])
        let range = NSMakeRange(0, myString.characters.count)
        let CountString = myString.characters.count
        let modString = regex.stringByReplacingMatchesInString(myString, options: [], range: range, withTemplate: "valid")
        
        if(modString == "valid")
        {
            result = true
        }
        else
        {
            result = false
        }
        
        return result
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "sendmoneyAccount_to_success") {
            let nextview = segue.destinationViewController as! CollectMoneySuccessViewController
            nextview.transactionID = transactionid
            nextview.status = status
            nextview.descriptiontxt = descriptiontxt
            
        }
    }
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textFieldToChange == amountTxtField
        {
            characterCountLimit = 6
            if((amountTxtField.text?.rangeOfString(".")) != nil){
                characterCountLimit = 9
                let strcount = amountTxtField.text! + string
                let strarray = strcount.componentsSeparatedByString(".")
                
                for(var i = 0; i<strarray.count; i++){
                    if i == 1{
                        if strarray[1].isEmpty{
                            
                        }
                        else{
                            if strarray[1].characters.count == 3{
                                return false
                            }
                            else{
                                return true
                            }
                        }
                    }
                }
                
            }
            else if string == "." && amountTxtField.text?.characters.count == 6{
                return true
            }
        }
        else if textFieldToChange == accountTxtField
        {
            characterCountLimit = 16
        }
        else if textFieldToChange == confirmAccountTxtField
        {
            characterCountLimit = 16
        }
        else {
            characterCountLimit = 50
        }
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
        
        
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        if(textField == amountTxtField) {
            animateViewMoving(true, moveValue: 165)
        }
        if(textField == remarkTxtfield) {
            animateViewMoving(true, moveValue: 165)
        }

    }
    func textFieldDidEndEditing(textField: UITextField) {
        if(textField == amountTxtField) {
            animateViewMoving(false, moveValue: 165)
        }
        if(textField == remarkTxtfield) {
            animateViewMoving(false, moveValue: 165)
        }
        
    }
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
        
    }
    

    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
}
