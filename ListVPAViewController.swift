//
//  ListVPAViewController.swift
//  purZ
//
//  Created by Vertace on 25/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class ListVPAViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITabBarControllerDelegate {
    
    var vpa = [String]()
    var VPAresponse = [String]()

    @IBOutlet var tableView: UITableView!    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.delegate = self
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.startAnimating()
            initi()
       
        
    }
    
    @IBAction func closeBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "Are you sure you want to delete?",message: "",preferredStyle: .Alert)
        let action1 = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            self!.removeVPA(indexPath.row)
            
            })
        let action2 = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            
            
            })
        
        
        alertController?.addAction(action1)
        alertController?.addAction(action2)
        self.presentViewController(alertController!, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func initi(){
        ApiClient().getMappedVirtualAddresses { (success, error, response) in
            if success {
                if response != nil {
                   
                    do{
                            // do whatever with jsonResult
                            let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(response, options: NSJSONWritingOptions.PrettyPrinted)
                            let json = JSON(data: jsonData)
                        
                        for item1 in json.arrayValue {
                            let item2 = item1["VirtualAddress"]
                            let items = item2["accounts"]
                            self.vpa.append(items["handle"].stringValue)
                            for item in items["CustomerAccount"].arrayValue {
                                if item["default-debit"].stringValue == "D"{
                                    AccountConstant.vpaProviderName.append(item["account-provider-name"].stringValue)
                                    self.VPAresponse.append(String(item))
                                }
                            }
                        }
                        
                        
                        AccountConstant.vpanames = self.vpa
                        AccountConstant.vparesponses = self.VPAresponse
                        
                        
                    }
                    catch {
                        print("error")
                    }
                
                }
                
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                self.tableView.reloadData()
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ListCell", forIndexPath: indexPath) as UITableViewCell!
       let VPAnamelbl = cell.viewWithTag(1) as! UILabel
        
        VPAnamelbl.text = vpa[indexPath.row]
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vpa.count
    }
  
    
    func removeVPA(indexPath: Int){
        AccountConstant.vpanames.removeAll()
        AccountConstant.vparesponses.removeAll()
        AccountConstant.vpaProviderName.removeAll()
        do{
            self.activityIndicator.startAnimating()
            try ApiClient().removeVPA(vpa[indexPath], reason: "NA", withCompletion: { (success, error, response) in
               
                
                self.view.userInteractionEnabled = true
                
                if success {
                    let response_success = response.valueForKey("success")
                    if String(response_success!) == "1" {
                        self.vpa.removeAtIndex(indexPath)
                        self.VPAresponse.removeAtIndex(indexPath)
                        self.presentViewController(Alert().alert("VPA deleted",message: ""), animated: true, completion: nil)
                    }
                }
                else{
                    
                    self.presentViewController(Alert().alert("VPA not deleted",message: ""), animated: true, completion: nil)
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                    self.tableView.reloadData()
                }
            })
        }
        
    }
    
 
    
    

    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "listVPA_to_createVPA") {
            let nextview = segue.destinationViewController as! CreateVPAViewController
            nextview.fromListVPA = true
        }
    }

}
