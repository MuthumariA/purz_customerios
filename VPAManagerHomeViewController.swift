//
//  VPAManagerHomeViewController.swift
//  purZ
//
//  Created by Vertace on 24/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class VPAManagerHomeViewController: UIViewController,UITabBarControllerDelegate {

    @IBOutlet weak var ListVPAView: UIView!
    @IBOutlet weak var LinkVPAView: UIView!
    @IBOutlet weak var createVPAView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialfunc()
    }
    func initialfunc()
    {
        tabBarController?.delegate = self
        createVPAView.userInteractionEnabled = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "createVPAFunc")
        createVPAView.addGestureRecognizer(tap)
        
        ListVPAView.userInteractionEnabled = true
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "listVPAFunc")
        ListVPAView.addGestureRecognizer(tap1)
        
        LinkVPAView.userInteractionEnabled = true
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "linkVPAFunc")
        LinkVPAView.addGestureRecognizer(tap2)
        // Do any additional setup after loading the view.
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, ListVPAView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine.backgroundColor = UIColor.lightGrayColor().CGColor
        ListVPAView.layer.addSublayer(bottomLine)
        
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, LinkVPAView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine1.backgroundColor = UIColor.lightGrayColor().CGColor
        LinkVPAView.layer.addSublayer(bottomLine1)
        
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, createVPAView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine2.backgroundColor = UIColor.lightGrayColor().CGColor
        createVPAView.layer.addSublayer(bottomLine2)
        
        
        
        
        

    }
    func createVPAFunc()
    {
        print("Tapped")
        self.performSegueWithIdentifier("To_CreateVPA", sender: self)
        
        
    }
    func listVPAFunc()
    {
        print("Tapped")
        self.performSegueWithIdentifier("To_ListVPA", sender: self)
        
        
    }
    
    func linkVPAFunc()
    {
        print("Tapped")
        self.performSegueWithIdentifier("To_LinkVPA", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
            
}
}
}