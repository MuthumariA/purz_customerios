//
//  TransactionModel.swift
//  Cippy
//
//  Created by apple on 07/12/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import Foundation

class TransactionModel {

    var amount: String
    var trans_type: String
    var type: String
    var time: String
    var tx_ref: String
    var beneficiaryname: String
    var beneficiaryid: String
    var descripition: String
    var otherpartyname: String
    var otherpartyid: String
    var txnorigin: String
    var transactionstatus: String
    var selected: Bool
    var transid: String
    init?(amount: String, trans_type: String, type: String, time: String, tx_ref: String, beneficiaryname: String, beneficiaryid: String, descripition: String, otherpartyname: String, otherpartyid: String, txnorigin: String, transactionstatus: String, selected: Bool, transid: String){
        self.amount = amount
        self.trans_type = trans_type
        self.type = type
        self.time = time
        self.tx_ref = tx_ref
        self.beneficiaryname = beneficiaryname
        self.beneficiaryid = beneficiaryid
        self.descripition = descripition
        self.otherpartyname = otherpartyname
        self.otherpartyid = otherpartyid
        self.txnorigin = txnorigin
        self.transactionstatus = transactionstatus
        self.selected = selected
        self.transid = transid
    }
    
}
