//
//  SendtoAadharViewController.swift
//  purZ
//
//  Created by Vertace on 24/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class SendtoAadharViewController: UIViewController,UITabBarControllerDelegate,UITextFieldDelegate {
    
    
    @IBOutlet weak var payeeNameTxtField: UITextField!
    @IBOutlet weak var amountTxtField: UITextField!
    @IBOutlet weak var AadharNumberTxtField: UITextField!
    @IBOutlet weak var remarkTxtField: UITextField!
    @IBOutlet weak var bankListTxtField: UITextField!
    @IBOutlet weak var VPAListTxtField: UITextField!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var refreshBtn: UIButton!
    
    var contact_PayeeName = ""
    var contact_Aadhar = ""
    var characterCountLimit = 0
    var vpa = [String]()
    var VPAResponse = [String]()
    var payerVPA=NSDictionary()
    var accountProviderName = ""
    var accountProvidrId = ""
    var accountIin = ""
    var bankList = false
    var  VpaList = false
    var status = ""
    var descriptiontxt = ""
    var transactionid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialFunc()
              
        VPAList()
        getproviderlist()
         pick_contact()
        
    }
    @IBAction func sendBtnAction(sender: AnyObject) {
        let AadharNo = AadharNumberTxtField.text!
        if payeeNameTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Name can't be empty",message:""), animated: true, completion: nil)
        }
        else if AadharNumberTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Aadhar number can't be empty",message:""), animated: true, completion: nil)
        }
        else if AadharNo.characters.count != 12
        {
            self.presentViewController(Alert().alert("Aadhar number should be 12 digit length",message:""), animated: true, completion: nil)
        }
        else if amountTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Amount can't be empty",message:""), animated: true, completion: nil)
        }
       else if Double(amountTxtField.text!)! < 1
            
        {
            self.presentViewController(Alert().alert("Amount can't be zero",message: ""), animated: true, completion: nil)
        }
        else if bankListTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Please select the bank",message:""), animated: true, completion: nil)
            
        }
        else if VPAListTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Please select the VPA",message:""), animated: true, completion: nil)
        }
        else
        {
            let accountProvider = AccountProvider.init(accountProviderId: accountProvidrId, provider: accountProviderName, providerIin: accountIin)
            let payeeAccount = PayeeParam.init(adharIin: payeeNameTxtField.text!, aadhar: AadharNumberTxtField.text!, iin: accountProvider)
            let payerAccount = VirtualAddress.init(payerVPA as! [NSObject : AnyObject], handle: VPAListTxtField.text!)
            let transaction = TransactionParameters.init(transaction: payerAccount, payee: payeeAccount, amount: NSDecimalNumber(string: amountTxtField.text!), remark: remarkTxtField.text!)
            self.activityIndicator.startAnimating()
            self.sendBtn.userInteractionEnabled = false
            ApiClient().pay(transaction!, view: self, withCompletion: { (success, error, response) in
                if success
                {
                    
                    let response_success = response.valueForKey("success")
                    if String(response_success!) == "1" {
                        self.status = "Success"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("sendmoneyAadhar_to_success", sender: self)
                    }
                    else {
                        self.status = "Failed"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("sendmoneyAadhar_to_success", sender: self)
                    }
                }
                else
                {
                    if response != nil
                    {
                        self.status = "Failed"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("sendmoneyAadhar_to_success", sender: self)
                    }
                }
                self.activityIndicator.stopAnimating()
                self.sendBtn.userInteractionEnabled = true
            })
        }
        
    }
    
    @IBAction func contactBtnAction(sender: AnyObject) {
         Appconstant.sendMoney_Aadhar = true
        self.performSegueWithIdentifier("To_AadharContact", sender: self)
    }
    @IBAction func BankListBtnAction(sender: AnyObject) {
        
//        remarkTxtField!.resignFirstResponder()
//        amountTxtField.resignFirstResponder()
        //UIApplication.sharedApplication().sendAction("resignFirstResponder", to:nil, from:nil, forEvent:nil)
//        dispatch_async(dispatch_get_main_queue()) {
            self.view.endEditing(true)
//        }
        NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(SendtoAadharViewController.ShowAlert), userInfo: nil, repeats: false)
        
    }
    
    func ShowAlert() {
        
        self.bankList = true
        
        let alertView: UIAlertView = UIAlertView()
        alertView.delegate = self
        alertView.title = "Select Bank"
        
        for i in 0 ..< AccountProviderList.AccountProviderName.count {
            alertView.addButtonWithTitle(AccountProviderList.AccountProviderName[i])
        }
        alertView.addButtonWithTitle("Cancel")
        if AccountProviderList.AccountProviderName.count != 0
        {
            alertView.show()
        }

    }
    
    @IBAction func VPAListBtnAction(sender: AnyObject) {
//        self.view.endEditing(true)
        VpaList = true
        let alertView: UIAlertView = UIAlertView()
        alertView.delegate = self
        alertView.title = "Debit VPA"
        for i in 0 ..< vpa.count {
            alertView.addButtonWithTitle(vpa[i])
        }
        alertView.addButtonWithTitle("Cancel")
        if self.vpa.count != 0
        {
            alertView.show()
        }
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        dismissKeyboard()
        
        return true
    }
    @IBAction func refreshBtnAction(sender: AnyObject) {
        UIView.animateWithDuration(0.5, animations: {
            let angle: CGFloat? = CGFloat((self.refreshBtn.valueForKeyPath("layer.transform.rotation.z") as? NSNumber)!)
            let transform = CGAffineTransformMakeRotation(angle! + CGFloat(M_PI))
            self.refreshBtn.transform = transform
        })
        
        payeeNameTxtField.text = ""
        AadharNumberTxtField.text = ""
        amountTxtField.text = ""
        remarkTxtField.text = ""
        bankListTxtField.text = ""
        VPAListTxtField.text = ""
        
    }
    
    func initialFunc()
    {
        payeeNameTxtField.userInteractionEnabled = true
        AadharNumberTxtField.userInteractionEnabled = true

        tabBarController?.delegate = self
        amountTxtField.delegate = self
        sendBtn.layer.borderWidth = 2
        sendBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        sendBtn.layer.cornerRadius = sendBtn.frame.size.height/2
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, payeeNameTxtField.frame.size.height - 1 , payeeNameTxtField.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                payeeNameTxtField.borderStyle = UITextBorderStyle.None
        payeeNameTxtField.layer.addSublayer(bottomLine)
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, AadharNumberTxtField.frame.size.height - 1 , AadharNumberTxtField.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                AadharNumberTxtField.borderStyle = UITextBorderStyle.None
        AadharNumberTxtField.layer.addSublayer(bottomLine1)
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, amountTxtField.frame.size.height - 1 , amountTxtField.frame.size.width, 1.0)
        bottomLine2.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                amountTxtField.borderStyle = UITextBorderStyle.None
        amountTxtField.layer.addSublayer(bottomLine2)
        let bottomLine3 = CALayer()
        bottomLine3.frame = CGRectMake(0.0, remarkTxtField.frame.size.height - 1 , remarkTxtField.frame.size.width, 1.0)
        bottomLine3.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                remarkTxtField.borderStyle = UITextBorderStyle.None
        remarkTxtField.layer.addSublayer(bottomLine3)
        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRectMake(0.0, bankListTxtField.frame.size.height - 1 , bankListTxtField.frame.size.width, 1.0)
        bottomLine4.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                bankListTxtField.borderStyle = UITextBorderStyle.None
        bankListTxtField.layer.addSublayer(bottomLine4)
        let bottomLine5 = CALayer()
        bottomLine5.frame = CGRectMake(0.0, VPAListTxtField.frame.size.height - 1 , VPAListTxtField.frame.size.width, 1.0)
        bottomLine5.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        VPAListTxtField.borderStyle = UITextBorderStyle.None
        VPAListTxtField.layer.addSublayer(bottomLine5)
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GenerateQRViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    func pick_contact()
    {
        payeeNameTxtField.text = contact_PayeeName
        AadharNumberTxtField.text = contact_Aadhar
        if payeeNameTxtField.text! != "" {
        payeeNameTxtField.userInteractionEnabled = false
        }
        if AadharNumberTxtField.text! != "" {
        AadharNumberTxtField.userInteractionEnabled = false
        }
        
    }
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getproviderlist(){
        
        
        if AccountProviderList.AccountProviderName.count == 0{
            let providerlist:NSDictionary = ApiClient().getAccountProviderList()
            
            if providerlist.count != 0
            {
            
            do{
                let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(providerlist, options: NSJSONWritingOptions.PrettyPrinted)
                //                let json = NSString(data: jsonData, encoding: NSUTF8StringEncoding)
                let json = JSON(data: jsonData)
                
                for items in json.arrayValue {
                    
                    AccountProviderList.AccountProviderName.append(items["account-provider"].stringValue)
                    AccountProviderList.AccountProviderid.append(items["id"].stringValue)
                    AccountProviderList.IIN.append(items["iin"].stringValue)
                }
            }
            catch {
            }
            print("error")
        }
            else
            {
                print("provider not available")
            }
        }
    }
    func VPAList()
    {
        self.activityIndicator.startAnimating()
        var error: NSError?
        ApiClient().getMappedVirtualAddresses
            { (success, error, response) in
                self.activityIndicator.stopAnimating()
                if success
                {
                    
                    if response != nil
                    {
                        
                        var jsonData: NSData?
                        
                        do
                            
                        {
                            jsonData = try NSJSONSerialization.dataWithJSONObject(response, options:NSJSONWritingOptions.PrettyPrinted)
                            let json = JSON(data: jsonData!)
                            
//                            for item in json.arrayValue {
//                                self.vpa.append(item["va"].stringValue)
//                                self.VPAResponse.append(String(item))
//                                print("VPA:\(self.vpa)")
//                            }
                            for item1 in json.arrayValue {
                                
                                let item2 = item1["VirtualAddress"]
                                let items = item2["accounts"]
                                
                                self.vpa.append(items["handle"].stringValue)
                                for item in items["CustomerAccount"].arrayValue {
                                    if item["default-debit"].stringValue == "D"{
                                        self.VPAResponse.append(String(item))
                                    }
                                }
                            }
                            
                        }
                        catch let error as NSError {
                        }
                        catch
                        {
                            jsonData = nil
                            
                        }
                        let jsonDataLength = "\(jsonData!.length)"
                    }
                }
                
        }
    }
    
    
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
       self.view.endEditing(true)
        if VpaList == true
        {
            VpaList = false
            if buttonIndex == VPAResponse.count{
                
                
            }
            else{
                payerVPA = self.convertStringToDictionary(VPAResponse[buttonIndex])! as NSDictionary
                VPAListTxtField.text = vpa[buttonIndex]
                
                
            }
        }
        else if bankList == true
        {
    
            if buttonIndex != AccountProviderList.AccountProviderName.count
            {
                bankListTxtField.text = AccountProviderList.AccountProviderName[buttonIndex]
                accountProviderName = AccountProviderList.AccountProviderName[buttonIndex]
                accountProvidrId = AccountProviderList.AccountProviderid[buttonIndex]
                accountIin = AccountProviderList.IIN[buttonIndex]
            }
            
        }
        
    }
    
    func convertStringToDictionary(text: String) -> [NSObject:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [NSObject:AnyObject]
            }
            catch
            {
                print(error)
            }
            catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textFieldToChange == amountTxtField
        {
            characterCountLimit = 6
            if((amountTxtField.text?.rangeOfString(".")) != nil){
                characterCountLimit = 9
                let strcount = amountTxtField.text! + string
                let strarray = strcount.componentsSeparatedByString(".")
                
                for(var i = 0; i<strarray.count; i++){
                    if i == 1{
                        if strarray[1].isEmpty{
                            
                        }
                        else{
                            if strarray[1].characters.count == 3{
                                return false
                            }
                            else{
                                return true
                            }
                        }
                    }
                }
                
            }
            else if string == "." && amountTxtField.text?.characters.count == 6{
                return true
            }
        }
        else if textFieldToChange == AadharNumberTxtField
        {
            characterCountLimit = 12
        }
        else {
            characterCountLimit = 50
        }
        
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "sendmoneyAadhar_to_success") {
            let nextview = segue.destinationViewController as! CollectMoneySuccessViewController
            nextview.transactionID = transactionid
            nextview.status = status
            nextview.descriptiontxt = descriptiontxt
            
        }
    }
    
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
}
