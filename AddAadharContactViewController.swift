
//
//  AddAadharContactViewController.swift
//  purZ
//
//  Created by Vertace on 25/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class AddAadharContactViewController: UIViewController,UITabBarControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var registeredTxtField: UITextField!
    @IBOutlet weak var aadharNumberTxtField: UITextField!
 
    var existing_Aadhar_no = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialFunc()
               // Do any additional setup after loading the view.
    }
    @IBAction func confirmBtnAction(sender: AnyObject) {
        var alreadyexist = false
        for(var i=0;i<existing_Aadhar_no.count;i++) {
            if existing_Aadhar_no[i] == aadharNumberTxtField.text! {
                alreadyexist = true
            }
        }
        if !alreadyexist {
        let aadharNo = aadharNumberTxtField.text!
        if (aadharNumberTxtField.text == "" )
        {
            
            self.presentViewController(Alert().alert("Aadhar number cannot be empty!",message: ""), animated: true, completion: nil)
        }
        else if registeredTxtField.text == ""
        {
            
            self.presentViewController(Alert().alert("please enter valid name",message: ""), animated: true, completion: nil)
        }
        else if aadharNo.characters.count < 12 {
            self.presentViewController(Alert().alert("Invalid Aadhar",message: ""), animated: true, completion: nil)
        }
        else
        {
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            if purzDB.open() {
                let insert = "INSERT INTO BENEFICIARY_AADHAR (AADHAR_NO,REGISTERED_NAME) VALUES ('\(aadharNumberTxtField.text!)','\(registeredTxtField.text!)')"
                let result = purzDB.executeUpdate(insert, withArgumentsInArray: nil)
                if !result  {
                    //   status.text = "Failed to add contact"
                    print("Error: \(purzDB.lastErrorMessage())")
                }
            }
        }
        performSegueWithIdentifier("To_AadharList", sender: self)
        }
        else {
            aadharNumberTxtField.text = ""
            registeredTxtField.text = ""
            self.presentViewController(Alert().alert("Beneficiary Aadhar already exist!", message: ""), animated: true, completion: nil)
        }
    }

    func initialFunc()
    {
        tabBarController?.delegate = self
        confirmBtn.layer.borderWidth = 2
        confirmBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        confirmBtn.layer.cornerRadius = confirmBtn.frame.size.height/2
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, registeredTxtField.frame.size.height - 1 , registeredTxtField.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                registeredTxtField.borderStyle = UITextBorderStyle.None
        registeredTxtField.layer.addSublayer(bottomLine)
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, aadharNumberTxtField.frame.size.height - 1 , aadharNumberTxtField.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                aadharNumberTxtField.borderStyle = UITextBorderStyle.None
        aadharNumberTxtField.layer.addSublayer(bottomLine1)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GenerateQRViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

    }
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        if textFieldToChange == aadharNumberTxtField
        {
            
            return newLength <= 12
            
        }
               return true
    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
}
