//
//  File.swift
//  Purz
//
//  Created by Vertace on 01/03/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import Foundation

class PayBillMode  {
    
    
    
    var entityId: String
    var dateTime: String
    var billerId: String
    var authenticator1: String
    var authenticator2: String
    var authenticator3: String
    var transactionId: String
    var fromEntityId: String

    var toEntityId: String
    var comment: String
    var businessEntityId: String
    var productId: String
    var transactionType: String
    var transactionOrigin: String
    var yapcode: String
    var business: String

    var amount: String
    var refTxnId: String
    var billId: String
    var billNumber: String
    var billDate: String
    var billDueDate: String
    var billAmount: String
    var payWithOutBill: String
    var partialPayment: String
    var filler1: String
    var filler2: String
    var filler3: String

    init?(entityId: String, dateTime: String, billerId: String, authenticator1: String, authenticator2: String, authenticator3: String, transactionId: String, fromEntityId: String, toEntityId: String, comment: String, businessEntityId: String, productId: String, transactionType: String, transactionOrigin: String, yapcode: String, business: String, amount: String, refTxnId: String, billId: String, billNumber: String, billDate: String, billDueDate: String, billAmount: String, payWithOutBill: String, partialPayment: String, filler1: String, filler2: String, filler3: String){
        self.entityId = entityId
        self.dateTime = dateTime
        self.billerId = billerId
        self.authenticator1 = authenticator1
        self.authenticator2 = authenticator2
        self.authenticator3 = authenticator3
        self.transactionId = transactionId
        self.fromEntityId = fromEntityId
        self.toEntityId = toEntityId
        self.comment = comment
        self.businessEntityId = businessEntityId
        self.productId = productId
        self.transactionType = transactionType
        self.transactionOrigin = transactionOrigin
        self.yapcode = yapcode
        self.business = business
        self.amount = amount
        self.refTxnId = refTxnId
        self.billId = billId
        self.billNumber = billNumber
        self.billDate = billDate
        self.billDueDate = billDueDate
        self.billAmount = billAmount
        self.payWithOutBill = payWithOutBill

        self.partialPayment = partialPayment
        self.filler1 = filler1
        self.filler2 = filler2
        self.filler3 = filler3
    }
    
}


