//
//  AccountProviderList.swift
//  purZ
//
//  Created by Vertace on 20/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import Foundation

struct AccountProviderList {

    static var AccountProviderName = [String]()
    static var AccountProviderid = [String]()
    static var IIN = [String]()
    
    
    
}
struct AccountConstant {
    static var accountNumber = [String]()
    static var bankname = [String]()
    static var ifsc = [String]()
    static var accounttype = [String]()
    static var upi = [String]()
    static var accountRefNo = [String]()
    static var indexpath = 0
    static var accountdetail = ""
    static var VPA = ""
    static var accountno = ""
    static var initiateSDK = false
    static var defaultdebit = false
    static var defaultcredit = false
    static var providerid = ""
    static var providername = ""
    static var iin = ""
    static var fromaccount = false
    
    static var vpanames = [String]()
    static var vparesponses = [String]()
    static var vpaProviderName = [String]()
    
}