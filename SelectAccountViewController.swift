//
//  SelectAccountViewController.swift
//  purZ
//
//  Created by Vertace on 31/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class SelectAccountViewController: UIViewController,UITableViewDataSource, UITableViewDelegate, UITabBarControllerDelegate {

    
    @IBOutlet var tableView: UITableView!
    
    var bankname = ""
    var accountNo = ""
    var ifsc = ""
    var accounttype = ""
    var upi = ""
    var accountdetail = ""
    var accountRefNo = ""
    var fromAccountProvider = false
    var fromAvailableAccounts = false
    var vpa = ""
    var checkavailability = false
    var fromListVPA = false
    
    var providerid = [String]()
    var providername = [String]()
    var iin = [String]()
    
    var temp_bankname = [String]()
    var temp_accountNo = [String]()
    var temp_ifsc = [String]()
    var temp_accounttype = [String]()
    var temp_upi = [String]()
    var temp_accountdetail = [String]()
    var temp_accountRefNo = [String]()
    var fromselectaccount = false
    
    var fromLinkVPA = false
    var vpadetails = ""
    
    var vpaname = [String]()
    var VPAresponse = [String]()
    var textcolor = UIColor.blackColor()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tabBarController?.delegate = self
        getUserBankAccountList()
    }
    @IBAction func backBtnAction(sender: AnyObject) {
        if fromLinkVPA {
            
            self.performSegueWithIdentifier("To_LinkVPA", sender: self)
        }
        else {
            self.performSegueWithIdentifier("To_CreateVPA", sender: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.f
    }
    func getUserBankAccountList(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let select = "SELECT * FROM ACCOUNTS"
            let result:FMResultSet = purzDB.executeQuery(select,
                                                         withArgumentsInArray: nil)
            while(result.next()){
                temp_bankname.append(result.stringForColumn("BANK_NAME"))
                temp_accountNo.append(result.stringForColumn("ACCOUNT_NO"))
                temp_ifsc.append(result.stringForColumn("IFSC"))
                temp_accounttype.append(result.stringForColumn("TYPE"))
                temp_upi.append(result.stringForColumn("UPI"))
                temp_accountRefNo.append(result.stringForColumn("ACCOUNTREF_NO"))
                temp_accountdetail.append(result.stringForColumn("ACCOUNTDETAIL"))
                providerid.append(result.stringForColumn("PROVIDER_ID"))
                providername.append(result.stringForColumn("PROVIDER_NAME"))
                iin.append(result.stringForColumn("IIN"))
            }
            
            self.tableView.reloadData()
            
        }
        purzDB.close()
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SelectAccount", forIndexPath: indexPath) as UITableViewCell!
        
        let accountnolbl = cell.viewWithTag(1) as! UILabel
        let banknamelbl = cell.viewWithTag(2) as! UILabel
        
        accountnolbl.text = self.temp_accountNo[indexPath.row]
        banknamelbl.text = self.temp_bankname[indexPath.row]
     
//                    cell.layer.borderWidth = 1
//                    cell.layer.borderColor = UIColor(red: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1).CGColor
                    cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.temp_accountNo.count
            }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        bankname = self.temp_bankname[indexPath.row]
        accountNo = self.temp_accountNo[indexPath.row]
        ifsc = self.temp_ifsc[indexPath.row]
        accounttype = self.temp_accounttype[indexPath.row]
        upi = self.temp_upi[indexPath.row]
        accountdetail = self.temp_accountdetail[indexPath.row]
        accountRefNo = self.temp_accountRefNo[indexPath.row]

        AccountConstant.providerid = providerid[indexPath.row]
        AccountConstant.providername = providername[indexPath.row]
        AccountConstant.iin = iin[indexPath.row]
    
        if fromLinkVPA {
            let AccountProviderdetail = AccountProvider.init(accountProviderId: providerid[indexPath.row], provider: providername[indexPath.row], providerIin: iin[indexPath.row])
            ApiClient().getCustomerAccounts(AccountProviderdetail) { (success, error, response) in

                if success && response != nil {
                    do{
                        if let dictionaryvalue = try response as? NSDictionary as? [String : AnyObject] {
                            // do whatever with jsonResult
                            let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(dictionaryvalue, options: NSJSONWritingOptions.PrettyPrinted)
                            let json = JSON(data: jsonData)
                            
                            var count = 0
                            let json_account = json["accountProvider"]
                            for items in json["customerDetails"].arrayValue {
                                count++
                                if self.accountNo == items["account"].stringValue {
                                    self.upi = items["mbeba"].stringValue
                                    break
                                }
                            }
                            if count == 0 {
                                let jsonValue = JSON(data: jsonData)
                                let json_array = jsonValue["customerDetails"]
                                self.upi = json_array["mbeba"].stringValue
                            }
                            self.performSegueWithIdentifier("To_LinkVPA", sender: self)
                        }
                    }
                    catch {
                        print("error from get account details")
                    }
                }
                else {
                    self.upi = "N"
                    self.performSegueWithIdentifier("To_LinkVPA", sender: self)
                }
            }
            
            
        }
        else {
            self.performSegueWithIdentifier("To_CreateVPA", sender: self)
        }
        
    }
    
    

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "To_CreateVPA") {
            let nextview = segue.destinationViewController as! CreateVPAViewController
            nextview.bankname = self.bankname
            nextview.accountNo = self.accountNo
            nextview.ifsc = self.ifsc
            nextview.accounttype = self.accounttype
            nextview.upi = self.upi
            nextview.accountdetail = self.accountdetail
            nextview.accountRefNo = self.accountRefNo
            nextview.fromAccountProvider = self.fromAccountProvider
            nextview.fromAvailableAccounts = self.fromAvailableAccounts
            nextview.fromselectaccount = true
            nextview.fromListVPA = self.fromListVPA
            nextview.vpa = vpa
            nextview.checkavailability = checkavailability
            nextview.textcolor = textcolor
            
        }
        else if(segue.identifier == "To_LinkVPA") {
            let nextview = segue.destinationViewController as! LinkVPAViewController
            nextview.vpa = vpa
            nextview.vpadetails = vpadetails
            nextview.accountdetails = accountdetail
            nextview.accountno = accountNo
            nextview.bankname = bankname
            nextview.vpaname = vpaname
            nextview.VPAresponse = VPAresponse
            nextview.upi = self.upi
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
