//
//  CreateVPAViewController.swift
//  purZ
//
//  Created by Vertace on 24/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class CreateVPAViewController: UIViewController, UITabBarControllerDelegate, UITextFieldDelegate {

    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var vpaTextField: UITextField!
    @IBOutlet var selectAccountTextField: UITextField!
    @IBOutlet var checkavailabilitybtn: UIButton!
    @IBOutlet var submitbtn: UIButton!
    @IBOutlet var receivepaymentbtn: UIButton!
    @IBOutlet var sendpaymentbtn: UIButton!
    
    
    var bankname = ""
    var accountNo = ""
    var ifsc = ""
    var accounttype = ""
    var upi = ""
    var accountdetail = ""
    var accountRefNo = ""
    var fromAccountProvider = false
    var fromAvailableAccounts = false
    var default_send = false
    var default_receive = false
    var checkavailability = false
    var fromselectaccount = false
    var vpa = ""
    var fromListVPA = false
    var textcolor = UIColor.blackColor()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarController?.delegate = self
        vpaTextField.delegate = self
        selectAccountTextField.delegate = self
        selectAccountTextField.allowsEditingTextAttributes = false
       initialfunction()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GenerateQRViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @IBAction func selectAccountTextAction(sender: AnyObject) {
        self.performSegueWithIdentifier("vpa_to_selectaccount", sender: self)
    }
    
    
    @IBAction func sendPaymentBtnAction(sender: AnyObject) {
        if default_send {
            default_send = false
            sendpaymentbtn.setImage(UIImage(named: "unchecked.png"), forState: .Normal)
        }
        else{
            default_send = true
            sendpaymentbtn.setImage(UIImage(named: "checked_box.png"), forState: .Normal)
        }
        
    }
    
    
    @IBAction func receivePaymentBtnAction(sender: AnyObject) {
        if default_receive {
            default_receive = false
            
            receivepaymentbtn.setImage(UIImage(named: "unchecked.png"), forState: .Normal)
        }
        else{
            default_receive = true
            receivepaymentbtn.setImage(UIImage(named: "checked_box.png"), forState: .Normal)
        }
        
    }
    
    
    @IBAction func checkAvailabilityBtnAction(sender: AnyObject) {
        view.endEditing(true)
        ApiClient().checkVPA(self.vpaTextField.text!+"@purz") { (success, error, response) in
            if success {
                self.checkavailability = true
                self.vpaTextField.textColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1)
                self.presentViewController(Alert().alert("Your VPA available",message: ""), animated: true, completion: nil)
            }
            else{
                self.checkavailability = false
                self.vpaTextField.textColor = UIColor.redColor()
                if response != nil {
                    let msg = response.valueForKey("message")
                    if String(msg!) == "virtual address already exist" {
                        self.presentViewController(Alert().alert("VPA not available",message: ""), animated: true, completion: nil)
                    }
                    else {
                        self.presentViewController(Alert().alert(String(msg!),message: ""), animated: true, completion: nil)
                    }
                }
                else{
                    self.presentViewController(Alert().alert("VPA not available",message: ""), animated: true, completion: nil)
                }
            }
        }
        
    }
    
    @IBAction func bachBtnAction(sender: AnyObject) {
        if fromAccountProvider || fromAvailableAccounts {
            self.performSegueWithIdentifier("createVPA_to_Accounts", sender: self)
        }
        else if fromListVPA {
            self.performSegueWithIdentifier("createVPA_to_listVPA", sender: self)
        }
        else{
            self.performSegueWithIdentifier("createvpa_to_VPAmanager", sender: self)
        }
    }
    @IBAction func submitBtnAction(sender: AnyObject) {
        if vpaTextField.text! == "" {
            self.presentViewController(Alert().alert("Please enter the VPA",message: ""), animated: true, completion: nil)
        }
        else if selectAccountTextField.text! == ""{
            self.presentViewController(Alert().alert("Please select the Bank Account",message: ""), animated: true, completion: nil)
        }
        else if !checkavailability {
            self.view.userInteractionEnabled = false
            self.activityIndicator.startAnimating()
            
            ApiClient().checkVPA(self.vpaTextField.text!+"@purz") { (success, error, response) in
                if success {
                    self.checkavailability = true
                    self.vpaTextField.textColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1)
                    
                    var defaultDebitCredit = false
                    var default_Credit = false
                    if self.default_receive {
                        AccountDefaultStatus.init(defaultCredit: true)
                        AccountDefaultStatus().defaultCredit = true
                        default_Credit = true
                        AccountConstant.defaultcredit = true
                    }
                    else{
                        AccountDefaultStatus.init(defaultCredit: false)
                        AccountDefaultStatus().defaultCredit = false
                        default_Credit = false
                        AccountConstant.defaultcredit = false
                    }
                    if self.default_send {
                        AccountDefaultStatus.init(defaultDebit: true)
                        AccountDefaultStatus().defaultDebit = true
                        defaultDebitCredit = true
                        AccountConstant.defaultdebit = true
                    }
                    else{
                        AccountDefaultStatus.init(defaultDebit: false)
                        AccountDefaultStatus().defaultDebit = false
                        defaultDebitCredit = false
                        AccountConstant.defaultdebit = false
                    }
                    AccountDefaultStatus.init(defaultDebitCredit: defaultDebitCredit, default_Credit: default_Credit)
                    
                    
                    let email = Appconstant.mobileno + "@test.com"
                    
                    AccountConstant.accountdetail = self.accountdetail
                    AccountConstant.VPA = self.vpaTextField.text!+"@purz"
                    AccountConstant.accountno = self.accountNo
                    if self.upi == "N" {
                        if self.fromAccountProvider || self.fromAvailableAccounts {
                            AccountConstant.fromaccount = true
                        }
                        self.performSegueWithIdentifier("popover_to_card", sender: self)
                    }
                    else{
                        do {
                            
                            let ClientViewModel = SdkClientApplication.init(userName: Appconstant.customername, appName: "com.equitas.purz", instituteId: "999", channel: "equitas-purz", bankRefUrl: "https://www.equitasbank.com", userMail: email, mobile: Appconstant.mobileno, userIdentity: Appconstant.customerid, transactionID_Prefix: "ESF")
                            
                            //            let data = accountdetail.dataUsingEncoding(NSUTF8StringEncoding)
                            let customeracc = self.convertStringToDictionary(self.accountdetail)! as NSDictionary
                           
                            //                            let AccountProviderViewModel = AccountProvider.init(accountProviderId: AccountConstant.providerid, provider: AccountConstant.providername, providerIin: AccountConstant.iin)
                            
                            //                let detail = CustomerAccount().collectCustomerDetails(customeracc as! [NSObject : AnyObject], provider: AccountProviderViewModel)
                            
                            let CustomerAcc_viewmodel = CustomerAccount.init(accountDetails: customeracc as! [NSObject : AnyObject])
                            
                            
                            let defaultstatus = AccountDefaultStatus.init(defaultDebitCredit: defaultDebitCredit, default_Credit: default_Credit)
                            
                            try ApiClient().mapAccount(ClientViewModel, vpa: self.vpaTextField.text!+"@purz", custAccount: CustomerAcc_viewmodel, accountDefaultStatus: defaultstatus, withCompletion: { (success, error, response) in
                                
                                self.view.userInteractionEnabled = true
                                self.activityIndicator.stopAnimating()
                                AccountConstant.vpanames.removeAll()
                                AccountConstant.vparesponses.removeAll()
                                AccountConstant.vpaProviderName.removeAll()
                                if success {
                                    let response_success = response.valueForKey("success")
                                    if String(response_success!) == "1" {
                                        self.presentViewController(Alert().alert("VPA created successfully!",message: ""), animated: true, completion: nil)
                                        self.vpaTextField.text = ""
                                        self.selectAccountTextField.text = ""
                                        self.sendpaymentbtn.setImage(UIImage(named: "unchecked.png"), forState: .Normal)
                                        self.receivepaymentbtn.setImage(UIImage(named: "unchecked.png"), forState: .Normal)

                                    }
                                }
                                else{
                                    if response != nil {
                                        self.presentViewController(Alert().alert("VPA failed to create",message: ""), animated: true, completion: nil)
                                    }
                                }
                            })
                        }
                        catch{
                            
                            self.view.userInteractionEnabled = true
                            self.activityIndicator.stopAnimating()
                            
                        }
                    }
                    
                }
                else{
                    self.view.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
                    
                    self.checkavailability = false
                    self.vpaTextField.textColor = UIColor.redColor()
                    if response != nil {
                        let msg = response.valueForKey("message")
                        self.createtoastmessage(String(msg!))
                    }
                }
            }
        }
        else{
            var defaultDebitCredit = false
            var default_Credit = false
            if self.default_receive {
                AccountDefaultStatus.init(defaultCredit: true)
                AccountDefaultStatus().defaultCredit = true
                default_Credit = true
                AccountConstant.defaultcredit = true
            }
            else{
                AccountDefaultStatus.init(defaultCredit: false)
                AccountDefaultStatus().defaultCredit = false
                default_Credit = false
                AccountConstant.defaultcredit = false
            }
            if self.default_send {
                AccountDefaultStatus.init(defaultDebit: true)
                AccountDefaultStatus().defaultDebit = true
                defaultDebitCredit = true
                AccountConstant.defaultdebit = true
            }
            else{
                AccountDefaultStatus.init(defaultDebit: false)
                AccountDefaultStatus().defaultDebit = false
                defaultDebitCredit = false
                AccountConstant.defaultdebit = false
            }
            AccountDefaultStatus.init(defaultDebitCredit: defaultDebitCredit, default_Credit: default_Credit)
            
            
            let email = Appconstant.mobileno + "@test.com"
            
            AccountConstant.accountdetail = self.accountdetail
            AccountConstant.VPA = self.vpaTextField.text!+"@purz"
            AccountConstant.accountno = self.accountNo
            if self.upi == "N" {
                self.performSegueWithIdentifier("popover_to_card", sender: self)
            }
            else{
                do {
                    
                    let userdetail = SdkClientApplication.init(userName: Appconstant.customername, appName: "com.equitas.purz", instituteId: "999", channel: "equitas-purz", bankRefUrl: "https://www.equitasbank.com", userMail: email, mobile: Appconstant.mobileno, userIdentity: Appconstant.customerid, transactionID_Prefix: "ESF")
                    
                    //let data = accountdetail.dataUsingEncoding(NSUTF8StringEncoding)
                    
                    let customeracc = self.convertStringToDictionary(self.accountdetail)! as NSDictionary
                    
                    // let AccountProviderViewModel = AccountProvider.init(accountProviderId: AccountConstant.providerid, provider: AccountConstant.providername, providerIin: AccountConstant.iin)
                    
                    //                let detail = CustomerAccount().collectCustomerDetails(customeracc as! [NSObject : AnyObject], provider: AccountProviderViewModel)
                    
                    let CustomerAcc_detail = CustomerAccount.init(accountDetails: customeracc as! [NSObject : AnyObject])
                    
                    
                    let defaultstatus = AccountDefaultStatus.init(defaultDebitCredit: defaultDebitCredit, default_Credit: default_Credit)
                    
                    try ApiClient().mapAccount(userdetail, vpa: self.vpaTextField.text!+"@purz", custAccount: CustomerAcc_detail, accountDefaultStatus: defaultstatus, withCompletion: { (success, error, response) in
                        
                        self.view.userInteractionEnabled = true
                        self.activityIndicator.stopAnimating()
                        
                        if success {
                            AccountConstant.vpanames.removeAll()
                            AccountConstant.vparesponses.removeAll()
                            AccountConstant.vpaProviderName.removeAll()
                            let response_success = response.valueForKey("success")
                            if String(response_success!) == "1" {
                                self.presentViewController(Alert().alert("VPA created successfully!",message: ""), animated: true, completion: nil)
                                self.vpaTextField.text = ""
                                self.selectAccountTextField.text = ""
                                
                            }
                        }
                        else{
                            if response != nil {
                                let msg = String(response.valueForKey("message")!)
                                if msg != "" {
                                    self.presentViewController(Alert().alert(msg,message: ""), animated: true, completion: nil)
                                }
                                else {
                                    self.presentViewController(Alert().alert("VPA failed to create",message: ""), animated: true, completion: nil)
                                }
                            }
                        }
                    })
                }
                catch{
                    
                    self.view.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
                }
            }
            
        }
        
        
    }
    


    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    func initialfunction(){
        activityIndicator.hidesWhenStopped = true
        submitbtn.layer.borderWidth = 2
        submitbtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        submitbtn.layer.cornerRadius = submitbtn.frame.size.height/2 - 2
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, vpaTextField.frame.height - 1 , vpaTextField.frame.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        vpaTextField.borderStyle = UITextBorderStyle.None
        vpaTextField.layer.addSublayer(bottomLine)
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, selectAccountTextField.frame.height - 1 , selectAccountTextField.frame.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        selectAccountTextField.borderStyle = UITextBorderStyle.None
        selectAccountTextField.layer.addSublayer(bottomLine1)
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        checkavailabilitybtn.titleLabel?.attributedText = NSAttributedString(string: "Check Availability", attributes: underlineAttribute)
        self.vpaTextField.textColor = textcolor
        if vpa != "" {
            vpaTextField.text = vpa
        }
        if fromselectaccount {
            vpaTextField.text = vpa
            selectAccountTextField.text = self.bankname + " " + self.accountNo
            if checkavailability {
                self.vpaTextField.textColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1)
            }
        }
        
        if AccountConstant.fromaccount {
            AccountConstant.fromaccount = false
             fromAccountProvider = true
            fromAvailableAccounts = true
        }
    }
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        checkavailability = false
        if textFieldToChange == vpaTextField{
            vpaTextField.textColor = UIColor.blackColor()
        }
        
        return true
    }
       func convertStringToDictionary(text: String) -> [NSObject:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [NSObject:AnyObject]
            }
            catch
            {
                print(error)
            }
            catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
   
    func createtoastmessage(message: String){
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 135, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        toastLabel.textColor = UIColor.whiteColor()
        toastLabel.textAlignment = .Center
        toastLabel.font = UIFont(name: "Calibri", size: 7.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        
        
        UIView.animateWithDuration(4.0, delay: 1.0, options: .CurveEaseInOut, animations: {
            toastLabel.alpha = 0.0
            }, completion: { (false) in
                toastLabel.removeFromSuperview()
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "vpa_to_selectaccount") {
            let nextview = segue.destinationViewController as! SelectAccountViewController
            nextview.bankname = self.bankname
            nextview.accountNo = self.accountNo
            nextview.ifsc = self.ifsc
            nextview.accounttype = self.accounttype
            nextview.upi = self.upi
            nextview.accountdetail = self.accountdetail
            nextview.accountRefNo = self.accountRefNo
            nextview.fromAccountProvider = self.fromAccountProvider
            nextview.fromAvailableAccounts = self.fromAvailableAccounts
            nextview.fromListVPA = self.fromListVPA
            nextview.vpa = self.vpaTextField.text!
            nextview.checkavailability = self.checkavailability
            nextview.textcolor = self.vpaTextField.textColor!
        }
        else if(segue.identifier == "popover_to_card") {
            let nextview = segue.destinationViewController as! CardDetailViewController
            
            
        }
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    

    
    
}
