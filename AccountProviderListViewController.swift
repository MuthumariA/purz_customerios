//
//  AccountProviderListViewController.swift
//  purZ
//
//  Created by Vertace on 20/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class AccountProviderListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITabBarControllerDelegate {
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    
    
    
    
    var bankname = ""
    var accountNo = [String]()
    var ifsc = [String]()
    var accounttype = [String]()
    var upi = [String]()
    var accountexists = false
    var accountRefNo = [String]()
    var accountdetail = [String]()
    var dict = NSDictionary()
    
    override func viewDidLoad() {
        tabBarController?.delegate = self
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ListCell", forIndexPath: indexPath) as UITableViewCell!
        let providenamelbl = cell.viewWithTag(1) as! UILabel
        providenamelbl.text = AccountProviderList.AccountProviderName[indexPath.row]
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return AccountProviderList.AccountProviderName.count
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.activityIndicator.startAnimating()
        self.tableView.userInteractionEnabled = false
        AccountConstant.indexpath = indexPath.row
        
        bankname = AccountProviderList.AccountProviderName[indexPath.row]
        AccountConstant.providerid = AccountProviderList.AccountProviderid[indexPath.row]
        AccountConstant.providername = AccountProviderList.AccountProviderName[indexPath.row]
        AccountConstant.iin = AccountProviderList.IIN[indexPath.row]
        let AccountProviderdetail = AccountProvider.init(accountProviderId: AccountProviderList.AccountProviderid[indexPath.row], provider: AccountProviderList.AccountProviderName[indexPath.row], providerIin: AccountProviderList.IIN[indexPath.row])
        
        ApiClient().getCustomerAccounts(AccountProviderdetail) { (success, error, response) in
            
            if response != nil {
                
                do{
                    if let dictionaryvalue = try response as? NSDictionary as? [String : AnyObject] {
                        // convert jsonResult format from NSDictionary
                        let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(dictionaryvalue, options: NSJSONWritingOptions.PrettyPrinted)
                        
                        
                        self.accountNo.removeAll()
                        self.ifsc.removeAll()
                        self.accounttype.removeAll()
                        self.upi.removeAll()
                        self.accountRefNo.removeAll()
                        self.accountdetail.removeAll()
                        
                        let json = JSON(data: jsonData)
                        
                        let json_account = json["accountProvider"]
                        for items in json["customerDetails"].arrayValue {
                            var str = ""
                            
                            str = "{\"customerDetails\":"+String(items)+",\n"+"\"accountProvider\":"+String(json_account)+"\n}"
                           
                            self.accountdetail.append(str)
                            self.accountNo.append(items["account"].stringValue)
                            self.ifsc.append(items["ifsc"].stringValue)
                            self.accounttype.append(items["accountType"].stringValue)
                            self.upi.append(items["mbeba"].stringValue)
                            self.accountRefNo.append(items["accRefNumber"].stringValue)
                        }
                        if self.accountNo.count == 0 {
                            self.accountNo.removeAll()
                            self.ifsc.removeAll()
                            self.accounttype.removeAll()
                            self.upi.removeAll()
                            self.accountRefNo.removeAll()
                            self.accountdetail.removeAll()
                            
                            let jsonValue = JSON(data: jsonData)
                            let json_array = jsonValue["customerDetails"]
                            
                            self.accountNo.append(json_array["account"].stringValue)
                            self.ifsc.append(json_array["ifsc"].stringValue)
                            self.accounttype.append(json_array["accountType"].stringValue)
                            self.upi.append(json_array["mbeba"].stringValue)
                            self.accountRefNo.append(json_array["accRefNumber"].stringValue)
                            self.accountdetail.append(String(jsonValue))
                        }
                        
                        if self.accountNo.count == 0 || self.accountNo[0] == ""{
                            self.tableView.userInteractionEnabled = true
                            self.activityIndicator.stopAnimating()
                            self.presentViewController(Alert().alert("You have no account associated with this Bank",message: ""), animated: true, completion: nil)
                        }
                        else{
                            for(var i=0;i<AccountConstant.accountNumber.count;i++){
                                if AccountConstant.accountNumber[i] == self.accountNo[0] {
                                    self.accountexists = true
                                }
                            }
                            if self.accountexists && self.accountNo.count == 1{
                                
                                self.performSegueWithIdentifier("back_to_accounts", sender: self)
                                
                            }
                            else if self.accountNo.count > 1 {
                                
                                self.performSegueWithIdentifier("provider_to_accountList", sender: self)
                                
                            }
                            else if self.accountNo.count == 1{
                                
                                DBHelper().purzDB()
                                let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
                                let databasePath = databaseURL.absoluteString
                                let purzDB = FMDatabase(path: databasePath as String)
                                if purzDB.open() {
                                    let insertsql = "INSERT INTO ACCOUNTS (CUSTOMER_ID,BANK_NAME,ACCOUNT_NO,IFSC,TYPE,VPA,BALANCE,UPI,ACCOUNTREF_NO,ACCOUNTDETAIL,PROVIDER_ID,PROVIDER_NAME,IIN) VALUES ('\(Appconstant.customerid)','\(self.bankname)','\(self.accountNo[0])','\(self.ifsc[0])','\(self.accounttype[0])','\("")','\("")','\(self.upi[0])','\(self.accountRefNo[0])','\(self.accountdetail[0])','\(AccountProviderList.AccountProviderid[indexPath.row])','\(AccountProviderList.AccountProviderName[indexPath.row])','\(AccountProviderList.IIN[indexPath.row])')"
                                    
                                    let result = purzDB.executeUpdate(insertsql,
                                                                      withArgumentsInArray: nil)
                                    
                                    if !result {
                                        print("Error: \(purzDB.lastErrorMessage())")
                                    }
                                    else{
                                        // go to create VPA page
                                        self.performSegueWithIdentifier("AccProvider_to_VPA", sender: self)
                                    }
                                }
                                purzDB.close()
                            }
                        }
                    }
                    else{
                        let response_array = response as! [[String : AnyObject]]
                        
                        let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(response_array, options: NSJSONWritingOptions.PrettyPrinted)
                        let json = JSON(data: jsonData)
                        
                        self.accountNo.removeAll()
                        self.ifsc.removeAll()
                        self.accounttype.removeAll()
                        self.upi.removeAll()
                        self.accountRefNo.removeAll()
                        self.accountdetail.removeAll()
                        
                        for items in json.arrayValue {
                            let str: String = String(items)
                            
                            self.accountdetail.append(str)
                            self.accountNo.append(items["account"].stringValue)
                            self.ifsc.append(items["ifsc"].stringValue)
                            self.accounttype.append(items["accountType"].stringValue)
                            self.upi.append(items["mbeba"].stringValue)
                            self.accountRefNo.append(items["accRefNumber"].stringValue)
                        }
                        for(var i=0;i<AccountConstant.accountNumber.count;i++){
                            if self.accountNo.count > 0 {
                                if AccountConstant.accountNumber[i] == self.accountNo[0] {
                                    self.accountexists = true
                                }
                            }
                        }
                        if self.accountexists && self.accountNo.count == 1{
                            
                            self.performSegueWithIdentifier("back_to_accounts", sender: self)
                            
                        }
                        else if self.accountNo.count > 1 {
                            self.performSegueWithIdentifier("provider_to_accountList", sender: self)
                        }
                        else if self.accountNo.count == 1{
                            
                            DBHelper().purzDB()
                            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
                            let databasePath = databaseURL.absoluteString
                            let purzDB = FMDatabase(path: databasePath as String)
                            if purzDB.open() {
                                let insertsql = "INSERT INTO ACCOUNTS (CUSTOMER_ID,BANK_NAME,ACCOUNT_NO,IFSC,TYPE,VPA,BALANCE,UPI,ACCOUNTREF_NO,ACCOUNTDETAIL,PROVIDER_ID,PROVIDER_NAME,IIN) VALUES ('\(Appconstant.customerid)','\(self.bankname)','\(json["account"].stringValue)','\(json["ifsc"].stringValue)','\(json["accountType"].stringValue)','\("")','\("")','\(json["mbeba"].stringValue)','\(json["accRefNumber"].stringValue)','\(json)','\(AccountProviderList.AccountProviderid[indexPath.row])','\(AccountProviderList.AccountProviderName[indexPath.row])','\(AccountProviderList.IIN[indexPath.row])')"
                                
                                let result = purzDB.executeUpdate(insertsql,
                                                                  withArgumentsInArray: nil)
                                
                                if !result {
                                    print("Error: \(purzDB.lastErrorMessage())")
                                }
                                else{
                                    self.tableView.userInteractionEnabled = true
                                    self.activityIndicator.stopAnimating()
                                    // go to create VPA page
                                    self.performSegueWithIdentifier("AccProvider_to_VPA", sender: self)
                                }
                            }
                            purzDB.close()
                        }
                    }
                    
                    
                }
                catch {
                    self.tableView.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
                }
                
            }
            else {
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
                    self.presentViewController(Alert().alert("You have no account associated with this Bank",message: ""), animated: true, completion: nil)
                }
                
            }
        }
        //        provider_to_accountList
        
    }
    
    func createtoastmessage(message: String){
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 135, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        toastLabel.textColor = UIColor.whiteColor()
        toastLabel.textAlignment = .Center
        toastLabel.font = UIFont(name: "Calibri", size: 7.0)
        toastLabel.text = message
        toastLabel.numberOfLines = 5
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        
        
        UIView.animateWithDuration(4.0, delay: 1.0, options: .CurveEaseInOut, animations: {
            toastLabel.alpha = 0.0
            }, completion: { (false) in
                toastLabel.removeFromSuperview()
        })
    }
    
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "back_to_accounts") {
            let nextview = segue.destinationViewController as! AccountsViewController
            if accountexists {
                nextview.toastmessage = "Beneficiary Account already exist!"
            }
        }
        else if(segue.identifier == "provider_to_accountList") {
            let nextview = segue.destinationViewController as! AvailableAccountViewController
            nextview.bankname = self.bankname
            nextview.accountNo = self.accountNo
            nextview.bankname = self.bankname
            nextview.ifsc = self.ifsc
            nextview.accounttype = self.accounttype
            nextview.upi = self.upi
            nextview.accountRefNo = self.accountRefNo
            nextview.accountdetails = self.accountdetail
            nextview.providerid = AccountProviderList.AccountProviderid[AccountConstant.indexpath]
            nextview.providername = AccountProviderList.AccountProviderName[AccountConstant.indexpath]
            nextview.iin = AccountProviderList.IIN[AccountConstant.indexpath]
            
        }
        else if(segue.identifier == "AccProvider_to_VPA"){
            let nextview = segue.destinationViewController as! CreateVPAViewController
            nextview.bankname = self.bankname
            nextview.accountNo = self.accountNo[0]
            nextview.bankname = self.bankname
            nextview.ifsc = self.ifsc[0]
            nextview.accounttype = self.accounttype[0]
            nextview.upi = self.upi[0]
            nextview.accountdetail = self.accountdetail[0]
            nextview.accountRefNo = self.accountRefNo[0]
            nextview.fromAccountProvider = true
        }
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
}
