//
//  sendMoneyMMIDViewController.swift
//  purZ
//
//  Created by Vertace on 24/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class sendMoneyMMIDViewController: UIViewController, UITabBarControllerDelegate,UITextFieldDelegate {
    
    
    
    @IBOutlet weak var VPAListTxtField: UITextField!
    @IBOutlet weak var Remark: UITextField!
    @IBOutlet weak var Amount: UITextField!
    @IBOutlet weak var payeeMMID: UITextField!
    @IBOutlet weak var payeeMobile: UITextField!
    @IBOutlet weak var payeeName: UITextField!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var refreshBtn: UIButton!
    @IBOutlet weak var sendBtn: UIButton!
    
    var contact_PayeeName = ""
    var contact_MobileNo = ""
    var contact_MMID = ""
    var vpa = [String]()
    var VPAResponse = [String]()
    var payerVPA=NSDictionary()
    var alertView: UIAlertView = UIAlertView()
    var characterCountLimit = 0
    var status = ""
    var descriptiontxt = ""
    var transactionid = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
      
        initialfunc()
        pick_contact()
        VPAList()
        // Do any additional setup after loading the view.
    }
    @IBAction func refreshBtnAction(sender: AnyObject)
    {
        print("Tapped")
        UIView.animateWithDuration(0.5, animations: {
            let angle: CGFloat? = CGFloat((self.refreshBtn.valueForKeyPath("layer.transform.rotation.z") as? NSNumber)!)
            let transform = CGAffineTransformMakeRotation(angle! + CGFloat(M_PI))
            self.refreshBtn.transform = transform
        })
        payeeName.text=""
        payeeMobile.text=""
        payeeMMID.text=""
        Amount.text=""
        Remark.text=""
        VPAListTxtField.text=""
    }
    @IBAction func VPAListBtnAction(sender: AnyObject) {
        
        self.view.endEditing(true)
        
        if self.vpa.count != 0
        {
            alertView.show()
        }
    }
    @IBAction func contactBtnAction(sender: AnyObject) {
          Appconstant.sendMoney_MMID = true
    }
    
    @IBAction func sendBtnAction(sender: AnyObject) {
        let mobileNo = payeeMobile.text!
        let mmid = payeeMMID.text!
        if  payeeName.text == ""
        {
            self.presentViewController(Alert().alert("Name can't be empty",message: ""), animated: true, completion: nil)
            
        }
        else if payeeMobile.text == ""
        {
            self.presentViewController(Alert().alert("Mobile number can't be empty",message: ""), animated: true, completion: nil)
        }
        else if mobileNo.characters.count < 10
        {
            self.presentViewController(Alert().alert("Please enter valid mobile number",message: ""), animated: true, completion: nil)
        }
            
        else if payeeMMID.text == ""
        {
            self.presentViewController(Alert().alert("MMID can't be empty",message: ""), animated: true, completion: nil)
            
        }
        else if mmid.characters.count < 7
        {
            self.presentViewController(Alert().alert("Please enter valid MMID",message: ""), animated: true, completion: nil)
        }
        else if Amount.text == ""
        {
            self.presentViewController(Alert().alert("Amount can't be empty",message: ""), animated: true, completion: nil)
            
        }
      else if Double(Amount.text!)! < 1
            
        {
            self.presentViewController(Alert().alert("Amount can't be zero",message: ""), animated: true, completion: nil)
        }
        else if VPAListTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Please select the VPA",message: ""), animated: true, completion: nil)
            
        }
        else{
            let virtualAddress = VirtualAddress.init(payerVPA as! [NSObject : AnyObject], handle: VPAListTxtField.text!)
            let payeeparam = PayeeParam.init(mobileMmid: payeeName.text!, mobile: payeeMobile.text!, mmid: Int32(payeeMMID.text!)!)
            let transaction = TransactionParameters.init(transaction: virtualAddress, payee: payeeparam!, amount: NSDecimalNumber(string: Amount.text!), remark: Remark.text!)
            self.activityIndicator.startAnimating()
            self.sendBtn.userInteractionEnabled = false
            ApiClient().pay(transaction!, view: self, withCompletion: { (success, error, response) in
                if success
                {
                    
                    let response_success = response.valueForKey("success")
                    if String(response_success!) == "1" {
                        self.status = "Success"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("sendmoneyMMID_to_success", sender: self)
                    }
                    else {
                        self.status = "Failed"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("sendmoneyMMID_to_success", sender: self)
                    }
                }
                else
                {
                    if response != nil
                    {
                        self.status = "Failed"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("sendmoneyMMID_to_success", sender: self)
                    }
                }
                self.activityIndicator.stopAnimating()
                self.sendBtn.userInteractionEnabled = true
            })
            
            
        }
        
        
        
    }
    
    func initialfunc()
    {
        payeeName.userInteractionEnabled = true
        payeeMobile.userInteractionEnabled = true
        payeeMMID.userInteractionEnabled = true
        tabBarController?.delegate = self
        Amount.delegate = self
        Remark.delegate = self
        sendBtn.layer.borderWidth = 2
        sendBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        sendBtn.layer.cornerRadius = sendBtn.frame.size.height/2
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, payeeName.frame.size.height - 1 , payeeName.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                payeeName.borderStyle = UITextBorderStyle.None
        payeeName.layer.addSublayer(bottomLine)
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, payeeMobile.frame.size.height - 1 , payeeMobile.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                payeeMobile.borderStyle = UITextBorderStyle.None
        payeeMobile.layer.addSublayer(bottomLine1)
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, payeeMMID.frame.size.height - 1 , payeeMMID.frame.size.width, 1.0)
        bottomLine2.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                payeeMMID.borderStyle = UITextBorderStyle.None
        payeeMMID.layer.addSublayer(bottomLine2)
        let bottomLine3 = CALayer()
        bottomLine3.frame = CGRectMake(0.0, Amount.frame.size.height - 1 , Amount.frame.size.width, 1.0)
        bottomLine3.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                Amount.borderStyle = UITextBorderStyle.None
        Amount.layer.addSublayer(bottomLine3)
        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRectMake(0.0, Remark.frame.size.height - 1 , Remark.frame.size.width, 1.0)
        bottomLine4.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                Remark.borderStyle = UITextBorderStyle.None
        Remark.layer.addSublayer(bottomLine4)
        let bottomLine5 = CALayer()
        bottomLine5.frame = CGRectMake(0.0, VPAListTxtField.frame.size.height - 1 , VPAListTxtField.frame.size.width, 1.0)
        bottomLine5.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        VPAListTxtField.borderStyle = UITextBorderStyle.None
        VPAListTxtField.layer.addSublayer(bottomLine5)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GenerateQRViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    func VPAList()
    {
        self.activityIndicator.startAnimating()
        var error: NSError?
        ApiClient().getMappedVirtualAddresses
            { (success, error, response) in
                self.activityIndicator.stopAnimating()
                if success
                {
                    
                    if response != nil
                    {
                        
                        var jsonData: NSData?
                        
                        do
                            
                        {
                            self.vpa.removeAll()
                            self.VPAResponse.removeAll()
                            
                            jsonData = try NSJSONSerialization.dataWithJSONObject(response, options:NSJSONWritingOptions.PrettyPrinted)
                            let json = JSON(data: jsonData!)
//                            for item in json.arrayValue {
//                                self.vpa.append(item["va"].stringValue)
//                                self.VPAResponse.append(String(item))
//                                print("VPA:\(self.vpa)")
//                            }
                            for item1 in json.arrayValue {
                                
                                let item2 = item1["VirtualAddress"]
                                let items = item2["accounts"]
                                
                                self.vpa.append(items["handle"].stringValue)
                                for item in items["CustomerAccount"].arrayValue {
                                    if item["default-debit"].stringValue == "D"{
                                        self.VPAResponse.append(String(item))
                                    }
                                }
                            }
                            
                        }
                        catch
                        {
                            jsonData = nil
                            
                        }
                        let jsonDataLength = "\(jsonData!.length)"
                    }
                    
                    self.alertView.delegate = self
                    self.alertView.title = "Debit VPA"
                    for(var i = 0; i<self.vpa.count; i += 1){
                        self.alertView.addButtonWithTitle(self.vpa[i])
                    }
                    self.alertView.addButtonWithTitle("Cancel")
                }
                
        }
    }
    func convertStringToDictionary(text: String) -> [NSObject:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [NSObject:AnyObject]
            }
            catch
            {
                print(error)
            }
            catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    func pick_contact()
    {
        payeeName.text = contact_PayeeName
        payeeMobile.text = contact_MobileNo
        payeeMMID.text = contact_MMID
        if payeeName.text! != "" {
        payeeName.userInteractionEnabled = false
        }
        if payeeMobile.text! != "" {
        payeeMobile.userInteractionEnabled = false
        }
        if payeeMMID.text! != "" {
        payeeMMID.userInteractionEnabled = false
        }
        
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        if buttonIndex != VPAResponse.count
        {
            payerVPA = self.convertStringToDictionary(VPAResponse[buttonIndex])! as NSDictionary
            VPAListTxtField.text = vpa[buttonIndex]
            
            
        }
    }
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    
    
    
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "sendmoneyMMID_to_success") {
            let nextview = segue.destinationViewController as! CollectMoneySuccessViewController
            nextview.transactionID = transactionid
            nextview.status = status
            nextview.descriptiontxt = descriptiontxt
            
        }
     }
 
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textFieldToChange == Amount
        {
            characterCountLimit = 6
            if((Amount.text?.rangeOfString(".")) != nil){
                characterCountLimit = 9
                let strcount = Amount.text! + string
                let strarray = strcount.componentsSeparatedByString(".")
                
                for(var i = 0; i<strarray.count; i++){
                    if i == 1{
                        if strarray[1].isEmpty{
                            
                        }
                        else{
                            if strarray[1].characters.count == 3{
                                return false
                            }
                            else{
                                return true
                            }
                        }
                    }
                }
                
            }
            else if string == "." && Amount.text?.characters.count == 6{
                return true
            }
        }
        else if textFieldToChange == payeeMobile
        {
            characterCountLimit = 10
        }
        else if textFieldToChange == payeeMMID
        {
            characterCountLimit = 7
        }
        else {
            characterCountLimit = 50
        }
        
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
        
        
    }
    func textFieldDidBeginEditing(textField: UITextField) {
       
        if(textField == Remark) {
            animateViewMoving(true, moveValue: 135)
        }
        
    }
    func textFieldDidEndEditing(textField: UITextField) {
        
        if(textField == Remark) {
            animateViewMoving(false, moveValue: 135)
        }
        
    }
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
        
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
