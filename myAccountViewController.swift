
//
//  myAccountViewController.swift
//  Purz
//
//  Created by Vertace on 04/02/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import UIKit

class myAccountViewController: UIViewController, UITabBarControllerDelegate {
    @IBOutlet weak var backBtn: UIButton!
    
        @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var badgebtn: UIButton!
    @IBOutlet weak var profileimg: UIImageView!
    var image = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.databaseForGettingProfileImage()
        
        profileimg.clipsToBounds = true
        profileimg.layer.borderWidth = 3
        profileimg.layer.borderColor = UIColor(red:230/255.0, green:175/255.0, blue:7/255.0, alpha:1.0).CGColor;                          
        print(self.navigationController?.viewControllers)
        //        if let topController = UIApplication.topViewController()
        //        {
        //            print(topController)
        //            print(topController.presentedViewController)
        //
        //        }
        

        
        // Do any additional setup after loading the view, typically from a nib.
               namelbl.text = Appconstant.customername
        
tabBarController?.delegate = self
        navigationController?.navigationBarHidden = true
self.navigationController?.navigationBar.hidden = true
        badgebtn.layer.cornerRadius = self.badgebtn.frame.size.height/2
        //     profilebtn.layer.cornerRadius = self.profilebtn.frame.size.height/2
        profileimg.layer.cornerRadius = self.profileimg.frame.size.height/2
        if(Appconstant.notificationcount > 0){
            badgebtn.hidden = false
            badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
            badgebtn.userInteractionEnabled = false
        }
        else{
            badgebtn.hidden = true
        }
 
        
    }
    @IBAction func NotificationBtnAction(sender: AnyObject) {
     }
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    
    @IBAction func logoutAction(sender: AnyObject) {
        //        let attributedString = NSAttributedString(string: "Are you sure want to logout?", attributes: [
        //            NSFontAttributeName : UIFont.systemFontOfSize(15),                    //your font here,
        //            NSForegroundColorAttributeName : UIColor.redColor()
        //            ])
        let alert = UIAlertController(title: "Are you sure you want to logout?", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        //        alert.setValue(attributedString, forKey: "attributedTitle")
        //        alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { alertAction in
        //
        //        }))
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: { alertAction in
            Appconstant.notificationcount = 0
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(Appconstant.notificationcount, forKey: "purzbadgecount")
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            
            if purzDB.open() {
                Appconstant.mainbalance = ""
                let deleteSQL =  "DELETE FROM PROFILE_INFO"
                print(Appconstant.customerid)
                
                let deleteSQL2 =  "DELETE FROM CUSTOMERDETAIL"
                let deleteSQL3 =  "DELETE FROM IMAGETABLE"
                let delete3 = "DELETE FROM TRANSACTIONS"
                let delete4 = "DELETE FROM NOTIFICATION"
                let delete5 = "DELETE FROM ACCOUNTS"
                let delete6 = "DELETE FROM CVV_TABLE"
                
                let result = purzDB.executeUpdate(deleteSQL,
                    withArgumentsInArray: nil)
                let result1 = purzDB.executeUpdate(deleteSQL3,
                    withArgumentsInArray: nil)
                
                let result2 = purzDB.executeUpdate(deleteSQL2,
                    withArgumentsInArray: nil)
                let result3 = purzDB.executeUpdate(delete3,
                    withArgumentsInArray: nil)
                let result4 = purzDB.executeUpdate(delete4,
                    withArgumentsInArray: nil)
                let result5 = purzDB.executeUpdate(delete5,
                    withArgumentsInArray: nil)
                let result6 = purzDB.executeUpdate(delete6,
                    withArgumentsInArray: nil)
                
                if !result && !result1 && !result2 && !result3 && !result4  && !result5  && !result6{
                    //   status.text = "Failed to add contact"
                    print("Error: \(purzDB.lastErrorMessage())")
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        Appconstant.customerid = ""
                        Appconstant.mobileno = ""
                        Appconstant.pwd = ""
                        Appconstant.firstname = ""
                       Appconstant.lastname = ""
                        Appconstant.email = ""
                        Appconstant.otp = ""
                        Appconstant.customername = ""
                        
                        Appconstant.dummycustomer = false
                        Appconstant.newcustomer = false
                        Appconstant.mainbalance = ""
                        Appconstant.notificationcount = 0
                        Appconstant.gcmid = ""
                        Appconstant.Url = ""
                        Appconstant.unreadcount = 0
                        AccountConstant.initiateSDK = false
                        self.performSegueWithIdentifier("myaccount_initial", sender: self)
                    }
                }
            }
            purzDB.close()
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { alertAction in
            
        }))
        
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
        func databaseForGettingProfileImage()
    {
        profileimg.clipsToBounds = true
        profileimg.layer.borderWidth = 3
        profileimg.layer.borderColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let payableDB = FMDatabase(path: databasePath as String)
        if payableDB.open() {
            let querySQL = "SELECT * FROM IMAGETABLE WHERE CUSTOMER_ID=" + Appconstant.customerid
            
            let results:FMResultSet! = payableDB.executeQuery(querySQL,
                                                              withArgumentsInArray: nil)
           
            if(results.next()) {
                
              self.image.append(results.stringForColumn("IMAGE_PATH"))
                
                
                let item3 = results.stringForColumn("IMAGE_PATH")
//                print("Image Null==>>")
//                print(item3)
                
                if item3 == "empty" || item3.isEmpty
                {
//                    print(results.stringForColumn("IMAGE_PATH"))
                    
                }
                
                else if item3 == ""
                {
                    //                    print(results.stringForColumn("IMAGE_PATH"))
                    
                }
                    
                    
                else
                {
                    let dataDecoded:NSData = NSData(base64EncodedString: results.stringForColumn("IMAGE_PATH") , options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
                    print(dataDecoded.length)
                    if dataDecoded.length > 10{
                        let img = UIImage(data: dataDecoded)!
                        profileimg.layer.cornerRadius = self.profileimg.frame.size.height / 2
                        profileimg.clipsToBounds = true
                        self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
                        profileimg.image = img
                        profileimg.layer.borderWidth = 3
                        profileimg.layer.borderColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
                    }
                    
                    
                }
                
                
            }
            
            /*
            if(results.next()) {
                
                self.image.append(results.stringForColumn("IMAGE_PATH"))
                
                let dataDecoded:NSData = NSData(base64EncodedString: results.stringForColumn("IMAGE_PATH") , options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
                print(dataDecoded.length)
                if dataDecoded.length > 10{
                    let img = UIImage(data: dataDecoded)!
                    profileimg.layer.cornerRadius = self.profileimg.frame.size.height / 2
                    profileimg.clipsToBounds = true
                    self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
                    profileimg.image = img
                }
                
                
            }
 */
            else
            {
                
            }
            
            print("Image_Count")
            print(image.count)
            
        }
        
        
    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2 ")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
}



