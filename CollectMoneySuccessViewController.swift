//
//  CollectMoneySuccessViewController.swift
//  purZ
//
//  Created by Vertace on 18/08/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class CollectMoneySuccessViewController: UIViewController,UITabBarDelegate {
    
    
    @IBOutlet weak var goHomeBtn: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var transactionIDlbl: UILabel!
    @IBOutlet weak var successlbl: UILabel!
    @IBOutlet weak var descriptionlbl: UILabel!
    
    var transactionID = ""
    var status = ""
    var descriptiontxt = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        transactionIDlbl.text = "Transaction ID: " + transactionID
        successlbl.text = status
        descriptionlbl.text = descriptiontxt
        
        goHomeBtn.layer.cornerRadius = 9
        goHomeBtn.layer.borderColor =  UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        goHomeBtn.layer.borderWidth = 2
        
        if status == "Success"{
            imageView.image = UIImage(named:"Collect_Success.png")
        }
        else {
            imageView.image = UIImage(named:"Cancelled.png")
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        
    }
}
