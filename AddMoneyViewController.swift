//
//  AddMoneyViewController.swift
//  Cippy
//
//  Created by apple on 17/12/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit

class AddMoneyViewController: UIViewController, UITextFieldDelegate, UITabBarControllerDelegate {
    
    @IBOutlet weak var badgebtn: UIButton!
    
    @IBOutlet weak var amounttxtfield: UITextField!
    @IBOutlet weak var typedispalyTextfield: UILabel!
    @IBOutlet weak var symbollbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstbtn: UIButton!
    @IBOutlet weak var secondbtn: UIButton!
    @IBOutlet weak var thirdbtn: UIButton!
    @IBOutlet weak var completepaymentbtn: UIButton!
    @IBOutlet weak var upibtn: UIButton!
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var characterCountLimit = 10
    var radioselected = ""
    var pgtype = ""
    var vpa = [String]()
    var VPAresponse = [String]()
    var alertView = UIAlertView()
    
    //    func dismissKeyboard()
    //    {
    //        self.view.endEditing(true)
    //    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        Appconstant.SideMenu = 0;
        tabBarController?.delegate = self
        typedispalyTextfield.text="Type Amount or Select\nQuick Amount Options"
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, amounttxtfield.frame.height - 1 , amounttxtfield.frame.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;        amounttxtfield.borderStyle = UITextBorderStyle.None
        amounttxtfield.layer.addSublayer(bottomLine)
        symbollbl.text = "\u{20B9}"
        initialize()
        getVPA()

        //        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        //        self.view.addGestureRecognizer(tap)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
            }
    
    
    func initialize(){
        
        if(Appconstant.notificationcount > 0){
            badgebtn.hidden = false
            badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
            badgebtn.userInteractionEnabled = false
        }
        else{
            badgebtn.hidden = true
        }
        badgebtn.layer.cornerRadius  = self.badgebtn.frame.size.height/2
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        scrollView.addGestureRecognizer(tap)
        firstbtn.setTitle("\u{20B9}"+"500" , forState: .Normal)
        secondbtn.setTitle("\u{20B9}"+"2000" , forState: .Normal)
        thirdbtn.setTitle("\u{20B9}"+"5000" , forState: .Normal)
        firstbtn.layer.cornerRadius = self.firstbtn.frame.size.height/2
        secondbtn.layer.cornerRadius = self.secondbtn.frame.size.height/2
        thirdbtn.layer.cornerRadius = self.thirdbtn.frame.size.height/2
        firstbtn.layer.borderWidth = 1
        secondbtn.layer.borderWidth = 1
        thirdbtn.layer.borderWidth = 1
        firstbtn.layer.borderColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1).CGColor;
        secondbtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        thirdbtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        
        completepaymentbtn.layer.cornerRadius = 10
        upibtn.layer.cornerRadius = 10
    }
    func getVPA(){
        ApiClient().getMappedVirtualAddresses { (success, error, response) in
            if success {
                
                self.alertView.delegate = self
                self.alertView.title = "Select VPA"
                
                if response != nil {
                    
                    do{
                        // do whatever with jsonResult
                        let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(response, options: NSJSONWritingOptions.PrettyPrinted)
                        let json = JSON(data: jsonData)
                        
                        for item1 in json.arrayValue {
                            let item2 = item1["VirtualAddress"]
                            let items = item2["accounts"]
                            self.vpa.append(items["handle"].stringValue)
                            self.alertView.addButtonWithTitle(items["handle"].stringValue)
                            for item in items["CustomerAccount"].arrayValue {
                                if item["default-debit"].stringValue == "D"{
                                    AccountConstant.vpaProviderName.append(item["account-provider-name"].stringValue)
                                    self.VPAresponse.append(String(item))
                                }
                            }
                        }
                        
                        self.alertView.addButtonWithTitle("Cancel")
                        AccountConstant.vpanames = self.vpa
                        AccountConstant.vparesponses = self.VPAresponse
                        
                        
                    }
                    catch {
                        print("error")
                    }
                    
                }
                else{
                    if response != nil {
                        print(response)
                    }
                }
            }
        }
    }
    
    @IBAction func backbtnAction(sender: AnyObject) {
        self.performSegueWithIdentifier("addmoney-home", sender: self)
    }
    func dismissKeyboard(){
        self.view.endEditing(true)
        //        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 400)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func firstBtnAction(sender: AnyObject) {
        firstbtnaction()
    }
    func firstbtnaction(){
        firstbtn.layer.backgroundColor = UIColor(red: 250.0/255.0, green: 194.0/255.0, blue: 35.0/255.0, alpha: 1).CGColor
        
        firstbtn.layer.borderColor = UIColor(red: 250.0/255.0, green: 194.0/255.0, blue: 35.0/255.0, alpha: 1).CGColor
        firstbtn.titleLabel?.textColor = UIColor.blackColor()
        secondbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        secondbtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        secondbtn.titleLabel?.textColor = UIColor.blackColor()
        
        thirdbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        thirdbtn.titleLabel?.textColor = UIColor.blackColor()
        thirdbtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        amounttxtfield.text = "500"
    }
    @IBAction func secondBtnAction(sender: AnyObject) {
        secondbtnaction()
    }
    func secondbtnaction(){
        secondbtn.layer.backgroundColor = UIColor(red: 250.0/255.0, green: 194.0/255.0, blue: 35.0/255.0, alpha: 1).CGColor
        secondbtn.layer.borderColor = UIColor(red: 250.0/255.0, green: 194.0/255.0, blue: 35.0/255.0, alpha: 1).CGColor
         secondbtn.titleLabel?.textColor = UIColor.blackColor()
        firstbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        firstbtn.titleLabel?.textColor = UIColor.blackColor()
        firstbtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        thirdbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        thirdbtn.titleLabel?.textColor = UIColor.blackColor()
        thirdbtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        amounttxtfield.text = "2000"
    }
    @IBAction func thirdBtnAction(sender: AnyObject) {
        thirdbtnaction()
    }
    func thirdbtnaction(){
        thirdbtn.layer.backgroundColor = UIColor(red: 250.0/255.0, green: 194.0/255.0, blue: 35.0/255.0, alpha: 1).CGColor
        thirdbtn.layer.borderColor = UIColor(red: 250.0/255.0, green: 194.0/255.0, blue: 35.0/255.0, alpha: 1).CGColor
         thirdbtn.titleLabel?.textColor = UIColor.blackColor()
        firstbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        firstbtn.titleLabel?.textColor = UIColor.blackColor()
        firstbtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        secondbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        secondbtn.titleLabel?.textColor = UIColor.blackColor()
        secondbtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        amounttxtfield.text = "5000"
    }
    
    func clearBtnaction()
    {
        thirdbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        
        thirdbtn.layer.borderColor =  UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        
        firstbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        firstbtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        
        secondbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        secondbtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        //        amounttxtfield.text = "5000"
    }
    
    
    
    
    
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        //        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 630)
        print("String==>>")
        print(string)
        print(self.amounttxtfield.text!)
        let txtvalue = Double(self.amounttxtfield.text! + string)!
        
        
        let userEnteredString = amounttxtfield.text
        
        let newString = (userEnteredString! as NSString).stringByReplacingCharactersInRange(range, withString: string) as NSString
        
        print("newString=====")
        print(newString)
        print(newString.length)
        
        print("txtvalue==>>")
        print(txtvalue)
        print("amounttxtfield_text==>>")
        print(amounttxtfield.text!)
        
        
        
        if(newString == 500)
        {
            firstbtnaction()
        }
        else if(newString == 2000)
        {
            secondbtnaction()
        }
        else if(newString == 5000)
        {
            thirdbtnaction()
        }
        else
        {
            clearBtnaction()
            firstbtn.titleLabel?.textColor = UIColor.blackColor()
            secondbtn.titleLabel?.textColor = UIColor.blackColor()
            thirdbtn.titleLabel?.textColor = UIColor.blackColor()
            //           amounttxtfield.text = newString as String
        }
        
        
        if(txtvalue == 500 && string != ""){
            firstbtnaction()
            firstbtn.titleLabel?.textColor = UIColor.blackColor()
            secondbtn.titleLabel?.textColor = UIColor.blackColor()
            thirdbtn.titleLabel?.textColor = UIColor.blackColor()
            amounttxtfield.text = "50"
        }
        else if(string == "" && amounttxtfield.text! == "5000"){
            firstbtnaction()
            firstbtn.titleLabel?.textColor = UIColor.blackColor()
            secondbtn.titleLabel?.textColor = UIColor.blackColor()
            thirdbtn.titleLabel?.textColor = UIColor.blackColor()
            amounttxtfield.text = "5000"
        }
        else if(txtvalue == 2000 && string != ""){
            secondbtnaction()
            secondbtn.titleLabel?.textColor = UIColor.blackColor()
            firstbtn.titleLabel?.textColor = UIColor.blackColor()
            thirdbtn.titleLabel?.textColor = UIColor.blackColor()
            amounttxtfield.text = "200"
        }
        else if(string == "" && amounttxtfield.text! == "20000"){
            secondbtnaction()
            secondbtn.titleLabel?.textColor = UIColor.blackColor()
            firstbtn.titleLabel?.textColor = UIColor.blackColor()
            thirdbtn.titleLabel?.textColor = UIColor.blackColor()
            
            
            amounttxtfield.text = "20000"
        }
        else if(txtvalue == 5000 && string != ""){
            thirdbtnaction()
            thirdbtn.titleLabel?.textColor = UIColor.blackColor()
            firstbtn.titleLabel?.textColor = UIColor.blackColor()
            secondbtn.titleLabel?.textColor = UIColor.blackColor()
            amounttxtfield.text = "500"
        }
        else if(string == "" && amounttxtfield.text! == "50000"){
            thirdbtnaction()
            thirdbtn.titleLabel?.textColor = UIColor.blackColor()
            firstbtn.titleLabel?.textColor = UIColor.blackColor()
            secondbtn.titleLabel?.textColor = UIColor.blackColor()
            amounttxtfield.text = "50000"
        }
        //        else{
        //            firstbtn.layer.borderColor = UIColor(red: .0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        //            secondbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        //            thirdbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        //            firstbtn.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        //            secondbtn.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        //            thirdbtn.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        //            firstbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        //            secondbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        //            thirdbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        //        }
        if(textFieldToChange == amounttxtfield){
            characterCountLimit = 10
        }
        
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    }
    
    @IBAction func completepaymentBtnAction(sender: AnyObject) {
        print(amounttxtfield.text)
        print(Appconstant.mainbalance)
        if (amounttxtfield.text! == ""){
            self.presentViewController(Alert().alert("Oops! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if((Int(amounttxtfield.text!)) == 0){
            self.presentViewController(Alert().alert("Pick any amount upto "+"\u{20B9}"+"20,000", message: ""),animated: true,completion: nil)
        }
        else if((Double(amounttxtfield.text!)) > 20000){
            self.presentViewController(Alert().alert("That's just a bit much!20,000 is th emaximum balanceyou can have.Load money accordingly!", message: ""),animated: true,completion: nil)
        }
        else if((Double(amounttxtfield.text!)! + Double(Appconstant.mainbalance)!) > 20000.0){
            self.presentViewController(Alert().alert("That's just a bit much!20,000 is th emaximum balanceyou can have.Load money accordingly!", message: ""),animated: true,completion: nil)
        }
        else{
            self.activityIndicator.startAnimating()
            let addmoneyviewmodel = AddMoneyViewModel.init(entityId: Appconstant.customerid, amount: Float(amounttxtfield.text!)!, pgType: pgtype, bankName: radioselected)!
            let serializedjson  = JSONSerializer.toJson(addmoneyviewmodel)
            print(serializedjson)
            sendrequesttoserverBillDeskIntegration(Appconstant.BASE_URL+Appconstant.URL_BILL_DESK_INTEGRATION, values: serializedjson)
            
            
        }
    }
    func sendrequesttoserverBillDeskIntegration(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                print(error)
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                let item = json["result"]
                print(json["result"])
                print(json["result"].stringValue)
                if json["result"].isEmpty{
                    let item1 = json["exception"]
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert(item1["detailMessage"].stringValue, message: ""),animated: true,completion: nil)
                    }
                }
                else{
                    if self.radioselected == ""{
                        Appconstant.Url = item["targetUrl"].stringValue + "?msg=" + item["requestMessage"].stringValue
                    }
                    else if self.radioselected == "DCBBANK"{
                        Appconstant.Url = item["targetUrl"].stringValue + "&msg=" + item["requestMessage"].stringValue
                    }
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.performSegueWithIdentifier("web", sender: self)
                    }
                }
            }
        }
        
        task.resume()
        
    }
    
    func convertStringToDictionary(text: String) -> [NSObject:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do
            {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [NSObject:AnyObject]
            }
            catch
            {
                print(error)
            }
            catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    
    @IBAction func upiBtnAction(sender: AnyObject) {
        if (amounttxtfield.text! == ""){
            self.presentViewController(Alert().alert("Oops! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if((Int(amounttxtfield.text!)) == 0){
            self.presentViewController(Alert().alert("Pick any amount upto "+"\u{20B9}"+"20,000", message: ""),animated: true,completion: nil)
        }
        else if((Double(amounttxtfield.text!)) > 20000){
            self.presentViewController(Alert().alert("That's just a bit much!20,000 is th emaximum balanceyou can have.Load money accordingly!", message: ""),animated: true,completion: nil)
        }
        else if((Double(amounttxtfield.text!)! + Double(Appconstant.mainbalance)!) > 20000.0){
            self.presentViewController(Alert().alert("That's just a bit much!20,000 is th emaximum balanceyou can have.Load money accordingly!", message: ""),animated: true,completion: nil)
        }
        else{
            if self.vpa.count > 0 {
                self.alertView.show()
            }
            else {
                self.presentViewController(Alert().alert("No VPA created or linked. Please create/link  VPA to continue to load your money", message: ""), animated: true, completion: nil)
            }
        }
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        if self.vpa.count != buttonIndex {
            let payeeAccount = PayeeParam.init(VPA: "purzupiloading@equitas")
            let payerAccount = self.convertStringToDictionary(self.VPAresponse[buttonIndex])! as NSDictionary
            let payerVirtualAccount = VirtualAddress.init(payerAccount as! [NSObject : AnyObject], handle: self.vpa[buttonIndex])
            let amount = NSDecimalNumber(string:self.amounttxtfield.text!)
            let transaction = TransactionParameters.init(transaction: payerVirtualAccount, payee: payeeAccount, amount: amount, remark: "NA")
            print(payeeAccount.getVpa())
            print(payerAccount)
            print(self.vpa[buttonIndex])
            print("handle:\(payerVirtualAccount.getHandle())")
            print("ParameterMode \(transaction.amount)")
            self.activityIndicator.startAnimating()
            self.upibtn.userInteractionEnabled = false
            ApiClient().pay(transaction!, view: self, withCompletion: { (success, error, response) in
                
                if success
                {
                    print(response)
                    let response_success = response.valueForKey("success")
                    if String(response_success!) == "1" {
                        let alertController = UIAlertController(title: "SUCCESS",
                            message: String(response.valueForKey("message")!) ,
                            preferredStyle: .Alert)
                        
                        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                            dispatch_async(dispatch_get_main_queue()) {
                                self!.performSegueWithIdentifier("addmoney-home", sender: self)
                            }
                            
                            })
                        
                        alertController.addAction(action)
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                    else {
                        let alertController = UIAlertController(title: "FAILED",
                            message: String(response.valueForKey("message")!) ,
                            preferredStyle: .Alert)
                        
                        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                            dispatch_async(dispatch_get_main_queue()) {
                                self!.performSegueWithIdentifier("addmoney-home", sender: self)
                            }
                            
                            })
                        
                        alertController.addAction(action)
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                }
                else
                {
                    print(response)
//                    if response != nil
//                    {
//                        self.status = "Failed"
//                        self.descriptiontxt = String(response.valueForKey("message")!)
//                        self.transactionid = String(response.valueForKey("BankRRN")!)
//                        self.performSegueWithIdentifier("sendmoneyVPA_to_success", sender: self)
//                    }
                }
                self.activityIndicator.stopAnimating()
                self.upibtn.userInteractionEnabled = true
            })

        }
    }
    
    
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        
        if tabBarIndex == 0 {
            print("Selected item 0")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("HomeView")
            
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        
    //        else if tabBarIndex == 2{
//            Appconstant.fromlogout = false
//            let alert = UIAlertController(title: "Are you sure want to logout?", message: "", preferredStyle: UIAlertControllerStyle.Alert)
//            alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: { alertAction in
//                exit (0)
//            }))
//            
//            alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { alertAction in
//            }))
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }
    }
}
