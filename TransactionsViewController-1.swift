//
//  TransactionViewController.swift
//  Cippy
//
//  Created by apple on 30/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//
import UIKit
import MessageUI
class TransactionsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITabBarControllerDelegate, MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var badgebtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var transactionlbl: UILabel!
    
    var transactionamt = [String]()
    var balanceamt = [String]()
    var trans_date = [String]()
    var trans_type = [String]()
    var type = [String]()
    var txref = [String]()
    var ben_name = [String]()
    var ben_id = [String]()
    var descriptions = [String]()
    var otherpartyname = [String]()
    var otherpartyid = [String]()
    var txnorigin = [String]()
    var transid = [String]()
    var trans_status = [String]()
    var selected = [Bool]()
    var refresh = true
    var transactionsItem = [TransactionModel]()
    var FilteredItems = [TransactionModel]()
    var cellheight = [Bool]()
    var mobilenumber = [String]()
    var status = [String]()
    // var fromsplitbill = false
    var tx_ref = ""
    var wallet = [String]()
    var benname = ""
    var time = ""
    //  var splitamt = ""
    var refreshControl = UIRefreshControl()
    var fromhome = false
    var selectedindex = 0
    var pagesize = 6
    var pageno = 1
    var additionalcount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        getMobileNumberForTransaction(Appconstant.BASE_URL+Appconstant.MOBILENUMBER_FOR_TRANSACTIONHISTORY+"5445729934945732374")
        print(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=10")
        Appconstant.SideMenu = 0;
        tabBarController?.delegate = self
        transactionlbl.text = "No transactions yet!\nBut you can change that in a jiffy...\nTry out our easy Recharge & Bill payments, Or do you fancy some other features?"
        transactionlbl.tintColor = UIColor.blueColor()
        transactionlbl.numberOfLines = 5
        navigationController?.navigationBarHidden = true
        badgebtn.layer.cornerRadius = badgebtn.frame.size.height/2
        initializefunc()
        calltransactiontable()
        badgebtn.userInteractionEnabled = false
        refreshControl.addTarget(self, action: #selector(TransactionsViewController.refreshaction), forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refreshControl)
        
        self.getTentransaction(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=10")
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    
    @IBAction func email_BtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        var ref_id =  indexPath.row
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "Purz",
                                            message: "Are you sure want you to mail our support?",
                                            preferredStyle: .Alert)
        
        let action1 = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            let mailComposeViewController = self!
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.mailComposeDelegate = self
            mailComposerVC.setToRecipients(["purzwallet@equitasbank.com"])
            //        mailComposerVC.setSubject("Sending you an in-app e-mail...")
            //        """""""""""""""""""
            mailComposerVC.setMessageBody("Transaction Ref: \(self!.txref[indexPath.row])\n Date: \(self!.trans_date[indexPath.row]) \n Transaction Mode: \(self!.txnorigin[indexPath.row]) \n Transaction Type: \(self!.trans_type[indexPath.row]) \n Type(credit/Debit): \(self!.type[indexPath.row]) \n Wallet Type: GENERAL \n Merchant Name: \(self!.ben_name[indexPath.row]) \n Merchant ID: \(self!.ben_id[indexPath.row]) \n Amount: \(self!.transactionamt[indexPath.row]) \n Balance: \(Appconstant.mainbalance) \n Transaction Status: \(self!.trans_status[indexPath.row]) \n Description: ) " , isHTML: false)
            
            
            
            
            
            if MFMailComposeViewController.canSendMail() {
                self?.presentViewController(mailComposerVC, animated: true, completion: nil)
                
            } else {
                self!.showSendMailErrorAlert()
            }
            })
        let action = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            })
        
        
        alertController?.addAction(action1)
        alertController?.addAction(action)
        self.presentViewController(alertController!, animated: true, completion: nil)
        
    }
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        
        switch result {
            
        case MFMailComposeResultCancelled:
            print("Cancelled mail")
            break
        case MFMailComposeResultSent:
            print("Message sent")
            break
        default:
            break
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    func initializefunc(){
        badgebtn.layer.cornerRadius = badgebtn.frame.size.height/2
        if(Appconstant.notificationcount > 0){
            badgebtn.hidden = false
            badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
        }
        else{
            badgebtn.hidden = true
        }
        let bottomLine = CALayer()
        let topLine = CALayer()
        
        bottomLine.borderWidth = 1
        topLine.backgroundColor = UIColor.whiteColor().CGColor
        bottomLine.backgroundColor = UIColor.blackColor().CGColor
        transactionlbl.hidden = true
        titlelbl.hidden = false
    }
    func calltransactiontable(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let selectSQL = "SELECT * FROM TRANSACTIONS"
            
            let result:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                          withArgumentsInArray: nil)
            
            while(result.next()){
                transactionamt.append(result.stringForColumn("AMOUNT"))
                trans_type.append(result.stringForColumn("TRANSACTION_TYPE"))
                type.append(result.stringForColumn("TYPE"))
                trans_date.append(result.stringForColumn("TIME"))
                txref.append(result.stringForColumn("TX_REF"))
                ben_name.append(result.stringForColumn("BENEFICIARY_NAME"))
                ben_id.append(result.stringForColumn("BENEFICIARY_ID"))
                descriptions.append(result.stringForColumn("DESCRIPTION"))
                otherpartyname.append(result.stringForColumn("OTHER_PARTY_NAME"))
                otherpartyid.append(result.stringForColumn("OTHER_PARTY_ID"))
                txnorigin.append(result.stringForColumn("TXN_ORIGIN"))
                trans_status.append(result.stringForColumn("TRANSACTION_STATUS"))
                transid.append(result.stringForColumn("TRANSACTIONID"))
                self.selected.append(false)
                
                
            }
            if fromhome{
                self.selected[selectedindex] = true
            }
            for i in 0 ..< self.transactionamt.count {
                let transaction = TransactionModel(amount: self.transactionamt[i], trans_type: self.trans_type[i], type: self.type[i], time: self.trans_date[i], tx_ref: self.txref[i], beneficiaryname: self.ben_name[i], beneficiaryid: self.ben_id[i], descripition: self.descriptions[i], otherpartyname: self.otherpartyname[i], otherpartyid: self.otherpartyid[i], txnorigin: self.txnorigin[i], transactionstatus: self.trans_status[i], selected: self.selected[i], transid: self.transid[i])!
                self.transactionsItem += [transaction]
                if((self.type[i] == "DEBIT") && (Double(self.transactionamt[i]) >= 2.0)){
                    self.cellheight.append(true)
                    //              self.getTentransaction(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=10")
                    
                }
                else{
                    self.cellheight.append(false)
                }
                
            }
            self.FilteredItems = self.transactionsItem
            
        }
        purzDB.close()
        dispatch_async(dispatch_get_main_queue())
        {
            if self.transactionamt.count == 0{
                print("gfhhjh\(self.transactionamt)")
                self.tableView.hidden = true
                self.transactionlbl.hidden = false
            }
        }
    }
    
    func refreshaction(){
        if refresh{
            self.view.userInteractionEnabled = false
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            
            if purzDB.open() {
                
                let delete = "DELETE FROM TRANSACTIONS"
                let result = purzDB.executeUpdate(delete,
                                                  withArgumentsInArray: nil)
                if !result {
                    //   status.text = "Failed to add contact"
                    print("Error: \(purzDB.lastErrorMessage())")
                }
                
            }
            self.pagesize = 6
            self.pageno = 1
            print(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=10")
            self.getTentransaction(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=10")
        }
        else{
            self.refreshControl.endRefreshing()
        }
        
        
    }
    
    
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
    }
    
    
    func getTentransaction(url: String){
        
        
        self.view.userInteractionEnabled = false
        activityIndicator.startAnimating()
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("Basic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                self.activityIndicator.stopAnimating()
                self.view.userInteractionEnabled = true
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                self.refreshControl.endRefreshing()
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                self.view.userInteractionEnabled = true
                self.refreshControl.endRefreshing()
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            self.refreshControl.endRefreshing()
            self.transactionamt.removeAll()
            self.trans_date.removeAll()
            
            self.transactionamt.removeAll()
            self.trans_date.removeAll()
            self.trans_type.removeAll()
            self.type.removeAll()
            self.txref.removeAll()
            self.ben_name.removeAll()
            self.ben_id.removeAll()
            self.descriptions.removeAll()
            self.otherpartyname.removeAll()
            self.otherpartyid.removeAll()
            self.txnorigin.removeAll()
            self.trans_status.removeAll()
            self.selected.removeAll()
            self.transid.removeAll()
            self.wallet.removeAll()
            self.transactionsItem.removeAll()
            self.balanceamt.removeAll()
            for item1 in json["result"].arrayValue{
                let item = item1["transaction"]
                
                self.trans_type.append(item["transactionType"].stringValue)
                self.type.append(item["type"].stringValue)
                self.txref.append(item["txRef"].stringValue)
                self.ben_name.append(item["beneficiaryName"].stringValue)
                self
                if(!item["beneficiaryId"].stringValue.isEmpty){
                    let benid = item["beneficiaryId"].stringValue.componentsSeparatedByString("+91")
                    print(benid)
                    var i = 0
                    for(i=0; i<benid.count; i++){
                        
                    }
                    self.ben_id.append(benid[i-1])
                }
                else{
                    self.ben_id.append(item["beneficiaryId"].stringValue)
                }
                
                let matches = self.matchesForRegexInText("[0-9]", text: item["description"].stringValue)
                let desc = matches.joinWithSeparator("")
                self.descriptions.append(desc)
                self.otherpartyname.append(item["otherPartyName"].stringValue)
                self.otherpartyid.append(item["otherPartyId"].stringValue)
                self.txnorigin.append(item["txnOrigin"].stringValue)
                self.wallet.append(item["yourWallet"].stringValue)
                
                let balance_two_decimal = String(format: "%.2f", item["balance"].doubleValue)
                let amt1 = balance_two_decimal.componentsSeparatedByString(".")
                if(amt1[1].characters.count == 1){
                    let finalamount = balance_two_decimal + "0"
                    self.balanceamt.append(finalamount)
                }
                else{
                    self.balanceamt.append(balance_two_decimal)
                }
                self.trans_status.append(item["transactionStatus"].stringValue)
                self.transid.append(item["externalTransactionId"].stringValue)
                self.selected.append(false)
                
                let amt_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                let amt = amt_two_decimal.componentsSeparatedByString(".")
                if(amt[1].characters.count == 1){
                    let finalamount = amt_two_decimal + "0"
                    self.transactionamt.append(finalamount)
                }
                else{
                    self.transactionamt.append(amt_two_decimal)
                }
                
                let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                //self.transactiontype.append(item["type"].stringValue)
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd MMM"
                dateFormatter.timeZone = NSTimeZone(name: "UTC")
                let dateString = dateFormatter.stringFromDate(date)
                if(!dateString.isEmpty){
                    let datearray = dateString.componentsSeparatedByString(" ")
                    if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                        let correctdate = datearray[0] + "st " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                        let correctdate = datearray[0] + "nd " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                        let correctdate = datearray[0] + "rd " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    else{
                        let correctdate = datearray[0] + "th " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    
                    //                let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                    //
                    //                let dateFormatter = NSDateFormatter()
                    //                dateFormatter.dateFormat = "dd MMM"
                    //                dateFormatter.timeZone = NSTimeZone(name: "UTC")
                    //                let dateString = dateFormatter.stringFromDate(date)
                    //                if(!dateString.isEmpty){
                    //                    let datearray = dateString.componentsSeparatedByString(" ")
                    //                    if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                    //                        let correctdate = datearray[0] + "st " + datearray[1]
                    //                        self.trans_date.append(correctdate)
                    //                    }
                    //                    else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                    //                        let correctdate = datearray[0] + "nd " + datearray[1]
                    //                        self.trans_date.append(correctdate)
                    //                    }
                    //                    else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                    //                        let correctdate = datearray[0] + "rd " + datearray[1]
                    //                        self.trans_date.append(correctdate)
                    //                    }
                    //                    else{
                    //                        let correctdate = datearray[0] + "th " + datearray[1]
                    //                        self.trans_date.append(correctdate)
                    //                    }
                    //
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                
            }
            for(var i = 0; i<self.transactionamt.count; i++){
                let transaction = TransactionModel(amount: self.transactionamt[i], trans_type: self.trans_type[i], type: self.type[i], time: self.trans_date[i], tx_ref: self.txref[i], beneficiaryname: self.ben_name[i], beneficiaryid: self.ben_id[i], descripition: self.descriptions[i], otherpartyname: self.otherpartyname[i], otherpartyid: self.otherpartyid[i], txnorigin: self.txnorigin[i], transactionstatus: self.trans_status[i], selected: self.selected[i], transid: self.transid[i])!
                self.transactionsItem += [transaction]
                if((self.type[i] == "DEBIT") && (Double(self.transactionamt[i]) >= 2.0)){
                    self.cellheight.append(true)
                }
                else{
                    self.cellheight.append(false)
                }
                
                DBHelper().purzDB()
                let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
                let databasePath = databaseURL.absoluteString
                let purzDB = FMDatabase(path: databasePath as String)
                if purzDB.open() {
                    
                    let insert = "INSERT INTO TRANSACTIONS (AMOUNT,BENEFICIARY_ID,TRANSACTION_TYPE,TYPE,TIME,TRANSACTION_STATUS,TX_REF,BENEFICIARY_NAME,DESCRIPTION,OTHER_PARTY_NAME,OTHER_PARTY_ID,TXN_ORIGIN,TRANSACTIONID) VALUES"
                    let value0 =  "('"+self.transactionamt[i]+"','\(self.ben_id[i])','\(self.trans_type[i])','\(self.type[i])',"
                    let value1 = "'"+self.trans_date[i]+"','\(self.trans_status[i])','\(self.txref[i])','\(self.ben_name[i])',"
                    let value2 = "'\(self.descriptions[i])','\(self.otherpartyname[i])','\(self.otherpartyid[i])','\(self.txnorigin[i])','\(self.transid[i])')"
                    let insertsql = insert+value0+value1+value2
                    let result = purzDB.executeUpdate(insertsql,
                                                      withArgumentsInArray: nil)
                    
                    if !result {
                        
                        print("Error: \(purzDB.lastErrorMessage())")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                    
                    purzDB.close()
                }
                
            }
            dispatch_async(dispatch_get_main_queue()) {
                if self.refresh{
                    self.refresh = false
                    self.presentViewController(Alert().alert("Yey! Transaction list is here, have a look.", message: ""),animated: true,completion: nil)
                    
                }
            }
            self.FilteredItems.removeAll()
            self.FilteredItems = self.transactionsItem
            dispatch_async(dispatch_get_main_queue()) {
                if(self.FilteredItems.count == 0){
                    self.transactionlbl.hidden = false
                    self.tableView.hidden = true
                }
                self.view.userInteractionEnabled = true
                self.tableView.reloadData()
            }
        }
        
        task.resume()
    }
    
    func matchesForRegexInText(regex: String, text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                                                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TransactionCell", forIndexPath: indexPath) as UITableViewCell!
        
        
        let symbolimg = cell.viewWithTag(1) as! UIImageView
        
        let trans_namelbl = cell.viewWithTag(11) as! UILabel
        let trans_namelbl2 = cell.viewWithTag(41) as! UILabel
        
        let amountlbl = cell.viewWithTag(13) as! UILabel
        let amountlbl2 = cell.viewWithTag(42) as! UILabel
        
        let time1lbl = cell.viewWithTag(12) as! UILabel
        let time2lbl = cell.viewWithTag(21) as! UILabel
        let mobilelbl = cell.viewWithTag(43) as! UILabel
        let statuslbl = cell.viewWithTag(22) as! UILabel
        
        let image1 = cell.viewWithTag(31)
        let image2 = cell.viewWithTag(32)
        let image3 = cell.viewWithTag(33)
        let image4 = cell.viewWithTag(34)
        
        
        
        //  let descriptionlbl = cell.viewWithTag(22) as! UILabel
        // let splitbillbtn = cell.viewWithTag(9) as! UIButton
        let share = cell.viewWithTag(3) as! UIButton
        let mail = cell.viewWithTag(4) as! UIButton
        // splitbillbtn.layer.cornerRadius = 7
        
        share.hidden = true
        mail.hidden = true
        trans_namelbl2.hidden = true
        amountlbl2.hidden = true
        mobilelbl.hidden = true
        image1!.hidden = true
        image2!.hidden = true
        image3!.hidden = true
        image4!.hidden = true
        
        let transaction = FilteredItems[indexPath.row]
        
        // time1lbl.text = transaction.time
        // time2lbl.text = transaction.time
        amountlbl.text = "₹ " + transaction.amount
        amountlbl2.text = "₹ " + transaction.amount
        mobilelbl.text = transaction.tx_ref
        
        let font:UIFont? = UIFont(name: "Helvetica", size:14)
        let fontSuper:UIFont? = UIFont(name: "Helvetica", size:10)
        let attString:NSMutableAttributedString = NSMutableAttributedString(string: self.trans_date[indexPath.row], attributes: [NSFontAttributeName:font!])
        attString.setAttributes([NSFontAttributeName:fontSuper!,NSBaselineOffsetAttributeName:7], range: NSRange(location:2,length:2))
        
        time1lbl.attributedText = attString;
        time2lbl.attributedText = attString;
        print("cell indexPath:\(indexPath.row)\nPageSize:\(pagesize)")
        print("indexPathe: \(indexPath.row)")
        print("trans_type: \(self.trans_type)")
        print("trans_id: \(self.transid)")
//        if mobilenumber.count < indexPath.row+1 {
//            if self.trans_type[indexPath.row] == "TPP" && self.transid[indexPath.row] != ""{
//            getMobileNumberForTransaction(Appconstant.BASE_URL+Appconstant.MOBILENUMBER_FOR_TRANSACTIONHISTORY+self.transid[indexPath.row])
//            }
//            else {
//                status.append("")
//                mobilenumber.append("")
//            }
//        }
        
//        if status.count > indexPath.row {
//            statuslbl.text = status[indexPath.row]
//        }
        
        
        if FilteredItems[indexPath.row].selected{
            
            if(transaction.type == "DEBIT") {
                trans_namelbl.text = "To " + transaction.beneficiaryname
                
                trans_namelbl2.text = "To " + transaction.beneficiaryname
                symbolimg.image = UIImage(named: "Out.png")
                if mobilenumber[indexPath.row] != "" {
                    trans_namelbl.text = "To " + mobilenumber[indexPath.row]
                    trans_namelbl2.text = "To " + mobilenumber[indexPath.row]
                }
            }
            else if(transaction.type == "CREDIT") {
                trans_namelbl.text = "From " + transaction.beneficiaryname
                trans_namelbl2.text = "From " + transaction.beneficiaryname
                symbolimg.image = UIImage(named: "In.png")
                
                
            }
            
            
            trans_namelbl.hidden = true
            time1lbl.hidden = true
            amountlbl.hidden = true
            time2lbl.hidden = false
            trans_namelbl2.hidden = false
            amountlbl2.hidden = false
            
            mobilelbl.hidden = false
            image1!.hidden = false
            image2!.hidden = false
            image3!.hidden = false
            image4!.hidden = false
            
            share.hidden = false
            mail.hidden = false
      
        }
        else{
            
            if(transaction.type == "DEBIT") {
                trans_namelbl.text = "To " + transaction.beneficiaryname
                
                trans_namelbl2.text = "To " + transaction.beneficiaryname
                if mobilenumber[indexPath.row] != "" {
                    trans_namelbl.text = "To " + mobilenumber[indexPath.row]
                    trans_namelbl2.text = "To " + mobilenumber[indexPath.row]
                }
                
                symbolimg.image = UIImage(named: "Out.png")
                
            }
            else if(transaction.type == "CREDIT") {
                trans_namelbl.text = "From " + transaction.beneficiaryname
                trans_namelbl2.text = "From " + transaction.beneficiaryname
                symbolimg.image = UIImage(named: "In.png")
                
            }
            
            time2lbl.hidden = true
            trans_namelbl.hidden = false
            time1lbl.hidden = false
            amountlbl.hidden = false
            share.hidden = true
            mail.hidden = true
        }
        if pagesize == indexPath.row{
            pagesize = pagesize + 10
            pageno = pageno + 1
            getAdditionaltransaction(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNumber=\(pageno)&pageSize=10")
        }
        
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGrayColor().CGColor
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("filter item count: \(FilteredItems.count)")
        return FilteredItems.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let selectionColor = UIView() as UIView
        selectionColor.layer.borderWidth = 1
        selectionColor.layer.borderColor = UIColor.clearColor().CGColor
        selectionColor.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = selectionColor
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(type[indexPath.row])
        //        if fromsplitbill{
        //            tx_ref = self.txref[indexPath.row]
        //            benname = self.ben_name[indexPath.row]
        //            time = trans_date[indexPath.row]
        //            splitamt = transactionamt[indexPath.row]
        //            if(Double(splitamt)! < 2){
        //                dispatch_async(dispatch_get_main_queue()) {
        //                    self.presentViewController(Alert().alert("Please select value more than ₹2", message: ""),animated: true,completion: nil)
        //
        //                }
        //            }
        //
        //            else if(type[indexPath.row] == "CREDIT"){
        //                dispatch_async(dispatch_get_main_queue()) {
        //                    self.presentViewController(Alert().alert("Please select transaction paid from your wallet. Or simply type the amount you want to split!", message: ""),animated: true,completion: nil)
        //
        //                }
        //            }
        //            else{
        //                self.activityIndicator.startAnimating()
        //               // self.sendrequesttoserverToCheckSplitBill(Appconstant.BASE_URL+Appconstant.URL_SPLIT_TRANSACTION+tx_ref)
        //            }
        //        }
        
        if FilteredItems[indexPath.row].selected {
            FilteredItems[indexPath.row].selected = false
        }
        else{
            FilteredItems[indexPath.row].selected = true
        }
        self.view.endEditing(true)
        self.tableView.reloadRowsAtIndexPaths([tableView.indexPathForSelectedRow!], withRowAnimation: .Fade)
        print(indexPath.row)
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        //        let cell = tableView.cellForRowAtIndexPath(indexPath)
        print(selected)
        print(indexPath.row)
        print(FilteredItems.count)
        if FilteredItems[indexPath.row].selected{
            
            if cellheight[indexPath.row]{
                return 143
            }
            else{
                return 143
            }
        }
        else{
            return 70
        }
        return 70
    }
    
    func getMobileNumberForTransaction(url: String){
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {            // check for fundamental networking error
                
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                
                return
                
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            
            let item = json["result"]
            print(item)
            self.mobilenumber.append(item["MobileNo"].stringValue)
            if item["Status"].stringValue.containsString("|") {
            let statusInt = item["Status"].stringValue.componentsSeparatedByString("|")
          
            if Int(statusInt[0]) == 0 {
                self.status.append("Completed")
            }
            else if statusInt.count == 2{
                self.status.append(statusInt[1])
            }
            else {
                self.status.append("")
            }
            }
            else {
                self.status.append(item["Status"].stringValue)
            }
            print(self.mobilenumber)
            print(self.status)
            
            
        }
        
        task.resume()
    }
    
    
    func getAdditionaltransaction(url: String){
        
        self.view.userInteractionEnabled = false
        activityIndicator.startAnimating()
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {            // check for fundamental networking error
                dispatch_async(dispatch_get_main_queue()) {
                    self.view.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
                }
                
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                self.refreshControl.endRefreshing()
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                
                self.refreshControl.endRefreshing()
                dispatch_async(dispatch_get_main_queue()) {
                    self.view.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
                }
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            self.additionalcount = 0
            
            for item1 in json["result"].arrayValue{
                let item = item1["transaction"]
                print(item)
                self.additionalcount = self.additionalcount + 1
                self.trans_type.append(item["transactionType"].stringValue)
                self.type.append(item["type"].stringValue)
                self.txref.append(item["txRef"].stringValue)
                self.ben_name.append(item["beneficiaryName"].stringValue)
                if(!item["beneficiaryId"].stringValue.isEmpty){
                    let benid = item["beneficiaryId"].stringValue.componentsSeparatedByString("+91")
                    
                    var i = 0
                    for(i=0; i<benid.count; i++){
                        
                    }
                    self.ben_id.append(benid[i-1])
                }
                else{
                    self.ben_id.append(item["beneficiaryId"].stringValue)
                }
                
                let matches = self.matchesForRegexInText("[0-9]", text: item["description"].stringValue)
                let desc = matches.joinWithSeparator("")
                self.descriptions.append(desc)
                self.otherpartyname.append(item["otherPartyName"].stringValue)
                self.otherpartyid.append(item["otherPartyId"].stringValue)
                self.txnorigin.append(item["txnOrigin"].stringValue)
                self.trans_status.append(item["transactionStatus"].stringValue)
                self.transid.append(item["externalTransactionId"].stringValue)
                self.selected.append(false)
                print("amt\(item["amount"].doubleValue)")
                let balance_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                let amt = balance_two_decimal.componentsSeparatedByString(".")
                if(amt[1].characters.count == 1){
                    let finalamount = balance_two_decimal + "0"
                    self.transactionamt.append(finalamount)
                }
                else{
                    self.transactionamt.append(balance_two_decimal)
                }
                
                
                let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd MMM"
                dateFormatter.timeZone = NSTimeZone(name: "UTC")
                let dateString = dateFormatter.stringFromDate(date)
                if(!dateString.isEmpty){
                    let datearray = dateString.componentsSeparatedByString(" ")
                    if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                        let correctdate = datearray[0] + "st " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                        let correctdate = datearray[0] + "nd " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                        let correctdate = datearray[0] + "rd " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    else{
                        let correctdate = datearray[0] + "th " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                
            }
            //            self.transactionsItem.removeAll()
            
            let indexcount = self.txref.count - self.additionalcount
            
            for(var i = indexcount; i<indexcount+self.additionalcount; i++){
                let transaction = TransactionModel(amount: self.transactionamt[i], trans_type: self.trans_type[i], type: self.type[i], time: self.trans_date[i], tx_ref: self.txref[i], beneficiaryname: self.ben_name[i], beneficiaryid: self.ben_id[i], descripition: self.descriptions[i], otherpartyname: self.otherpartyname[i], otherpartyid: self.otherpartyid[i], txnorigin: self.txnorigin[i], transactionstatus: self.trans_status[i], selected: self.selected[i], transid: self.transid[i])!
                self.transactionsItem += [transaction]
                self.FilteredItems += [transaction]
                if((self.type[i] == "DEBIT") && (Double(self.transactionamt[i]) >= 2.0)){
                    self.cellheight.append(true)
                }
                else{
                    self.cellheight.append(false)
                }
                
                
            }
            
            
            dispatch_async(dispatch_get_main_queue()) {
                self.view.userInteractionEnabled = true
                self.tableView.reloadData()
            }
        }
        
        task.resume()
    }
    @IBAction func shareBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        
        var msg = "Message"
        //        let msg = "Share your message"
        
        if(self.trans_type[indexPath.row] == "TPP") {
            msg = "On " + self.trans_date[indexPath.row] + ", " + self.ben_name[indexPath.row] + " has Recharged ₹ " + self.transactionamt[indexPath.row] + " on " + self.descriptions[indexPath.row] + " account"
        }
        else if((self.trans_type[indexPath.row] == "C2C") && (self.type[indexPath.row] == "DEBIT")) {
            msg = "On " + self.trans_date[indexPath.row] + ", " + self.ben_name[indexPath.row] + " has Sent ₹ " + self.transactionamt[indexPath.row] + " to " + self.ben_id[indexPath.row]
        }
        else if ((self.trans_type[indexPath.row] == "C2C") && (self.type[indexPath.row] == "CREDIT")) {
            msg = "On " + self.trans_date[indexPath.row] + ", " + self.ben_name[indexPath.row] + " has Received ₹ " + self.transactionamt[indexPath.row] + " from " + self.ben_id[indexPath.row]
        }
        else if ((self.trans_type[indexPath.row] == "C2M") && (self.type[indexPath.row] == "DEBIT")) {
            msg = "On " + self.trans_date[indexPath.row] + ", " + self.ben_name[indexPath.row] + " has Paid ₹ " + self.transactionamt[indexPath.row] + " to " + self.ben_name[indexPath.row] + ",reference transaction no is " + self.txref[indexPath.row]
        }
        else if ((self.trans_type[indexPath.row] == "PG") || (self.trans_type[indexPath.row] == "M2C") && (self.type[indexPath.row] == "CREDIT")) {
            msg = "On " + self.trans_date[indexPath.row] + ", " + self.ben_name[indexPath.row] + " has Loaded ₹ " + self.transactionamt[indexPath.row] + " to Purz,Reference transaction no is " + self.txref[indexPath.row]
        }
        
        let objectsToShare = [msg]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        activityVC.excludedActivityTypes = [ UIActivityTypePostToFacebook, UIActivityTypePostToTwitter, UIActivityTypePostToWeibo, UIActivityTypePrint, UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr, UIActivityTypePostToVimeo,UIActivityTypePostToTencentWeibo,UIActivityTypeAirDrop, UIActivityTypeOpenInIBooks, UIActivityTypeAddToReadingList]
        
        if let urlString = msg.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.sharedApplication().canOpenURL(whatsappURL) {
                    UIApplication.sharedApplication().openURL(whatsappURL)
                } else {
                    // Cannot open whatsapp
                }
            }
        }
        activityVC.popoverPresentationController?.sourceView = sender as? UIView
        self.presentViewController(activityVC, animated: true, completion: nil)
        
    }
    
    //    @IBAction func billsplitBtnAction(sender: AnyObject) {
    //
    //        let point = sender.convertPoint(CGPointZero, toView: tableView)
    //        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
    //        tx_ref = self.txref[indexPath.row]
    //        benname = self.ben_name[indexPath.row]
    //        time = trans_date[indexPath.row]
    //        splitamt = transactionamt[indexPath.row]
    //       self.activityIndicator.startAnimating()
    //        self.sendrequesttoserverToCheckSplitBill(Appconstant.BASE_URL+Appconstant.URL_SPLIT_TRANSACTION+tx_ref)
    //
    //    }
    func sendrequesttoserverToCheckSplitBill(url : String)
    {
        self.view.userInteractionEnabled = false
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        print(url)
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {
                self.view.userInteractionEnabled = true
                self.activityIndicator.stopAnimating()// check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                self.view.userInteractionEnabled = true
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                //                    dispatch_async(dispatch_get_main_queue()) {
                //                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                //                    }
            }
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            self.view.userInteractionEnabled = true
            dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
            }
            let item = json["exception"]
            if item["detailMessage"].stringValue == "No Split Found"{
                self.performSegueWithIdentifier("trans_splitbill", sender: self)
            }
            else{
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Looks like this bill is already split! Please Check again.", message: ""),animated: true,completion: nil)
                    
                }
            }
        }
        
        task.resume()
        
    }
    
    
    @IBAction func backBtnAction(sender: AnyObject) {
        //        if fromsplitbill{
        //            self.performSegueWithIdentifier("goto_splitbill", sender: self)
        //        }
        //        else{
        self.performSegueWithIdentifier("back_trans_home", sender: self)
        
    }
    
    @IBAction func complaintmailBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        
        let alert = UIAlertController(title: "Are you sure want to report on this transactional issue?", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: { alertAction in
            
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.Default, handler: { alertAction in
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
        
    }
    
    
    
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("back_trans_home", sender: self)
            }
            
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
            //        else if tabBarIndex == 2{
            //            Appconstant.fromlogout = false
            //            let alert = UIAlertController(title: "Are you sure want to logout?", message: "", preferredStyle: UIAlertControllerStyle.Alert)
            //            alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: { alertAction in
            //                exit (0)
            //            }))
            //            
            //            alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { alertAction in
            //            }))
            //            self.presentViewController(alert, animated: true, completion: nil)
            //            
            //        }
        else if tabBarIndex == 2{
            // Appconstant.fromlogout = false
            print("Selected item 2")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
}
