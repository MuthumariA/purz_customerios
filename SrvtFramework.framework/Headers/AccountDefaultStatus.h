//
//  AccountDefaultStatus.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountDefaultStatus : NSObject <NSCoding>{
    BOOL defaultCredit;
    BOOL defaultDebit;
}

/**
 * Default Credit property.
 */
@property (nonatomic) BOOL defaultCredit;

/**
 * Default Debit property.
 */
@property (nonatomic) BOOL defaultDebit;

/**
 *  Initialise with Default Credit.
 */
-(id)initWithDefaultCredit : (BOOL)default_Credit;

/**
 *  Initialise with Default Debit.
 */
-(id)initWithDefaultDebit : (BOOL)default_Debit;

/**
 *  Initialise with Default Debit , Default Credit.
 */
-(id)initWithDefaultDebitCredit : (BOOL)default_Debit default_Credit:(BOOL)default_Credit;

/**
 * Get Default Debit.
 */
-(BOOL) getDefaultDebit;

/**
 * Get Default Credit.
 */
-(BOOL) getDefaultCredit;


@end
