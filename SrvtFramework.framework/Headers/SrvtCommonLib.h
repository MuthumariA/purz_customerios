//
//  SrvtCommonLib.h
//  SrvtFramework
//
//  Created by Charushila on 06/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//  This file contains call to common library.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CommonLibrary/CLServices.h>

@interface SrvtCommonLib : NSObject

/**
 *  Returns challange if it is a valid string .
 */
- (NSString*)getChallenge:(NSString *)type device_Id:(NSString*)device_Id app_Id:(NSString*)app_Id ;

/**
 *    Returns true if hmac is valid with respect of appid, mobile, deviceid .
 */
- (BOOL)registerApp:(NSString *)app_Id mobile_no:(NSString*)mobile_no device_Id:(NSString*)device_Id token:(NSString*)token;

/**
 *  getCredential is called during transaction.
 */
//- (NSMutableDictionary*)getCredential:(NSString*)keyKode keyXMLPayload:(NSString*)keyXMLPayload controls:(NSDictionary*)controls configuration:(NSDictionary*)configuration salt:(NSDictionary*)salt payInfo:(NSArray*)payInfo language:(NSString*)language token:(NSString*)token view:(UIViewController *)viewcontroller;

- (NSMutableDictionary*)getCredential:(NSString*)keyKode keyXMLPayload:(NSString*)keyXMLPayload controls:(NSDictionary*)controls configuration:(NSDictionary*)configuration salt:(NSDictionary*)salt payInfo:(NSArray*)payInfo language:(NSString*)language token:(NSString*)token view:(UIViewController *)viewcontroller withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;
@end
