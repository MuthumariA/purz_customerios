//
//  CardData.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CardData : NSObject<NSCoding>{
    NSString* lastSix;
    NSString* expiryMonth;
    NSString* expiryYear;

}
@property (nonatomic) NSString* lastSix;
@property (nonatomic) NSString* expiryMonth;
@property (nonatomic) NSString* expiryYear;

/**
 * Initialise card data.
 *
 * @return CardData object.
 */
-(id)initWith :(NSString*)lastSixDigit expiry_month:(NSString*)expiry_month expiry_year:(NSString*)expiry_year;

/**
 * Get last six digits of the Card
 *
 * @return cards six digits
 */
-(NSString*) getLastSix;

/**
 * Set last six digits of the Card
 *
 * @param lastSix Last six digits of the Card (Plain text)
 */
-(void) setLastSix:(NSString*)lastSix;

/**
 * Get Expiry Month of the Card
 *
 * @return Expiry Month
 */
-(NSString*) getExpiryMonth;

/**
 * Set Expiry Month of the Card
 *
 * @param expiryMonth Expiry Month
 */
-(void) setExpiryMonth:(NSString*) expiryMonth ;

/**
 * Get Expiry Year of the Card
 *
 * @return Expiry Year
 */
-(NSString*)getExpiryYear;

/**
 * Set Expiry Year of the Card
 *
 * @param expiryYear Expiry Year
 */
-(void) setExpiryYear:(NSString*) expiryYear ;

//-(void) validate;

/**
 * Validate Card Data.
 */
-(void)validate:(CardData*)cardData;

@end
