//
//  TransactionParameters.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomerAccount.h"
#import "PayeeParam.h"
#import "PayerParam.h"
#import "VirtualAddress.h"

/*!
 @header  TransactionParameters.h
 @brief   UPI Transaction Parameters. Host application will set Transaction parameters and initiates different types of transctions.
 */

@interface TransactionParameters : NSObject{

    NSDecimalNumber* amount;
    NSString* remark;
    CustomerAccount* customerAccount;
    PayeeParam* payeeParam;
    PayerParam* payerParam;

    BOOL usingDefaultAccount;
    BOOL aadhaarEnabled;
    NSString* vpa;
    VirtualAddress* virtualAddress;

    //private AccountProvider provider;
}

typedef enum { Pay = 1, Collect, Manage, MerchantCollect} ParameterMode;
typedef enum { VPA = 1 , Account, Mobile, AADHAAR_IIN} PayeeMode;

@property (nonatomic) PayeeMode payeeMode;
@property (nonatomic) ParameterMode parameterMode;
@property (nonatomic) PayeeParam* payeeParam;
@property (nonatomic) PayerParam* payerParam;

@property (nonatomic) NSDecimalNumber* amount;
@property (nonatomic) NSString* remark;
@property (nonatomic) CustomerAccount* customerAccount;

-(BOOL)isUsingDefaultAccount;

-(int)getParameterMode;

- (id)initWithVPA:(NSString *)vpa;

- (id)initWithVirtualAddress:(VirtualAddress *)virtualAddr;

-(id)initWithTransaction:(VirtualAddress *)from_vpa
                   payee:(PayeeParam *)payee
                  amount:(NSDecimalNumber *)amountToTrans
                  remark:(NSString *)tranRemark;

-(id)initWithTransaction:(VirtualAddress *)payee payer:(PayerParam*)payer  amount:(NSDecimalNumber *)amountToTrans remark:(NSString *)tranRemark;

-(void)validate:(ParameterMode*)parameter_mode;

- (void)findDefaultDebit:(PayerParam*)payer;

@end
