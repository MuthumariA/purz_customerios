//
//  ApiProxyProtocol.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//
//  This class contain all Api calls.
//

#import <Foundation/Foundation.h>
#import "SdkConnector.h"
#import "SdkClientApplication.h"
#import "DeviceProfile.h"
#import "CustomerAccount.h"
#import "TransactionParameters.h"


@interface ApiProxyProtocol : NSObject{
    
    __block NSString *userId;
    NSString *sessionId;
    NSString *transactionId;
    NSDictionary *listAccountProvider;
    NSMutableDictionary *listKeys;
    NSString *channel;
    NSString *appName;
    NSString *sequenceNo;
    NSString *mobileNo;
    NSArray *listMappedVirtualId;
    NSString* xmlPayload;
    BOOL serviceConnected;
    SdkConnector* sdkConnector;
    SdkClientApplication* clientApplication;
    DeviceProfile* deviceProfile;
    CustomerAccount* customerAccount;


}

@property (nonatomic, retain) __block NSString *userId;
@property (nonatomic, retain) NSString *sessionId;
@property (nonatomic, retain) NSString *transactionId;
@property (nonatomic, retain) NSString *channel;
@property (nonatomic, retain) NSString *appName;
@property (nonatomic, retain) NSString *sequenceNo;
@property (nonatomic, retain) NSString *mobileNo;
@property (nonatomic, retain) NSString *xmlPayload;
@property (nonatomic, retain) CustomerAccount *customerAccount;
@property (nonatomic, retain) DeviceProfile *deviceProfile;
@property (nonatomic, retain) SdkClientApplication *clientApplication;

@property BOOL serviceConnected;



@property (nonatomic, retain) NSDictionary *listAccountProvider;
@property (nonatomic, retain) NSMutableDictionary *listKeys;
@property (nonatomic, retain) NSArray *listMappedVirtualId;

@property (nonatomic, retain) SdkConnector *sdkConnector;


+ (id)sharedManager;

-(BOOL)initSDKservice;

/*
 *  This method activates sdk or login user also initiates common library.
 */
-(NSDictionary*)activateSdk :(NSDictionary *)parametersDict withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  This method activates sdk or login user also initiates common library.
 */
-(NSDictionary*)loginUser:(NSDictionary*)param withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;


/**
 *  This method gives list of Account Provider .
 */
-(void)listAccountProviderForDeviceId :(NSDictionary*)param userId:(NSString*)userId sessionId:(NSString*)sessionId withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  This method gives list of  customer Account  .
 */
-(void)listAccount :(AccountProvider*)provider commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 * this method gives list of Account Provider .
 */
-(void)checkVpa:(NSString*)vpa commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *     this method generates OTP .
 */
-(void)generateOTP :(NSString*)accountprovider commonParam:(NSDictionary*)commonParam ifsc:(NSString*)ifsc accountnumber:(NSString*)accountnumber withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;
/*
 * this method gives ministatement.
 */
-(NSDictionary*)miniStatement:(NSDictionary*)param withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  This method stores VPA .
 */
-(void)storeVPA :(NSDictionary*)param withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  This method gives list of mapped virtual addresses .
 */
-(void)listMappedVirtualAddresses :(NSDictionary*)param withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  This method gives list keys .
 */
-(void)listKeys :(NSDictionary*)param sessionId:(NSString*)sessionid withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Activate common library.
 */
-(void)activateCommonLib :(NSString*)appId deviceId:(NSString*)deviceId channel:(NSString*)channelcode sessionid:(NSString*)sessionid userId:(NSString*)userid mobile:(NSString*)mobileNo seqNo:(NSString*)seqNo;

/**
 *  Register mobile number.
 */
-(void)registerMobile :(NSDictionary*)param withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Validate virtual address.
 */
-(void)validateVirtualAddresses :(NSDictionary*)param commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Remove Virtual address.
 */
-(void)removeVPA :(NSString*)vpa reason:(NSString*)reason params:(NSDictionary*)params withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Deregister User .
 */
-(void)deregister :(NSString*)vpa params:(NSDictionary*)params withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Generate Random Sequence number .
 */
-(NSString *) getRandomSequenceNoString;

/**
 *  Perform pay action .
 */
-(NSDictionary*)pay:(NSDictionary*)param otherParam:(NSDictionary*)otherParam commonParam:(NSDictionary*)commonParam mpin:(NSString*)mpin withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Perform change pin action .
 */
-(void)changeMpin :(NSDictionary*)params commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Perform reset pin action .
 */
-(void)resetMpin :(NSDictionary*)params commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Perform change default account .
 */
-(void)changeVPADefaultAccount :(NSDictionary*)param commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Perform collect request action .
 */
-(void)collect :(TransactionParameters*)transParam commonParams:(NSDictionary*)commonParam transId:(NSString*)transId expierAfter:(int)expierAfter view:(UIViewController*)view withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Get pending transactions.
 */
-(void)getPendingTransaction :(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Perform balance inquiry .
 */
-(void)balanceInquiry :(NSDictionary*)params commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Perform Approve or Reject .
 */
-(void)approveReject:(NSDictionary*)commonParam otherParams:(NSDictionary*)otherParams withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;


//-(void)listVa :(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;
-(void)responseWithUrl:(NSDictionary*)commonParam urlString:(NSString*)url withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

-(void)complaintResponse:(NSDictionary*)commonParam parameters:(NSDictionary*)parameters urlString:(NSString*)url withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

@end
