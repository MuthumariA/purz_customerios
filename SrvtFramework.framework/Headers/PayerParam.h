//
//  PayerParam.h
//  SrvtFramework
//
//  Created by Charushila on 12/07/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParticipantParam.h"
#import "VirtualAddress.h"

@interface PayerParam : NSObject{
    ParticipantParam* participantParam;
    NSString* payerVpa;
    VirtualAddress* vr;
}

@property (nonatomic) NSString* payerVpa;
@property (nonatomic) VirtualAddress* vr;

/**
 *  Initialise with VirtualAddress class object.
 */
-(id)initWithVPA:(VirtualAddress *)virtualAddress;

/**
 *  Initialise with vpa.
 */
-(id)initWith_VPA:(NSString *)vpa;

/**
 *  Get payer vpa.
 */
-(VirtualAddress*)getPayerVpa;


@end
