//
//  SdkConnector.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CommonLibrary/CLServices.h>
#import "SdkClientApplication.h"
#import "DeviceProfile.h"
#import "CommonUtils.h"


@interface SdkConnector : NSObject{

    SdkClientApplication* clientApplication;
    DeviceProfile* deviceProfile;
    NSString* token ;
    NSString* secureKeys ;
    NSMutableDictionary* accountProviderMap;     // [String ,AccountProvider] ;
    NSDictionary* accountProviders;
    
}

@property (nonatomic) SdkClientApplication* clientApplication;
@property (nonatomic) DeviceProfile* deviceProfile;
@property (nonatomic) NSString* token;
@property (nonatomic) NSString* secureKeys;
@property (nonatomic) NSMutableDictionary* accountProviderMap;
@property (nonatomic) NSDictionary* accountProviders;




-(id)initWith : (SdkClientApplication*)client_Application deviceProfile:(DeviceProfile*)device_Profile;

/**
 *  Returns challange if it is a valid string .
 */
- (NSString*)getChallenge:(NSString *)type device_Id:(NSString*)device_Id app_Id:(NSString*)app_Id ;

-(NSString*)getToken;

-(CLServices*)getCLService;

/**
 *    Integrate Common Library .
 */
-(void)integrateLibrary:(TokenType) tokenType;

/**
 *    Returns true if hmac is valid with respect of appid, mobile, deviceid .
 */
- (BOOL)registerApp:(NSString *)app_Id mobile_no:(NSString*)mobile_no device_Id:(NSString*)device_Id token:(NSString*)token;

- (NSMutableDictionary*)getAccountProviderMap;

- (NSDictionary*)getAccountProviders;

- (NSString*)getSecureKeys;

- (void)getProviders;

-(id)initSDK;








@end
