//
//  SdkClientApplication.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @header  SdkClientApplication.h
 @brief   Implementing this protocol, Host application will pass SDK's Host(that is UPI Client) Application details.
 Most of the details will be provided by the Sarvatra, which needs to be passed by the Application.
 */
@interface SdkClientApplication : NSObject{

    NSString* appName;
    NSString* transactionId;
    NSString* instituteId;
    NSString* channel;
    NSString* bankRefUrl;
    NSString* userName;
    NSString* userMail;
    NSString* mobile;
    NSString* userIdentity;

}

@property (nonatomic) NSString* appName;
@property (nonatomic) NSString* transactionId;
@property (nonatomic) NSString* instituteId;
@property (nonatomic) NSString* channel;
@property (nonatomic) NSString* bankRefUrl;
@property (nonatomic) NSString* userName;
@property (nonatomic) NSString* userMail;
@property (nonatomic) NSString* mobile;
@property (nonatomic) NSString* userIdentity;




/*!
 @brief Get Client Application Name.
 @return Application Name
 */
-(NSString*) getApplicationName;

/*!
 @brief Get Transaction ID Prefix.
 @return Transaction ID Prefix
 */
-(NSString*) getTransactionIDPrefix;

/*!
 @brief SDK will use this method to store data in the application private data store.
 @param key Key of the Data
 @param value Value of the Data.
 */
-(void) storeValue :(NSString*)key value:(NSString*)value;

/*!
 @brief SDK will use this method to read data from application's private data store.
 @param key Key to read.
 @return value of the data. Null if not present.
 */
-(NSString*) readValue :(NSString*)key;

/*!
 @brief Get Institute ID assigned by Sarvatra.
 @return Institute ID.
 */
-(NSString*) getInstitute;

/*!
 @brief Get Channel Code assigned by Sarvatra.
 @return Channel Code.
 */
-(NSString*) getChannel;

/*!
 @brief Returns Bank's Url..
 @return Banks url.
 */
-(NSString*) getBankRefUrl;

/*!
 @brief Returns host Application User Name.
 @return  User Name.
 */
-(NSString*)getUserName;

/*!
 @brief Returns Host Application User Email.
 @return  User Email.
 */
-(NSString*)getUserEmail;

/*!
 @brief Returns Host Application User Mobile.
 @return Mobile (without +91 or 91).
 */
-(NSString*)retrieveMobile;

/*!
 @brief Returns Host Application Unique ID of the current User.
 @return  Unique ID.
 */
-(NSString*)getUserIdentity;

/*!
 @brief Initialise SdkClientApplication with user details.
 @return SdkClientApplication object.
 */
-(id)initWithUserName:(NSString*)user_Name appName:(NSString*)app_Name instituteId:(NSString*)institute_Id channel:(NSString*)channel_Code bankRefUrl:(NSString*)bankRef_Url userMail:(NSString*)user_Mail mobile:(NSString*)mobile_No userIdentity:(NSString*)user_Identity transactionID_Prefix:(NSString*)transactionID_Prefix;
@end
