//
//  LinkableTransaction.h
//  SrvtFramework
//
//  Created by Charushila on 04/08/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/*!
 @header  LinkableTransaction.h
 @brief   Details for Linking Transaction with other PSP apps via Deep linking, QR.
 */
@interface LinkableTransaction : NSObject{

    NSString* payeeVPA;
    NSString* payeeName;
    NSString* payeeMerchantCode;
    NSString* transactionID;
    NSString* transactionReferenceID;
    NSString* remarks;
    NSString* currencyCode;
    NSString* transactionDetailsUrl;
    NSDecimalNumber* amount;
    NSDecimalNumber* minimumAmount;
    NSString* ifsc;
    NSString* accNo;
    NSString* aadharNo;



   // private Mode transactionMode;
}

typedef void(^completionBlock)(BOOL success);

/**
 * Mode of the LinkableTransaction
 */
typedef enum { Simple=0 , Dynamic=1} tMode;
/**
 * Mode property for LinkableTransaction
 */
@property (nonatomic) tMode t_mode;

/**
 * Initialise LinkableTransaction with Simple Mode, assigns 0000 as merchant code, INR as currency Code.
 */
-(id)initWith:(NSString*)payee_VPA  payeeName:(NSString*)payee_Name ;

/**
 * Initialise LinkableTransaction with given mode, assigns 0000 as merchant code, INR as currency Code.
 */
-(id)initWithPayeeName:(NSString*)payee_VPA  payeeName:(NSString*)payee_Name mode:(int)mode;

/**
 * Validate the Transaction Details, true if successful. SdkException Otherwise
 *
 * @return
 */
-(BOOL)validate;

///**
// * Get Transaction Mode
// *
// * @return mode.
// */
//-(tMode*)get_tMode;
//
///**
// * Set Transaction Mode
// *
// * @param mode tMode
// */
//-(void)set_tMode : (tMode*)mode;


/**
 * Get Payee VPA
 *
 * @return Payee VPA
 */
-(NSString*)getPayeeVPA;


/**
 * Set Payee VPA
 *
 * @param payee_VPA Payee VPA
 */
-(void)setPayeeVPA : (NSString*)payee_VPA;


/**
 * Get Payee Name
 *
 * @return Payee Name
 */
-(NSString*)getPayeeName;


/**
 * Set Payee Name
 *
 * param payee_Name
 */
-(void)setPayeeName : (NSString*)payee_Name;

/**
 * Get Merchant Category Code
 *
 * return
 */
-(NSString*)getPayeeMerchantCode;

/**
 * Set Merchant Category Code
 *
 * param payee_Merchant_Code
 */
-(void)setPayeeMerchantCode : (NSString*)payee_Merchant_Code;

/**
 * Get Transaction ID
 *
 * return
 */
-(NSString*)getTransactionID;

/**
 * Set Transaction ID
 *
 * param transactionID
 */
-(void)setTransactionID : (NSString*)transaction_ID;

/**
 * Get Transaction Reference ID
 *
 * return
 */
-(NSString*)getTransactionReferenceID;

/**
 * Set Transaction Reference ID
 *
 * param transactionReferenceID
 */
-(void)setTransactionReferenceID : (NSString*)transaction_ReferenceID;

/**
 * Get Transaction Remarks
 *
 * @return remarks.
 */
-(NSString*)getRemarks;

/**
 * Get Transaction Remarks
 *
 * param remarks
 */
-(void)setRemarks : (NSString*)transaction_remarks;

/**
 * Get Transaction Currency Code
 *
 * return
 */
-(NSString*)getCurrencyCode;

/**
 * Get Transaction Currency Code
 *
 * param currency_Code
 */
-(void)setCurrencyCode : (NSString*)currency_Code;

/**
 * Get Transaction Details URL
 *
 * return
 */
-(NSString*)getTransactionDetailsUrl;

/**
 * Set Transaction Details URL
 *
 * param transaction_DetailsUrl
 */
-(void)setTransactionDetailsUrl : (NSString*)transaction_DetailsUrl;

/**
 * Get Transaction Amount
 *
 * return
 */
-(NSDecimalNumber*)getAmount;

/**
 * Set Transaction Amount
 *
 * param amount
 */
-(void)setAmount : (NSDecimalNumber*)transaction_amount;

/**
 * Get Minimum Transaction Amount
 *
 * @return minimum Amount.
 */
-(NSDecimalNumber*)getMinimumAmount;

/**
 * Set Minimum Transaction Amount
 *
 * param minimumAmount
 */
-(void)setMinimumAmount : (NSDecimalNumber*)minimum_Amount;


/**
 * Get Account Number
 *
 * @return Account Number
 */
-(NSString*)getAccNo;


/**
 * Set Account Number
 *
 * @param acc_No
 */
-(void)setAccNo : (NSString*)acc_No;

/**
 * Get IFSC code
 *
 * @return IFSC code
 */
-(NSString*)getIfsc;


/**
 * Set IFSC code
 *
 * @param ifsc
 */
-(void)setIfsc : (NSString*)ifsc;


/**
 * Get Aadhar Number
 *
 * @return Aadhar Number
 */
-(NSString*)getAadharNo;


/**
 * Set Aadhar Number
 *
 * @param aadhar_no
 */
-(void)setAadharNo : (NSString*)aadhar_no;


/**
 * Return IntentData (QR data) as per "UPI Linking Specifications"
 *
 * @return QR Data
 *
 */
- (NSString *)toIntentData : (LinkableTransaction *)transaction;

/**
 * Parse qr code data scanned by app. "
 *
 * @return LinkableTransaction object with all acanned data.
 *
 */
- (LinkableTransaction *)parseIntentData : (NSString *)transactionUrl;

/**
 * Share the Transaction with the PSP apps. Trigger the intent which can be received by the registered PSP apps.
 * @param linkableTransaction
 */
-(void)sharedWithPSP : (LinkableTransaction *)linkableTransaction;

-(NSString*)generateBharatQrCode : (LinkableTransaction*)linkableTransaction;

@end
