//
//  CustomerAccount.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountProvider.h"

@interface CustomerAccount : NSObject{
    
    NSString* mmid;
    NSString* account;
    NSString* maskedAccount;
    NSString* providerid;

    NSString* ifsc;
    NSString* name;
    BOOL mobileBankingEnabled;
    BOOL aadhaarEnabled;
    AccountProvider* provider;

    BOOL defaultCredit;
    BOOL defaultDebit;
    NSString* accountProvider;
    NSString* accountType;
    NSDictionary* credentialData;
    NSString* credentialRequired;
    NSString* mobileData;
    NSMutableDictionary* credArr;
    NSDictionary* credDict;
    NSArray* credArray;



}

@property (nonatomic) NSString* accountProvider;
@property (nonatomic) AccountProvider* provider;

/**
 * Get Customer mmid.
 */
-(NSString*)getMmid;

/**
 * Get Customer Account.
 */
-(NSString*)getAccount;

/**
 * Get Customer Ifsc.
 */
-(NSString*)getIfsc;

/**
 * Get Customer Name.
 */
-(NSString*)getName;

/**
 * Get Customer mobile banking anabled or not.
 */
-(BOOL)getMobileBankingEnabled;

/**
 * Get Customer Aadhar anabled or not.
 */
-(BOOL)getAadhaarEnabled;

/**
 * Get Customer credentials array.
 */
-(NSMutableDictionary*)getCredArr;

@property (nonatomic) BOOL defaultCredit;
@property (nonatomic) BOOL defaultDebit;


-(AccountProvider*) getProvider;

/**
 * Get Customer account provider name.
 */
-(NSString*)getAccountProvider;
/**
 * Get Customer Provider id.
 */
-(NSString*)getId;

/**
 * Get Customer Account Type.
 */
-(NSString*)getAccountType;

/**
 * Get Customer Masked Account Number.
 */
-(NSString*)getMaskedAccount;

/**
 * Get Default Credit.
 */
-(BOOL)getDefaultCredit;

/**
 * Get Default Debit.
 */
-(BOOL)getDefaultDebit;

/**
 * Get Credential Data.
 */
-(NSDictionary*)getCredentialData;

/**
 * Get Credential Required.
 */
-(NSString*)getCredentialRequired;

/**
 *  Initialise Customer Object with customer details and  Account Provider Details.
 */
-(id)collectCustomerDetails : (NSDictionary*)details provider:(AccountProvider*)accprovider;

/**
 *  Initialise Customer Object with customer details.
 */
-(id)initWithAccountDetails:(NSDictionary *)customerAccount;

@end
