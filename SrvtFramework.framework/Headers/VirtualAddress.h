//
//  VirtualAddress.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomerAccount.h"

@interface VirtualAddress : NSObject{

    CustomerAccount* custAccount;
    NSString* handle;
    NSArray* custAccountList;

}
@property (nonatomic) NSString* handle;
@property (nonatomic) CustomerAccount* custAccount;
@property (nonatomic) NSArray* custAccountList;


/**
 *   Get Handle.
 */
-(NSString*)getHandle;

/**
 *  Initialise Customer Account.
 */
-(CustomerAccount*)getCustomerAccounts;

/**
 *  Initialise Customer Account list.
 */
-(NSArray*)getCustAccountList;

/**
 *  Initialise Account details and vpa.
 */
-(id)initWith:(NSDictionary *)account handle:(NSString*)vpa_handle;



@end
