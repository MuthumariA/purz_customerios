//
//  AccountProvider.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @header  AccountProvider.h
 @brief   Account Service provides to functionality to work on the customer accounts.
 */

@interface AccountProvider : NSObject{

    NSString* providerId;
    NSString* iin;
    NSString* accountProvider;
}
@property (nonatomic) NSString* providerId;
@property (nonatomic) NSString* iin;
@property (nonatomic) NSString* accountProvider;

/**
 * Get Account Provider id.
 */
-(NSString*)getId;

/**
 * Get Account Provider Iin.
 */
-(NSString*)getIin;

/**
 * Get Account Provider name.
 */
-(NSString*)getAccountProvider;

/**
 * Initialise Account Provider with details.
 */
-(id)initWithAccountProviderId:(NSString *)provider_id
                               provider:(NSString *)provider
                               providerIin:(NSString *)provider_Iin;

@end
