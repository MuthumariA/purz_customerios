//
//  ApiClient.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CardData.h"
#import "CustomerAccount.h"
#import "SdkClientApplication.h"
#import "RegisterVPAService.h"
#import "AccountDefaultStatus.h"
#import "DeviceProfile.h"
#import "TransactionParameters.h"
#import "VirtualAddress.h"
#import "PendingTransactionInfo.h"


@interface ApiClient : NSObject


@property (nonatomic) dispatch_semaphore_t semaphore;


typedef void(^completion)(BOOL success, NSError* error, id responce);


/**
 *   Activate Sdk and initialise Common Lib.
 */
-(void)activateSdk:(SdkClientApplication*)clientApplication withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  Get list of account provider .
 *  @return account provider list
 */
-(NSDictionary*)getAccountProviderList;

/**
 *  Get list of customer accounts .
 *  return customer accounts.
 */
-(void)getCustomerAccounts:(AccountProvider*) provider withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 * Check if VPA is available for Use
 *
 * @param vpa VPA to check
 * return true if VPA available, false otherwise.
 */
-(void)checkVPA:(NSString*)vpa :(completion)completionBlock ;


/**
 * Generate OTP for given account details.
 */
-(void)generateOTP :(NSString*)accountprovider ifsc:(NSString*)ifsc accountnumber:(NSString*)accountnumber withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 * Get Transaction Mini Statement
 *
 * @param fromDate Start of Date Range
 * @param toDate   End Date Range
 * return List of UPI Transaction Information
 *
 */
-(void)miniStatement:(NSString*)fromDate toDate:(NSString*)toDate withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 * Initiates Payment Transaction
 *
 * @param transParam (transactionParameters object) Pay Transaction
 *
 */
-(void)pay:(TransactionParameters*)transParam view:(UIViewController*)view withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 * Get the Mapped Virtual Addresses of the Customer.
 *
 * return List of the virtual Addresses
 */
-(void) getMappedVirtualAddresses:(completion) completionBlock;

/**
 * Create new VPA if available, map Customer account to it with default status provided.
 *
 * @param vpa                  VPA
 * @param custAccount              Account
 * @param status Account default status information
 *
 */
-(void)mapAccount :(SdkClientApplication *)clientApp vpa:(NSString*)vpa custAccount:(CustomerAccount *)custAccount  accountDefaultStatus:(AccountDefaultStatus *)status withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 * @param vpa                  VPA
 * @param custAccount              Account
 * @param card             Card data
 * @param status Account default status information
 *
 */
-(void)mapAccount :(CardData *)card vpa:(NSString*)vpa custAccount:(CustomerAccount *)custAccount accountDefaultStatus:(AccountDefaultStatus *)status view:(UIViewController*)view  withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 * Check if Payee VPA is valid. Retrieves data associates with the VPA.
 *
 * @param virtualAddr VPA to check
 * return UPI SDK Response. If success check for "name" element in the Result data for Payee Name.
 *
 */
-(void)validatePayeeVirtualAddress:(NSString*)virtualAddr withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 * Remove Virtual Address
 *
 * @param vpa Virtual Address to remove
 * @param reason
 * return true if successful, false otherwise
 */
-(void)removeVPA :(NSString*)vpa reason:(NSString*)reason withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 * Deregister UPI profile
 *
 * @param reason Reason of Deregister
 * @return true if profile de-registered successfully, false otherwise.
 */
-(void)deRegister :(NSString*)reason withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;

/**
 *  generate Transaction ID .
 *
 */
-(NSString *) generateTransactionID;

/**
 * Change MPIN for the account
 *
 * @param virtualaddress Virtual Address
 * @param custAccount Customer Account to add
 */
-(void)changeMPIN :(VirtualAddress*)virtualaddress custAccount:(CustomerAccount*)custAccount view:(UIViewController*)view : (completion) completionBlock;

/**
 * Reset MPIN for the account
 *
 * @param virtualaddress Virtual Address
 * @param custAccount        Customer Account to add
 * @param carddata       Card data
 */
-(void)resetMPIN :(VirtualAddress*)virtualaddress custAccount:(CustomerAccount*)custAccount carddata:(CardData*)carddata view:(UIViewController*)view :(completion) completionBlock;

/**
 * Map Customer account to already present Virtual Address
 * @param virtualaddress Virtual Address
 * @param custAccount        Customer Account to add
 * return true if successful, false otherwise
 */
-(void)changeDefaultAccount :(VirtualAddress*)virtualaddress custAccount:(CustomerAccount *)custAccount  accountDefaultStatus:(AccountDefaultStatus *)status withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;



/**
 * Initiate Collect Transaction
 *
 * @param payerVirtualaddress Payer Virtual Address
 * @param payee               Virtual Address to Credit (used Default Credit Account set in this Virtual Account)
 * @param amount              Transaction Amount
 * @param remark              Remark
 * @param expireAfter         Expire After Interval in minutes
 *
 */
-(void)collect :(NSString*)payerVirtualaddress payee:(VirtualAddress*)payee amount:(NSDecimalNumber*)amount remark:(NSString*)remark expireAfter:(int)expireAfter  view:(UIViewController*)view :(completion) completionBlock;



/**
 *  Stop sdk service .
 *
 */
-(void)stopSdkService;



/**
 * Get Pending Transactions as Payee (balance is not yet credited)
 *
 * return List of Pending Transactions
 */
-(void)getPendingTransactionAsPayee:(completion) completionBlock;



/**
 * Get Pending Transactions as Payer (waiting for debit approval from customer)
 *
 * return List of Pending Transactions
 */
-(void)getPendingTransactionForApproval :(completion) completionBlock;


/**
 * Approve Pending Transaction. Accept MPIN for the account and then initiates Approve Transaction. Note on successful MPIN validation, API will only initiate the Payment. Call MiniStatement API for actual status.
 *
 * @param fromVirtualAddress     Debit Virtual Address (Debit Account)
 * @param pendingTransactionInfo Pending Transaction Information
 * @param remark                 Approval Remark
 */
-(void)approve:(VirtualAddress*)fromVirtualAddress pendingTransactionInfo:(PendingTransactionInfo *)pendingTransactionInfo remark:(NSString*)remark view:(UIViewController*)view :(completion) completionBlock;



/**
 * @param virtualaddress         Virtual Address for which Collect is initiated (need details to send NPCI)
 * @param pendingTransactionInfo Pending Transaction to reject
 * @param remark                 Rejection Remark
 */
-(void)reject:(VirtualAddress*)virtualaddress pendingTransactionInfo:(PendingTransactionInfo *)pendingTransactionInfo remark:(NSString*)remark view:(UIViewController*)view :(completion) completionBlock;



/**
 * Check Balance of the Account mapped to Virtual Address. If successful Balance will be in 'balance' property - Check UpiResult.get('balance').
 *
 * @param virtualaddress Virtual Address
 * @param custAccount        Account to check balance
 */
-(void)balanceInquiry:(VirtualAddress*)virtualaddress custAccount:(CustomerAccount *)custAccount view:(UIViewController*)view :(completion) completionBlock;

/**
 * Get version Of SDK
 */
-(NSString*)getVersionOfSDK;

/**
 * Get Customer VPA list.
 */
-(void)customerVPAList:(completion)completionBlock;

/**
 * Get complaint reason code list.
 */
-(void)getComplaintReasonCode:(completion)completionBlock;

/**
 * Get complaint list.
 */
-(void)getComplaintList:(completion)completionBlock;

/**
 * Get complaint status.
 * @param complaintId .
 */
-(void)getComplaintStatus:(NSString*)complaintId :(completion)completionBlock;

/**
 * Register Complaint.
 * @param reasonCode Complaint reason code.
 * @param comment comment.
 * @param rrn Bank rrn .
 * @param txnType Transaction Type.
 * @param txnDate Transaction Date.
 */
-(void)registerComplaint:(NSString*)reasonCode comment:(NSString*)comment rrn:(NSString*)rrn txnType:(NSString*)txnType txnDate:(NSString*)txnDate :(completion)completionBlock;



@end
