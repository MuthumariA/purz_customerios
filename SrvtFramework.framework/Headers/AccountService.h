//
//  AccountService.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SdkClientApplication.h"
#import "DeviceProfile.h"
#import "SdkConnector.h"

@interface AccountService : NSObject{

   //  SdkClientApplication* clientApplication;
   //  DeviceProfile* deviceProfile;
   //  SdkConnector* sdkConnector;

}
-(id)initWith :(SdkClientApplication*) client_Application  device_Profile:(DeviceProfile*) device_Profile sdk_Connector:(SdkConnector*) sdk_Connector;
@end
