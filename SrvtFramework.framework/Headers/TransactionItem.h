//
//  TransactionItem.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionItem : NSObject{

    NSString* payeeVa;
    NSString* payerVa;
    NSString* remark;
    NSString* amount;
    NSString* transactionType;
    NSString* status;
    NSString* seqNo;
}
@property (nonatomic) NSString* payeeVa;
@property (nonatomic) NSString* payerVa;
@property (nonatomic) NSString* remark;
@property (nonatomic) NSString* amount;
@property (nonatomic) NSString* transactionType;
@property (nonatomic) NSString* status;
@property (nonatomic) NSString* seqNo;

typedef enum { Pending,MiniStatement} TransactionItemMode;

@property (nonatomic) TransactionItemMode tm_mode;

-(id)initWith:(NSString*)payee_Va payer_Va:(NSString*)payer_Va transac_Remark:(NSString*)transac_Remark transac_Amount:(NSString*)transac_Amount transaction_Type:(NSString*)transaction_Type transaction_status:(NSString*)transaction_status transaction_SeqNo:(NSString*)transaction_SeqNo;




-(NSString*)getPayeeVa;
-(NSString*)getPayerVa;
-(NSString*)getRemark;
-(NSString*)getAmount;
-(NSString*)getTransactionType;
-(NSString*)getStatus;
-(NSString*)getSeqNo;

@end
