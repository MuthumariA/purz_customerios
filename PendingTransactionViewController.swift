//
//  PendingTransactionViewController.swift
//  purZ
//
//  Created by Vertace on 26/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class PendingTransactionViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITabBarControllerDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    var amount = [String]()
    var payeeVa = [String]()
    var payerVa = [String]()
    var date = [String]()
    var expiredate = [String]()
    var pending_trans_detail = [String]()
    var vpaname = [String]()
    var vpaaccountdetail = [String]()
    var vpaaccountname = [String]()
    var alertView = UIAlertView()
    var selectvpa = ""
    var selectVPAdetail = ""
    var status = ""
    var transactionid = ""
    var descriptiontxt = ""
    var approveindex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialFunc()
       
    }
    
    func initialFunc()
    {
        tabBarController?.delegate = self
        activityIndicator.startAnimating()
        self.alertView.title = "Select VPA"
        self.alertView.delegate = self
        ApiClient().getPendingTransactionForApproval { (success, error, response) in
            if response != nil && success{
                do {
                    let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(response, options: NSJSONWritingOptions.PrettyPrinted)
                    let json = JSON(data: jsonData)
                    
                    for item in json.arrayValue {
                        self.pending_trans_detail.append(String(item))
                        self.amount.append(item["amount"].stringValue)
                        self.payeeVa.append(item["payeeVa"].stringValue)
                        self.payerVa.append(item["payerVa"].stringValue)
                        
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-ddHH:mm:ss"
                        dateFormatter.timeZone = NSTimeZone(name: "UTC")
                        var tmpDate = dateFormatter.dateFromString(item["expireAfter"].stringValue)
                        dateFormatter.dateFormat = "dd-MM-yy"
                        let finaldate = dateFormatter.stringFromDate(tmpDate!)
                        self.expiredate.append(finaldate)
                        dateFormatter.dateFormat = "yyyyMMddHHmmss"
                        dateFormatter.timeZone = NSTimeZone(name: "UTC")
                        tmpDate = dateFormatter.dateFromString(item["date"].stringValue)
                        dateFormatter.dateFormat = "dd MMM"
                        let dateString1 = dateFormatter.stringFromDate(tmpDate!)
                        if(!dateString1.isEmpty){
                            let datearray = dateString1.componentsSeparatedByString(" ")
                            if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                                let correctdate = datearray[0] + "st " + datearray[1]
                                self.date.append(correctdate)
                            }
                            else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                                let correctdate = datearray[0] + "nd " + datearray[1]
                                self.date.append(correctdate)
                            }
                            else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                                let correctdate = datearray[0] + "rd " + datearray[1]
                                self.date.append(correctdate)
                            }
                            else{
                                let correctdate = datearray[0] + "th " + datearray[1]
                                self.date.append(correctdate)
                            }
                            
                        }
                        
                    }
                    if AccountConstant.vpanames.count == 0 && self.amount.count > 0{
                        self.getVPAList()
                    }
                    else{
                        if self.amount.count == 0 {
                            self.presentViewController(Alert().alert("You don't have any pending transactions for approval",message: ""), animated: true, completion: nil)
                        }
                        self.vpaname = AccountConstant.vpanames
                        self.vpaaccountdetail = AccountConstant.vparesponses
                        self.vpaaccountname = AccountConstant.vpaProviderName
                        for(var i=0;i<self.vpaname.count;i++){
                            self.alertView.addButtonWithTitle(self.vpaname[i])
                        }
                        self.alertView.addButtonWithTitle("Cancel")
                    }
                    self.activityIndicator.stopAnimating()
                    self.tableView.reloadData()
                }
                catch let error as NSError {
                    self.activityIndicator.stopAnimating()
                }
            }
            else {
                self.activityIndicator.stopAnimating()
            }
        }
        
        // Do any additional setup after loading the view.
    
    }
    func getVPAList() {
        self.activityIndicator.startAnimating()
        ApiClient().getMappedVirtualAddresses { (success, error, response) in
            self.activityIndicator.stopAnimating()
            if success {
                if response != nil {
                    
                    do{
                        // do whatever with jsonResult
                        let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(response, options: NSJSONWritingOptions.PrettyPrinted)
                        let json = JSON(data: jsonData)
                        
//                        for item in json.arrayValue {
//                            print(String(item))
//                            self.vpaname.append(item["va"].stringValue)
//                            self.vpaaccountdetail.append(String(item))
//                            self.vpaaccountname.append(item["account-provider-name"].stringValue)
//                            self.alertView.addButtonWithTitle(item["va"].stringValue)
//                        }
                        
                        for item1 in json.arrayValue {
                            
                            let item2 = item1["VirtualAddress"]
                            let items = item2["accounts"]
                            
                            self.vpaname.append(items["handle"].stringValue)
                            self.alertView.addButtonWithTitle(items["handle"].stringValue)
                            for item in items["CustomerAccount"].arrayValue {
                                if item["default-debit"].stringValue == "D"{
                                    self.vpaaccountname.append(item["account-provider-name"].stringValue)
                                    self.vpaaccountdetail.append(String(item))
                                }
                            }
                        }
                        
                        
                        self.alertView.addButtonWithTitle("Cancel")
                        AccountConstant.vpanames = self.vpaname
                        AccountConstant.vparesponses = self.vpaaccountdetail
                        AccountConstant.vpaProviderName = self.vpaaccountname
                        self.activityIndicator.stopAnimating()
                    }
                    catch {
                        self.activityIndicator.stopAnimating()
                    }
                    
                }
                else{
                    self.activityIndicator.stopAnimating()
                }
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("pendingListCell", forIndexPath: indexPath) as UITableViewCell!
        
        let payeelbl = cell.viewWithTag(1) as! UILabel
        let payerlbl = cell.viewWithTag(2) as! UILabel
        let expiredatelbl = cell.viewWithTag(3) as! UILabel
        let datelbl = cell.viewWithTag(4) as! UILabel
        let approvebtn = cell.viewWithTag(5) as! UIButton
        let declinebtn = cell.viewWithTag(6) as! UIButton
        
        payeelbl.text = "Rs "+amount[indexPath.row]+" to "+payeeVa[indexPath.row]
        payerlbl.text = "From "+payerVa[indexPath.row]
        expiredatelbl.text = "Valid till: "+expiredate[indexPath.row]
        datelbl.text = date[indexPath.row]
        
        approvebtn.layer.cornerRadius = approvebtn.frame.size.height/2
        approvebtn.layer.borderWidth = 2
        approvebtn.layer.borderColor = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        declinebtn.layer.cornerRadius = declinebtn.frame.size.height/2
        declinebtn.layer.borderWidth = 2
        declinebtn.layer.borderColor = UIColor(red: 128.0/255.0, green: 187.0/255.0, blue: 65.0/255.0, alpha: 1).CGColor
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return amount.count
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
   
    func convertStringToDictionary(text: String) -> [NSObject:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [NSObject:AnyObject]
            }
            catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        if buttonIndex != vpaname.count {
            approve(buttonIndex)
        }
    }
    
    func approve(indexpath: Int){
        
        selectvpa = vpaname[indexpath]
        selectVPAdetail = self.vpaaccountdetail[indexpath]
        //        for(var i=0;i<self.vpaname.count;i++){
        //            if self.vpaname[i] == payerVa[self.approveindex] {
        //                selectVPAdetail = self.vpaaccountdetail[i]
        //                selectvpa = self.vpaname[i]
        //                break;
        //            }
        //        }
        
        self.activityIndicator.startAnimating()
        let account = self.convertStringToDictionary(selectVPAdetail)! as NSDictionary
        let VirtualAddr_details = VirtualAddress.init(account as! [NSObject : AnyObject], handle: selectvpa)
        let transactiondetail = self.convertStringToDictionary(pending_trans_detail[self.approveindex])! as NSDictionary
        let pendingTrans_info = PendingTransactionInfo.init(transactiondetail as! [NSObject : AnyObject])
        ApiClient().approve(VirtualAddr_details, pendingTransactionInfo: pendingTrans_info, remark: "NA", view: self) { (success, error, response) in
            if response != nil {
                self.amount.removeAtIndex(self.approveindex)
                self.payeeVa.removeAtIndex(self.approveindex)
                self.payerVa.removeAtIndex(self.approveindex)
                self.date.removeAtIndex(self.approveindex)
                
                let response_success = response.valueForKey("success")
                if String(response_success!) == "1" {
                    self.status = "Success"
                    self.descriptiontxt = String(response.valueForKey("message")!)
                    self.transactionid = String(response.valueForKey("BankRRN")!)
                    self.performSegueWithIdentifier("pending_to_success", sender: self)
                }
                else {
                    self.status = "Failed"
                    self.descriptiontxt = String(response.valueForKey("message")!);
                    self.transactionid = String(response.valueForKey("BankRRN")!)
                    self.performSegueWithIdentifier("pending_to_success", sender: self)
                }
            }
            self.tableView.reloadData()
        }
    }
    
       func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "pending_to_success") {
            let nextview = segue.destinationViewController as! CollectMoneySuccessViewController
            nextview.transactionID = transactionid
            nextview.status = status
            nextview.descriptiontxt = descriptiontxt
            
        }
    }
    @IBAction func declineBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        let alert = UIAlertController(title: "Are you sure you want to decline this request", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        //             alert.setValue(attributedString, forKey: "attributedTitle")
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { alertAction in
            self.selectvpa = self.payerVa[indexPath.row]
            for(var i=0;i<self.vpaname.count;i++){
                if self.vpaname[i] == self.payerVa[indexPath.row] {
                    self.selectVPAdetail = self.vpaaccountdetail[i]
                    self.selectvpa = self.vpaname[i]
                    break;
                }
            }
            self.activityIndicator.startAnimating()
            let account = self.convertStringToDictionary(self.selectVPAdetail)! as NSDictionary
            let VirtualAddr_detail = VirtualAddress.init(account as! [NSObject : AnyObject], handle: self.selectvpa)
            let transactiondetail = self.convertStringToDictionary(self.pending_trans_detail[indexPath.row])! as NSDictionary
            let pendingTrans_info = PendingTransactionInfo.init(transactiondetail as! [NSObject : AnyObject])
            ApiClient().reject(VirtualAddr_detail, pendingTransactionInfo: pendingTrans_info, remark: "NA", view: self) { (success, error, response) in
                if response != nil {
                    self.amount.removeAtIndex(indexPath.row)
                    self.payeeVa.removeAtIndex(indexPath.row)
                    self.payerVa.removeAtIndex(indexPath.row)
                    self.date.removeAtIndex(indexPath.row)
                    let response_success = response.valueForKey("success")
                    
                    if String(response_success!) == "1" {
                        self.status = "Success"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("pending_to_success", sender: self)
                    }
                    else {
                        self.status = "Failed"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("pending_to_success", sender: self)
                    }
                }
                self.tableView.reloadData()
            }
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: { alertAction in
        }))
        self.presentViewController(alert, animated: true, completion: nil)
       
        
    }
    
    @IBAction func approveBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        self.approveindex = indexPath.row
        self.alertView.show()
    }
 

}
