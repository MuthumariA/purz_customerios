//
//  TransactionViewModel.swift
//  Cippy
//
//  Created by apple on 30/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import Foundation

class TransactionViewModel {
    
    var amount: String
    var balance: String
    var otherPartyName: String
    init?(amount: String, balance: String, otherPartyName: String){
        self.amount = amount
        self.balance = balance
        self.otherPartyName = otherPartyName
    }
    
}

class ApprovalViewModel{
    var fundRequestNumber: String
    var status: String
    var yapcode: String
    init?(fundRequestNumber: String, status: String, yapcode: String){
        self.fundRequestNumber = fundRequestNumber
        self.status = status
        self.yapcode = yapcode
    }
}


class GetRecentViewModel {
    var fromEntityId: String
    var utilityType: String
    init?(fromEntityId: String, utilityType: String){
        self.fromEntityId = fromEntityId
        self.utilityType = utilityType
    }
}

class AddRecentViewModel {
    var fromEntityId: String
    var toContactNo: String
    var amount: String
    var txnType: String
    var operatorCode: String
    var serviceProvider: String
    var utilityType: String
    var qrCode: String
    var authorization1: String
    var authorization2: String
    var authorization3: String
    var tip: String
    
    init?(fromEntityId: String, toContactNo: String, amount: String, txnType: String, operatorCode: String, serviceProvider: String, utilityType: String, qrCode: String, authorization1: String, authorization2: String, authorization3: String, tip: String){
        
        self.fromEntityId = fromEntityId
        self.toContactNo = toContactNo
        self.amount = amount
        self.txnType = txnType
        self.operatorCode = operatorCode
        self.serviceProvider = serviceProvider
        self.utilityType = utilityType
        self.qrCode = qrCode
        self.authorization1 = authorization1
        self.authorization2 = authorization2
        self.authorization3 = authorization3
        self.tip = tip
    }
    
}
