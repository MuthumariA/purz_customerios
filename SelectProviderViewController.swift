//
//  SelectProviderViewController.swift
//  Cippy
//
//  Created by Vertace on 13/04/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import UIKit

class SelectProviderViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITabBarControllerDelegate {

    var operatorName = [String]()
//    var operatorCode = [String]()
//    var operatorType = [String]()
//    var special = [Bool]()
//    var minimumbillamt = [String]()
//    var auth1 = [String]()
//    var auth2 = [String]()
//    var auth3 = [String]()
    var providername = ""
//    var titlename = ""
//    var placeholder1 = ""
//    var placeholder2 = ""
    var index = 0
    @IBOutlet
    var tableView: UITableView!
    
    @IBOutlet weak var Provtitlelbl: UILabel!
    
var mobileno = ""
    var segmentNumberSelectedInProvider = 0
    override func viewDidLoad() {
        super.viewDidLoad()
          self.tabBarController?.delegate = self
        if (segmentNumberSelectedInProvider == 0 || segmentNumberSelectedInProvider == 1 ||
            segmentNumberSelectedInProvider == 2 ||
            segmentNumberSelectedInProvider == 6)
        {
           Provtitlelbl.text = "SELECT PROVIDER"
        }
        else if segmentNumberSelectedInProvider == 3
        {
            Provtitlelbl.text = "SELECT ELECTRICITY BOARD"
        }
        else if segmentNumberSelectedInProvider == 4
        {
            Provtitlelbl.text = "SELECT GAS COMPANY"
        }
        else if segmentNumberSelectedInProvider == 5
        {
            Provtitlelbl.text = "SELECT COMPANY"
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell!
        print(indexPath.row)
        
        let providerlbl = cell.viewWithTag(1) as! UILabel
        
        providerlbl.text = operatorName[indexPath.row].uppercaseString
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
            
       
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return operatorName.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let selectionColor = UIView() as UIView
        selectionColor.layer.borderWidth = 1
        selectionColor.layer.borderColor = UIColor.clearColor().CGColor
        selectionColor.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = selectionColor
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
      providername = operatorName[indexPath.row]
        index = indexPath.row
        self.performSegueWithIdentifier("from_provider", sender: self)

        
    }
    

    @IBAction func BackBrnAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
//         self.performSegueWithIdentifier("from_provider", sender: self)
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "from_provider") {
            let nextview = segue.destinationViewController as! RechargeorPayBills
            nextview.segmentno_provider = segmentNumberSelectedInProvider
            nextview.provider_Name  =  providername
            nextview.index_prov = index
            nextview.mobileno = mobileno
//            print(provider_Name)
            print("provider_name")
            print(index)
            print(segmentNumberSelectedInProvider)
            nextview.from_provider = true
                   }
    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
}
