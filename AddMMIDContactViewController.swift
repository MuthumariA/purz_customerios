//
//  AddMMIDContactViewController.swift
//  purZ
//
//  Created by Vertace on 25/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class AddMMIDContactViewController: UIViewController,UITabBarControllerDelegate, UITextFieldDelegate {
  
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var registerNameTxtField: UITextField!
    @IBOutlet weak var mobileNoTxtField: UITextField!
    @IBOutlet weak var MMIDTxtField: UITextField!
    
    var existing_mmid = [String]()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        initialFunc()
          }
    @IBAction func confirmBtnAction(sender: AnyObject) {
        var alreadyexist = false
        for(var i=0;i<existing_mmid.count;i++) {
            if existing_mmid[i] == MMIDTxtField.text! {
                alreadyexist = true
            }
        }
        if !alreadyexist {
        let mmid = MMIDTxtField.text!
        let mobileNo = mobileNoTxtField.text!
        if (mmid == "" )
        {
            self.presentViewController(Alert().alert("MMID number  can't be empty", message: ""),animated: true,completion: nil)
        }
            
            
        else if(mmid.characters.count != 7 )
        {
            self.presentViewController(Alert().alert("Invalid MMID", message: ""),animated: true,completion: nil)
            
        }
        else if(mobileNo == "" )
        {
            self.presentViewController(Alert().alert("Mobile number  can't be empty", message: ""),animated: true,completion: nil)
        }
        else if(mobileNo.characters.count != 10 )
        {
            self.presentViewController(Alert().alert("Please enter valid mobile number", message: ""),animated: true,completion: nil)
        }
            
        else if(registerNameTxtField.text == "" )
        {
            self.presentViewController(Alert().alert("Please enter valid name", message: ""),animated: true,completion: nil)
        }
        else
        {
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            if purzDB.open() {
                let insert = "INSERT INTO BENEFICIARY_MMID (MMID,REGISTERED_NAME,MOBILE_NO) VALUES ('\(MMIDTxtField.text!)','\(registerNameTxtField.text!)','\(mobileNoTxtField.text!)')"
                let result = purzDB.executeUpdate(insert, withArgumentsInArray: nil)
                if !result  {
                    //   status.text = "Failed to add contact"
                    print("Error: \(purzDB.lastErrorMessage())")
                }
            }
        }
        performSegueWithIdentifier("To_MMIDList", sender: self)
        }
        else {
            MMIDTxtField.text = ""
            mobileNoTxtField.text = ""
            registerNameTxtField.text = ""
            self.presentViewController(Alert().alert("Beneficiary MMID already exist!", message: ""), animated: true, completion: nil)
        }
    }

    func initialFunc()
    {
        tabBarController?.delegate = self
        confirmBtn.layer.borderWidth = 2
        confirmBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        confirmBtn.layer.cornerRadius = confirmBtn.frame.size.height/2
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, MMIDTxtField.frame.size.height - 1 , MMIDTxtField.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                MMIDTxtField.borderStyle = UITextBorderStyle.None
        MMIDTxtField.layer.addSublayer(bottomLine)
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, mobileNoTxtField.frame.size.height - 1 , mobileNoTxtField.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                mobileNoTxtField.borderStyle = UITextBorderStyle.None
        mobileNoTxtField.layer.addSublayer(bottomLine1)
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, registerNameTxtField.frame.size.height - 1 , registerNameTxtField.frame.size.width, 1.0)
        bottomLine2.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                registerNameTxtField.borderStyle = UITextBorderStyle.None
        registerNameTxtField.layer.addSublayer(bottomLine2)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GenerateQRViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.

    }
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
      func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        if textFieldToChange == MMIDTxtField
        {
            
            return newLength <= 7
            
        }
      else if textFieldToChange == mobileNoTxtField
        {
            
            return newLength <= 10
            
        }
        
        
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
}
