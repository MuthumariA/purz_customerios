
//
//  AddVPAcontactViewController.swift
//  purZ
//
//  Created by Vertace on 24/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class AddVPAcontactViewController: UIViewController,UITabBarControllerDelegate,UITextFieldDelegate {

    @IBOutlet weak var beneficiaryVPATxtField: UITextField!
    @IBOutlet weak var registeredNameTxtField: UITextField!
    
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var verifyBtn: UIButton!
    
    var errorMsg = ""
    var beneficiaryName = ""
    var existingVPA = [String]()
    
    var attrs = [
        NSFontAttributeName : UIFont.systemFontOfSize(18.0),
        NSForegroundColorAttributeName : UIColor.redColor(),
        NSUnderlineStyleAttributeName : 1]

    
    override func viewDidLoad() {
        super.viewDidLoad()
   initialfunc()
        // Do any additional setup after loading the view.
    }
    @IBAction func confirmBtnAction(sender: AnyObject) {
        confirmBtn.userInteractionEnabled = false
        if self.beneficiaryVPATxtField.text! != "" {
        ApiClient().validatePayeeVirtualAddress(self.beneficiaryVPATxtField.text!) { (success, error, response) in
            if success {
                self.beneficiaryName = response as! String
            }
            else{
                self.errorMsg = response as! String
                self.presentViewController(Alert().alert(response as! String, message: ""),animated: true,completion: nil)
            }
            
        var alreadyexist = false
        for(var i=0;i<self.existingVPA.count;i++) {
            if self.existingVPA[i] == self.beneficiaryVPATxtField.text! {
                alreadyexist = true
            }
        }
        if !alreadyexist {
        let vpa = self.beneficiaryVPATxtField.text!
        if vpa == ""
        {
            self.confirmBtn.userInteractionEnabled = true
            self.presentViewController(Alert().alert("Beneficiary VPA can't be empty",message: ""), animated: true, completion: nil)
        }
       else if self.registeredNameTxtField.text == ""
        {
            self.confirmBtn.userInteractionEnabled = true
            self.presentViewController(Alert().alert("Registered name can't be empty",message: ""), animated: true, completion: nil)
        }
        else if self.errorMsg != ""
        {
            self.confirmBtn.userInteractionEnabled = true
           self.registeredNameTxtField.text = " "
            self.presentViewController(Alert().alert(self.errorMsg,message: ""), animated: true, completion: nil)
             self.errorMsg = ""
        }
            
        else
        {
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            
            if purzDB.open() {
                let insert = "INSERT INTO BENEFICIARY_VPA (VPA,REGISTERED_NAME) VALUES ('\(self.beneficiaryVPATxtField.text!)','\(self.registeredNameTxtField.text!)')"
                let result = purzDB.executeUpdate(insert, withArgumentsInArray: nil)
                if !result  {
                    //   status.text = "Failed to add contact"
                    self.confirmBtn.userInteractionEnabled = true
                    print("Error: \(purzDB.lastErrorMessage())")
                }
            }
            self.performSegueWithIdentifier("To_ListVPA", sender: self)
        }
        }
        else {
            self.confirmBtn.userInteractionEnabled = true
            self.presentViewController(Alert().alert("Beneficiary VPA already exist!", message: ""), animated: true, completion: nil)
            self.beneficiaryVPATxtField.text = ""
            self.registeredNameTxtField.text = ""
        }
    }
    }
        else {
            self.confirmBtn.userInteractionEnabled = true
            self.presentViewController(Alert().alert("Beneficiary VPA can't be empty",message: ""), animated: true, completion: nil)
        }
    }
    
    @IBAction func verifyBtnAtn(sender: AnyObject) {
        registeredNameTxtField.text = ""
        verifyPayeeName()
    }
    func initialfunc()
    {
         let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
          verifyBtn.titleLabel?.attributedText = NSAttributedString(string: "Verify", attributes: underlineAttribute)
        registeredNameTxtField.userInteractionEnabled = false
        tabBarController?.delegate = self
        confirmBtn.layer.borderWidth = 2
        confirmBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        confirmBtn.layer.cornerRadius = confirmBtn.frame.size.height/2
        beneficiaryVPATxtField.delegate = self
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, beneficiaryVPATxtField.frame.size.height - 1 , beneficiaryVPATxtField.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                beneficiaryVPATxtField.borderStyle = UITextBorderStyle.None
        beneficiaryVPATxtField.layer.addSublayer(bottomLine1)
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, registeredNameTxtField.frame.size.height - 1 , registeredNameTxtField.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                registeredNameTxtField.borderStyle = UITextBorderStyle.None
        registeredNameTxtField.layer.addSublayer(bottomLine)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GenerateQRViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    func dismissKeyboard(){
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    func verifyPayeeName()
    {
        registeredNameTxtField.text = ""
        ApiClient().validatePayeeVirtualAddress(self.beneficiaryVPATxtField.text!) { (success, error, response) in
            if success {
                self.beneficiaryName = response as! String
                self.registeredNameTxtField.text = self.beneficiaryName
            }
            else{
                self.errorMsg = response as! String
                if self.errorMsg == "rc message not configured" {
                    self.presentViewController(Alert().alert("VPA not present", message: ""),animated: true,completion: nil)
                }
                else {
                    self.presentViewController(Alert().alert(response as! String, message: ""),animated: true,completion: nil)
                }
            }
        }

    }
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {

        if textFieldToChange == beneficiaryVPATxtField{
        registeredNameTxtField.text = ""
        }
        
        return true
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }


}
