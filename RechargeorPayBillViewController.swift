//
//  RechargeorPayBillViewController.swift
//  Cippy
//
//  Created by apple on 24/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit

class RechargeorPayBillViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var badgeBtn: UIButton!
    @IBOutlet weak var `switch`: UISwitch!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var prepaidlbl: UILabel!
    @IBOutlet weak var postpaidlbl: UILabel!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var selectproviderBtn: UIButton!
    @IBOutlet weak var radioView: UIView!
    @IBOutlet weak var topupradio: UIButton!
    @IBOutlet weak var specialradio: UIButton!
    @IBOutlet weak var mobilenotxtField: UITextField!
    @IBOutlet weak var amounttxtField: UITextField!
    @IBOutlet weak var rechargeorpaybtn: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView2: UIScrollView!
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var customeridtxtfield: UITextField!
    @IBOutlet weak var dthamounttxtfield: UITextField!
    @IBOutlet weak var dthrechargebtn: UIButton!
    
    var pre_operatorCode = [String]()
    var pre_operatorName = [String]()
    var pre_operatorId = [String]()
    var pre_operatorType = [String]()
    var pre_special = [Bool]()
    
    var post_operatorCode = [String]()
    var post_operatorName = [String]()
    var post_operatorId = [String]()
    var post_operatorType = [String]()
    var post_special = [Bool]()
    
    var dth_operatorCode = [String]()
    var dth_operatorName = [String]()
    var dth_operatorId = [String]()
    var dth_operatorType = [String]()
    var dth_special = [Bool]()
    var radio = 0
    var switchvalue = "PREPAID"
    var characterCountLimit = 10
    var providername = "Select Provider"
    var operatorcode = ""
    var special = false
    var recharge_amount = ""
    var recharge_number = ""
    var fromotp = false
    var radioview = false
    
    var dth = false
    var mobile = true
    var dthprovidername = "SELECT"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initialfunc()
        badgeBtn.userInteractionEnabled = false
        //        scrollView.scrollEnabled = false
        //        scrollView2.scrollEnabled = false
        
    }
    
    func initialfunc(){
        
        let attrs = [NSFontAttributeName:UIFont(name: "Quicksand-Bold", size: 14.0)!]
        segmentController.setTitleTextAttributes(attrs, forState: UIControlState.Normal)
        badgeBtn.layer.cornerRadius  = self.badgeBtn.frame.size.height/2
        if(Appconstant.notificationcount > 0){
            badgeBtn.hidden = false
            badgeBtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
        }
        else{
            badgeBtn.hidden = true
        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        scrollView.addGestureRecognizer(tap)
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        scrollView2.addGestureRecognizer(tap1)
        
        (segmentController.subviews[0] as UIView).tintColor = UIColor.grayColor()
        (segmentController.subviews[0] as UIView).backgroundColor = UIColor(red: 234.0/255.0, green: 234.0/255.0, blue: 234.0/255.0, alpha: 1)
        selectproviderBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        selectBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        radioView.hidden = true
        rechargeorpaybtn.layer.cornerRadius = 10
        dthrechargebtn.layer.cornerRadius = 10
        scrollView2.hidden = true
        if fromotp{
            if  dth{
                segmentController.selectedSegmentIndex = 1
                (segmentController.subviews[0] as UIView).backgroundColor = UIColor.whiteColor()
                (segmentController.subviews[0] as UIView).tintColor = UIColor(red: 42.0/255.0, green: 203.0/255.0, blue: 229.0/255.0, alpha: 1)
                (segmentController.subviews[1] as UIView).tintColor = UIColor.grayColor()
                (segmentController.subviews[1] as UIView).backgroundColor = UIColor(red: 234.0/255.0, green: 234.0/255.0, blue: 234.0/255.0, alpha: 1)
                scrollView2.hidden = false
                scrollView.hidden = true
                self.customeridtxtfield.text = recharge_number
                self.dthamounttxtfield.text = recharge_amount
                selectBtn.setTitle(dthprovidername, forState: .Normal)
            }
            else{
                scrollView2.hidden = true
                scrollView.hidden = false
                self.mobilenotxtField.text = recharge_number
                self.amounttxtField.text = recharge_amount
                selectproviderBtn.setTitle(providername, forState: .Normal)
                if(switchvalue == "PREPAID"){
                    rechargeorpaybtn.setTitle("PREPAID", forState: .Normal)
                }
                else{
                    rechargeorpaybtn.setTitle("PAY", forState: .Normal)
                }
                if(radioview){
                    if special{
                        specialradio.setImage(UIImage(named: "radio-on-button.png"), forState: .Normal)
                        topupradio.setImage(UIImage(named: "Radio.png"), forState: .Normal)
                    }
                    else{
                        topupradio.setImage(UIImage(named: "radio-on-button.png"), forState: .Normal)
                        specialradio.setImage(UIImage(named: "Radio.png"), forState: .Normal)
                    }
                }
            }
            
            
            callserverForRecharge()
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == mobilenotxtField{
            amounttxtField.becomeFirstResponder()
        }
        if textField == customeridtxtfield{
            dthamounttxtfield.becomeFirstResponder()
        }
        
        return true // We do not want UITextField to insert line-breaks.
    }
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 550)
        scrollView2.contentSize = CGSize(width: self.view.frame.size.width, height: 500)
        if(textFieldToChange == mobilenotxtField){
            characterCountLimit = 10
        }
        else if(textFieldToChange.placeholder == "Enter 4 digit Password"){
            characterCountLimit = 4
        }
        else{
            characterCountLimit = 50
        }
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    }
    
    @IBAction func segmentBtnAction(sender: AnyObject) {
        switch segmentController.selectedSegmentIndex {
        case 0:
            (segmentController.subviews[1] as UIView).backgroundColor = UIColor.whiteColor()
            (segmentController.subviews[1] as UIView).tintColor = UIColor(red: 42.0/255.0, green: 203.0/255.0, blue: 229.0/255.0, alpha: 1)
            (segmentController.subviews[0] as UIView).tintColor = UIColor.grayColor()
            (segmentController.subviews[0] as UIView).backgroundColor = UIColor(red: 234.0/255.0, green: 234.0/255.0, blue: 234.0/255.0, alpha: 1)
            scrollView.hidden = false
            scrollView2.hidden = true
            mobile = true
            dth = false
            view.endEditing(true)
        case 1:
            (segmentController.subviews[0] as UIView).backgroundColor = UIColor.whiteColor()
            (segmentController.subviews[0] as UIView).tintColor = UIColor(red: 42.0/255.0, green: 203.0/255.0, blue: 229.0/255.0, alpha: 1)
            (segmentController.subviews[1] as UIView).tintColor = UIColor.grayColor()
            (segmentController.subviews[1] as UIView).backgroundColor = UIColor(red: 234.0/255.0, green: 234.0/255.0, blue: 234.0/255.0, alpha: 1)
            scrollView.hidden = true
            scrollView2.hidden = false
            mobile = false
            dth = true
            view.endEditing(true)
        default:
            break;
        }
    }
    
    @IBAction func switchAction(sender: AnyObject) {
        if `switch`.on{
            
            prepaidlbl.font = UIFont(name: "Quicksand-Regular", size: 17)
            postpaidlbl.font = UIFont(name: "Quicksand-Bold", size: 17)
            rechargeorpaybtn.setTitle("Pay", forState: .Normal)
            switchvalue = "POSTPAID"
            providername = "Select Provider"
            selectproviderBtn.setTitle("Select Provider", forState: .Normal)
            radioView.hidden = true
            
        }
        else{
            
            prepaidlbl.font = UIFont(name: "Quicksand-Bold", size: 17)
            postpaidlbl.font = UIFont(name: "Quicksand-Regular", size: 17)
            rechargeorpaybtn.setTitle("Recharge", forState: .Normal)
            switchvalue = "PREPAID"
            providername = "Select Provider"
            selectproviderBtn.setTitle("Select Provider", forState: .Normal)
            radioView.hidden = true
            
        }
        
    }
    func dismissKeyboard(){
        self.view.endEditing(true)
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 400)
        scrollView2.contentSize = CGSize(width: self.view.frame.size.width, height: 400)
    }
    func textFieldShouldBeginEditing(state: UITextField) -> Bool {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 550)
        scrollView2.contentSize = CGSize(width: self.view.frame.size.width, height: 500)
        return true
        
    }
    //        func textFieldDidEndEditing(textField: UITextField) {
    ////            scrollView.scrollEnabled = false
    ////            scrollView2.scrollEnabled = false
    //            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 550)
    //            scrollView2.contentSize = CGSize(width: self.view.frame.size.width, height: 550)
    //
    //        }
    
    @IBAction func DTHSelectBtnAction(sender: AnyObject) {
        let alertView: UIAlertView = UIAlertView()
        alertView.delegate = self
        alertView.addButtonWithTitle("SELECT")
        for(var i = 0; i<dth_operatorName.count; i++){
            alertView.addButtonWithTitle(dth_operatorName[i])
        }
        
        alertView.show()
    }
    
    @IBAction func selectProviderBtnAction(sender: AnyObject) {
        let alertView: UIAlertView = UIAlertView()
        alertView.delegate = self
        alertView.addButtonWithTitle("Select Provider")
        if(switchvalue == "PREPAID"){
            for(var i = 0; i<pre_operatorName.count; i++){
                alertView.addButtonWithTitle(pre_operatorName[i])
            }
        }
        else if(switchvalue == "POSTPAID"){
            for(var i = 0; i<post_operatorName.count; i++){
                alertView.addButtonWithTitle(post_operatorName[i])
            }
        }
        alertView.show()
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        if dth {
            if buttonIndex == 0{
                dthprovidername = "SELECT"
                selectBtn.setTitle("SELECT", forState: .Normal)
                special = false
            }
            else{
                special = false
                dthprovidername = dth_operatorName[buttonIndex-1]
                selectBtn.setTitle(dth_operatorName[buttonIndex-1], forState: .Normal)
                operatorcode = dth_operatorCode[buttonIndex-1]
            }
        }
        else if mobile{
            if(buttonIndex == 0){
                selectproviderBtn.setTitle("Select Provider", forState: .Normal)
                mobilenotxtField.text = ""
                amounttxtField.text = ""
                providername = "Select Provider"
                radioView.hidden = true
                
            }
            else{
                
                mobilenotxtField.text = ""
                amounttxtField.text = ""
                if(switchvalue == "PREPAID"){
                    selectproviderBtn.setTitle(pre_operatorName[buttonIndex-1], forState: .Normal)
                    providername = pre_operatorName[buttonIndex-1]
                    operatorcode = pre_operatorCode[buttonIndex-1]
                    special = pre_special[buttonIndex-1]
                    if(pre_special[buttonIndex-1]){
                        radioView.hidden = false
                    }
                    else{
                        radioView.hidden = true
                    }
                }
                else if(switchvalue == "POSTPAID"){
                    selectproviderBtn.setTitle(post_operatorName[buttonIndex-1], forState: .Normal)
                    providername = post_operatorName[buttonIndex-1]
                    operatorcode = post_operatorCode[buttonIndex-1]
                    special = post_special[buttonIndex-1]
                    radioView.hidden = true
                    
                }
            }
        }
    }
    @IBAction func topupBtnAction(sender: AnyObject) {
        radio = 0
        topupradio.setImage(UIImage(named: "radio-on-button.png"), forState: .Normal)
        specialradio.setImage(UIImage(named: "Radio.png"), forState: .Normal)
    }
    
    @IBAction func specialBtnAction(sender: AnyObject) {
        radio = 1
        topupradio.setImage(UIImage(named: "Radio.png"), forState: .Normal)
        specialradio.setImage(UIImage(named: "radio-on-button.png"), forState: .Normal)
    }
    
    @IBAction func rechargeorpayBtnAction(sender: AnyObject) {
        if(mobilenotxtField.text!.isEmpty || amounttxtField.text!.isEmpty){
            self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if(providername == "Select Provider"){
            self.presentViewController(Alert().alert("Uh - Oh! Something is wrong. Please check the mobile number and provider name again.", message: ""),animated: true,completion: nil)
        }
        else if(mobilenotxtField.text!.characters.count < 10){
            self.presentViewController(Alert().alert("Uh – Oh! Something is wrong, please check the number and try again.", message: ""),animated: true,completion: nil)
        }
        else if(Double(self.amounttxtField.text!) > Double(Appconstant.mainbalance)){
            self.presentViewController(Alert().alert("Ouch! Insufficient Funds..Add some money instantly, it’s simple!", message: ""),animated: true,completion: nil)
        }
        else{
            alertforproceed()
            
        }
    }
    
    
    
    
    func alertforproceed(){
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "PASSWORD",
            message: "Please enter your 4 digit secret password. This is necessary to initiate every transaction",
            preferredStyle: .Alert)
        alertController!.addTextFieldWithConfigurationHandler(
            {(textField: UITextField!) in
                
                textField.placeholder = "Enter 4 digit Password"
                textField.delegate = self
                textField.secureTextEntry  = true
                textField.keyboardType = UIKeyboardType.NumberPad
                
        })
        let action = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            if let textFields = alertController?.textFields{
                let theTextFields = textFields as [UITextField]
                let password = theTextFields[0].text!
                if((password == "") || (password.characters.count < 4) || (Appconstant.pwd != password)){
                    self!.alert()
                }
                else{
                    if self!.mobile{
                        self!.callserverForRecharge()
                    }
                    else if self!.dth{
                        self!.callserverForDTHRecharge()
                    }
                }
                
            }
            })
        let action1 = UIAlertAction(title: "Forgot?", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            self!.activityIndicator.startAnimating()
            print(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
            self!.sendrequesttoserverForForgotPassword(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
            
            })
        let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            })
        
        alertController?.addAction(action)
        alertController?.addAction(action1)
        alertController?.addAction(action2)
        self.presentViewController(alertController!, animated: true, completion: nil)
    }
    
    func callserverForRecharge(){
        if(radio == 0 && switchvalue == "PREPAID"){
            special = false
        }
        else{
            special = true
        }
        var description = ""
        if(self.switchvalue == "PREPAID"){
            description = "Prepaid Recharge for " + self.mobilenotxtField.text!
        }
        else if(self.switchvalue == "POSTPAID"){
            description = "Postpaid Recharge for " + self.mobilenotxtField.text!
        }
        let rechargeviewmodel = RechargeViewModel.init(amount: self.amounttxtField.text!, operatorCode: self.operatorcode, comment: "", number: self.mobilenotxtField.text!, yapcode: Appconstant.pwd, fromEntityId: Appconstant.customerid, productId: "GENERAL", transactionOrigin: "MOBILE", business: "DCBWALLET", toEntityId: "DCBWALLETRECHARGE", businessEntityId: "DCBWALLET", externalTransactionId: "123456", description: description, special: self.special, transactionType: "TPP")!
        let serializedjson  = JSONSerializer.toJson(rechargeviewmodel)
        print(serializedjson)
        self.activityIndicator.startAnimating()
        self.sendrequesttoserverForRechargeorPay(Appconstant.BASE_URL+Appconstant.URL_RECHARGE, values: serializedjson)
    }
    
    func sendrequesttoserverForRechargeorPay(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print(error)
                    self.activityIndicator.stopAnimating()
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                else{
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                    self.updatetransactionDB()
                    let json = JSON(data: data!)
                    self.activityIndicator.stopAnimating()
                    //                    let item = json["result"]
                    if(json["result"] != ""){
                        dispatch_async(dispatch_get_main_queue()) {
                            
                            var alertController:UIAlertController?
                            alertController?.view.tintColor = UIColor.blackColor()
                            alertController = UIAlertController(title: "Recharge",
                                message: "Swag! Your recharge is done..",
                                preferredStyle: .Alert)
                            
                            let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                                dispatch_async(dispatch_get_main_queue()) {
                                    self!.performSegueWithIdentifier("recharge_home", sender: self)
                                }
                                
                                })
                            
                            alertController?.addAction(action)
                            self.presentViewController(alertController!, animated: true, completion: nil)
                        }
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Recharge failed. Please try again later", message: ""),animated: true,completion: nil)
                        }
                    }
                }
        }
        
        task.resume()
        
    }
    
    func sendrequesttoserverForForgotPassword(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    self.activityIndicator.stopAnimating()
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    self.activityIndicator.stopAnimating()
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                self.activityIndicator.stopAnimating()
                let item = json["result"]
                if(item["success"].stringValue == "true"){
                    dispatch_async(dispatch_get_main_queue()) {
                        Appconstant.otp = item["otp"].stringValue
                        self.performSegueWithIdentifier("recharge_otp", sender: self)
                    }
                }
                    
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        }
                    }
                }
        }
        
        task.resume()
        
    }
    
    
    func alert(){
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(Alert().alert("Please enter a valid Password", message: ""),animated: true,completion: nil)
        }
    }
    
    @IBAction func DTHRechargeBtnAction(sender: AnyObject) {
        if(customeridtxtfield.text!.isEmpty || dthamounttxtfield.text!.isEmpty){
            self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if(dthprovidername == "SELECT"){
            self.presentViewController(Alert().alert("Uh - Oh! Something is wrong. Please check the provider name again.", message: ""),animated: true,completion: nil)
        }
        else if(Double(self.dthamounttxtfield.text!) > Double(Appconstant.mainbalance)){
            self.presentViewController(Alert().alert("Ouch! Insufficient Funds..Add some money instantly, it’s simple!", message: ""),animated: true,completion: nil)
        }
        else{
            alertforproceed()
        }
    }
    
    func callserverForDTHRecharge(){
        
        let description = "DTH Recharge for  " + self.mobilenotxtField.text!
        
        let rechargeviewmodel = RechargeViewModel.init(amount: self.dthamounttxtfield.text!, operatorCode: self.operatorcode, comment: "", number: self.customeridtxtfield.text!, yapcode: Appconstant.pwd, fromEntityId: Appconstant.customerid, productId: "GENERAL", transactionOrigin: "MOBILE", business: "DCBWALLET", toEntityId: "DCBWALLETRECHARGE", businessEntityId: "DCBWALLET", externalTransactionId: "123456", description: description, special: self.special, transactionType: "TPP")!
        let serializedjson  = JSONSerializer.toJson(rechargeviewmodel)
        print(serializedjson)
        self.activityIndicator.startAnimating()
        self.sendrequesttoserverForRechargeorPay(Appconstant.BASE_URL+Appconstant.URL_RECHARGE, values: serializedjson)
    }
    
    
    func updatetransactionDB(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        
        if purzDB.open() {
            let delete = "DELETE FROM TRANSACTIONS"
            
            let result = purzDB.executeUpdate(delete,
                withArgumentsInArray: nil)
            
            if !result{
                //   status.text = "Failed to add contact"
                print("Error: \(purzDB.lastErrorMessage())")
            }
            
        }
        purzDB.close()
        
        let request = NSMutableURLRequest(URL: NSURL(string: Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=10")!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    self.view.userInteractionEnabled = true
                    
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    //                    dispatch_async(dispatch_get_main_queue()) {
                    //                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    //
                    //                    }
                    
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                DBHelper().purzDB()
                let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
                let databasePath = databaseURL.absoluteString
                let purzDB = FMDatabase(path: databasePath as String)
                
                for item1 in json["result"].arrayValue{
                    let item = item1["transaction"]
                    
                    var transactionamt = ""
                    var ben_id = ""
                    var trans_date = ""
                    var descriptions = ""
                    let balance_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                    let amt = balance_two_decimal.componentsSeparatedByString(".")
                    if(amt[1].characters.count == 1){
                        let finalamount = balance_two_decimal + "0"
                        transactionamt = finalamount
                    }
                    else{
                        transactionamt = balance_two_decimal
                    }
                    if(!item["beneficiaryId"].stringValue.isEmpty){
                        let benid = item["beneficiaryId"].stringValue.componentsSeparatedByString("+91")
                        print(benid)
                        var i = 0
                        for(i=0; i<benid.count; i++){
                            
                        }
                        ben_id = benid[i-1]
                    }
                    else{
                        ben_id = item["beneficiaryId"].stringValue
                    }
                    let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "dd MMM"
                    dateFormatter.timeZone = NSTimeZone(name: "UTC")
                    let dateString = dateFormatter.stringFromDate(date)
                    if(!dateString.isEmpty){
                        let datearray = dateString.componentsSeparatedByString(" ")
                        if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                            let correctdate = datearray[0] + "st " + datearray[1]
                            trans_date = correctdate
                        }
                        else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                            let correctdate = datearray[0] + "nd " + datearray[1]
                            trans_date = correctdate
                        }
                        else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                            let correctdate = datearray[0] + "rd " + datearray[1]
                            trans_date = correctdate
                        }
                        else{
                            let correctdate = datearray[0] + "th " + datearray[1]
                            trans_date = correctdate
                        }
                        
                    }
                    let matches = self.matchesForRegexInText("[0-9]", text: item["description"].stringValue)
                    let desc = matches.joinWithSeparator("")
                    descriptions = desc
                    
                    if purzDB.open() {
                        
                        let insert = "INSERT INTO TRANSACTIONS (AMOUNT,BENEFICIARY_ID,TRANSACTION_TYPE,TYPE,TIME,TRANSACTION_STATUS,TX_REF,BENEFICIARY_NAME,DESCRIPTION,OTHER_PARTY_NAME,OTHER_PARTY_ID,TXN_ORIGIN) VALUES"
                        let value0 =  "('"+transactionamt+"','\(ben_id)','\(item["transactionType"].stringValue)','\(item["type"].stringValue)',"
                        let value1 = "'"+trans_date+"','\(item["transactionStatus"].stringValue)','\(item["txRef"].stringValue)','\(item["beneficiaryName"].stringValue)',"
                        let value2 = "'\(descriptions)','\(item["otherPartyName"].stringValue)','\(item["otherPartyId"].stringValue)','\(item["txnOrigin"].stringValue)')"
                        let insertsql = insert+value0+value1+value2
                        let result = purzDB.executeUpdate(insertsql,
                            withArgumentsInArray: nil)
                        
                        if !result {
                            //   status.text = "Failed to add contact"
                            print("Error: \(purzDB.lastErrorMessage())")
                            dispatch_async(dispatch_get_main_queue()) {
                                self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                                
                            }
                        }
                        purzDB.close()
                        
                    }
                }
        }
        
        task.resume()
        
        
    }
    func matchesForRegexInText(regex: String, text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        }
        catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "recharge_otp") {
            let nextview = segue.destinationViewController as! OTPViewController
            nextview.fromrecharge = true
            if mobile{
                nextview.recharge_amount = self.amounttxtField.text!
                nextview.recharge_number = self.mobilenotxtField.text!
                nextview.recharge_radio = radio
                nextview.providername = providername
                nextview.dth = false
            }
            else if dth{
                nextview.recharge_amount = self.dthamounttxtfield.text!
                nextview.recharge_number = self.customeridtxtfield.text!
                nextview.dthprovidername = self.dthprovidername
                nextview.dth = true
            }
            nextview.recharge_operatorcode = operatorcode
            nextview.recharge_switchvalue = switchvalue
            nextview.recharge_special = special
            
            if(radioView.hidden == true){
                nextview.radioview = false
            }
            else{
                nextview.radioview = true
            }
            
            
            nextview.pre_operatorCode = self.pre_operatorCode
            nextview.pre_operatorName = self.pre_operatorName
            nextview.pre_operatorId = self.pre_operatorId
            nextview.pre_operatorType = self.pre_operatorType
            nextview.pre_special = self.pre_special
            nextview.post_operatorCode = self.post_operatorCode
            nextview.post_operatorName = self.post_operatorName
            nextview.post_operatorId = self.post_operatorId
            nextview.post_operatorType = self.post_operatorType
            nextview.post_special = self.post_special
            nextview.dth_operatorCode = self.dth_operatorCode
            nextview.dth_operatorName = self.dth_operatorName
            nextview.dth_operatorId = self.dth_operatorId
            nextview.dth_operatorType = self.dth_operatorType
            nextview.dth_special = self.dth_special
        }
    }
    
    
    
    
    
    
    
    
    
}
