
//
//  PaymentSuccessfulViewController.swift
//  Cippy
//
//  Created by apple on 28/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit

class PaymentSuccessfulViewController: UIViewController,UITabBarControllerDelegate {

    
    @IBOutlet weak var tohomeBtn: UIButton!
    @IBOutlet weak var badgebtn: UIButton!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var paidamountlbl: UILabel!
    @IBOutlet weak var balancelbl: UILabel!
    
    var transactionamt = [String]()
    var txref = ""
    var ben_name = ""
    var time = ""
    var splitamount = ""
    override func viewDidLoad() {
        super.viewDidLoad()
          tabBarController?.delegate = self
        dispatch_async(dispatch_get_main_queue()) {
            self.getrecenttransaction(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=1")
        }

        // Do any additional setup after loading the view, typically from a nib.
        badgebtn.userInteractionEnabled = false
        badgebtn.layer.cornerRadius = self.badgebtn.frame.size.height/2
        if(Appconstant.notificationcount > 0){
            badgebtn.hidden = false
            badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
        }
        else{
            badgebtn.hidden = true
        }
           }
    
    
    @IBAction func BackBtnAction(sender: AnyObject) {
        self.performSegueWithIdentifier("To_Home", sender: self)
    }
    @IBAction func backtodashboardBtnAction(sender: AnyObject) {
        self.performSegueWithIdentifier("To_Home", sender: self)
    }
    
    
    func getrecenttransaction(url: String){
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
//                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                    }
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                self.transactionamt.removeAll()
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                for item1 in json["result"].arrayValue{
                    let item = item1["transaction"]
                    
                    let balance_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                    self.paidamountlbl.text = "\u{20B9}" + balance_two_decimal
                    self.splitamount = balance_two_decimal
                    
                    
                    let balance_two_decimal1 = String(format: "%.2f", item["balance"].doubleValue)
                                   
                    self.balancelbl.text = self.balancelbl.text! + " \u{20B9}" + balance_two_decimal1
                    
                    
                    self.namelbl.text = item["beneficiaryName"].stringValue
                    let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                    dispatch_async(dispatch_get_main_queue()) {
    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "dd MMM"
                    dateFormatter.timeZone = NSTimeZone(name: "UTC")
                    let dateString = dateFormatter.stringFromDate(date)
                    if(!dateString.isEmpty){
                        let datearray = dateString.componentsSeparatedByString(" ")
                        if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                            let correctdate = datearray[0] + "st " + datearray[1]
                            self.datelbl.text = correctdate
                        }
                        else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                            let correctdate = datearray[0] + "nd " + datearray[1]
                            self.datelbl.text = correctdate
                        }
                        else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                            let correctdate = datearray[0] + "rd " + datearray[1]
                            self.datelbl.text = correctdate
                        }
                        else{
                            let correctdate = datearray[0] + "th " + datearray[1]
                            self.datelbl.text = correctdate
                        }
                        self.time = self.datelbl.text!
                    }
                    self.txref = item["txRef"].stringValue
                    self.ben_name = item["beneficiaryName"].stringValue
                    
                }
                
                }
                
        }
        
        task.resume()
    }
    
    @IBAction func splitbillBtnAction(sender: AnyObject) {
//        dispatch_async(dispatch_get_main_queue()) {
//            self.presentViewController(Alert().alert("Coming Soon...", message: ""),animated: true,completion: nil)
//            
//        }
        self.performSegueWithIdentifier("payslip_splitbill", sender: self)
    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if(segue.identifier == "payslip_splitbill") {
//            let nextview = segue.destinationViewController as! SplitBillViewController
//            nextview.txref = txref
//            nextview.ben_name = ben_name
//            nextview.time = time
//            nextview.mainamount = splitamount
//            nextview.fromslip = true
//        }
//    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }


}
