//
//  WebViewController.swift
//  Cippy
//
//  Created by apple on 19/12/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController,UIWebViewDelegate ,WKNavigationDelegate, UITabBarControllerDelegate {

    
    @IBOutlet weak var webView: UIWebView!
    var webView1 : WKWebView!
    var alertmsg = ""
    
    var fromSendMoney = false
    var fromPayMerchant = false
    var fromRecharge = false
    
    

    @IBOutlet weak var hoembutton: UIButton!
    var back = UIBarButtonItem()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
          tabBarController?.delegate = self
        hoembutton.userInteractionEnabled=false
        hoembutton.hidden=true

        webView.delegate = self
        print(Appconstant.Url)
        let urlstrs: NSString = "https://d10uyic6ac0ktu.cloudfront.net/"
            //Appconstant.Url
        let urlStr : NSString = urlstrs.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let url = NSURL(string: urlStr as String)!
        let requestObj = NSURLRequest(URL: url);
        webView.loadRequest(requestObj)
    
    }
    
    @IBAction func backBtnAction(sender: AnyObject) {
        Appconstant.fromweb = true
        if fromSendMoney || fromPayMerchant || fromRecharge{
            self.navigationController?.popViewControllerAnimated(true)
        }
        else {
            self.performSegueWithIdentifier("back_to_addmoney", sender: self)
        }
    }
    
    

    func webViewDidStartLoad(webView: UIWebView)
    {
    
    }
    func webViewDidFinishLoad(webView: UIWebView) {
    
            let doc = webView.stringByEvaluatingJavaScriptFromString("document.documentElement.outerHTML")

        let result:Bool = !(doc!.rangeOfString("\"result\"")==nil);
        if result{
            let str = doc?.componentsSeparatedByString("<pre style=\"word-wrap: break-word; white-space: pre-wrap;\">")
            let str1 = str![1]
            let finalstr = str1.componentsSeparatedByString("</pre>")
            let encodedString : NSData = (finalstr[0] as NSString).dataUsingEncoding(NSUTF8StringEncoding)!
            
            
            
            let finalJSON = JSON(data: encodedString)
            print(finalJSON)
            let item = finalJSON["result"]
            if item["authStatus"].stringValue == "0300"{
                 backtoMainView("Yeh! Money loaded successfully")
            }
            else if item["authStatus"].stringValue == "0399"{
                 backtoMainView("Sorry! This transaction failed.")
            }
            else if item["authStatus"].stringValue == "NA"{
                 backtoMainView("This happens sometimes, please try again")
            }
            else if item["authStatus"].stringValue == "0002"{
                 backtoMainView("BillDesk is waiting for Response from Bank")
            }
            else if item["authStatus"].stringValue == "0001"{
                 backtoMainView("Error at BillDesk")
            }
            else if item["authStatus"].stringValue == "0000"{
                 backtoMainView("Invalid payment request")
            }
            else{
                let mtdpresent:Bool = !(doc!.rangeOfString("methodName")==nil);
                if mtdpresent{
                     backtoMainView("Huh! Something went wrong.. Please try again later")
                }
                else{
                    let exception:Bool = !(doc!.rangeOfString("\"exception\"")==nil);
                    if(exception)
                    {
                        let str = doc?.componentsSeparatedByString("<pre style=\"word-wrap: break-word; white-space: pre-wrap;\">")
                        let str1 = str![1]
                        let finalstr = str1.componentsSeparatedByString("</pre>")
                        let encodedString : NSData = (finalstr[0] as NSString).dataUsingEncoding(NSUTF8StringEncoding)!
                        
                        let finalJSON = JSON(data: encodedString)
                        print("finalJSON")
                        print(finalJSON)
                        let item = finalJSON["exception"]
                        backtoMainView("Sorry! This transaction failed. This happens sometimes,please try again")
                        
                    }
                }
            }
            
            
            
        }
        else{
        let exception:Bool = !(doc!.rangeOfString("\"exception\"")==nil);
            if(exception)            
            {
            let str = doc?.componentsSeparatedByString("<pre style=\"word-wrap: break-word; white-space: pre-wrap;\">")
            let str1 = str![1]
            let finalstr = str1.componentsSeparatedByString("</pre>")
            let encodedString : NSData = (finalstr[0] as NSString).dataUsingEncoding(NSUTF8StringEncoding)!
            
            let finalJSON = JSON(data: encodedString)
            print(finalJSON)
            let item = finalJSON["exception"]
       
                 Appconstant.alertmsg =  item["detailMessage"].stringValue
                Appconstant.fromweb = true
            }
            
        }
        
    }
    
    
    func backtoMainView(alertmsg: String) {
        Appconstant.alertmsg = alertmsg
        Appconstant.fromweb = true
        if fromSendMoney || fromPayMerchant || fromRecharge{
            self.navigationController?.popViewControllerAnimated(true)
        }
        else {
            self.performSegueWithIdentifier("web_home", sender: self)
        }
    }
//    func webViewDidStartLoad(webView: UIWebView, decidePolicyForNavigationResponse navigationResponse: WKNavigationResponse, decisionHandler: (WKNavigationResponsePolicy) -> Void) {
//        print("start")
//    }

    func webView(webView: WKWebView, decidePolicyForNavigationResponse navigationResponse: WKNavigationResponse, decisionHandler: (WKNavigationResponsePolicy) -> Void) {
 //     make sure the response is a NSHTTPURLResponse
        guard let response = navigationResponse.response as? NSHTTPURLResponse else { return decisionHandler(.Allow) }
       
    
        print(response.URL)
        
        let url = (response.URL?.absoluteString)! as String
            let gotCode:Bool = !(url.rangeOfString("/processor/payment")==nil);
        if(gotCode)
        {
            print("payment");
          
        }
        
        // allow the request to continue
        decisionHandler(.Allow);
    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
}
