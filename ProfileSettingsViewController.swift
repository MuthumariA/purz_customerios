//
//  ProfileSettingsViewController.swift
//  Cippy
//
//  Created by apple on 26/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit
import CoreLocation

class ProfileSettingsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITabBarControllerDelegate, CLLocationManagerDelegate, ImageCropViewControllerDelegate {
    
    @IBOutlet weak var idTxtField: UITextField!
    @IBOutlet weak var IdTypeBtn: UIButton!
    @IBOutlet weak var namelbl: UITextField!
//    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var badgebtn: UIButton!
    @IBOutlet weak var profilebtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var savebtn: UIButton!
    //   @IBOutlet weak var forgotpwdbtn: UIButton!
    @IBOutlet weak var addr1txtField: UITextField!
    @IBOutlet weak var addr2txtField: UITextField!
    @IBOutlet weak var citytxtField: UITextField!
    @IBOutlet weak var statetxtField: UITextField!
    @IBOutlet weak var pintxtField: UITextField!
    @IBOutlet weak var securequestiontxtField: UITextField!
    @IBOutlet weak var secureanstxtField: UITextField!
    @IBOutlet weak var dobtxtField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var profileimg: UIImageView!
    let imagePicker = UIImagePickerController()
    var strBase64 = ""
    var profileimgstr = ""
    var datePickerView:UIDatePicker!
    var characterCountLimit = 6
    var date = ""
    var calendar : NSCalendar = NSCalendar.currentCalendar()
    var toolbarView = UIView()
    var imageView = UIImageView()
    var imageCropView = ImageCropView()
    var image : UIImage? = UIImage()
     var image1 = [String]()
//    var cropimage : UIImage? = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        idTxtField.userInteractionEnabled = false
      
//   if Appconstant.FromProfile_IDType
//        {
//            
//            Appconstant.FromProfile_IDType = false
//            IdTypeBtn.setTitle(Appconstant.IdType, forState: .Normal)
//IdTypeBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
//
//        }
        
               print(self.navigationController?.viewControllers)
        //        if let topController = UIApplication.topViewController()
        //        {
        //            print(topController)
        //            print(topController.presentedViewController)
        //
        //        }
        

        profileimg.clipsToBounds = true
        profileimg.layer.borderWidth = 3
        profileimg.layer.borderColor = UIColor(red:230/255.0, green:175/255.0, blue:7/255.0, alpha:1.0).CGColor;                  
        
        navigationController?.navigationBarHidden = true
        tabBarController?.hidesBottomBarWhenPushed = false
        tabBarController?.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
        setvaluefortetfield()
        badgebtn.userInteractionEnabled = false
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 600)
        
  
        
       
    }
    
    @IBAction func getTypeFunc(sender: AnyObject) {
        Appconstant.FromProfile_IDType = true
        self.performSegueWithIdentifier("To_IDType", sender: self)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
               print(self.navigationController?.viewControllers)
        if Appconstant.fromcropimage == true
        {
            Appconstant.fromcropimage = false
            databaseForGettingProfileImage()
        }
    }
    
    @IBAction func backBtnAction(sender: AnyObject) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    func setvaluefortetfield(){
        
              namelbl.text = Appconstant.customername
        badgebtn.layer.cornerRadius = self.badgebtn.frame.size.height/2
        profilebtn.layer.cornerRadius = self.profilebtn.frame.size.height/2
        profileimg.layer.cornerRadius = self.profileimg.frame.size.height/2
        profileimg.clipsToBounds = true
        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRectMake(0.0, namelbl.frame.height - 1 , namelbl.frame.width, 1.0)
        bottomLine4.backgroundColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha:1.0).CGColor;
        namelbl.borderStyle = UITextBorderStyle.None
        namelbl.layer.addSublayer(bottomLine4)
        savebtn.layer.cornerRadius = 10
        
        


        
        //    forgotpwdbtn.layer.cornerRadius = 10
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileSettingsViewController.dismissKeyboard))
        scrollView.addGestureRecognizer(tap)
        imagePicker.delegate = self
        if(Appconstant.notificationcount > 0){
            badgebtn.hidden = false
            badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
        }
        else{
            badgebtn.hidden = true
        }
       setvaluestoall()
        
    }
    
    func setvaluestoall(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            print(Appconstant.customerid)
            let selectSQL = "SELECT * FROM CUSTOMERDETAIL WHERE CUSTOMER_ID=" + Appconstant.customerid
            let selectsql1 = "SELECT * FROM IMAGETABLE"
            let result:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                withArgumentsInArray: nil)
          
            let result1:FMResultSet! = purzDB.executeQuery(selectsql1,
                                                          withArgumentsInArray: nil)
          
            if result1.next() {
                profileimgstr = result1.stringForColumn("IMAGE_PATH")
            }
          
            if (result.next()){
               
                date = result.stringForColumn("DATE_OF_BIRTH")
                addr1txtField.text = result.stringForColumn("ADDRESS_LINE_1")
                addr2txtField.text = result.stringForColumn("ADDRESS_LINE_2")
                citytxtField.text = result.stringForColumn("CITY")
                statetxtField.text = result.stringForColumn("STATE")
                pintxtField.text = result.stringForColumn("PIN")
           
                IdTypeBtn.setTitle(result.stringForColumn("ID_TYPE"), forState: .Normal)
                IdTypeBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
                idTxtField.text = result.stringForColumn("ID_NUMBER")
                
//                 checkAddress()
                
                
                if result.stringForColumn("SECURITY_QUESTION") == nil {
                    
                }
                else{
                    if(result.stringForColumn("SECURITY_QUESTION").isEmpty || result.stringForColumn("SECURITY_QUESTION") == "null"){
                    }
                    else{
                        securequestiontxtField.text = result.stringForColumn("SECURITY_QUESTION")
                    }
                    if(result.stringForColumn("SECURITY_ANSWER").isEmpty || result.stringForColumn("SECURITY_ANSWER") == "null"){
                    }
                    else{
                        secureanstxtField.text = result.stringForColumn("SECURITY_ANSWER")
                    }
                }
                
                if date.isEmpty || date == "empty"{
                    dobtxtField.text = "DOB    " + "dd/mm/yy"
                }
                    
                else{
                    
                    let deFormatter = NSDateFormatter()
                    deFormatter.dateFormat = "yyyy-MM-dd"
                    print(date)
                    let startTime = deFormatter.dateFromString(date)!
                    
                    let formatter = NSDateFormatter()
                    formatter.dateFormat = "dd/MM/yy"
                    let date1 = formatter.stringFromDate(startTime)
                    dobtxtField.text = "DOB    " + date1
                }
                if(profileimgstr == "empty" || profileimgstr.isEmpty){
                    
                }
                else{
                        if self.profileimg.image?.imageOrientation == UIImageOrientation.Up {
                        
                    }
                    else{
                        UIGraphicsBeginImageContextWithOptions((self.profileimg.image?.size)!, false, (self.profileimg.image?.scale)!)
                        self.profileimg.image?.drawInRect(CGRectMake(0, 0, (self.profileimg.image?.size.width)!, (self.profileimg.image?.size.height)!))
                        self.profileimg.image = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        
                    }

                    
                    let dataDecoded:NSData = NSData(base64EncodedString: profileimgstr, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
                    let img = UIImage(data: dataDecoded)!
                    //                    profilebtn.setImage(img, forState: UIControlState.Normal)
                    profileimg.image = img
                    self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
                    profileimg.clipsToBounds = true
                    profileimg.layer.borderWidth = 3
                    profileimg.layer.borderColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                }
            }
                
                
                
                
            else{
                //   status.text = "Failed to add contact"
                print("Error: \(purzDB.lastErrorMessage())")
            }
           
        }
        purzDB.close()
    }
    
    
    func checkAddress() {
        if(self.pintxtField.text! != "")
        {
            let location: String = pintxtField.text!
            let geocoder: CLGeocoder = CLGeocoder()
            geocoder.geocodeAddressString(location, completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
                if placemarks != nil{
                    if ((placemarks?.count)! > 0) {
                        let placemark: CLPlacemark = (placemarks?[0])!
                        print(placemark)
                        let country : String = placemark.country!
                        let state: String = placemark.administrativeArea!
                        let district: String = placemark.subAdministrativeArea!
                        let place:String = placemark.subLocality!
                        let pincode:String = placemark.postalCode!
                        print(state);
                        print(country)
                        print(place)
                        if(self.pintxtField.text != placemark.postalCode!  ){
                            self.presentViewController(Alert().alert("Pincode is Invalid", message: ""),animated: true,completion: nil)
                        }
                            //                else if(self.addr2txtField.text?.lowercaseString != place.lowercaseString)
                            //                    {
                            //                        print(self.addr2txtField.text?.lowercaseString)
                            //                        print(place.lowercaseString)
                            //                        self.presentViewController(Alert().alert("Oops! Enter a valid placename as \(place) ", message: ""),animated: true,completion: nil)
                            //                    }
                        
                        if self.addr2txtField.text!.lowercaseString != place.lowercaseString {
                            self.presentViewController(Alert().alert("Your place name is wrong", message: ""),animated: true,completion: nil)
                        }
                        if(self.citytxtField.text!.lowercaseString != district.lowercaseString)
                        {
                            self.presentViewController(Alert().alert("Your City name is wrong", message: ""),animated: true,completion: nil)
                        }
                            
                            
                        if(self.statetxtField.text!.lowercaseString != state.lowercaseString)
                        {
                            self.presentViewController(Alert().alert("Your State name is wrong", message: ""),animated: true,completion: nil)
                        }
                    }
                }
                } as! CLGeocodeCompletionHandler)
        }
    }
    
    func checkValidExpression(numberText:String, myPattern:String)-> String
    {
        print(numberText)
        var myString = "\(numberText)"
        let regex = try! NSRegularExpression(pattern: myPattern, options: NSRegularExpressionOptions.CaseInsensitive)
        let range = NSMakeRange(0, myString.characters.count)
        print(myString.characters.count)
        let CountString = myString.characters.count
        let modString = regex.stringByReplacingMatchesInString(myString, options: [], range: range, withTemplate: "valid")
        
        return modString
    }
    
    
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        let contextImage: UIImage = UIImage(CGImage: image.CGImage!)
        
        let contextSize: CGSize = contextImage.size
        var posX: CGFloat = contextSize.width
        var posY: CGFloat = contextSize.width
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        // See what size is longer and create the center off of that
        
        
        let rect: CGRect = CGRect(x: posX-cgwidth/2, y: posY-cgheight/2, width: cgwidth, height: cgheight)
        // Create bitmap image from context using the rect
         let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextImage.CGImage, rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(CGImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
//    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
//        
//        let contextImage: UIImage = UIImage(CGImage: image.CGImage!)
//        
//        let contextSize: CGSize = contextImage.size
//        
//        var posX: CGFloat = 0.0
//        var posY: CGFloat = 0.0
//        var cgwidth: CGFloat = CGFloat(width)
//        var cgheight: CGFloat = CGFloat(height)
//        
//        // See what size is longer and create the center off of that
//        if contextSize.width > contextSize.height {
//            posX = ((contextSize.width - contextSize.height) / 2)
//            posY = 0
//            cgwidth = contextSize.height
//            cgheight = contextSize.height
//        } else {
//            posX = 0
//            posY = ((contextSize.height - contextSize.width) / 2)
//            cgwidth = contextSize.width
//            cgheight = contextSize.width
//        }
    
//        let rect: CGRect = CGRectMake(posX, posY, cgwidth, cgheight)
//        
//        // Create bitmap image from context using the rect
//        let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextImage.CGImage, rect)!
//        
//        // Create a new image based on the imageRef and rotate back to the original orientation
//        let image: UIImage = UIImage(CGImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
//        
//        return image
//    }
    func textFieldShouldBeginEditing(state: UITextField) -> Bool {
        if(dobtxtField.text == "DOB    dd/mm/yy" && state == dobtxtField)
        {
            dobtxtField.text="DOB "
        }
       else if(addr1txtField.text == "My Address"  && state == addr1txtField)
            {
                addr1txtField.text=""
            }
      else if(addr2txtField.text == "Line 2"  && state == addr2txtField)
        {
            addr2txtField.text=""
        }
       else if(citytxtField.text == "My City"  && state == citytxtField)
        {
            citytxtField.text=""
        }
        else if(pintxtField.text == "123456"  && state == pintxtField)
        {
            pintxtField.text=""
        }
        else if(statetxtField.text == "Tamil nadu"  && state == statetxtField)
        {
            statetxtField.text=""
        }
        if state != dobtxtField {
            toolbarView.hidden = true
        }
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 790)
        return true
      
    }
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 790)
        if(textFieldToChange == pintxtField){
            characterCountLimit = 6
        }
        else if textFieldToChange == namelbl {
            characterCountLimit = 40
        }
        else{
            
            characterCountLimit = 25
            
        }
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
        
    }
    
    func dismissKeyboard(){
        toolbarView.hidden = true
        self.view.endEditing(true)
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 650)
    }
 @IBAction func profileimgBtnAction(sender: AnyObject) {
     var alertController:UIAlertController?
     alertController?.view.tintColor = UIColor.blackColor()
     alertController = UIAlertController(title: "Add Photo!",
     message: "",
     preferredStyle: .Alert)
     
     let action = UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
     self?.performSegueWithIdentifier("profile_image", sender: self)
        Appconstant.takephoto=true
//     self!.takePhotoFromCamera()
     
     })
     let action1 = UIAlertAction(title: "Choose from Gallery", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
        self?.performSegueWithIdentifier("profile_image", sender: self)
        Appconstant.cropimage=true
//     self!.takePhotofromGallery()
     
     })
        let action4 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
        
    }
  
         alertController?.addAction(action)
     alertController?.addAction(action1)
     alertController?.addAction(action4)
     self.presentViewController(alertController!, animated: true, completion: nil)
}
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField != dobtxtField {
            toolbarView.hidden = true
        }
    


    }
    func takePhotoFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
            imagePicker.allowsEditing = true
            self.presentViewController(imagePicker, animated: true, completion: nil)
            
        }
    }
    func takePhotofromGallery(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
            }
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if(segue.identifier == "from_provider") {
//            let nextview = segue.destinationViewController as! RechargeorPayBills
//            
//            nextview.provider_Name  =  providername
//           
//        }
//    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imageView.image = self.image
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
       
        
             if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
             {
            profileimg.image = pickedImage
            self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
            profilebtn.layer.cornerRadius = self.profilebtn.frame.size.height - 20 / 2
            profileimg.layer.cornerRadius = self.profileimg.frame.size.height / 2
            profileimg.clipsToBounds = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.FormSheet
            
            let image : UIImage = pickedImage
//            let imageData:NSData =  UIImagePNGRepresentation(image)!
            let imageData:NSData = UIImageJPEGRepresentation(image,1)!
            if self.profileimg.image?.imageOrientation == UIImageOrientation.Up {
                
            }
            else{
                UIGraphicsBeginImageContextWithOptions((self.profileimg.image?.size)!, false, (self.profileimg.image?.scale)!)
                self.profileimg.image?.drawInRect(CGRectMake(0, 0, (self.profileimg.image?.size.width)!, (self.profileimg.image?.size.height)!))
                self.profileimg.image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
            }

            strBase64 = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
            
            profilebtn.layer.cornerRadius = profilebtn.frame.height/2
            profilebtn.clipsToBounds = true
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            if purzDB.open() {
                let selectSQL = "SELECT * FROM IMAGETABLE WHERE CUSTOMER_ID=" + Appconstant.customerid
                
                let results:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                               withArgumentsInArray: nil)
                
                
                
                if (results.next()){
                    
                    let insertSQL = "UPDATE IMAGETABLE SET IMAGE_PATH = '"+self.strBase64+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                    
                    let result = purzDB.executeUpdate(insertSQL,
                                                      withArgumentsInArray: nil)
                    if !result {
                        //   status.text = "Failed to add contact"
                        print("Error: \(purzDB.lastErrorMessage())")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                }
                    
                    
                    
                else{
                    let insertsql = "INSERT INTO IMAGETABLE (CUSTOMER_ID,IMAGE_PATH) VALUES ('\(Appconstant.customerid)','"+self.strBase64+"')"
                    let result = purzDB.executeUpdate(insertsql,
                                                      withArgumentsInArray: nil)
                    if !result {
                        //   status.text = "Failed to add contact"
                        print("Error: \(purzDB.lastErrorMessage())")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                    
                }
            }
            purzDB.close()
            
        }
        
       UIAlertView(title: "Your profile image saved successfully!", message: "", delegate: nil, cancelButtonTitle: "Ok").show()
            
            
                    }
   
    func ImageCropViewController(controller: UIViewController!, didFinishCroppingImage croppedImage: UIImage!) {
        
        self.image = croppedImage
        self.imageCropView.image = self.image
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func ImageCropViewControllerDidCancel(controller: UIViewController!) {
        
        self.imageCropView.image = self.image
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func datepickerBtnAction(sender: AnyObject) {
        
        createDatePickerViewWithAlertController()
    }
    
    @IBAction func dobtxtFieldAction(sender: AnyObject) {
        if(dobtxtField.text == "DOB    dd/mm/yy")
        {
            dobtxtField.text="DOB "
        }

        createDatePickerViewWithAlertController()
    }
    
    @IBAction func NotificationBtnAction(sender: AnyObject) {
//        Appconstant.pushhome = false
        self.performSegueWithIdentifier("To_Notification", sender: self)

    }
    override func viewWillAppear(animated: Bool) {
         super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
        if Appconstant.FromProfile_IDType
        {
            IdTypeBtn.setTitle(Appconstant.IdType, forState: .Normal)
            IdTypeBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
            Appconstant.FromProfile_IDType = false
        }
        
    }
    var dateString = ""
    func createDatePickerViewWithAlertController()
    {
        let datePickerView:UIDatePicker = UIDatePicker()
//        datePickerView.maximumDate[NSDate, date]
        datePickerView.maximumDate = NSDate()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        if self.date != ""{
            dateString = self.date
        }
        else{
            dateString = "1990-01-01"
        }
//        let df = NSDateFormatter()
//        df.dateFormat = "yyyy-MM-dd"
//        let date = df.dateFromString(dateString)
//        datePickerView.setDate(date!, animated: false)
//      let dateString = "1990-01-01"
        let df = NSDateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = df.dateFromString(dateString)
        datePickerView.setDate(date!, animated: false)
        print(datePickerView.frame.size.height)
        dobtxtField.inputView = datePickerView
        toolbarView = UIView(frame: CGRectMake(0,self.view.frame.size.height-datePickerView.frame.size.height-44,self.view.frame.size.width,44))
        toolbarView.backgroundColor = UIColor.lightGrayColor()
        
        let doneButton = UIButton.init(frame: CGRectMake(self.view.frame.size.width - 60,5,50,34))
        doneButton.addTarget(self, action: "Clicked", forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.setTitle("Done", forState: UIControlState.Normal)
              NSTimer.scheduledTimerWithTimeInterval(0.2, target:self, selector: Selector("timerfunc"), userInfo: nil, repeats: false)
        toolbarView.addSubview(doneButton)
    
        
        datePickerView.addTarget(self, action: "datePickerValueChanged:", forControlEvents: UIControlEvents.ValueChanged)
   
    }
    
    func timerfunc(){
        toolbarView.hidden = false
        self.view.addSubview(toolbarView)
        
    }
    func Clicked(){
        
        toolbarView.hidden = true
        self.view.endEditing(true)
    }
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let currentdate = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: currentdate)
        
        let year =  components.year
        
        
        print(sender.date)
        let dateFormatter = NSDateFormatter()
        let dateFormatter1 = NSDateFormatter()
        let dateFormatter2 = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter1.dateFormat = "dd/MM/yy"
        dateFormatter2.dateFormat = "yyyy"
        
        print(dateFormatter2.stringFromDate(sender.date))
        let selectdate = Int(dateFormatter2.stringFromDate(sender.date))!
        if(selectdate+18 > Int(year)){
            if !toolbarView.hidden{
                toolbarView.hidden = true
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.dobtxtField.text = "DOB    " + dateFormatter1.stringFromDate(sender.date)
                self.date = dateFormatter.stringFromDate(sender.date)
            }
        }
            
            //        let ageComponents = calendar.components(.CalendarUnitYear,
            //            fromDate: dateFormatter1.stringFromDate(sender.date),
            //            toDate: NSDate(),
            //            options: nil)
            //        let age = ageComponents.year
        else{
            dobtxtField.text = "DOB    " + dateFormatter1.stringFromDate(sender.date)
            date = dateFormatter.stringFromDate(sender.date)
            toolbarView.hidden = false
        }
        
    }
    
    @IBAction func saveBtnAction(sender: AnyObject) {
              saveaction()
        Appconstant.customername = namelbl.text!
    }
    
    func saveaction(){
        
 Appconstant.IdNumber = idTxtField.text!
        if(date.isEmpty){
            self.presentViewController(Alert().alert("Oops! Seems you forgot to fill DOB..We need you to fill it and try again!", message: ""),animated: true,completion: nil)
        }
        else if(addr1txtField.text!.isEmpty || addr2txtField.text!.isEmpty || citytxtField.text!.isEmpty || statetxtField.text!.isEmpty || pintxtField.text!.isEmpty || securequestiontxtField.text!.isEmpty || secureanstxtField.text!.isEmpty){
            self.presentViewController(Alert().alert("Oops! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if(pintxtField.text!.characters.count != 6){
            self.presentViewController(Alert().alert("Oops! Enter a valid Pincode", message: ""),animated: true,completion: nil)
        }
        else if(self.pintxtField.text != "")
        {
        let location: String = pintxtField.text!
        let geocoder: CLGeocoder = CLGeocoder()
        geocoder.geocodeAddressString(location, completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
            if placemarks != nil{
            if ((placemarks?.count)! > 0) {
                let placemark: CLPlacemark = (placemarks?[0])!
                print(placemark)
                  let country : String = placemark.country!
                 let state: String = placemark.administrativeArea!
                let district: String = placemark.subAdministrativeArea!
                let place:String = placemark.subLocality!
                let pincode:String = placemark.postalCode!
                print(state);
                print(country)
                print(place)
                if(self.pintxtField.text != placemark.postalCode!  ){
                    self.presentViewController(Alert().alert("Oops! Enter a valid Pincode", message: ""),animated: true,completion: nil)
                }
//                else if(self.addr2txtField.text?.lowercaseString != place.lowercaseString)
//                    {
//                        print(self.addr2txtField.text?.lowercaseString)
//                        print(place.lowercaseString)
//                        self.presentViewController(Alert().alert("Oops! Enter a valid placename as \(place) ", message: ""),animated: true,completion: nil)
//                    }
             
                   else if(self.citytxtField.text!.lowercaseString != district.lowercaseString)
                    {
                        self.presentViewController(Alert().alert("Oops! Enter a valid cityname as \(district)", message: ""),animated: true,completion: nil)
                    }
                   
                    
                else if(self.statetxtField.text!.lowercaseString != state.lowercaseString)
                    {
                        self.presentViewController(Alert().alert("Oops! Enter a valid State as \(state)", message: ""),animated: true,completion: nil)
                    }
//                else if Appconstant.IdType == ""
//                {
//                    self.presentViewController(Alert().alert("Please select the ", message: ""),animated: true,completion: nil)
//                }
//               else if self.idTxtField.text == ""
//                {
//                    self.presentViewController(Alert().alert("Please enter the id", message: ""),animated: true,completion: nil)
//                }
//              
                  else{
//                    if Appconstant.IdType != "" && self.idTxtField.text != ""{
//                        self.ValidIDNumber()
//                        
//                        
//                    }
                    let description = self.securequestiontxtField.text! + ":" + self.secureanstxtField.text!
                    let profileviewmodel = ProfileViewModel.init(firstName: self.namelbl.text!, specialDate: self.date, entityId: Appconstant.customerid, address: self.addr1txtField.text!, address2: self.addr2txtField.text!, city: self.citytxtField.text!, state: self.statetxtField.text!, pincode: self.pintxtField.text!, description: description, idType: Appconstant.IdType, idNumber:Appconstant.IdNumber)!
                    let serializedjson  = JSONSerializer.toJson(profileviewmodel)
                    print(serializedjson)
                    self.activityIndicator.startAnimating()
                    self.sendrequesttoserverForUpdateEntity(Appconstant.BASE_URL+Appconstant.URL_UPDATE_ENTITY, values: serializedjson)
                }
                }
            }
            else {
                self.presentViewController(Alert().alert("Oops! Enter a valid Pincode", message: ""),animated: true,completion: nil)
            }
            } as! CLGeocodeCompletionHandler)
        }
        
           }
    
    func ValidIDNumber() {
        var isValid = ""
        
        if Appconstant.IdType == "Aadhaar No" {
            isValid = checkValidExpression(self.idTxtField.text!, myPattern: "[0-9]{12}")
        }
        else if Appconstant.IdType == "PAN Card" {
            isValid = checkValidExpression(self.idTxtField.text!, myPattern: "[A-Za-z]{5}\\d{4}[A-Za-z]{1}")
        }
        else if Appconstant.IdType == "Passport" {
            isValid = checkValidExpression(self.idTxtField.text!, myPattern: "^(?!^0+$)[a-zA-Z0-9]{3,20}$")
        }
        else if Appconstant.IdType == "Driving License" {
            isValid = checkValidExpression(self.idTxtField.text!, myPattern: "[A-Za-z]{2}\\d{13}")
        }
        else if Appconstant.IdType == "Voter ID" {
            isValid = checkValidExpression(self.idTxtField.text!, myPattern: "[A-Za-z]{3}\\d{7}")
        }
        print(isValid)
        if isValid == "valid" {
            updateCustomerIDDetails()
        }
        else {
            if Appconstant.IdType == "Aadhaar No" {
                
                self.presentViewController(Alert().alert("Please enter the valid Aadhaar number", message: ""),animated: true,completion: nil)
                
            }
            else if Appconstant.IdType == "PAN Card" {
                dispatch_async( dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Please enter the valid PAN card number", message: ""),animated: true,completion: nil)
                }
                
            }
            else if Appconstant.IdType == "Passport" {
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Please enter the valid passport number", message: ""),animated: true,completion: nil)
                }
            }
            else if Appconstant.IdType == "Driving License" {
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Please enter the valid driving license number", message: ""),animated: true,completion: nil)
                }
                
            }
            else if Appconstant.IdType == "Voter ID" {
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Please enter the valid voter id", message: ""),animated: true,completion: nil)
                }
            }
        }
    }
    
  
    
    func updateCustomerIDDetails(){
        let currentdate = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let registeredDate = dateFormatter.stringFromDate(currentdate)
        print(registeredDate)
        let profileviewmodel = UpdateUserCardIdViewModel.init(entityId: Appconstant.customerid, idType:  Appconstant.IdType, idNumber:  Appconstant.IdNumber, kycType: "MIN", registeredDate: registeredDate, description: "")!
        let serializedjson  = JSONSerializer.toJson(profileviewmodel)
        print(serializedjson)
        self.sendrequesttoserverForIDUpdateEntity(Appconstant.BASE_URL+Appconstant.URL_UPDATE_KYCDETAILS, values: serializedjson)
        
    }
    func sendrequesttoserverForIDUpdateEntity(url : String, values: String)
    {
        print(url)
        print(values)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                dispatch_async(dispatch_get_main_queue()) {
                    //                self.activityIndicator.stopAnimating()
                }
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {
                dispatch_async(dispatch_get_main_queue()) {
                    //                self.activityIndicator.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")  // check for http errors
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseStringUpdate = \(responseString)")
                let json = JSON(data: data!)
                
                //                self.activityIndicator.stopAnimating()
                //       }
                if(json["result"].stringValue == "true"){
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        self.saveintoIDDB()
//                        let alert = UIAlertController(title: "Your ID is updated successfully!",message: "", preferredStyle: UIAlertControllerStyle.Alert)
//                        alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.Default, handler: self.navigateFunc))
//                        self.presentViewController(alert, animated: true, completion: nil)
                        
                        
                        
                        
                    }
                    
                }
                
            }
        }
        
        task.resume()
        
    }
    func saveintoIDDB(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        
        if purzDB.open() {
            let selectSQL = "SELECT * FROM CUSTOMERDETAIL"
            
            let results:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                           withArgumentsInArray: nil)
            if (results.next()){
                let update11 = "UPDATE CUSTOMERDETAIL SET ID_TYPE='"+Appconstant.IdType+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update12 = "UPDATE CUSTOMERDETAIL SET ID_NUMBER='"+Appconstant.IdNumber+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                
                
                let result11 = purzDB.executeUpdate(update11,
                                                    withArgumentsInArray: nil)
                let result12 = purzDB.executeUpdate(update12,
                                                    withArgumentsInArray: nil)
                
                if (!result11 || !result12){
                    
                    print("Error: \(purzDB.lastErrorMessage())")
                    //                    dispatch_async(dispatch_get_main_queue()) {
                    //                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    //
                    //                    }
                }
            }
            
        }
        purzDB.close()
    }
    func sendrequesttoserverForUpdateEntity(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")  // check for http errors
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                if(json["result"].stringValue == "true"){
                    self.saveintoDB()
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        
                    }
                }
            }
        }
        
        task.resume()
        
    }
    func databaseForGettingProfileImage()
    {
        profileimg.clipsToBounds = true
        profileimg.layer.borderWidth = 3
        profileimg.layer.borderColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let payableDB = FMDatabase(path: databasePath as String)
        if payableDB.open() {
            let querySQL = "SELECT * FROM IMAGETABLE WHERE CUSTOMER_ID=" + Appconstant.customerid
            
            let results:FMResultSet! = payableDB.executeQuery(querySQL,
                                                              withArgumentsInArray: nil)
            
            if(results.next()) {
                
                self.image1.append(results.stringForColumn("IMAGE_PATH"))
                
                
                let item3 = results.stringForColumn("IMAGE_PATH")
                //                print("Image Null==>>")
                //                print(item3)
                
                if item3 == "empty" || item3.isEmpty
                {
                    //                    print(results.stringForColumn("IMAGE_PATH"))
                    
                }
                    
                else if item3 == ""
                {
                    //                    print(results.stringForColumn("IMAGE_PATH"))
                    
                }
                    
                    
                else
                {
                    let dataDecoded:NSData = NSData(base64EncodedString: results.stringForColumn("IMAGE_PATH") , options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
                    print(dataDecoded.length)
                    if dataDecoded.length > 10{
                        let img = UIImage(data: dataDecoded)!
                        profileimg.layer.cornerRadius = self.profileimg.frame.size.height / 2
                        profileimg.clipsToBounds = true
                        self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
                        profileimg.image = img
                        profileimg.layer.borderWidth = 3
                        profileimg.layer.borderColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
                    }
                    
                    
                }
                
                
            }
                
                /*
                 if(results.next()) {
                 
                 self.image.append(results.stringForColumn("IMAGE_PATH"))
                 
                 let dataDecoded:NSData = NSData(base64EncodedString: results.stringForColumn("IMAGE_PATH") , options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
                 print(dataDecoded.length)
                 if dataDecoded.length > 10{
                 let img = UIImage(data: dataDecoded)!
                 profileimg.layer.cornerRadius = self.profileimg.frame.size.height / 2
                 profileimg.clipsToBounds = true
                 self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
                 profileimg.image = img
                 }
                 
                 
                 }
                 */
            else
            {
                
            }
            
           
            
        }
        
        
    }
    
    func saveintoDB(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let selectSQL = "SELECT * FROM CUSTOMERDETAIL"
            
            let results:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                           withArgumentsInArray: nil)
            if (results.next()){
                let update1 = "UPDATE CUSTOMERDETAIL SET DATE_OF_BIRTH='"+self.date+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update2 = "UPDATE CUSTOMERDETAIL SET ADDRESS_LINE_1='"+self.addr1txtField.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update3 = "UPDATE CUSTOMERDETAIL SET ADDRESS_LINE_2='"+self.addr2txtField.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update4 = "UPDATE CUSTOMERDETAIL SET CITY='"+self.citytxtField.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update5 = "UPDATE CUSTOMERDETAIL SET STATE='"+self.statetxtField.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update6 = "UPDATE CUSTOMERDETAIL SET PIN='"+self.pintxtField.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update7 = "UPDATE CUSTOMERDETAIL SET SECURITY_QUESTION='"+self.securequestiontxtField.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update8 = "UPDATE CUSTOMERDETAIL SET SECURITY_ANSWER='"+self.secureanstxtField.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update9 = "UPDATE CUSTOMERDETAIL SET CUSTOMER_NAME='"+self.namelbl.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update10 = "UPDATE PROFILE_INFO SET CUSTOMER_NAME='"+self.namelbl.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update11 = "UPDATE CUSTOMERDETAIL SET CUSTOMER_NAME='"+self.namelbl.text!+"' WHERE ID_TYPE=" + Appconstant.IdType
                let update12 = "UPDATE PROFILE_INFO SET CUSTOMER_NAME='"+self.namelbl.text!+"' WHERE ID_NUMBER=" + Appconstant.IdNumber
                
                
                let result1 = purzDB.executeUpdate(update1,
                                                   withArgumentsInArray: nil)
                let result2 = purzDB.executeUpdate(update2,
                                                   withArgumentsInArray: nil)
                let result3 = purzDB.executeUpdate(update3,
                                                   withArgumentsInArray: nil)
                let result4 = purzDB.executeUpdate(update4,
                                                   withArgumentsInArray: nil)
                let result5 = purzDB.executeUpdate(update5,
                                                   withArgumentsInArray: nil)
                let result6 = purzDB.executeUpdate(update6,
                                                   withArgumentsInArray: nil)
                let result7 = purzDB.executeUpdate(update7,
                                                   withArgumentsInArray: nil)
                let result8 = purzDB.executeUpdate(update8,
                                                   withArgumentsInArray: nil)
                let result9 = purzDB.executeUpdate(update9,
                                                   withArgumentsInArray: nil)
                let result10 = purzDB.executeUpdate(update10,
                                                   withArgumentsInArray: nil)
                let result11 = purzDB.executeUpdate(update11,
                                                   withArgumentsInArray: nil)
                let result12 = purzDB.executeUpdate(update12,
                                                    withArgumentsInArray: nil)
                
                if (!result1 || !result2 || !result3 || !result4 || !result5 || !result6 || !result7 || !result8 || !result9 || !result10 || !result11 || !result12){
                    
                    print("Error: \(purzDB.lastErrorMessage())")
//                    dispatch_async(dispatch_get_main_queue()) {
//                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
//                        
//                    }
                }
//                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        Appconstant.customername = self.namelbl.text!
                        var alertController:UIAlertController?
                        alertController?.view.tintColor = UIColor.blackColor()
                        alertController = UIAlertController(title: "Yey! Your details saved successfully",
                                                            message: "",
                                                            preferredStyle: .Alert)
                        
                        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                            
                            self?.performSegueWithIdentifier("profile_to_myAccount", sender: self)
                            
                            
                            })
                        alertController?.addAction(action)
                        self.presentViewController(alertController!, animated: true, completion: nil)
                        
                        
                    }
//                }
            }
            else{
                let insertsql = "INSERT INTO CUSTOMERDETAIL (CUSTOMER_ID,CUSTOMER_NAME,DATE_OF_BIRTH,CUSTOMER_BANK,ADDRESS_LINE_1,ADDRESS_LINE_2,CITY,STATE,PIN,SECURITY_QUESTION,SECURITY_ANSWER,IMAGE_PATH,ID_TYPE,ID_NUMBER) VALUES ('\(Appconstant.customerid)','\(self.namelbl.text!)','\(self.date)','\("DCB")','\(self.addr1txtField.text!)','\(self.addr2txtField.text!)','\(self.citytxtField.text!)','\(self.statetxtField.text!)','\(self.pintxtField.text!)','\(self.securequestiontxtField.text!)','\(self.secureanstxtField.text!)','\(self.strBase64)','\(Appconstant.IdType)','\(Appconstant.IdNumber)')"
                
                let result = purzDB.executeUpdate(insertsql,
                                                  withArgumentsInArray: nil)
                
                if !result {
                    //   status.text = "Failed to add contact"
                    print("Error: \(purzDB.lastErrorMessage())")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        var alertController:UIAlertController?
                        alertController?.view.tintColor = UIColor.blackColor()
                        alertController = UIAlertController(title: "Yey! Your details saved successfully",
                                                            message: "",
                                                            preferredStyle: .Alert)
                        
                        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                            
                            self?.performSegueWithIdentifier("profile_to_myAccount", sender: self)
                            
                            
                            })
                        alertController?.addAction(action)
                        self.presentViewController(alertController!, animated: true, completion: nil)
                        
                        
                    }
                }
                
            }
        }
    }
    
    //    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
    //
    //        let tabBarIndex = tabBarController.selectedIndex
    //        if tabBarIndex == 1 {
    //            print("Selected item")
    //            let HomeVc = SWRevealViewController()
    //            self.navigationController?.pushViewController(HomeVc, animated: true)
    //        }
    //
    //
    //        print("Selected view controller")
    //
    //    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "profile_otp") {
            let nextview = segue.destinationViewController as! OTPViewController
            nextview.fromprofile = true
        }
    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
          //   Appconstant.pushhome = true
            self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
//            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
}
