//
//  AccountsViewController.swift
//  purZ
//
//  Created by Vertace on 17/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class AccountsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITabBarControllerDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var addaccountbtn: UIButton!
    
    
    var bankname = [String]()
    var accountNo = [String]()
    var ifsc = [String]()
    var accounttype = [String]()
    var upi = [String]()
    var toastmessage = ""
    var accountRefNo = [String]()
    var vpa = [String]()
    var accountdetail = [String]()
    var vpaaccountdetail = [String]()
    var vpaaccountname = [String]()
    var vpaname_constant = [String]()
    var vpaaccountdetail_constant = [String]()
    var vpaname = [String]()
    var amount = [String]()
    var acc_detailForCardData = ""
    var vpa_ForCardData = ""
    var vpa_acc_refNo = [String]()
//    var isVPAexecuted = false
    
    override func viewDidLoad() {
        tabBarController?.delegate = self
        getproviderlist()
        getUserBankAccountList()
        getVPAList()
        
        if toastmessage != "" {
            self.presentViewController(Alert().alert(toastmessage,message: ""), animated: true, completion: nil)
           
        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AccountsViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        dispatch_async(dispatch_get_main_queue()) {
            self.activityIndicator.stopAnimating()
        }
    }
    
    
    @IBAction func cancelBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        let alert = UIAlertController(title: "Are you sure you want to delete?", message: "", preferredStyle: UIAlertControllerStyle.Alert)
  
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: { alertAction in
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            
            if purzDB.open() {
                var deleteSQL = ""
                if self.bankname.count == 1 {
                    deleteSQL =  "DELETE FROM ACCOUNTS"
                }
                else{
                    deleteSQL =  "DELETE FROM ACCOUNTS WHERE ACCOUNTREF_NO = " + self.accountRefNo[indexPath.row]
                }
                
                let result = purzDB.executeUpdate(deleteSQL,
                    withArgumentsInArray: nil)
                
                if !result {
                    //   status.text = "Failed to add contact"
                    print("Error: \(purzDB.lastErrorMessage())")
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.bankname.removeAll()
                        self.accountNo.removeAll()
                        self.ifsc.removeAll()
                        self.accounttype.removeAll()
                        self.upi.removeAll()
                        self.accountRefNo.removeAll()
                        AccountConstant.accountNumber.removeAll()
                        AccountConstant.bankname.removeAll()
                        AccountConstant.ifsc.removeAll()
                        AccountConstant.accounttype.removeAll()
                        AccountConstant.upi.removeAll()
                        AccountConstant.accountRefNo.removeAll()
                        self.getUserBankAccountList()
                        
                    }
                }
            }
            purzDB.close()
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { alertAction in
            
        }))
        
        
        self.presentViewController(alert, animated: true, completion: nil)
        
        
        
    }
    
    @IBAction func checkbalanceBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
       
        var account_detail = ""
        var vpaexist = false
        var vpahandel = ""
        for(var i=0;i<self.vpaaccountname.count;i++){
          
            if (self.vpaaccountname[i] == bankname[indexPath.row]) && (self.vpa_acc_refNo[i] == self.accountRefNo[indexPath.row]) {
                vpaexist = true
                account_detail = self.vpaaccountdetail[i]
                vpahandel = self.vpaname[i]
                break;
            }
        }
        
        if vpaexist {
            self.activityIndicator.startAnimating()
            let customeraccount = self.convertStringToDictionary(accountdetail[indexPath.row])! as NSDictionary
           
            let CustomerAccountValue = CustomerAccount.init(accountDetails: customeraccount as! [NSObject : AnyObject])
            let account = self.convertStringToDictionary(account_detail)! as NSDictionary
            
            let VirtualAddr_detail = VirtualAddress.init(account as! [NSObject : AnyObject], handle: vpahandel)
            
            ApiClient().balanceInquiry(VirtualAddr_detail, custAccount: CustomerAccountValue, view: self, { (success, error, response) in
                self.activityIndicator.stopAnimating()
                if success && response != nil {
                    
                    self.amount[indexPath.row] = String(response!)
                    self.tableView.reloadData()
                    
                }
                else if response != nil {
                    
                    let msg = String(response.valueForKey("message")!)
                    if msg != "" {
                        self.presentViewController(Alert().alert(msg,message: ""), animated: true, completion: nil)
                    }
                }
            })
        }
            
        else{
//            if isVPAexecuted {
            dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(Alert().alert("You can check balance after creating VPA for this account",message: ""), animated: true, completion: nil)
            }
//            }
//            else {
//                    self.presentViewController(Alert().alert("Session Expired",message: ""), animated: true, completion: nil)
//              }
        }
        
    }
    
    
    @IBAction func changeUPIpinBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        
        var vpaexist = false
        var account_detail = ""
        var vpahandel = ""
        for(var i=0;i<self.vpaaccountname.count;i++){
            if (self.vpaaccountname[i] == bankname[indexPath.row]) && (self.vpa_acc_refNo[i] == self.accountRefNo[indexPath.row]){
                vpaexist = true
                account_detail = self.vpaaccountdetail[i]
                vpahandel = self.vpaname[i]
                break;
            }
        }
        if vpaexist {
            let customeraccount = self.convertStringToDictionary(accountdetail[indexPath.row])! as NSDictionary
            
            let CustomerAccountValue = CustomerAccount.init(accountDetails: customeraccount as! [NSObject : AnyObject])
            let account = self.convertStringToDictionary(account_detail)! as NSDictionary
            
            let VirtualAddr_detail = VirtualAddress.init(account as! [NSObject : AnyObject], handle: vpahandel)
            
            ApiClient().changeMPIN(VirtualAddr_detail, custAccount: CustomerAccountValue, view: self, { (success, error, response) in
                if success && response != nil {
                    
                    dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("UPI PIN Change successfully",message: ""), animated: true, completion: nil)
                    }
                }
                else if response != nil {
                    
                    let msg = String(response.valueForKey("message")!)
                    if msg != "" {
                        dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert(msg,message: ""), animated: true, completion: nil)
                        }
                    }
                }
            })
        }
        else{
            dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(Alert().alert("You can change MPIN after creating VPA for this account",message: ""), animated: true, completion: nil)
            }
        }
        
        
    }
    
    
    @IBAction func resetUPIpinBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        
        var vpaexist = false
        
        for i in 0 ..< self.vpaaccountname.count {
            if (self.vpaaccountname[i] == bankname[indexPath.row]) && (self.vpa_acc_refNo[i] == self.accountRefNo[indexPath.row]) {
                vpaexist = true
                acc_detailForCardData = self.vpaaccountdetail[i]
                vpa_ForCardData = self.vpaname[i]
                break;
            }
        }
        if vpaexist {
            AccountConstant.accountno = accountNo[indexPath.row]
            AccountConstant.accountdetail = accountdetail[indexPath.row]
            dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("accounts_to_carddetail", sender: self)
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(Alert().alert("You can change MPIN after creating VPA for this account",message: ""), animated: true, completion: nil)
            }
        }
        
    }
    
    
    
    @IBAction func backBtnAction(sender: AnyObject) {
        //        dismissViewControllerAnimated(false, completion: nil)
    }
    
    func getVPAList(){
    self.activityIndicator.startAnimating()

        ApiClient().getMappedVirtualAddresses { (success, error, response) in
//            self.isVPAexecuted = true
            if success {
                if response != nil {
                    
                    
                    do{
                        // get jsonResult from NSObject
                        let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(response, options: NSJSONWritingOptions.PrettyPrinted)
                        let json = JSON(data: jsonData)
                        var temp_providername = [String]()
                        for item1 in json.arrayValue {
                            
                            let item2 = item1["VirtualAddress"]
                            let items = item2["accounts"]
                            
                            self.vpaname_constant.append(items["handle"].stringValue)
                            for item in items["CustomerAccount"].arrayValue {
                                self.vpaname.append(item["va"].stringValue)
                                self.vpaaccountdetail.append(String(item))
                                self.vpaaccountname.append(item["account-provider-name"].stringValue)
                                self.vpa_acc_refNo.append(item["account-ref-no"].stringValue)
                                if item["default-debit"].stringValue == "D"{
                                    temp_providername.append(item["account-provider-name"].stringValue)
                                    self.vpaaccountdetail_constant.append(String(item))
                                }
                            }
                        }
                        self.activityIndicator.stopAnimating()
                        AccountConstant.vpanames = self.vpaname_constant
                        AccountConstant.vparesponses = self.vpaaccountdetail_constant
                        AccountConstant.vpaProviderName = temp_providername
                        
                    }
                    catch {
                        print("error")
                    }
                    
                }
                
            }
            
        }
       
    }
    
    
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    func getUserBankAccountList(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let select = "SELECT * FROM ACCOUNTS"
            let result:FMResultSet = purzDB.executeQuery(select,
                                                         withArgumentsInArray: nil)
            while(result.next()){
                bankname.append(result.stringForColumn("BANK_NAME"))
                accountNo.append(result.stringForColumn("ACCOUNT_NO"))
                ifsc.append(result.stringForColumn("IFSC"))
                accounttype.append(result.stringForColumn("TYPE"))
                upi.append(result.stringForColumn("UPI"))
                accountRefNo.append(result.stringForColumn("ACCOUNTREF_NO"))
                vpa.append(result.stringForColumn("VPA"))
                accountdetail.append(result.stringForColumn("ACCOUNTDETAIL"))
                amount.append("")
                
            }
            AccountConstant.accountNumber = self.accountNo
            AccountConstant.bankname = self.bankname
            AccountConstant.ifsc = self.ifsc
            AccountConstant.accounttype = self.accounttype
            AccountConstant.upi = self.upi
            AccountConstant.accountRefNo = accountRefNo
            
            self.tableView.reloadData()
            
        }
        purzDB.close()
        
    }
    func getproviderlist(){
        addaccountbtn.userInteractionEnabled = false
        self.activityIndicator.startAnimating()
        
        if AccountProviderList.AccountProviderName.count == 0{
            let providerlist:NSDictionary = ApiClient().getAccountProviderList()
      
            do{
                let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(providerlist, options: NSJSONWritingOptions.PrettyPrinted)
                //                let json = NSString(data: jsonData, encoding: NSUTF8StringEncoding)
                let json = JSON(data: jsonData)
                
                for items in json.arrayValue {
                    
                    AccountProviderList.AccountProviderName.append(items["account-provider"].stringValue)
                    AccountProviderList.AccountProviderid.append(items["id"].stringValue)
                    AccountProviderList.IIN.append(items["iin"].stringValue)
                }
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.addaccountbtn.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
                }
                
            }
            catch let error1 as NSError {
            }
            catch {
                dispatch_async(dispatch_get_main_queue()) {
                    self.addaccountbtn.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
                }
                print("error")
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue()) {
                self.addaccountbtn.userInteractionEnabled = true
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AccountCell", forIndexPath: indexPath) as UITableViewCell!
        
        
        let checkbalanceBtn = cell.viewWithTag(5) as! UIButton
        let changepinBtn = cell.viewWithTag(6) as! UIButton
        let resetpinBtn = cell.viewWithTag(7) as! UIButton
        let banknamelbl
            = cell.viewWithTag(1) as! UILabel
        let accountnolbl = cell.viewWithTag(2) as! UILabel
        let ifsclbl = cell.viewWithTag(3) as! UILabel
        let typelbl = cell.viewWithTag(4) as! UILabel
       
        banknamelbl.text = bankname[indexPath.row]
        accountnolbl.text = ": "+accountNo[indexPath.row]
        ifsclbl.text = ": "+ifsc[indexPath.row]
        typelbl.text = ": "+accounttype[indexPath.row]
        
        checkbalanceBtn.titleLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping;
        checkbalanceBtn.titleLabel!.textAlignment = NSTextAlignment.Center;
        if amount[indexPath.row] != "" {
            checkbalanceBtn.setTitle(amount[indexPath.row], forState: .Normal)
        }
        else {
            checkbalanceBtn.setTitle("Check Balance", forState: .Normal)
        }
        
        checkbalanceBtn.layer.borderWidth = 1
        changepinBtn.layer.borderWidth = 1
        resetpinBtn.layer.borderWidth = 1
        checkbalanceBtn.layer.borderColor = UIColor(red: 64.0/255.0, green: 168.0/255.0, blue: 60.0/255.0, alpha: 1).CGColor
        changepinBtn.layer.borderColor = UIColor(red: 64.0/255.0, green: 168.0/255.0, blue: 60.0/255.0, alpha: 1).CGColor
        resetpinBtn.layer.borderColor = UIColor(red: 64.0/255.0, green: 168.0/255.0, blue: 60.0/255.0, alpha: 1).CGColor
        checkbalanceBtn.layer.cornerRadius = checkbalanceBtn.frame.size.height/2
        changepinBtn.layer.cornerRadius = changepinBtn.frame.size.height/2
        resetpinBtn.layer.cornerRadius = resetpinBtn.frame.size.height/2
        
        cell.layer.borderWidth = 3
        cell.layer.borderColor = UIColor(red: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1).CGColor
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return accountNo.count
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    //    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //        return 10
    //    }
    
    func convertStringToDictionary(text: String) -> [NSObject:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [NSObject:AnyObject]
            }
            catch {
                print("error")
            }
            catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    // To create toast label
    func createtoastmessage(message: String){
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 125, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        toastLabel.textColor = UIColor.whiteColor()
        toastLabel.textAlignment = .Center
        toastLabel.font = UIFont(name: "Calibri", size: 7.0)
        toastLabel.text = message
        toastLabel.numberOfLines = 5
        toastLabel.sizeToFit()
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animateWithDuration(4.0, delay: 1, options: .CurveEaseInOut, animations: {
            toastLabel.alpha = 0.0
            }, completion: { (false) in
                toastLabel.removeFromSuperview()
        })
    }
    
    
    
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "accounts_to_carddetail") {
            let nextview = segue.destinationViewController as! CardDetailViewController
            nextview.fromaccounts = true
            nextview.accountdetail = acc_detailForCardData
            nextview.handelvpa = vpa_ForCardData
        }
    }
}
