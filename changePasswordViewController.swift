//
//  changePasswordViewController.swift
//  Purz
//
//  Created by Vertace on 07/02/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import UIKit

class changePasswordViewController: UIViewController, UITextFieldDelegate, UITabBarControllerDelegate
{
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var oldPasswordtxt: UITextField!
    @IBOutlet weak var changebtn: UIButton!
    
    @IBOutlet weak var confirmPasswordTxt: UITextField!
    @IBOutlet weak var newPasswordTxt: UITextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    let characterCountLimit = 4
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Appconstant.customerid======")
        print(Appconstant.customerid)

        navigationController?.navigationBarHidden = true
        tabBarController?.hidesBottomBarWhenPushed = false
        tabBarController?.delegate = self
        changebtn.layer.cornerRadius = 8
        navigationController?.navigationBarHidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        scrollView.addGestureRecognizer(tap)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
     //   Appconstant.pushhome = true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        
//        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 750)
    }
    
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        
//        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 790)
        
//        if(textFieldToChange == newPasswordTxt){
//            characterCountLimit
//        }
//        if(textFieldToChange == confirmPasswordTxt){
//            characterCountLimit
//            
//        }
//        else if(textFieldToChange != oldPasswordtxt&&textFieldToChange != newPasswordTxt && textFieldToChange != confirmPasswordTxt){
//            characterCountLimit
//        }
        //        if(string == ""){
        //           if textFieldToChange == firstnamelbl{
        //                lastnamelbl.becomeFirstResponder()
        //            }
        //
        //        }
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
        
        
    }
    
    
    @IBAction func backBtnAction(sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    @IBAction func changebtn(sender: AnyObject) {
        
        let oldPassword = oldPasswordtxt.text
       
        let password = newPasswordTxt.text
        let confirmPassword = confirmPasswordTxt.text
        
       
        
        
        if (oldPassword == "" || password == "" || confirmPassword == "" )
        {
            self.presentViewController(Alert().alert("Oops! Seems you forgot to fill some field.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            
        }
             else if(password!.characters.count != 4||confirmPassword!.characters.count != 4){
            self.presentViewController(Alert().alert("Punch in a 4 digit Password", message: ""),animated: true,completion: nil)
        }
        else if(oldPassword!.characters.count != 4)
        {
            self.presentViewController(Alert().alert("Punch in your 4 digit old Password", message: ""),animated: true,completion: nil)
            
        }
            
        else if (password != confirmPassword)
        {
            self.presentViewController(Alert().alert("Looks like the Passwords don't match", message: ""),animated: true,completion: nil)
            
        }
                   else if(oldPassword != Appconstant.pwd)
        {
            self.presentViewController(Alert().alert("Password is Invalid", message: ""),animated: true,completion: nil)
            
        }
       
//        else if(oldPassword! == password!)
//        {
//            self.presentViewController(Alert().alert("Looks like your new password is same as old password!", message: ""),animated: true,completion: nil)
//            
//        }
            else
        {
            
            let changeviewmodel = ChangeViewModel.init(customerId:Appconstant.customerid, oldYapcode: oldPasswordtxt.text!, newYapcode: newPasswordTxt.text!)!
            let serializedjson  = JSONSerializer.toJson(changeviewmodel)
            print(serializedjson)
            sendrequesttoserverForForgotPasscode(Appconstant.BASE_URL+Appconstant.URL_CHANGE_PASSCODE, values: serializedjson)
            
//            var alertController:UIAlertController?
//            alertController?.view.tintColor = UIColor.blackColor()
//            alertController = UIAlertController(title: "Yuppie! Your Password is changed successfully.",
//                                                message: "",
//                                                preferredStyle: .Alert)
//            
//            let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//                dispatch_async(dispatch_get_main_queue()) {
//                    self!.performSegueWithIdentifier("changepwd_myaccount", sender: self)
//                }
//                
//                })
//            
//            alertController?.addAction(action)
//            self.presentViewController(alertController!, animated: true, completion: nil)
            
        }
        
        //            self.presentViewController(Alert().alert("Your Password is changed successfully!", message: ""),animated: true,completion: nil)
        //
        //
        //        }
        //
        //         self.performSegueWithIdentifier("backBtn",sender: self)
        
    }
    
    func sendrequesttoserverForForgotPasscode(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                self.activityIndicator.stopAnimating()
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    
                }
            }
            else{
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                self.activityIndicator.stopAnimating()
                let item = json["result"]
                
                if(item["success"].stringValue == "true"){
                    self.SavePsw()
                    
//                    let httpres = response as? NSHTTPURLResponse
//                    let session = httpres?.allHeaderFields["TOKEN"]
//                    print("Sessiom\(session)")
                    
                            dispatch_async(dispatch_get_main_queue()) {
                                var alertController:UIAlertController?
                                alertController?.view.tintColor = UIColor.blackColor()
                                alertController = UIAlertController(title: "Yay! You have successfully changed your password.",
                                                                    message: "",
                                                                    preferredStyle: .Alert)
                                
                                let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                                    Appconstant.pwd = self!.newPasswordTxt.text!
                                     self!.performSegueWithIdentifier("To_Home",sender: self)
                                    
                                    })
                                
                                alertController?.addAction(action)
                                self.presentViewController(alertController!, animated: true, completion: nil)
                                
                           
                        }
                    }
                
                }
            
            }
 
        task.resume()
    }
    func SavePsw()
    {
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            
            
            let insertSQL = "UPDATE PROFILE_INFO SET YAP_CODE = '"+self.newPasswordTxt.text!+"' WHERE CUSTOMER_ID = " + Appconstant.customerid
            
            let result = purzDB.executeUpdate(insertSQL,
                                              withArgumentsInArray: nil)
            if !result {
                //   status.text = "Failed to add contact"
                print("Error: \(purzDB.lastErrorMessage())")
               
            }
           
        }
         purzDB.close()
    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            
            dispatch_async(dispatch_get_main_queue()) {
              //  Appconstant.pushhome = true
                print(tabBarIndex)
            self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 1")
              dispatch_async(dispatch_get_main_queue()) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        else if tabBarIndex == 1 {
            print("Selected item 2")
              dispatch_async(dispatch_get_main_queue()) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
//        print("Selected item")
//            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewControllerWithIdentifier("HomeView")
//            
//            
//            //            let HomeVc: HomeViewController = HomeViewController()
//            //
//            self.navigationController?.pushViewController(controller, animated: true)
//            //            self.navigationController?.pushViewController(HomeVc, animated: true)
//        }
//        else if tabBarIndex == 1 {
//            print("Selected item")
//            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
//            
//            
//            //            let HomeVc: HomeViewController = HomeViewController()
//            //
//            self.navigationController?.pushViewController(controller, animated: true)
//            //            self.navigationController?.pushViewController(HomeVc, animated: true)
//        }
////        else{
////            print("Selected item")
////            
////            let storyboard = UIStoryboard(name: "Main", bundle: nil)
////            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
////            
////        }
////
//        
//        
//        print("Selected view controller")
//        
//    }
    
}
