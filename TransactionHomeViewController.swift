//
//  TransactionHomeViewController.swift
//  purZ
//
//  Created by Vertace on 24/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class TransactionHomeViewController: UIViewController,UITabBarControllerDelegate{
    
    @IBOutlet var trans_historyView: UIView!
    @IBOutlet var pending_transView: UIView!
    @IBOutlet weak var complaint_listView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialFunc()
        // Dispose of any resources that can be recreated.
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, trans_historyView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine.backgroundColor = UIColor.lightGrayColor().CGColor
        trans_historyView.layer.addSublayer(bottomLine)
        
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, pending_transView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine1.backgroundColor = UIColor.lightGrayColor().CGColor
        pending_transView.layer.addSublayer(bottomLine1)
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, complaint_listView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine2.backgroundColor = UIColor.lightGrayColor().CGColor
        complaint_listView.layer.addSublayer(bottomLine2)
        
        pending_transView.userInteractionEnabled = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TransactionHomeViewController.pendingTransactionFunc))
        pending_transView.addGestureRecognizer(tap)
        
        trans_historyView.userInteractionEnabled = true
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TransactionHomeViewController.transactionHistoryFunc))
        trans_historyView.addGestureRecognizer(tap1)
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TransactionHomeViewController.complaintListFunc))
        complaint_listView.addGestureRecognizer(tap2)
        
        
        tabBarController?.delegate = self
        Appconstant.filter = false
        // Do any additional setup after loading the view.
    }
    func initialFunc()
    {
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, trans_historyView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine.backgroundColor = UIColor.lightGrayColor().CGColor
        trans_historyView.layer.addSublayer(bottomLine)
        
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, pending_transView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine1.backgroundColor = UIColor.lightGrayColor().CGColor
        pending_transView.layer.addSublayer(bottomLine1)
        pending_transView.userInteractionEnabled = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "pendingTransactionFunc")
        pending_transView.addGestureRecognizer(tap)
        
        trans_historyView.userInteractionEnabled = true
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "transactionHistoryFunc")
        trans_historyView.addGestureRecognizer(tap1)
        
        tabBarController?.delegate = self
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func pendingTransactionFunc()
    {
        self.performSegueWithIdentifier("To_PendingTransaction", sender: self)
    }
    func transactionHistoryFunc()
    {
        self.performSegueWithIdentifier("To_TransactionHistory", sender: self)
    }
    func complaintListFunc() {
        self.performSegueWithIdentifier("Transactions_to_ComplaintList", sender: self)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
}
