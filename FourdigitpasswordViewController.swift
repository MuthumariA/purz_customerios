//
//  FourdigitpasswordViewController.swift
//  Cippy
//
//  Created by Vertace on 20/01/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import UIKit
import QuartzCore


class FourdigitpasswordViewController: UIViewController {
    var pwd: String = ""
    var counter: Int = 0
    var pwdarr = [String]()
    
    @IBOutlet weak var btn1: UIButton!
    
    @IBOutlet weak var btn2: UIButton!
    
   @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
     @IBOutlet weak var btn7: UIButton!
    
    @IBOutlet weak var btn8: UIButton!
    
    @IBOutlet weak var btn9: UIButton!
    
    @IBOutlet weak var btn0: UIButton!
    
    @IBOutlet weak var backwardbtn: UIButton!
    
    @IBOutlet weak var clickherebtn: UIButton!
    @IBOutlet weak var roundlab1: UILabel!
    
    @IBOutlet weak var roundlab2: UILabel!
    
    @IBOutlet weak var roundlab4: UILabel!
    @IBOutlet weak var roundlab3: UILabel!
    
    @IBOutlet weak var namelbl: UILabel!
          override func viewDidLoad() {
        super.viewDidLoad()
            if Appconstant.FromFourDigit_IDType
            {
                Appconstant.FromFourDigit_IDType = false
            }
             dispatch_async(dispatch_get_main_queue()) {
            self.getCustomerDetails()
            }
            navigationController?.navigationBarHidden = true
            
            print("cus!!!"+Appconstant.customername)
               namelbl.text = "Hi, "+Appconstant.customername+"!"
            
        namelbl.layer.borderColor = UIColor.clearColor().CGColor
//        namelbl.textColor = UIColor.yellowColor()
            // Do any additional setup after loading the view.
        btn1.layer.cornerRadius = self.btn1.frame.size.height/2
        btn2.layer.cornerRadius = self.btn2.frame.size.height/2
        
        btn3.layer.cornerRadius = self.btn3.frame.size.height/2
        btn4.layer.cornerRadius = self.btn4.frame.size.height/2
        btn5.layer.cornerRadius = self.btn5.frame.size.height/2

        btn6.layer.cornerRadius = self.btn6.frame.size.height/2
        btn7.layer.cornerRadius = self.btn7.frame.size.height/2
        btn8.layer.cornerRadius = self.btn8.frame.size.height/2

        btn9.layer.cornerRadius = self.btn9.frame.size.height/2
        
        btn0.layer.cornerRadius = self.btn0.frame.size.height/2
         backwardbtn.layer.cornerRadius = self.btn0.frame.size.height/2
        roundlab1.layer.cornerRadius = self.roundlab1.frame.size.height/2
        roundlab1.layer.masksToBounds = true
        roundlab1.layer.borderWidth = 1.0
        roundlab1.layer.borderColor = UIColor.blackColor().CGColor
        roundlab2.layer.cornerRadius = self.roundlab2.frame.size.height/2
        roundlab2.layer.masksToBounds = true
        roundlab2.layer.borderWidth = 1.0
       roundlab2.layer.borderColor = UIColor.blackColor().CGColor
        roundlab3.layer.cornerRadius = self.roundlab3.frame.size.height/2
        roundlab3.layer.masksToBounds = true
        roundlab3.layer.borderWidth = 1.0
        roundlab3.layer.borderColor = UIColor.blackColor().CGColor
        roundlab4.layer.cornerRadius = self.roundlab4.frame.size.height/2
        roundlab4.layer.masksToBounds = true
        roundlab4.layer.borderWidth = 1.0
        roundlab4.layer.borderColor = UIColor.blackColor().CGColor
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        clickherebtn.titleLabel?.attributedText = NSAttributedString(string: "Click here!", attributes: underlineAttribute)

        namedbcall()
    }
   func getCustomerDetails()
    {
          self.sendrequesttoserverForGetCustomerDetail(Appconstant.BASE_URL+Appconstant.URL_FETCH_CUSTOMER_DETAILS_BY_MOBILENO+Appconstant.mobileno)

    }
    
    func sendrequesttoserverForGetCustomerDetail(url : String)
    {
        print(url)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Bad Gateway", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseStringcustomerDetails = \(responseString)")
                let json = JSON(data: data!)
                //                dispatch_async(dispatch_get_main_queue()) {
                //                    self.activityIndicator.stopAnimating()
                //                }
                let item = json["result"]
      
                        let email = "empty"
                        Appconstant.CustomerType = item["customerType"].stringValue
                        Appconstant.KitNo = item["kitNo"].stringValue
                        Appconstant.dateOfBirth = item["dob"].stringValue
                        Appconstant.expiredate = item["expiryDateList"][0].stringValue
                        Appconstant.IdType = item["idType"].stringValue
                        Appconstant.IdNumber = item["idNumber"].stringValue
                        print("IdType\(Appconstant.IdType)")
                        print("IdNumber\(Appconstant.IdNumber)")
                        
                        ////                        let dateFormatter = NSDateFormatter()
                        ////                        dateFormatter.dateFormat = "dd MMM yyyy tt:mm:ss GMT"
                        ////                        dateFormatter.timeZone = NSTimeZone(name: "UTC")
                        ////                        let dateString = dateFormatter.stringFromDate(date)
                        //
                        //
                        //                        let dateFormatter = NSDateFormatter()
                        //                        dateFormatter.dateFormat = "dd MMM yyyy tt:mm:ss"
                        ////                        dateFormatter.timeZone = NSTimeZone(name: "UTC")
                        //                        let dateString = dateFormatter.stringFromDate(date)
                        ////                        (item["expiryDate"].stringValue)
                        //                         print(dateString)
                        //
                        ////                        let dateFormatter1 = NSDateFormatter()
                        ////                        dateFormatter1.dateFormat = "yyMM"
                        ////                        let dateString1 = dateFormatter1.stringFromDate(dateString!)
                        ////                        (dateString!)
                        //
                        //                                 print(dateString)
                        ////                        print(dateString1)
                        //

                
                
                    }
                }
             task.resume()
        
    }
    
    
    func namedbcall()
    {
let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
let databasePath = databaseURL.absoluteString
let purzDB = FMDatabase(path: databasePath as String)
if purzDB.open() {
    let selectSQL = "SELECT CUSTOMER_NAME FROM PROFILE_INFO"
    
    let result:FMResultSet! = purzDB.executeQuery(selectSQL,
        withArgumentsInArray: nil)
//    namelbl.text = result.stringForColumn("CUSTOMER_NAME")
    
        }
    }
    
    @IBAction func onebtnAction(sender: AnyObject) {
        pwd = pwd + "1"
        pwdarr.append("1")
    labelfill()
        
    }
    
    @IBAction func twobtnAction(sender: AnyObject) {
    pwd = pwd + "2"
        pwdarr.append("2")
        labelfill()
    }
    
    @IBAction func threebtnAction(sender: AnyObject) {
         pwd = pwd + "3"
        pwdarr.append("3")
        labelfill()
    }
        @IBAction func fourbtnAction(sender: AnyObject) {
         pwd = pwd + "4"
            pwdarr.append("4")
            labelfill()
    }
    
    @IBAction func fivebtnAction(sender: AnyObject) {
        pwd = pwd + "5"
        pwdarr.append("5")
        labelfill()
}
   
    @IBAction func sixbtnAction(sender: AnyObject) {
        pwd = pwd + "6"
        pwdarr.append("6")
        labelfill()

    }
    @IBAction func sevenbtnAction(sender: AnyObject) {
        pwd = pwd + "7"
        pwdarr.append("7")
        labelfill()

    }
    @IBAction func eightbtnAction(sender: AnyObject) {
   
            pwd = pwd + "8"
        pwdarr.append("8")
        labelfill()
    }
    
    @IBAction func ninebtnAction(sender: AnyObject) {
        pwd = pwd + "9"
        pwdarr.append("9")
        labelfill()

    }
    
    @IBAction func zerobtnAction(sender: AnyObject) {
        pwd = pwd + "0"
        pwdarr.append("0")
        labelfill()
}
    
    func labelfill()
    {
       print(pwd)
   if(pwdarr.count == 1)
    {
       roundlab1.backgroundColor = UIColor.blackColor()
        roundlab2.backgroundColor = UIColor.whiteColor()
        roundlab3.backgroundColor = UIColor.whiteColor()
        roundlab4.backgroundColor = UIColor.whiteColor()
    
    }
        else if(pwdarr.count == 2)
        {
             roundlab1.backgroundColor = UIColor.blackColor()
             roundlab2.backgroundColor = UIColor.blackColor()
            roundlab3.backgroundColor = UIColor.whiteColor()
            roundlab4.backgroundColor = UIColor.whiteColor()
        }
    
    else if(pwdarr.count == 3)
    {
        roundlab1.backgroundColor = UIColor.blackColor()
        roundlab2.backgroundColor = UIColor.blackColor()
        roundlab3.backgroundColor = UIColor.blackColor()
        roundlab4.backgroundColor = UIColor.whiteColor()
        }
    else if(pwdarr.count == 4)
    {
        roundlab1.backgroundColor = UIColor.blackColor()
        roundlab2.backgroundColor = UIColor.blackColor()
        roundlab3.backgroundColor = UIColor.blackColor()
        roundlab4.backgroundColor = UIColor.blackColor()
        
        if(pwd == Appconstant.pwd)
        {
            if Appconstant.IdNumber == ""
                && Appconstant.IdType == ""
                
            {
                dispatch_async(dispatch_get_main_queue()) {
                    Appconstant.FromFourDigit_IDType = true
                    self.performSegueWithIdentifier("To_GetId", sender: self)
                    
                }
            }
            else
            {self.performSegueWithIdentifier("fourdigittohome", sender: self)
            }
        }
           else
        {
            dispatch_async(dispatch_get_main_queue()) {
                self.presentViewController(Alert().alert("Invalid Password", message: ""),animated: true,completion: nil)
            }
            pwd = ""
            pwdarr.removeAll()
            roundlab1.backgroundColor = UIColor.whiteColor()
            roundlab2.backgroundColor = UIColor.whiteColor()
            roundlab3.backgroundColor = UIColor.whiteColor()
            roundlab4.backgroundColor = UIColor.whiteColor()
    }
   }
       else if(pwdarr.count == 0)
        {
            roundlab1.backgroundColor = UIColor.whiteColor()
            roundlab2.backgroundColor = UIColor.whiteColor()
            roundlab3.backgroundColor = UIColor.whiteColor()
            roundlab4.backgroundColor = UIColor.whiteColor()
        }

   }

    
    @IBAction func backward(sender: AnyObject) {
        
// var someNumb: Int = Int(pwd)
        if pwd != ""{
        let someNumb: Int = Int(pwd)!/10
        pwd = String(someNumb)
            if pwdarr.count > 0 {
        pwdarr.removeLast()
            }
        counter = pwd.characters.count
            labelfill()
        }
        
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ClickhereAction(sender: AnyObject) {
        Appconstant.notificationcount = 0
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(Appconstant.notificationcount, forKey: "purzbadgecount")
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        
        if purzDB.open() {
            Appconstant.mainbalance = ""
            let deleteSQL =  "DELETE FROM PROFILE_INFO"
            print(Appconstant.customerid)
            
            let deleteSQL2 =  "DELETE FROM CUSTOMERDETAIL"
            let delete3 = "DELETE FROM TRANSACTIONS"
            let delete4 = "DELETE FROM NOTIFICATION"
            let delete5 = "DELETE FROM ACCOUNTS"
            let delete6 = "DELETE FROM CVV_TABLE"
            

            let result = purzDB.executeUpdate(deleteSQL,
                                              withArgumentsInArray: nil)
            let result2 = purzDB.executeUpdate(deleteSQL2,
                                               withArgumentsInArray: nil)
            let result3 = purzDB.executeUpdate(delete3,
                                               withArgumentsInArray: nil)
            let result4 = purzDB.executeUpdate(delete4,
                                               withArgumentsInArray: nil)
            let result5 = purzDB.executeUpdate(delete5,
                                               withArgumentsInArray: nil)
                
           let result6 = purzDB.executeUpdate(delete6,
                                                   withArgumentsInArray: nil)

            
            if !result && !result2 && !result3 && !result4 && !result5 && !result6{
                //   status.text = "Failed to add contact"
                print("Error: \(purzDB.lastErrorMessage())")
            }
            else{
                dispatch_async(dispatch_get_main_queue()) {
                    Appconstant.customerid = ""
                    Appconstant.mobileno = ""
                    Appconstant.pwd = ""
                    Appconstant.firstname = ""
                Appconstant.lastname = ""
                    Appconstant.email = ""
                    Appconstant.otp = ""
                    Appconstant.customername = ""
                    
                    Appconstant.dummycustomer = false
                    Appconstant.newcustomer = false
                    Appconstant.mainbalance = ""
                    Appconstant.notificationcount = 0
                    Appconstant.gcmid = ""
                    Appconstant.Url = ""
                    
                    self.performSegueWithIdentifier("fourdigit_signin", sender: self)
                }
            }
        }
        purzDB.close()

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
