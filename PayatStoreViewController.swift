//
//  PayatStoreViewController.swift
//  Cippy
//
//  Created by apple on 28/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit
import AVFoundation

class PayatStoreViewController: UIViewController, UITextFieldDelegate, QRCodeReaderViewControllerDelegate, UITabBarControllerDelegate {
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var badgebtn: UIButton!
    @IBOutlet weak var merchantcodetxtField: UITextField!
    @IBOutlet weak var balancelbl: UILabel!
    @IBOutlet weak var remarkstxtField: UITextField!
    @IBOutlet weak var amounttxtField: UITextField!
    @IBOutlet var merchant_citylbl: UILabel!
    @IBOutlet weak var paybtn: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    //    @IBOutlet weak var merchant_citylbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var recentlbl: UILabel!
    
    @IBOutlet weak var recentView1: UIView!
    @IBOutlet weak var recentView2: UIView!
    
    @IBOutlet weak var recentMobileNolbl1: UILabel!
    @IBOutlet weak var recentAmtlbl1: UILabel!
    @IBOutlet weak var recentMobileNolbl2: UILabel!
    @IBOutlet weak var recentAmtlbl2: UILabel!
    
    @IBOutlet weak var repeatbtn1: UIButton!
    @IBOutlet weak var repeatbtn2: UIButton!
    
    
    
    
    
    @IBOutlet weak var refundBtn: UIButton!
    @IBOutlet weak var cancelbtn: UIButton!
    var fromotp = false
    var characterCountLimit = 4
    var merchantcode = ""
    var paystoreamount = ""
    var payremarks = ""
    var qrdata = ""
    
    var cardno = ""
    var merchantname = ""
    var mcc = ""
    var cityname = ""
    var countrycode = ""
    var indianrscode = ""
    var terminalid = ""
    var defaultvalue = ""
    var scrollheight:CGFloat = 450
    var mvisaid = ""
    var masterid = ""
    var RuPayId = ""
    var trans_amt = ""
    var tipid = ""
    var fixedtipamt = ""
    var FixedTipPercentage = ""
    
    var recentMerchantCode = [String]()
    var recentAmt = [String]()
    var recentQRdata = [String]()
    var qrvalue: QRCodeReaderResult!
    
    
    
    lazy var reader: QRCodeReaderViewController = {
        let builder = QRCodeViewControllerBuilder { builder in
            builder.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode])
            builder.showTorchButton = true
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
         dispatch_async(dispatch_get_main_queue()) {
        let getrecentViewModel = GetRecentViewModel.init(fromEntityId: Appconstant.customerid, utilityType: "PAY MERCHANT")!
        let serializedjson  = JSONSerializer.toJson(getrecentViewModel)
        
        self.getRecentTransactions(Appconstant.BASE_URL+Appconstant.GET_RECENT_TRANSACTIONS, values: serializedjson)
        self.hiderecent()
        }
        initialize()
        
    }
    
    func initialize() {
        amounttxtField.delegate = self
      

        navigationController?.navigationBarHidden = true
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, refundBtn.frame.height - 1, refundBtn.frame.width, 1.0)
        bottomLine2.backgroundColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha:1.0).CGColor;                refundBtn.layer.addSublayer(bottomLine2)
        let paddingForFirst1 = UIView(frame: CGRectMake(0, 0, 10, self.remarkstxtField.frame.size.height))
        remarkstxtField.leftView = paddingForFirst1
        remarkstxtField.leftViewMode = UITextFieldViewMode .Always
        
        Appconstant.SideMenu = 0;
        
        // Do any additional setup after loading the view, typically from a nib.
        navigationController?.navigationBarHidden = true
        tabBarController?.delegate = self
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, merchantcodetxtField.frame.height - 0 , merchantcodetxtField.frame.width+50, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;        merchantcodetxtField.borderStyle = UITextBorderStyle.None
        merchantcodetxtField.layer.addSublayer(bottomLine)
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, amounttxtField.frame.height - 2, amounttxtField.frame.width+50, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;        amounttxtField.borderStyle = UITextBorderStyle.None
        amounttxtField.layer.addSublayer(bottomLine1)
        
        cancelbtn.layer.cornerRadius = 15
        cancelbtn.layer.borderColor = UIColor.redColor().CGColor
        cancelbtn.layer.borderWidth = 2
        paybtn.layer.cornerRadius = 15
        paybtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        remarkstxtField.layer.cornerRadius = 5
        paybtn.layer.borderWidth = 2
        badgebtn.userInteractionEnabled = false
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PayatStoreViewController.dismissKeyboard))
        scrollView.addGestureRecognizer(tap)
        badgebtn.layer.cornerRadius = self.badgebtn.frame.size.height/2
        
        balancelbl.text = self.balancelbl.text! + " \u{20B9} " + Appconstant.mainbalance
        
        
        funcforcallserver()
        
    }
    
    func hiderecent() {
        recentlbl.hidden = true
        recentView1.hidden = true
        recentView2.hidden = true
        repeatbtn1.layer.cornerRadius = 5
        repeatbtn2.layer.cornerRadius = 5
        setShadow(recentView1)
        setShadow(recentView2)
    }
    
    func setShadow(view: UIView) {
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowOffset = CGSizeZero
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 3
        view.layer.cornerRadius = 5
    }
    
    @IBAction func cancelBtnAction(sender: AnyObject) {
        backBtn.sendActionsForControlEvents(.TouchUpInside);
        
    }
    
    @IBAction func refundBtnAction(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: "http://yappay.in/app-static/eqwallet/tnc.html")!)
        
        
    }
    func funcforcallserver(){
        if(Appconstant.notificationcount > 0){
            badgebtn.hidden = false
            badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
        }
        else{
            badgebtn.hidden = true
        }
        if fromotp{
            merchantcodetxtField.text = merchantcode
            amounttxtField.text = paystoreamount
            remarkstxtField.text = payremarks
            activityIndicator.startAnimating()
            callserverForPay()
        }
        if(!merchantcode.isEmpty || !paystoreamount.isEmpty || !payremarks.isEmpty){
            merchantcodetxtField.text = merchantcode
            amounttxtField.text = paystoreamount
            remarkstxtField.text = payremarks
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == merchantcodetxtField{
            amounttxtField.becomeFirstResponder()
        }
        if textField == amounttxtField{
            remarkstxtField.becomeFirstResponder()
        }
        
        
        return true // We do not want UITextField to insert line-breaks.
    }
    func textFieldShouldBeginEditing(state: UITextField) -> Bool {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 530)
        return true
    }
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 530)
        if(textFieldToChange.placeholder == "Enter 4 digit Password"){
            characterCountLimit = 4
        }
       else if textFieldToChange.placeholder == "Amount"
        {
//       if textFieldToChange == amounttxtField
//        {
            let currentText = amounttxtField.text ?? ""
            let replacementText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Validate
            return replacementText.isValidDecimal(2)
//        }
        }
        else if(textFieldToChange == remarkstxtField){
            characterCountLimit = 32
        }
        else if((amounttxtField.text?.rangeOfString(".")) != nil){
            let strcount = amounttxtField.text! + string
            let strarray = strcount.componentsSeparatedByString(".")
            for(var i = 0; i<strarray.count; i++){
                if i == 1{
                    if strarray[1].isEmpty{
                        
                    }
                    else{
                        if strarray[1].characters.count == 3{
                            return false
                        }
                        else{
                            return true
                        }
                    }
                }
            }
        }
            
        else{
            characterCountLimit = 16
        }
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    }
    
    @IBAction func payBtnAction(sender: AnyObject) {
        if(merchantcodetxtField.text!.isEmpty){
            self.presentViewController(Alert().alert("Oops! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if(merchantcodetxtField.text?.characters.count < 5 || merchantcodetxtField.text?.characters.count > 16){
            self.presentViewController(Alert().alert("Ohh no! Invalid Merchant,punch in a correct one", message: ""),animated: true,completion: nil)
        }
            
        else if(amounttxtField.text!.isEmpty){
            self.presentViewController(Alert().alert("Please enter valid amount", message: ""),animated: true,completion: nil)
        }
        else if(Double(amounttxtField.text!)! < 1.0 || Double(amounttxtField.text!)! > 10000.0){
            self.presentViewController(Alert().alert("Please enter amount between Rs.1 - Rs.10000", message: ""),animated: true,completion: nil)
        }
        else if(Double(amounttxtField.text!)! > Double(Appconstant.mainbalance)!){
            
//            let addmoneyviewmodel = AddMoneyViewModel.init(entityId: Appconstant.customerid, amount: Float(Double(self.amounttxtField.text!)!-Double(Appconstant.mainbalance)!), pgType: "", bankName: "")!
//            let serializedjson  = JSONSerializer.toJson(addmoneyviewmodel)
//            print(serializedjson)
//            self.sendrequesttoserverBillDeskIntegration(Appconstant.BASE_URL+Appconstant.URL_BILL_DESK_INTEGRATION, values: serializedjson)
//            Oops! Insufficient Funds..Add some money now, it's simple!
            self.presentViewController(Alert().alert("Oops! Insufficient Funds..", message: ""),animated: true,completion: nil)
        }
        else{
            alertforproceed()
        }
        
    }
    
    func alertforproceed(){
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "PASSWORD",
                                            message: "Please enter your 4 digit secret password. This is necessary to initiate every transaction",
                                            preferredStyle: .Alert)
        alertController!.addTextFieldWithConfigurationHandler(
            {(textField: UITextField!) in
                
                textField.placeholder = "Enter 4 digit Password"
                textField.delegate = self
                textField.secureTextEntry  = true
                textField.keyboardType = UIKeyboardType.NumberPad
                
        })
        let action = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            if let textFields = alertController?.textFields{
                let theTextFields = textFields as [UITextField]
                let password = theTextFields[0].text!
                if((password == "") || (password.characters.count < 4) || (Appconstant.pwd != password)){
                    self!.alert()
                }
                else{
                    self!.callserverForPay()
                }
                
            }
            })
        let action1 = UIAlertAction(title: "Forgot?", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            self!.activityIndicator.startAnimating()
            print(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
            self!.sendrequesttoserverForForgotPassword(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
            
            })
        let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            })
        
        alertController?.addAction(action)
        alertController?.addAction(action1)
        alertController?.addAction(action2)
        self.presentViewController(alertController!, animated: true, completion: nil)
    }
    func alert(){
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(Alert().alert("Please enter a valid Password", message: ""),animated: true,completion: nil)
        }
    }
    func sendrequesttoserverForForgotPassword(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                self.activityIndicator.stopAnimating()
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                self.activityIndicator.stopAnimating()
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
                
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            self.activityIndicator.stopAnimating()
            let item = json["result"]
            if(item["success"].stringValue == "true"){
                dispatch_async(dispatch_get_main_queue()) {
                    Appconstant.otp = item["otp"].stringValue
                    self.performSegueWithIdentifier("pay_otp", sender: self)
                }
            }
                
            else{
                dispatch_async(dispatch_get_main_queue()) {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
            }
        }
        
        task.resume()
        
    }
    
    
    
    func sendrequesttoserverBillDeskIntegration(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                print(error)
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                let item = json["result"]
                print(json["result"])
                print(json["result"].stringValue)
                if json["result"].isEmpty{
                    let item1 = json["exception"]
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert(item1["detailMessage"].stringValue, message: ""),animated: true,completion: nil)
                    }
                }
                else{
                    
                    Appconstant.Url = item["targetUrl"].stringValue + "?msg=" + item["requestMessage"].stringValue
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.performSegueWithIdentifier("to_webPage", sender: self)
                    }
                }
            }
        }
        
        task.resume()
        
    }
    
    
    
    func callserverForPay(){
        var passcode = Appconstant.pwd
        let checkpwd: Int = Int(passcode.substringWithRange(passcode.startIndex.advancedBy(0)..<passcode.startIndex.advancedBy(1)))!
        print(checkpwd)
        if checkpwd == 0 {
            passcode = "1" + passcode
        }
        
        let payviewmodel = PayAtStoreViewModel.init(amount: self.amounttxtField.text!, description: self.remarkstxtField.text!, fromEntityId: Appconstant.customerid, toEntityId: self.merchantcodetxtField.text!, productId: "GENERAL", yapcode: passcode, transactionType: "PURCHASE", transactionOrigin: "MOBILE", businessId: "", business: "EQWALLET", businessType: "EQWALLET", qrData: self.qrdata, merchantData: qrdata)!
        let serializedjson  = JSONSerializer.toJson(payviewmodel)
        print(serializedjson)
        self.activityIndicator.startAnimating()
        self.sendrequesttoserverForPay(Appconstant.BASE_URL+Appconstant.URL_PAY_STORE, values: serializedjson)
    }
    
    func sendrequesttoserverForPay(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                self.activityIndicator.stopAnimating()
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            self.updatetransactionDB()
            let json = JSON(data: data!)
            self.activityIndicator.stopAnimating()
            let item = json["result"]
            if json["result"] != nil{
                if(item["txId"].stringValue != ""){
                    dispatch_async(dispatch_get_main_queue()) {
                        let addrecentViewModel = AddRecentViewModel.init(fromEntityId: Appconstant.customerid, toContactNo: "", amount: self.amounttxtField.text!, txnType: "C2M", operatorCode: "", serviceProvider: self.merchant_citylbl.text!, utilityType: "PAY MERCHANT", qrCode: self.qrdata, authorization1: self.merchantcodetxtField.text!, authorization2: "", authorization3: "", tip: "")!
                        let serializedjson  = JSONSerializer.toJson(addrecentViewModel)
                        
                        self.AddRecentTransactions(Appconstant.BASE_URL+Appconstant.ADD_RECENT_TRANSACTION, values: serializedjson)
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        self.merchantcodetxtField.text = ""
                        self.amounttxtField.text = ""
                        self.remarkstxtField.text = ""
                    }
                }
            }
            else if json["exception"] != ""{
                let item = json["exception"]
                if item["errorCode"].stringValue == "Y104"{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Pass code is Invalid", message: ""),animated: true,completion: nil)
                        self.merchantcodetxtField.text = ""
                        self.amounttxtField.text = ""
                        self.remarkstxtField.text = ""
                    }
                }
                else if item["errorCode"].stringValue == "Y202"{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Invalid Data", message: ""),animated: true,completion: nil)
                        self.merchantcodetxtField.text = ""
                        self.amounttxtField.text = ""
                        self.remarkstxtField.text = ""
                    }
                }
                else if item["errorCode"] == nil{
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        if item["shortMessage"] == nil{
                            self.presentViewController(Alert().alert("Internal server Error", message: ""),animated: true,completion: nil)
                        }
                        else
                        {
                            self.presentViewController(Alert().alert(item["shortMessage"].string!, message: ""),animated: true,completion: nil)
                        }
                        self.merchantcodetxtField.text = ""
                        self.amounttxtField.text = ""
                        self.remarkstxtField.text = ""
                        self.merchant_citylbl.text = ""
                        //item["shortMessage"].stringValue
                    }
                }
            }
            
        }
        
        task.resume()
        
    }
    
    @IBAction func repeatBtn1Action(sender: AnyObject) {
        print(self.recentMerchantCode)
            merchantcodetxtField.text = self.recentMerchantCode[0]
            amounttxtField.text = self.recentAmt[0]
        self.qrdata = self.recentQRdata[0]

        
    }
    
    @IBAction func repeatBtn2Action(sender: AnyObject) {
        merchantcodetxtField.text = recentMerchantCode[self.recentMerchantCode.count-1]
        amounttxtField.text = self.recentAmt[self.recentAmt.count-1]
         self.qrdata = self.recentQRdata[self.recentQRdata.count-1]
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
              if Appconstant.fromweb {
            Appconstant.fromweb = false
            self.activityIndicator.startAnimating()
            getbalancefromServer(Appconstant.BASE_URL+Appconstant.URL_FETCH_MULTI_BALANCE_INFO+Appconstant.customerid)
        }
        else {
            merchantcodetxtField.becomeFirstResponder()
        }
        
        
    }
    
    @IBAction func qrscanBtnAction(sender: AnyObject) {
        
        if QRCodeReader.supportsMetadataObjectTypes() {
            reader.modalPresentationStyle = .FormSheet
            reader.delegate = self
            dispatch_async(dispatch_get_main_queue()) {
                self.reader.completionBlock = { (result: QRCodeReaderResult?) in
                    if let result = result {
                        print("Completion with result: \(result.value) of type \(result.metadataType)")
                    }
                }
            }
            presentViewController(reader, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            
            presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    func reader(reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        
        self.dismissViewControllerAnimated(true, completion: { [weak self] in
            if result.value.lowercaseString.containsString("upi")  {
                self!.qrvalue = result
                self!.presentViewController(Alert().alert("Invalid QR!", message: ""),animated: true,completion: nil)
//                self?.performSegueWithIdentifier("To_sendmoneyVPA", sender: self)
            }
            else {
            self!.getQRDataFromJSON(result.value)
            }
            print(result)
            
            })
    }
    
    func readerDidCancel(reader: QRCodeReaderViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //    func getQRDataFromJSON(str: String)
    //    {
    //        let nsData = (str as NSString).dataUsingEncoding(NSUTF8StringEncoding)
    //        let reader = JSON(data: nsData!)
    //        print(reader)
    //    }
    
    func checkForValidMerchantID(merchantID : String) -> Bool
    {
        let badCharacters = NSCharacterSet.decimalDigitCharacterSet().invertedSet
        
        if merchantID.rangeOfCharacterFromSet(badCharacters) == nil {
            return true
        } else {
            return false
        }
    }
    
    
    func getQRDataFromJSON(str: String)
    {
          let qrimg = str.substringWithRange(str.startIndex.advancedBy(12)..<str.startIndex.advancedBy(14))
        let qr = str.substringWithRange(str.startIndex.advancedBy(0)..<str.startIndex.advancedBy(2))
        print(qr)
        if qr == "00"{
           
            getBharathQRdata(str)
        }
        else
        {
           getQRData(str)
        }
       
        
        //        let nsData = (str as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        //        let reader = JSON(data: nsData!)
        //        let merchantID = str
        
       
        
    }
    func getBharathQRdata(str: String)
    {
        print(str)
        qrdata = str
        var alphaNumeric = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var characters = [Character](str.characters)
        
        let n = str.characters.count
        //        print (n)
        var i:Int = 0;
        //        print(i)
        if str == "" || str.characters.count <= 10
        {
            // txtMerchantID.text = "1";
        }
        
        if str.characters.first  != "0"
        {
            // txtMerchantID.text = "1";
        }
        else
        {
            var startindex = 0
            
            while(i < n-1)
            {
                var len = 0
                var tmp  = ""
                let current_pos = str.substringWithRange(str.startIndex.advancedBy(startindex)..<str.startIndex.advancedBy(startindex+2))
                print("POS:\(current_pos)")
                startindex = startindex + 2
                let len_value = str.substringWithRange(str.startIndex.advancedBy(startindex)..<str.startIndex.advancedBy(startindex+2))
                var num = Int(len_value)
                if num == nil {
                    callalertfunc()
                    break
                }
                len = Int(len_value)!
                startindex = startindex + len + 2
                print("LEN:\(len)")
                len = len + 1
                if len<2{
                    break
                }
                else{
                    i = i + 2
                    for j in 2...len
                    {
                        tmp = tmp + String(characters[i+j])
                        //                        print(tmp)
                    }
                    print("VALUE\(tmp)")
                    
                    
                    
                    let no = Int(current_pos)!
                    
                    
                    
                    if no == 0
                    {
                        
                        
                        //                        defaultvalue = tmp
                        //                        merchantcodetxtField.text = defaultvalue
                    }
                        
                    else if no == 2
                    {
                        cardno = tmp ;
                        terminalid = tmp
                        mvisaid = tmp
                        merchantcodetxtField.text = tmp
                        //                        merchantname = tmp
                        
                    }
                    else if no == 4
                    {
                        if merchantcodetxtField.text!.isEmpty{
                            merchantcodetxtField.text = tmp
                        }
                        cardno = tmp
                        defaultvalue = tmp
                        masterid = tmp
                        
                        
                    }
                    else if no == 6
                    {
                        if merchantcodetxtField.text!.isEmpty{
                            merchantcodetxtField.text = tmp
                        }
                        cardno = tmp
                        defaultvalue = tmp
                        RuPayId = tmp
                        
                    }
                    else if no == 52
                    {
                        mcc = tmp
                        
                    }
                    else if no == 53
                    {
                        indianrscode = tmp
                    }
                    else if no == 54{
                        trans_amt = tmp
                        amounttxtField.text = tmp
                    }
                    else if no == 55{
                        tipid = tmp
                    }
                    else if no == 56{
                        fixedtipamt = tmp
                       
                    }
                    else if no == 57{
                        FixedTipPercentage = tmp
                    }
                    else if no == 58
                    {
                        countrycode = tmp
                        //                        terminalid = tmp
                    }
                    else if no == 59
                    {
                        merchantname = tmp
                        
                        //                        if str.containsString("M2PY")
                        
                    }
                    else if no == 60{
                        cityname = tmp
                    }
                    
                    i = (i+1) + len;
                    print (tmp)
                }
                print(merchantname + "-" + cityname)
                
                merchant_citylbl.text = merchantname + "-" + cityname
//                qrcardimg.hidden = false
            }
        }
//        remarksconstraint.constant = 78
//        tiptxtField.hidden = false
//        tiplinelbl.hidden = false
//        tiptxtlbl.hidden = false
        
    }
    
    //Parsing QR data from mVisa format
    func getQRData(str: String)
    {
        print(str)
        qrdata = str
        var alphaNumeric = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var characters = [Character](str.characters)
        
        let n = str.characters.count
        print (n)
        var i:Int = 0;
        print(i)
        if str == "" || str.characters.count <= 10
        {
            // txtMerchantID.text = "1";
        }
        
        if str.characters.first  != "0"
        {
            // txtMerchantID.text = "1";
        }
        else
        {
            while(i < n )
            {
                var tmp  = "";
                
                var len:Int = 0
                if characters[i+1] == "A" {      len = 10      }
                else if characters[i+1] == "B" {      len = 11      }
                else if characters[i+1] == "C" {      len = 12      }
                else if characters[i+1] == "D" {      len = 13      }
                else if characters[i+1] == "E" {      len = 14      }
                else if characters[i+1] == "F" {      len = 15      }
                else if characters[i+1] == "G" {      len = 16      }
                else if characters[i+1] == "H" {      len = 17      }
                else if characters[i+1] == "I" {      len = 18      }
                else if characters[i+1] == "J" {      len = 19      }
                else if characters[i+1] == "K" {      len = 20      }
                else if characters[i+1] == "L" {      len = 21      }
                else if characters[i+1] == "M" {      len = 22      }
                else if characters[i+1] == "N" {      len = 23      }
                else if characters[i+1] == "O" {      len = 24      }
                else if characters[i+1] == "P" {      len = 25      }
                else if characters[i+1] == "Q" {      len = 26      }
                else if characters[i+1] == "R" {      len = 27      }
                else if characters[i+1] == "S" {      len = 28      }
                else if characters[i+1] == "T" {      len = 29      }
                else if characters[i+1] == "U" {      len = 30      }
                else if characters[i+1] == "V" {      len = 31      }
                else if characters[i+1] == "W" {      len = 32      }
                else if characters[i+1] == "X" {      len = 33      }
                else if characters[i+1] == "Y" {      len = 34      }
                else if characters[i+1] == "Z" {      len = 35      }
                else if characters[i+1] == "1" {      len = 1      }
                else if characters[i+1] == "2" {      len = 2      }
                else if characters[i+1] == "3" {      len = 3      }
                else if characters[i+1] == "4" {      len = 4      }
                else if characters[i+1] == "5" {      len = 5      }
                else if characters[i+1] == "6" {      len = 6      }
                else if characters[i+1] == "7" {      len = 7      }
                else if characters[i+1] == "8" {      len = 8      }
                else if characters[i+1] == "9" {      len = 9      }
                
                // print(alphaNumeric.startIndex.distanceTo((alphaNumeric.rangeOfString(characters[i+1])?.startIndex)!))
                
                // var len = alphaNumeric.startIndex.distanceTo((alphaNumeric.rangeOfString(characters[i+1])?.startIndex)!)+2;
                //  var len = str.startIndex.distanceToq((alphaNumeric.rangeOfString(characters[i+1])?.startIndex)!) + 2;
                
                
                //  print(len)
                len = len + 1
                print(len)
                if len < 2{
                    callalertfunc()
                    break
                }
                else{
                    for j in 2...len
                    {
                        //print(j)
                        
                        tmp = tmp + String(characters[i+j])
                        print(tmp)
                    }
                    
                    let c = characters[i];
                    
                    
                    if str.containsString("M2PY")
                    {
                        print(c)
                        if c == "N"
                        {
                            
                            defaultvalue = tmp
                            merchantcodetxtField.text = defaultvalue
                            
                            //                        let merchantIDCount = tmp.characters.count;
                            //                        let substring = merchantIDCount - 2
                            //                        merchantcodetxtField.text = (tmp as NSString).substringToIndex(substring);
                            //                        print("merchantID"+merchantcodetxtField.text!)
                            break;
                            // self.txtMerchantID.text = tmp
                            
                        }
                        
                    }
                    else{
                        
                        print(c)
                        if c == "0"
                        {
                            cardno = tmp ;
                            defaultvalue = tmp
                            merchantcodetxtField.text = defaultvalue
                        }
                    }
                    
                    //               if c == "0"
                    //                {
                    //                        cardno = tmp ;
                    //                        defaultvalue = tmp
                    //                        merchantcodetxtField.text = defaultvalue
                    //                }
                    if c == "1"
                    {
                        merchantname = tmp
                        
                    }
                    if c == "2"
                    {
                        mcc = tmp
                    }
                    if c == "3"
                    {
                        cityname = tmp
                    }
                    if c == "4"
                    {
                        countrycode = tmp
                    }
                    if c == "5"
                    {
                        indianrscode = tmp
                    }
                    if c == "N"
                    {
                        terminalid = tmp
                    }
                    if c == "6"
                    {
                        
                        let amt = Float(tmp)!
                        self.amounttxtField.text = "\(amt)"
                        if str.containsString("M2PY")
                        {
                        }
                        else{
                            break;
                        }
                    }
                    
                    i = (i+1) + len;
                    print (tmp)
                }
                merchant_citylbl.text = merchantname + "-" + cityname
            }
        }
        
    }
    func callalertfunc(){
        self.presentViewController(Alert().alert("Invalid QR!", message: ""),animated: true,completion: nil)
    }
    func dismissKeyboard(){
        view.endEditing(true)
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: scrollheight)
    }
    
    func updatetransactionDB(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        
        if purzDB.open() {
            let delete = "DELETE FROM TRANSACTIONS"
            
            let result = purzDB.executeUpdate(delete,
                                              withArgumentsInArray: nil)
            
            if !result{
                //   status.text = "Failed to add contact"
                print("Error: \(purzDB.lastErrorMessage())")
            }
            
        }
        purzDB.close()
        
        let request = NSMutableURLRequest(URL: NSURL(string: Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=10")!)
        request.HTTPMethod = "GET"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                self.view.userInteractionEnabled = true
                
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            
            for item1 in json["result"].arrayValue{
                let item = item1["transaction"]
                
                var transactionamt = ""
                var ben_id = ""
                var trans_date = ""
                var descriptions = ""
                let balance_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                let amt = balance_two_decimal.componentsSeparatedByString(".")
                if(amt[1].characters.count == 1){
                    let finalamount = balance_two_decimal + "0"
                    transactionamt = finalamount
                }
                else{
                    transactionamt = balance_two_decimal
                }
                if(!item["beneficiaryId"].stringValue.isEmpty){
                    let benid = item["beneficiaryId"].stringValue.componentsSeparatedByString("+91")
                    print(benid)
                    var i = 0
                    for(i=0; i<benid.count; i++){
                        
                    }
                    ben_id = benid[i-1]
                }
                else{
                    ben_id = item["beneficiaryId"].stringValue
                }
                let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd MMM"
                dateFormatter.timeZone = NSTimeZone(name: "UTC")
                let dateString = dateFormatter.stringFromDate(date)
                if(!dateString.isEmpty){
                    let datearray = dateString.componentsSeparatedByString(" ")
                    if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                        let correctdate = datearray[0] + "st " + datearray[1]
                        trans_date = correctdate
                    }
                    else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                        let correctdate = datearray[0] + "nd " + datearray[1]
                        trans_date = correctdate
                    }
                    else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                        let correctdate = datearray[0] + "rd " + datearray[1]
                        trans_date = correctdate
                    }
                    else{
                        let correctdate = datearray[0] + "th " + datearray[1]
                        trans_date = correctdate
                    }
                    
                }
                let matches = self.matchesForRegexInText("[0-9]", text: item["description"].stringValue)
                let desc = matches.joinWithSeparator("")
                descriptions = desc
                
                if purzDB.open() {
                    
                    let insert = "INSERT INTO TRANSACTIONS (AMOUNT,BENEFICIARY_ID,TRANSACTION_TYPE,TYPE,TIME,TRANSACTION_STATUS,TX_REF,BENEFICIARY_NAME,DESCRIPTION,OTHER_PARTY_NAME,OTHER_PARTY_ID,TXN_ORIGIN,TRANSACTIONID) VALUES"
                    let value0 =  "('"+transactionamt+"','\(ben_id)','\(item["transactionType"].stringValue)','\(item["type"].stringValue)',"
                    let value1 = "'"+trans_date+"','\(item["transactionStatus"].stringValue)','\(item["txRef"].stringValue)','\(item["beneficiaryName"].stringValue)',"
                    let value2 = "'\(descriptions)','\(item["otherPartyName"].stringValue)','\(item["otherPartyId"].stringValue)','\(item["txnOrigin"].stringValue)','\(item["externalTransactionId"].stringValue)')"
                    let insertsql = insert+value0+value1+value2
                    let result = purzDB.executeUpdate(insertsql,
                                                      withArgumentsInArray: nil)
                    
                    if !result {
                        //   status.text = "Failed to add contact"
                        print("Error: \(purzDB.lastErrorMessage())")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                    purzDB.close()
                    
                }
            }
        }
        
        task.resume()
        
        
    }
    func matchesForRegexInText(regex: String, text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                                                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "pay_otp") {
            let nextview = segue.destinationViewController as! OTPViewController
            nextview.frompay = true
            nextview.merchantcode = self.merchantcodetxtField.text!
            nextview.paystoreamount = self.amounttxtField.text!
            nextview.payremarks = self.remarkstxtField.text!
            nextview.qrdata = self.qrdata
            
        }
        else if segue.identifier == "to_webPage"{
            let nextview = segue.destinationViewController as! WebViewController
            nextview.fromPayMerchant = true
        }
        else if segue.identifier == "To_sendmoneyVPA" {
            let nextview = segue.destinationViewController as! sendMoneytoVPAViewController
            nextview.qrvalue = self.qrvalue
            nextview.fromPayatStore = true
        }

    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
            
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    func AddRecentTransactions(url : String, values: String)
    {
        print(url)
        print(values)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            print(json)
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("pay_success", sender: self)
                self.merchantcodetxtField.text = ""
                self.amounttxtField.text = ""
                self.remarkstxtField.text = ""
            }
            
        }
        
        task.resume()
        
        
    }
    
    func getRecentTransactions(url : String, values: String)
    {
        print(url)
        print(values)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                
            }
             dispatch_async(dispatch_get_main_queue()) {
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            
            let result = json["result"]
            
            for item in result["content"].arrayValue {
                self.recentMerchantCode.append(item["authorization1"].stringValue)
                self.recentAmt.append(item["amount"].stringValue)
                self.recentQRdata.append(item["qrCode"].stringValue)
            }
            
            self.setRecentTransaction()
            }
        }
        
        task.resume()
        
        
    }
    
    func setRecentTransaction() {
         dispatch_async(dispatch_get_main_queue()) {
        if self.recentAmt.count == 1 {
           
            self.recentView1.hidden = false
            self.recentMobileNolbl1.text = self.recentMerchantCode[0]
           
            self.recentAmtlbl1.text = "\u{20B9}" + self.recentAmt[0] + " | PAY MERCHANT"
            self.scrollheight = 540
            self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.scrollheight)
             self.recentlbl.hidden = false
        }
        else if self.recentAmt.count > 0 {
           
            self.recentView1.hidden = false
            self.recentView2.hidden = false
            self.recentMobileNolbl1.text = self.recentMerchantCode[0]
            self.recentAmtlbl1.text = "\u{20B9}" + self.recentAmt[0] + " | PAY MERCHANT"
            self.recentMobileNolbl2.text = self.recentMerchantCode[self.recentMerchantCode.count-1]
            self.recentAmtlbl2.text = "\u{20B9}" + self.recentAmt[self.recentMerchantCode.count-1] + " | PAY MERCHANT"
            self.scrollheight = 630
            self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.scrollheight)
             self.recentlbl.hidden = false
        }
        }
    }
    
    @IBAction func repeat1BtnAction(sender: AnyObject) {
        self.merchantcodetxtField.text = self.recentMerchantCode[self.recentMerchantCode.count-1]
        self.amounttxtField.text = self.recentAmt[self.recentAmt.count-1]
        self.qrdata = self.recentQRdata[self.recentQRdata.count-1]

    }
    
    @IBAction func repeat2BtnAction(sender: AnyObject) {
        self.merchantcodetxtField.text = self.recentMerchantCode[0]
        self.amounttxtField.text = self.recentAmt[0]
        self.qrdata = self.recentQRdata[0]
        
    }
    
    
    func getbalancefromServer(url: String){
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                
                for item in json["result"].arrayValue{
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        
                        if item["balance"].doubleValue >= Double(self.amounttxtField.text!) {
                            self.callserverForPay()
                        }
                        else {
                            dispatch_async(dispatch_get_main_queue()) {
                                self.activityIndicator.stopAnimating()
                                self.presentViewController(Alert().alert("Oops! Insufficient Funds..Add some money now, it’s simple!", message: ""),animated: true,completion: nil)
                            }
                        }
                        
                        
                    }
                }
                
            }
        }
        task.resume()
    }
    
    
}
extension String{
    
    private static let decimalFormatter:NSNumberFormatter = {
        let formatter = NSNumberFormatter()
        formatter.allowsFloats = true
        return formatter
    }()
    
    private var decimalSeparator:String{
        return String.decimalFormatter.decimalSeparator ?? "."
    }
    
    func isValidDecimal(maximumFractionDigits:Int)->Bool{
        
        // Depends on you if you consider empty string as valid number
        guard self.isEmpty == false else {
            return true
        }
        
        // Check if valid decimal
        
        if String.decimalFormatter.numberFromString(self) != nil{
            
            // Get fraction digits part using separator
            let numberComponents = self.componentsSeparatedByString(decimalSeparator)
            
            let fractionDigits = numberComponents.count == 2 ? numberComponents.last ?? "" : ""
            return fractionDigits.characters.count <= maximumFractionDigits
        }
        
        return false
    }
    
}


