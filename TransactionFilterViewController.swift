//
//  TransactionFilterViewController.swift
//  purZ
//
//  Created by Vertace on 02/08/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class TransactionFilterViewController: UIViewController,UITabBarControllerDelegate {
    @IBOutlet weak var successView: UIView!
    @IBOutlet weak var rejectView: UIView!
    @IBOutlet weak var creditView: UIView!
    @IBOutlet weak var debitView: UIView!
    @IBOutlet weak var PopupView: UIView!
    
    @IBOutlet weak var successCheckboxImg: UIImageView!
    @IBOutlet weak var rejectCheckboxImg: UIImageView!
    @IBOutlet weak var debitCheckboxImg: UIImageView!
    @IBOutlet weak var creditCheckboxImg: UIImageView!
    
    var successCheckBox = false
    var rejectCheckBox = false
    var creditCheckBox = false
    var debitCheckBox = false
    
    var filter_StartDate = ""
    var filter_EndDate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialize()
    }
    
    
    func initialize() {
        tabBarController?.delegate = self
        Appconstant.filter = true
        view.userInteractionEnabled = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TransactionFilterViewController.dismissView))
        view.addGestureRecognizer(tap)
        
        successView.userInteractionEnabled = true
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TransactionFilterViewController.sucessChechBoxAction))
        successView.addGestureRecognizer(tap1)
        successView.backgroundColor = UIColor.clearColor()
        
        rejectView.userInteractionEnabled = true
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TransactionFilterViewController.rejectChechBoxAction))
        rejectView.addGestureRecognizer(tap2)
        rejectView.backgroundColor = UIColor.clearColor()
        
        creditView.userInteractionEnabled = true
        let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TransactionFilterViewController.creditChechBoxAction))
        creditView.addGestureRecognizer(tap3)
        creditView.backgroundColor = UIColor.clearColor()
        
        debitView.userInteractionEnabled = true
        let tap4: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TransactionFilterViewController.debitChechBoxAction))
        debitView.addGestureRecognizer(tap4)
        debitView.backgroundColor = UIColor.clearColor()
        
        PopupView.userInteractionEnabled = true
        let tapBack: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TransactionFilterViewController.popupViewAction))
        PopupView.addGestureRecognizer(tapBack)
        
        if Appconstant.debitCheckBox {
            debitCheckBox = true
            debitCheckboxImg.image = UIImage(named: "Green_checkbox")
        }
        if Appconstant.creditCheckBox {
            creditCheckBox = true
            creditCheckboxImg.image = UIImage(named: "Green_checkbox")
        }
        if Appconstant.successCheckBox {
            successCheckBox = true
            successCheckboxImg.image = UIImage(named: "Green_checkbox")
        }
        if Appconstant.rejectCheckBox {
            rejectCheckBox = true
            rejectCheckboxImg.image = UIImage(named: "Green_checkbox")
        }
        
    }
    
    func popupViewAction()
    {
    }
    
    func sucessChechBoxAction()
    {
        if !successCheckBox
        {
            successCheckBox = true
            successCheckboxImg.image = UIImage(named: "Green_checkbox")
        }
        else
        { successCheckBox = false
            successCheckboxImg.image = UIImage(named: "checkbox_square")
            
        }
        
    }
    
    
    func rejectChechBoxAction()
    {
        if !rejectCheckBox
        {
            
            rejectCheckBox = true
            rejectCheckboxImg.image = UIImage(named: "Green_checkbox")
            
            
        }
        else
        {
            
            rejectCheckBox = false
            rejectCheckboxImg.image = UIImage(named: "checkbox_square")
            
        }
        
        
    }
    
    
    func debitChechBoxAction()
    {
        if !debitCheckBox         {
            
            debitCheckBox = true
            debitCheckboxImg.image = UIImage(named: "Green_checkbox")
            
            
        }
        else
        {
            
            debitCheckBox = false
            debitCheckboxImg.image = UIImage(named: "checkbox_square")
            
        }
    }
    func creditChechBoxAction()
    {
        if !creditCheckBox
        {
            
            creditCheckBox = true
            creditCheckboxImg.image = UIImage(named: "Green_checkbox")
            
            
        }
        else
        {
            
            creditCheckBox = false
            creditCheckboxImg.image = UIImage(named: "checkbox_square")
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissView()
    {
        
        toTransaction()
    }
    
    @IBAction func cancelBtnAction(sender: AnyObject) {
        Appconstant.filter = true
        toTransaction()
        
    }
    
    @IBAction func BackBtnAction(sender: AnyObject) {
        Appconstant.filter = true
        toTransaction()
    }
    
    
    @IBAction func OKBtnAction(sender: AnyObject) {
        Appconstant.successCheckBox = successCheckBox
        Appconstant.rejectCheckBox = rejectCheckBox
        Appconstant.creditCheckBox = creditCheckBox
        Appconstant.debitCheckBox = debitCheckBox
        Appconstant.filter = true
        if !successCheckBox && !rejectCheckBox && !creditCheckBox && !debitCheckBox {
            Appconstant.filter = false
        }
        toTransaction()
    }
    
    func toTransaction() {
        performSegueWithIdentifier("filter_transaction", sender: self)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "filter_transaction") {
            let nextview = segue.destinationViewController as! TransactionHistoryViewController
            if Appconstant.filter {
                nextview.filter_StartDate = self.filter_StartDate
                nextview.filter_EndDate = self.filter_EndDate
            }
        }
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    
}
