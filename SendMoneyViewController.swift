//
//  SendMoneyViewController.swift
//  purZ
//
//  Created by Vertace on 21/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class SendMoneyViewController: UIViewController,UITabBarControllerDelegate {
    
    @IBOutlet weak var VPAView: UIView!
    @IBOutlet weak var Accountview: UIView!
    @IBOutlet weak var MMIDView: UIView!
    @IBOutlet weak var AadharView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialFunc()
    }
    func initialFunc()
    {
       
        tabBarController?.delegate = self
        VPAView.userInteractionEnabled = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "sendMoneytoVPA")
        VPAView.addGestureRecognizer(tap)
        
        Accountview.userInteractionEnabled = true
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "paytoAccount")
        Accountview.addGestureRecognizer(tap1)
        
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "sendMoneytoMMID")
        MMIDView.addGestureRecognizer(tap2)
        
        let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "sendMoneytoAadhar")
        AadharView.addGestureRecognizer(tap3)
        
        // Do any additional setup after loading the view.
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, VPAView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine.backgroundColor = UIColor.lightGrayColor().CGColor
        VPAView.layer.addSublayer(bottomLine)
        
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, Accountview.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine1.backgroundColor = UIColor.lightGrayColor().CGColor
        Accountview.layer.addSublayer(bottomLine1)
        
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, MMIDView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine2.backgroundColor = UIColor.lightGrayColor().CGColor
        MMIDView.layer.addSublayer(bottomLine2)
        
        let bottomLine3 = CALayer()
        bottomLine3.frame = CGRectMake(0.0, AadharView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine3.backgroundColor = UIColor.lightGrayColor().CGColor
        AadharView.layer.addSublayer(bottomLine3)
        
    }
    func sendMoneytoAadhar()
    {
        self.performSegueWithIdentifier("To_SendMoneyAadhar", sender: self)
        
        
    }
    func sendMoneytoMMID()
    {
        self.performSegueWithIdentifier("To_SendMoneyMMID", sender: self)
        
        
    }
    
    func sendMoneytoVPA()
    {
        self.performSegueWithIdentifier("To_SendMoneyVPA", sender: self)
        
        
    }
    func paytoAccount()
    {
        self.performSegueWithIdentifier("To_PayAccount", sender: self)
        
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController)
    {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
