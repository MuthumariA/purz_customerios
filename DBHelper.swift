//
//  DBHelper.swift
//  YAP Customer
//
//  Created by Admin on 4/7/16.
//  Copyright © 2016 yap. All rights reserved.
//

import Foundation

class DBHelper {
    
    var databasePath = NSString()
    func purzDB()
    {
        let filemgr = NSFileManager.defaultManager()
        //     let dirPaths =
        NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
            .UserDomainMask, true)
        
        //    let docsDir = dirPaths[0]
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        databasePath = databaseURL.absoluteString
        //databasePath = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("yapcustomer.db")
        // databasePath = docsDir.stringByAppendingPathComponent("yapcustomer.db")
        //   stringByAppendingPathComponent("yapcustomer.db")
        print(databasePath as String)
        if !filemgr.fileExistsAtPath(databasePath as String)
        {
            
            CreateTable();
        }
        
    }
    

    
    internal func CreateTable() {
        let purzDB = FMDatabase(path: databasePath as String)
        
        if purzDB.open() {
            
            
            let profile_info_table = "CREATE TABLE IF NOT EXISTS PROFILE_INFO (ID INTEGER PRIMARY KEY AUTOINCREMENT, CUSTOMER_ID TEXT, YAP_CODE TEXT, CUSTOMER_NAME TEXT, MOBILE_NUMBER TEXT, CARD_NUMBER TEXT, EMAIL TEXT, CUSTOMER_TYPE TEXT, CARDNUMBER TEXT, EXPIREDATE TEXT,KITNO TEXT,DATEOFBIRTH TEXT, NIKI TEXT)"
            if !purzDB.executeStatements(profile_info_table) {
                print("creating table profile info")
                print("Error: \(purzDB.lastErrorMessage())")
            }
            
            let profile_table = "CREATE TABLE IF NOT EXISTS CUSTOMERDETAIL (ID INTEGER PRIMARY KEY AUTOINCREMENT, CUSTOMER_ID TEXT, CUSTOMER_NAME TEXT, DATE_OF_BIRTH TEXT, CUSTOMER_BANK TEXT, IMAGE_PATH TEXT, ADDRESS_LINE_1 TEXT, ADDRESS_LINE_2 TEXT, CITY TEXT, STATE TEXT, PIN TEXT, SECURITY_QUESTION TEXT, SECURITY_ANSWER TEXT, ID_TYPE TEXT, ID_NUMBER TEXT)"
            if !purzDB.executeStatements(profile_table) {
                print("Error: \(purzDB.lastErrorMessage())")
            }
            
            let recharge_company_table = "CREATE TABLE IF NOT EXISTS RECHARGECOMPANY (ID INTEGER PRIMARY KEY AUTOINCREMENT, TELECOM_NAME TEXT, TELECOM_OPERATORCODE TEXT, TELECOM_OPERATORTYPE TEXT, TELECOM_SPECIAL TEXT)"
            if !purzDB.executeStatements(recharge_company_table) {
                print("Error: \(purzDB.lastErrorMessage())")
            }
            
            
            let transaction_create_table = "CREATE TABLE IF NOT EXISTS TRANSACTIONS (ID INTEGER PRIMARY KEY AUTOINCREMENT,  AMOUNT TEXT, TRANSACTION_TYPE TEXT, TYPE TEXT, TIME TEXT, TX_REF TEXT, BENEFICIARY_NAME TEXT, BENEFICIARY_ID TEXT, DESCRIPTION TEXT, OTHER_PARTY_NAME TEXT, OTHER_PARTY_ID TEXT, TXN_ORIGIN TEXT, TRANSACTION_STATUS TEXT, TRANSACTIONID TEXT)"
            if !purzDB.executeStatements(transaction_create_table) {
                print("Error: \(purzDB.lastErrorMessage())")
            }
            
            let notification_table = "CREATE TABLE IF NOT EXISTS NOTIFICATION (ID INTEGER PRIMARY KEY AUTOINCREMENT, AMOUNT TEXT, DATE TEXT, FUND_REQ_NUMBER TEXT, STATUS TEXT, APPROVE_DECLINE, REQ_TO_US TEXT, ISREAD TEXT, PHONENUMBER TEXT)"
            if !purzDB.executeStatements(notification_table) {
                print("Error: \(purzDB.lastErrorMessage())")
            }
            
            let img_table = "CREATE TABLE IF NOT EXISTS IMAGETABLE (ID INTEGER PRIMARY KEY AUTOINCREMENT, CUSTOMER_ID TEXT, IMAGE_PATH TEXT)"
            if !purzDB.executeStatements(img_table) {
                print("Error: \(purzDB.lastErrorMessage())")
            }
            
            let account_table = "CREATE TABLE IF NOT EXISTS ACCOUNTS (ID INTEGER PRIMARY KEY AUTOINCREMENT, CUSTOMER_ID TEXT, BANK_NAME TEXT, ACCOUNT_NO TEXT, IFSC TEXT, TYPE TEXT, VPA TEXT, BALANCE TEXT, UPI TEXT, ACCOUNTREF_NO TEXT, ACCOUNTDETAIL TEXT, PROVIDER_ID TEXT, PROVIDER_NAME TEXT, IIN TEXT)"
            if !purzDB.executeStatements(account_table) {
                print("Error: \(purzDB.lastErrorMessage())")
            }
            let beneficiaryVPA_table = "CREATE TABLE IF NOT EXISTS BENEFICIARY_VPA (ID INTEGER PRIMARY KEY AUTOINCREMENT, VPA TEXT, REGISTERED_NAME TEXT)"
            if !purzDB.executeStatements(beneficiaryVPA_table) {
                print("Error: \(purzDB.lastErrorMessage())")
            }

            let beneficiaryIFSC_table = "CREATE TABLE IF NOT EXISTS BENEFICIARY_IFSC (ID INTEGER PRIMARY KEY AUTOINCREMENT, IFSC_CODE TEXT, REGISTERED_NAME TEXT, ACCOUNT_NUMBER TEXT, SECURE_ACCOUNT_NUMBER TEXT)"
            if !purzDB.executeStatements(beneficiaryIFSC_table) {
                print("Error: \(purzDB.lastErrorMessage())")
            }
            let beneficiaryAadhar_table = "CREATE TABLE IF NOT EXISTS BENEFICIARY_AADHAR (ID INTEGER PRIMARY KEY AUTOINCREMENT, AADHAR_NO TEXT, REGISTERED_NAME TEXT)"
            if !purzDB.executeStatements(beneficiaryAadhar_table) {
                print("Error: \(purzDB.lastErrorMessage())")
            }
            let beneficiaryMMID_table = "CREATE TABLE IF NOT EXISTS BENEFICIARY_MMID (ID INTEGER PRIMARY KEY AUTOINCREMENT, MMID TEXT, REGISTERED_NAME TEXT, MOBILE_NO TEXT)"
            if !purzDB.executeStatements(beneficiaryMMID_table) {
                print("Error: \(purzDB.lastErrorMessage())")
            }
            let cvv_table = "CREATE TABLE IF NOT EXISTS CVV_TABLE (ID INTEGER PRIMARY KEY AUTOINCREMENT, CUSTOMER_ID TEXT, CVV_NO TEXT)"
            if !purzDB.executeStatements(cvv_table) {
                print("Error: \(purzDB.lastErrorMessage())")
            }
            purzDB.close()
        }
        
    }

}

