//
//  AvailableAccountViewController.swift
//  purZ
//
//  Created by Vertace on 20/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class AvailableAccountViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITabBarControllerDelegate {
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    
    var existingaccounts = [String]()
    var accountNo = [String]()
    var ifsc = [String]()
    var accounttype = [String]()
    var upi = [String]()
    var accountexists = false
    var bankname = ""
    var accountRefNo = [String]()
    var accountdetails = [String]()
    var indexpath = 0
    var providerid = ""
    var providername = ""
    var iin = ""

    override func viewDidLoad() {
        tabBarController?.delegate = self
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AccountCell", forIndexPath: indexPath) as UITableViewCell!
        
        let banknamelbl = cell.viewWithTag(1) as! UILabel
        let accountnolbl = cell.viewWithTag(2) as! UILabel
        let ifsclbl = cell.viewWithTag(3) as! UILabel
        let typelbl = cell.viewWithTag(4) as! UILabel
        let upilbl = cell.viewWithTag(5) as! UILabel
        
        banknamelbl.text = bankname
        
        accountnolbl.text = ": "+accountNo[indexPath.row]
        
        ifsclbl.text = ": "+ifsc[indexPath.row]
        typelbl.text = ": "+accounttype[indexPath.row]
        if upi[indexPath.row] == "N" {
            upilbl.text = "4 Digit UPI Pin Doesn't Exist"
        }
        else{
            upilbl.text = "4 Digit UPI Pin Exist"
        }
        
        cell.layer.borderWidth = 3
        cell.layer.borderColor = UIColor(red: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1).CGColor
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return accountNo.count
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        indexpath = indexPath.row
        for(var i=0;i<AccountConstant.accountNumber.count;i++){
            if AccountConstant.accountNumber[i] == accountNo[indexPath.row] {
                self.accountexists = true
            }
        }
        
        if self.accountexists {
            self.performSegueWithIdentifier("back_to_accounts", sender: self)
        }
        else {
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            if purzDB.open() {
                let insertsql = "INSERT INTO ACCOUNTS (CUSTOMER_ID,BANK_NAME,ACCOUNT_NO,IFSC,TYPE,VPA,BALANCE,UPI,ACCOUNTREF_NO,ACCOUNTDETAIL,PROVIDER_ID,PROVIDER_NAME,IIN) VALUES ('\(Appconstant.customerid)','\(bankname)','\(accountNo[indexPath.row])','\(ifsc[indexPath.row])','\(accounttype[indexPath.row])','\("")','\("")','\(upi[indexPath.row])','\(accountRefNo[indexPath.row])','\(accountdetails[indexPath.row])','\(providerid)','\(providername)','\(iin)')"
                
                let result = purzDB.executeUpdate(insertsql,
                                                  withArgumentsInArray: nil)
                
                if !result {
                    print("Error: \(purzDB.lastErrorMessage())")
                }
                else{
                    // go to create VPA page
                    self.performSegueWithIdentifier("availableAcc_to_VPA", sender: self)
                }
            }
            purzDB.close()

        }
    }
    
    
 
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "back_to_accounts") {
            let nextview = segue.destinationViewController as! AccountsViewController
            if accountexists {
                nextview.toastmessage = "Beneficiary Account already exist!"
            }
        }
        else if(segue.identifier == "availableAcc_to_VPA"){
            let nextview = segue.destinationViewController as! CreateVPAViewController
            nextview.bankname = self.bankname
            nextview.accountNo = self.accountNo[indexpath]
            nextview.bankname = self.bankname
            nextview.ifsc = self.ifsc[indexpath]
            nextview.accounttype = self.accounttype[indexpath]
            nextview.upi = self.upi[indexpath]
            nextview.accountdetail = self.accountdetails[indexpath]
            nextview.accountRefNo = self.accountRefNo[indexpath]
            nextview.fromAvailableAccounts = true
            
        }

    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }

}
