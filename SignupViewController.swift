
//
//  SignupViewController.swift
//  Cippy
//
//  Created by apple on 15/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController, UITextFieldDelegate {
    
    
//    @IBOutlet weak var idTypeBtn: UIButton!
//    @IBOutlet weak var idTxtField: UITextField!
    @IBOutlet weak var activityindicater: NSLayoutConstraint!
   
    @IBOutlet var showhidebtn: UIButton!
    @IBOutlet weak var signinbtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
       
    @IBOutlet weak var TermsConditionBtn: UIButton!
    @IBOutlet weak var logoimage: UIImageView!
        @IBOutlet weak var mobilenolbl: UITextField!
    @IBOutlet weak var passwordlbl: UITextField!
    @IBOutlet weak var firstnamelbl: UITextField!
    
  
    @IBOutlet weak var emaillbl: UITextField!
    
    @IBOutlet weak var signupBluebtn: UIButton!
    
    @IBOutlet weak var activityIndicater: UIActivityIndicatorView!
    var alertController:UIAlertController?
    var characterCountLimit = 10
    var showpwd = false
    var segue = false
    var agreeterms = true
    
    
    var mobileNo = ""
    var fromsignin = false
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        let paddingForFirst1 = UIView(frame: CGRectMake(0, 0, 10, self.mobilenolbl.frame.size.height))
         mobilenolbl.leftView = paddingForFirst1
        mobilenolbl.leftViewMode = UITextFieldViewMode .Always
       
        let paddingForFirst2 = UIView(frame: CGRectMake(0, 0, 10, self.passwordlbl.frame.size.height))
        passwordlbl.leftView = paddingForFirst2
        passwordlbl.leftViewMode = UITextFieldViewMode .Always
        
        let paddingForFirst3 = UIView(frame: CGRectMake(0, 0, 10, self.firstnamelbl.frame.size.height))
        firstnamelbl.leftView = paddingForFirst3
        firstnamelbl.leftViewMode = UITextFieldViewMode .Always
    
        let paddingForFirst4 = UIView(frame: CGRectMake(0, 0, 10, self.emaillbl.frame.size.height))
        emaillbl.leftView = paddingForFirst4
        emaillbl.leftViewMode = UITextFieldViewMode .Always
//        let paddingForFirst5 = UIView(frame: CGRectMake(0, 0, 10, self.idTxtField.frame.size.height))
//        idTxtField.leftView = paddingForFirst5
//        idTxtField.leftViewMode = UITextFieldViewMode .Always
//        showhidebtn.hidden = true
        // Do any additional setup after loading the view, typically from a nib.
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        signinbtn.titleLabel?.attributedText = NSAttributedString(string: "Sign in", attributes: underlineAttribute)
            TermsConditionBtn.titleLabel?.attributedText = NSAttributedString(string: "Terms & Conditions", attributes: underlineAttribute)

     navigationController?.navigationBarHidden = true
        signupBluebtn.layer.cornerRadius = 9
        settextfield()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignupViewController.dismissKeyboard))
scrollView.addGestureRecognizer(tap)
        mobilenolbl.delegate = self
        passwordlbl.delegate = self
        
       scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 650)

        passwordlbl.secureTextEntry = true
        self.navigationController?.navigationBarHidden = true
        logoimage.layer.borderWidth = 5
        logoimage.layer.masksToBounds = false
        logoimage.layer.borderColor = UIColor(red:230/255.0, green:175/255.0, blue:7/255.0, alpha:1.0).CGColor;           logoimage.layer.cornerRadius =  logoimage.frame.size.height/2
        logoimage.clipsToBounds = true
//scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)
    }
    
    func settextfield(){
        mobilenolbl.font = UIFont(name: "calibri", size: 15.0)
        passwordlbl.font = UIFont(name: "calibri", size: 15.0)
        firstnamelbl.font = UIFont(name: "calibri", size: 15.0)
        emaillbl.font = UIFont(name: "calibri", size: 15.0)

       
        mobilenolbl.attributedPlaceholder = NSAttributedString(string:"Mobile Number",
                                                               attributes:[NSForegroundColorAttributeName: UIColor.grayColor()])
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, mobilenolbl.frame.size.height - 2 , mobilenolbl.frame.size.width+50, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        mobilenolbl.borderStyle = UITextBorderStyle.None
        mobilenolbl.layer.addSublayer(bottomLine)
        passwordlbl.attributedPlaceholder = NSAttributedString(string:"Select 4 digit password",
            attributes:[NSForegroundColorAttributeName: UIColor.grayColor()])
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, passwordlbl.frame.size.height - 2 , passwordlbl.frame.size.width+50, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        passwordlbl.borderStyle = UITextBorderStyle.None
        passwordlbl.layer.addSublayer(bottomLine1)
        firstnamelbl.attributedPlaceholder = NSAttributedString(string:"Name",
            attributes:[NSForegroundColorAttributeName: UIColor.grayColor()])
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, firstnamelbl.frame.size.height - 2 , firstnamelbl.frame.size.width+40, 1.0)
        bottomLine2.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                firstnamelbl.borderStyle = UITextBorderStyle.None
        firstnamelbl.layer.addSublayer(bottomLine2)
//        idTxtField.attributedPlaceholder = NSAttributedString(string:"Enter ID",
//                                                               attributes:[NSForegroundColorAttributeName: UIColor.grayColor()])
//        let bottomLine12 = CALayer()
//        bottomLine12.frame = CGRectMake(0.0, idTxtField.frame.size.height - 2 , idTxtField.frame.size.width+50, 1.0)
//        bottomLine12.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                idTxtField.borderStyle = UITextBorderStyle.None
//        idTxtField.layer.addSublayer(bottomLine12)
//        let bottomLine13 = CALayer()
//        bottomLine13.frame = CGRectMake(0.0, idTypeBtn.frame.size.height - 2 , idTypeBtn.frame.size.width-45, 1.0)
//        bottomLine13.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
//        idTypeBtn.layer.addSublayer(bottomLine13)

        emaillbl.attributedPlaceholder = NSAttributedString(string:"Email ID(Optional)",
            attributes:[NSForegroundColorAttributeName: UIColor.grayColor()])
        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRectMake(0.0, emaillbl.frame.size.height - 2 , emaillbl.frame.size.width+50, 1.0)
        bottomLine4.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        emaillbl.borderStyle = UITextBorderStyle.None
         TermsConditionBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(12);emaillbl.layer.addSublayer(bottomLine4)
        if segue {
            self.mobilenolbl.text! = Appconstant.mobileno
            self.passwordlbl.text! = Appconstant.pwd
            self.firstnamelbl.text! = Appconstant.firstname
        self.emaillbl.text! = Appconstant.email
        }
        if fromsignin
        {
            self.mobilenolbl.text! = mobileNo
        }
//        if Appconstant.From_Signup_IDType
//        {
//            idTypeBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
//
//            Appconstant.From_Signup_IDType = false
//            idTypeBtn.setTitle("  " + Appconstant.IdType, forState: .Normal)
//        }
        
    }
    
    @IBAction func IDTypeBtnaCtion(sender: AnyObject) {
        
        Appconstant.From_Signup_IDType = true
        self.performSegueWithIdentifier("To_GetId", sender: self)
    }
    @IBAction func termscondition(sender: AnyObject) {
        
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.equitasbank.com/website-terms-conditions.php")!)
        }
    func textFieldDidBeginEditing(textField: UITextField) {
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 750)
//        if(textField == firstnamelbl) {
//            animateViewMoving(true, moveValue: 100)
//        }
//        if(textField == lastnamelbl) {
//            animateViewMoving(true, moveValue: 200)
//        }
//        if(textField == emaillbl) {
//            animateViewMoving(true, moveValue: 200)
//        }
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
               if textField == firstnamelbl{
                emaillbl.becomeFirstResponder()
        }
               if textField == emaillbl{
                    passwordlbl.becomeFirstResponder()
                
      }
        return true // We do not want UITextField to insert line-breaks.
    }
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 790)
        if(textFieldToChange == mobilenolbl){
            characterCountLimit = 10
            
        }
        if(mobilenolbl.text!+string == "000000000" || mobilenolbl.text!+string == "9999999999" || mobilenolbl.text!+string == "8888888888" || mobilenolbl.text!+string == "7777777777" || mobilenolbl.text!+string == "6666666666" || mobilenolbl.text!+string == "5555555555" || mobilenolbl.text!+string == "4444444444" || mobilenolbl.text!+string == "3333333333" || mobilenolbl.text!+string == "2222222222" || mobilenolbl.text!+string == "1111111111")
        {
            self.presentViewController(Alert().alert("Please enter the valid mobile number!", message: ""),animated: true,completion: nil)
            mobilenolbl.text = ""
        }
        else if(mobilenolbl.text?.characters.count == 9){
            let number = self.mobilenolbl.text! + string
            print(Appconstant.BASE_URL+Appconstant.MANAGER_BUSINESS_ENTITY+Appconstant.URL_CHECK_CUSTOMER+number)
            sendrequesttoserverForCheckCustomer(Appconstant.BASE_URL+Appconstant.MANAGER_BUSINESS_ENTITY+Appconstant.URL_CHECK_CUSTOMER+number)
            
            
        }
        
        if(textFieldToChange == passwordlbl){
            characterCountLimit = 4
            if(passwordlbl.text?.characters.count == 4)
            {
             //showhidebtn.hidden = false
            }
        }
            
            
        else if(textFieldToChange != passwordlbl && textFieldToChange != mobilenolbl){
            characterCountLimit = 30
        }
       let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    
    }
    func sendrequesttoserverForCheckCustomer(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                    }
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                
                let item = json["result"]
                if((item["registered"].stringValue == "true")&&(item["isPassCodeSet"].stringValue == "true")){
                    dispatch_async(dispatch_get_main_queue()) {
                        self.alertController = UIAlertController(title: "Purz",
                                                                 message: "You are already registered! please signin",
                                                                 preferredStyle: .Alert)
                        
                        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                            self!.performSegueWithIdentifier("to_signin", sender: self) })
                        self.alertController?.addAction(action)
                         self.presentViewController(self.alertController!, animated: true, completion: nil)
                    }
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.firstnamelbl.becomeFirstResponder()
                    }
                }
        }
        
        task.resume()
        
    }


        @IBAction func termsandconditions(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://yappay.in/app-static/eqwallet/tnc.html")!)
    }
    
    
    func dismissKeyboard(){
        self.view.endEditing(true)
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 500)
    }
    
    
    
    @IBAction func signupBlueBtnAction(sender: AnyObject) {
        Appconstant.mobileno = self.mobilenolbl.text!
        Appconstant.pwd = self.passwordlbl.text!
        Appconstant.firstname = self.firstnamelbl.text!
                Appconstant.email = self.emaillbl.text!
//        activityindicater.startAnimating()
        if(Appconstant.mobileno.isEmpty || Appconstant.pwd.isEmpty || Appconstant.firstname.isEmpty)   {
            self.presentViewController(Alert().alert("Oops! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if(Appconstant.mobileno.characters.count != 10){
            self.presentViewController(Alert().alert("Type a mobile number or directly add from saved contacts here.", message: ""),animated: true,completion: nil)
        }
        else if(mobilenolbl.text! == "0000000000" || mobilenolbl.text! == "9999999999" || mobilenolbl.text! == "8888888888" || mobilenolbl.text! == "7777777777" || mobilenolbl.text! == "6666666666" || mobilenolbl.text! == "5555555555" || mobilenolbl.text! == "4444444444" || mobilenolbl.text! == "3333333333" || mobilenolbl.text! == "2222222222" || mobilenolbl.text! == "1111111111")
        {
            self.presentViewController(Alert().alert("Please enter the valid mobile number!", message: ""),animated: true,completion: nil)
            mobilenolbl.text = ""
        }
        else if(Appconstant.pwd.characters.count != 4){
            self.presentViewController(Alert().alert("Punch in a 4 digit Password", message: ""),animated: true,completion: nil)
        }
     
       else
        {
            if(Appconstant.email.isEmpty)
            {
                                activityIndicater.startAnimating();
                print(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+self.mobilenolbl.text!)
                Appconstant.newcustomer = true
              self.performSegueWithIdentifier("to_otp", sender: self)
                //self.sendrequesttoserverForGenerateOTP(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+self.mobilenolbl.text!)
            }
            else
            {
                let check = isValidEmail(Appconstant.email)
                if(check)
                {
                    if(!agreeterms){
                        self.presentViewController(Alert().alert("Please agree on terms and conditions", message: ""),animated: true,completion: nil)
                    }
                    else{
                        
                        activityIndicater.startAnimating();
                        print(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+self.mobilenolbl.text!)
                        Appconstant.newcustomer = true
                        self.performSegueWithIdentifier("to_otp", sender: self)
//                        self.sendrequesttoserverForGenerateOTP(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+self.mobilenolbl.text!)
                    }
                    

                }
                else
                {
                   self.presentViewController(Alert().alert("Please select valid mail id", message: ""),animated: true,completion: nil)
                    
                }
            
            }
                
            }
        
        
        
    }
    /*
    func idGetAlert()
    {
        let alertController = UIAlertController(title: "Please select your Card Type",
                                                message: "",
                                                preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            dispatch_async(dispatch_get_main_queue()) {
                self!.typeGetAlert()
            }
            
            })
        alertController.addAction(action)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func typeGetAlert()
    {
        let alertController = UIAlertController(title: "Please select your Card Type to continue app",
                                                message: "",
                                                preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "PAN Card", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            dispatch_async(dispatch_get_main_queue()) {
                Appconstant.IdType = "PAN Card"
                self!.afterGetType()
            }
            
            })
        let action1 = UIAlertAction(title: "Passport", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            dispatch_async(dispatch_get_main_queue()) {
                Appconstant.IdType = "Passport"
                self!.afterGetType()
            }
            
            })
        let action2 = UIAlertAction(title: "Driving License", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            dispatch_async(dispatch_get_main_queue()) {
                Appconstant.IdType = "Driving License"
                self!.afterGetType()
            }
            
            })
        let action3 = UIAlertAction(title: "Voter's ID", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            dispatch_async(dispatch_get_main_queue()) {
                Appconstant.IdType = "Voter's ID"
                self!.afterGetType()
            }
            
            })
        let action4 = UIAlertAction(title: "Aadhaar No", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            dispatch_async(dispatch_get_main_queue()) {
                Appconstant.IdType = "Aadhaar No"
                self!.afterGetType()
            }
            
            })
        
        alertController.addAction(action)
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(action3)
        alertController.addAction(action4)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }

    func afterGetType()
    {
        
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "Please enter the " +   (Appconstant.IdType) + "'s number to continue app",
                                            message: "",
                                            preferredStyle: .Alert)
        alertController!.addTextFieldWithConfigurationHandler(
            {(textField: UITextField!) in
                
                textField.placeholder = "Enter the Number"
                textField.delegate = self
                textField.secureTextEntry  = true
                textField.keyboardType = UIKeyboardType.NumberPad
                
        })
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            if let textFields = alertController?.textFields{
                let theTextFields = textFields as [UITextField]
                let CardNumber = theTextFields[0].text!
                if(CardNumber == ""){
                    print(CardNumber)
                    
                    self!.alert()
                }
                else{
                    Appconstant.IdNumber = theTextFields[0].text!
                    self!.updateCustomerDetails()
                }
                
            }
            })
        
        let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            })
        
        alertController?.addAction(action)
        
        alertController?.addAction(action2)
        self.presentViewController(alertController!, animated: true, completion: nil)
        
    }
    
    func alert(){
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(Alert().alert("Please enter a valid id", message: ""),animated: true,completion: nil)
        }
    }
    
    func updateCustomerDetails(){
        let profileviewmodel = UpdateUserCardIdViewModel.init(entityId: Appconstant.customerid, idType:  Appconstant.IdType, idNumber:  Appconstant.IdNumber)!
        let serializedjson  = JSONSerializer.toJson(profileviewmodel)
        print(serializedjson)
        self.sendrequesttoserverForUpdateEntity(Appconstant.BASE_URL+Appconstant.URL_UPDATE_ENTITY, values: serializedjson)
        
    }
    func sendrequesttoserverForUpdateEntity(url : String, values: String)
    {
        print(url)
        print(values)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                dispatch_async(dispatch_get_main_queue()) {
                    //                self.activityIndicator.stopAnimating()
                }
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {
                dispatch_async(dispatch_get_main_queue()) {
                    //                self.activityIndicator.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")  // check for http errors
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseStringUpdate = \(responseString)")
                let json = JSON(data: data!)
                
                //                self.activityIndicator.stopAnimating()
                //       }
                if(json["result"].stringValue == "true"){
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Your ID is updated successfully!", message: ""),animated: true,completion: nil)
                        self.saveintoDB()
                        self.performSegueWithIdentifier("signin_home", sender: self)
                        
                        
                    }
                    
                }
                
            }
        }
        
        task.resume()
        
    }
    func saveintoDB(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let selectSQL = "SELECT * FROM CUSTOMERDETAIL"
            
            let results:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                           withArgumentsInArray: nil)
            if (results.next()){
                let update11 = "UPDATE CUSTOMERDETAIL SET ID_TYPE='"+Appconstant.IdType+"' WHERE CUSTOMER_NAME=" + Appconstant.customername
                let update12 = "UPDATE CUSTOMERDETAIL SET ID_NUMBER='"+Appconstant.IdNumber+"' WHERE CUSTOMER_NAME=" + Appconstant.customername
                
 
                let result11 = purzDB.executeUpdate(update11,
                                                    withArgumentsInArray: nil)
                let result12 = purzDB.executeUpdate(update12,
                                                    withArgumentsInArray: nil)
                
                if (!result11 || !result12){
                    
                    print("Error: \(purzDB.lastErrorMessage())")
                    //                    dispatch_async(dispatch_get_main_queue()) {
                    //                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    //
                    //                    }
                }
            }
    
        }
          purzDB.close()
    }
 */
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
//    func sendrequesttoserverForGenerateOTP(url : String)    {
//        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
//        request.HTTPMethod = "GET"
//        
//        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
//        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        
//        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
//            { data, response, error in
//                guard error == nil && data != nil else {
//                    self.activityIndicater.stopAnimating()// check for fundamental networking error
//                    if Reachability.isConnectedToNetwork() == true {
//                    } else {
//                        print("Internet connection FAILED")
//                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
//                        
//                    }
//                    return
//                }
//                
//                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
//                    self.activityIndicater.stopAnimating()
//                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
//                    print("response = \(response)")
//                    dispatch_async(dispatch_get_main_queue()) {
//                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
//                    }
//                }
//                
//                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
//                print("responseString = \(responseString)")
//                let json = JSON(data: data!)
//                dispatch_async(dispatch_get_main_queue()) {
//                    self.activityIndicater.stopAnimating()
//                }
//
//                let item = json["result"]
//                if((item["success"].stringValue == "true")&&(item["isCustomerExists"].stringValue == "true")){
//                    dispatch_async(dispatch_get_main_queue()) {
//                        Appconstant.otp = item["otp"].stringValue
//                        Appconstant.customerid = item["customerId"].stringValue
//                        Appconstant.customername = item["customerName"].stringValue
//                        Appconstant.dummycustomer = true
//                        Appconstant.newcustomer = false
//                        self.performSegueWithIdentifier("to_otp", sender: self)
//                    }
//                }
//                else if((item["success"].stringValue == "true")&&(item["isCustomerExists"].stringValue == "false")){
//                    dispatch_async(dispatch_get_main_queue()) {
//                        Appconstant.otp = item["otp"].stringValue
//                        Appconstant.newcustomer = true
//                        Appconstant.dummycustomer = false
//                        self.performSegueWithIdentifier("to_otp", sender: self)
//                    }
//                }
//                else{
//                    dispatch_async(dispatch_get_main_queue())
//                    {
//                            self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
//                    }
//                }
//            }
//        
//        task.resume()
//        
//    }
    
override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "to_signin") {
            let nextview = segue.destinationViewController as! SigninViewController
            nextview.fromsignup = true
            nextview.mobileno = self.mobilenolbl.text!
        }
    }
}
