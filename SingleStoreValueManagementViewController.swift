//
//  SingleStoreValueManagementViewController.swift
//  purZ
//
//  Created by Vertace on 05/12/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit
import Foundation
import CryptoSwift


class SingleStoreValueManagementViewController: UIViewController,UITextFieldDelegate, UITabBarControllerDelegate


{

//       @IBOutlet weak var calenderImg: UIImageView!
//    @IBOutlet weak var dateOfBirthTxtField: UITextField!
    @IBOutlet weak var OkBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var confirmPinTxtField: UITextField!
    @IBOutlet weak var newPinConstrain: NSLayoutConstraint!
    @IBOutlet weak var newPinTxtField: UITextField!
      let characterCountLimit = 4
    
    var datePickerView:UIDatePicker = UIDatePicker()
    var date1 = ""
    var calendar : NSCalendar = NSCalendar.currentCalendar()
    var currentDateTime = NSDate()
    var df = NSDateFormatter()
    var toolbarView = UIView()
    var dateString = ""
    var viewDatePicker:UIView!
    var month = ""
    var year = ""
     var datevalueChange = ""
     var serializedjson = ""
    var Key = "89555EED0B07495D00A6C11789D40E1D"
    var finalKitNo = ""
    var encryptedString = ""
    var date_TxtField = ""
  var selectdate = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        initFunc()
        createDatePickerViewWithAlertController()
        kitnoChange_format()
        setvaluestoall()
        alertCalFunc()


        // Do any additional setup after loading the view.
    }
    func alertCalFunc()
    {
     
        if Appconstant.dateOfBirth == ""
        {
        newPinTxtField.userInteractionEnabled = false
            confirmPinTxtField.userInteractionEnabled = false
            let alert = UIAlertController(title: "Your date of birth is required to set pin, please fill it!",message: "", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.Default, handler: self.showCalenderFunc))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    func showCalenderFunc(action: UIAlertAction) {
        
        viewDatePicker.hidden=false
        datePickerView.hidden=false
        //Use acti	on.title
    }
    @IBAction func cancelBtnAction(sender: AnyObject) {
       self.performSegueWithIdentifier("To_Home", sender: self)
    }
    func initFunc()
    {
        
        
        tabBarController?.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        navigationController?.navigationBar.hidden = true
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, newPinTxtField.frame.size.height - 1 , newPinTxtField.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                newPinTxtField.borderStyle = UITextBorderStyle.None
        newPinTxtField.layer.addSublayer(bottomLine)
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, confirmPinTxtField.frame.size.height - 1 , confirmPinTxtField.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                confirmPinTxtField.borderStyle = UITextBorderStyle.None
        confirmPinTxtField.layer.addSublayer(bottomLine1)
     

        OkBtn.layer.borderWidth = 2
        OkBtn.layer.borderColor =  UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        OkBtn.layer.cornerRadius = OkBtn.frame.size.height/2
        cancelBtn.layer.borderWidth = 2
        cancelBtn.layer.borderColor = UIColor.redColor().CGColor
        cancelBtn.layer.cornerRadius = cancelBtn.frame.size.height/2
         print(newPinTxtField.text!)
         print(confirmPinTxtField.text!)
       
           }
 
    
    func createDatePickerViewWithAlertController()
    {
        let currentdate = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: currentdate)
        
        year =  String(components.year)
        //        nodataLbl.hidden = true
        df.dateFormat = "yyyy"
        self.selectdate = Int(df.stringFromDate(currentdate))!
        self.viewDatePicker = UIView(frame: CGRectMake(0, self.view.frame.size.height - 288, self.view.frame.size.width, 288))
        viewDatePicker.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(viewDatePicker)
        
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.maximumDate = NSDate()
        
        var pickerSize : CGSize = datePickerView.sizeThatFits(CGSizeZero)
        datePickerView.frame = CGRectMake(0, self.view.frame.size.height - 244, self.view.frame.size.width, 244)
        datePickerView.backgroundColor = UIColor.clearColor()
        print(datePickerView.frame.origin.y)
        print(datePickerView.frame.size.height)
        self.view.addSubview(datePickerView)
        let toolbarView: UIView = UIView(frame: CGRectMake(0,0,self.view.frame.size.width,44))
        toolbarView.backgroundColor = UIColor.grayColor()
        //        let toolBar = UIToolbar(frame: CGRectMake(0,0,self.view.frame.size.width,44))
        let doneButton = UIButton.init(frame: CGRectMake(self.view.frame.size.width - 60,5,50,34))
        doneButton.addTarget(self, action: #selector(self.doneClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        
                datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
//        dateString = df.stringFromDate(currentDateTime)
//        print("Date"+dateString)
        
        toolbarView.addSubview(doneButton)
       
        
        print(toolbarView.frame.origin.y)
        self.viewDatePicker.addSubview(toolbarView)
        viewDatePicker.hidden = true
        datePickerView.hidden = true
    }
  
    func doneClicked(sender: UIButton)
    {
        print("date1")
        print(date1)
        newPinTxtField.userInteractionEnabled = true
        confirmPinTxtField.userInteractionEnabled = true
        self.view.endEditing(false)
        print(year)
        print(selectdate)
      
        if(selectdate+18 > Int(year)){
            dispatch_async(dispatch_get_main_queue()) {
                self.presentViewController(Alert().alert("Sorry! The minimum age for using Purz is 18 years", message: ""),animated: true,completion: nil)
            }
           
            }
        else
        {
         

            self.viewDatePicker.hidden = true
            self.datePickerView.hidden = true
            if   self.datevalueChange != ""
            {
                                  let profileviewmodel = UpdateProfileViewModel.init(specialDate: self.datevalueChange, entityId: Appconstant.customerid)!
                let serializedjson  = JSONSerializer.toJson(profileviewmodel)
                print(serializedjson)
                self.sendrequesttoserverForUpdateEntity(Appconstant.BASE_URL+Appconstant.URL_UPDATE_ENTITY, values: serializedjson)
        }
        
        }
        
    }
    

    func datePickerValueChanged(sender:UIDatePicker) {
        print("Sender\(sender)")
   
        
        
      dispatch_async(dispatch_get_main_queue()) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "ddMMyyyy"
        self.date1 = dateFormatter.stringFromDate(sender.date)
        
       
        
        let dateFormatter3 = NSDateFormatter()
        dateFormatter3.dateFormat = "yyyy"
        
        print(dateFormatter3.stringFromDate(sender.date))
         self.selectdate = Int(dateFormatter3.stringFromDate(sender.date))!
        
            let dateFormatter1 = NSDateFormatter()
            dateFormatter1.dateFormat = "yyyy-MM-dd"
            self.datevalueChange = dateFormatter1.stringFromDate(sender.date)
            //        datevalueChange = String(Int64(sender.date.timeIntervalSince1970 * 1000))
            print("Date\(self.datevalueChange)")
        }
        
        
    }
    func sendrequesttoserverForUpdateEntity(url : String, values: String)
    {
        print(url)
        print(values)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                dispatch_async(dispatch_get_main_queue()) {
                    //                self.activityIndicator.stopAnimating()
                }
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {
                dispatch_async(dispatch_get_main_queue()) {
                    //                self.activityIndicator.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")  // check for http errors
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseStringUpdate = \(responseString)")
                let json = JSON(data: data!)
                
                //                self.activityIndicator.stopAnimating()
                //       }
                if(json["result"].stringValue == "true"){
                    dispatch_async(dispatch_get_main_queue()) {
                       self.presentViewController(Alert().alert("Your Date of Birth is updated successfully!", message: ""),animated: true,completion: nil)
                        self.saveintoDB()
                    }
                    
                }
                
            }
        }
        
        task.resume()
        
    }
    
   
    
    // The iv is prefixed to the encrypted data
//    func aesCBCEncrypt(data:Data, keyData:Data) throws -> Data {
//        let keyLength = keyData.count
//        let validKeyLengths = [kCCKeySizeAES128, kCCKeySizeAES192, kCCKeySizeAES256]
//        if (validKeyLengths.contains(keyLength) == false) {
//            throw AESError.KeyError(("Invalid key length", keyLength))
//        }
//        
//        let ivSize = kCCBlockSizeAES128;
//        let cryptLength = size_t(ivSize + data.count + kCCBlockSizeAES128)
//        var cryptData = Data(count:cryptLength)
//        
//        let status = cryptData.withUnsafeMutableBytes {ivBytes in
//            SecRandomCopyBytes(kSecRandomDefault, kCCBlockSizeAES128, ivBytes)
//        }
//        if (status != 0) {
//            throw AESError.IVError(("IV generation failed", Int(status)))
//        }
//        
//        var numBytesEncrypted :size_t = 0
//        let options   = CCOptions(kCCOptionPKCS7Padding)
//        
//        let cryptStatus = cryptData.withUnsafeMutableBytes {cryptBytes in
//            data.withUnsafeBytes {dataBytes in
//                keyData.withUnsafeBytes {keyBytes in
//                    CCCrypt(CCOperation(kCCEncrypt),
//                        CCAlgorithm(kCCAlgorithmAES),
//                        options,
//                        keyBytes, keyLength,
//                        cryptBytes,
//                        dataBytes, data.count,
//                        cryptBytes+kCCBlockSizeAES128, cryptLength,
//                        &numBytesEncrypted)
//                }
//            }
//        }
//        
//        if UInt32(cryptStatus) == UInt32(kCCSuccess) {
//            cryptData.count = numBytesEncrypted + ivSize
//        }
//        else {
//            throw AESError.CryptorError(("Encryption failed", Int(cryptStatus)))
//        }
//         setPinFunc(date1)
//        
//        return cryptData;
//    }
    

    func setPinFunc(DOB: String)
    {
        print("dateofBirth:\(date1)")
        let setPinViewModel =
            SetPin.init(entityId: Appconstant.customerid,pin: self.encryptedString, kitNo: Appconstant.KitNo, expiryDate: Appconstant.expiredate, dob: self.date1)!
        
        self.serializedjson  = JSONSerializer.toJson(setPinViewModel)
        
        self.setPinToServer(Appconstant.BASE_URL+Appconstant.SET_PIN,values: self.serializedjson)
        
    }
    
    func setPinToServer(url : String, values: String)
    {
        print(url)
        print(values)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                dispatch_async(dispatch_get_main_queue()) {
                    //                    self.activityIndicater.stopAnimating()
                }
                print(error)
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        //                        self.activityIndicater.stopAnimating()
                    }
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                dispatch_async(dispatch_get_main_queue()) {
                    //                    self.activityIndicater.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                
                //                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                //                self.activityIndicater.stopAnimating()
                
            }
            dispatch_async(dispatch_get_main_queue()) {
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseStringPin = \(responseString)")
                let json = JSON(data: data!)
                
                
                if json["result"].isEmpty{
                    
                     let item1 = json["exception"]
                    
                   if item1["errorCode"].stringValue == "Y3003" && Appconstant.expiredate == ""
                   {
                    let alert = UIAlertController(title: "You are unable to reset card pin due to expiry date!",message: "", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.Default, handler: self.backAction))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                    }
                    else
                   {
                    let alert = UIAlertController(title: item1["shortMessage"].stringValue,message: "", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.Default, handler: self.backAction))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                    }
                    self.newPinTxtField.text = ""
                    self.confirmPinTxtField.text = ""
                   
                  
                    }
                else
                {
                            let item = json["result"]
                    print(item)
                    print(item["status"])
                    if item["status"].stringValue == "true"
                    {
                        
                        let alert = UIAlertController(title: "Card PIN reset successfully",message: "", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.Default, handler: self.backAction))
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                        self.newPinTxtField.text = ""
                        self.confirmPinTxtField.text = ""
                        
                    
                }
              }
            }
        }        
        task.resume()
        
    }
    func backAction(action: UIAlertAction) {
        
        self.performSegueWithIdentifier("To_Home", sender: self)
        //Use action.title
    }
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    func saveintoDB(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let selectSQL = "SELECT * FROM CUSTOMERDETAIL"
            
            let results:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                           withArgumentsInArray: nil)
            if (results.next()){
                let update1 = "UPDATE CUSTOMERDETAIL SET DATE_OF_BIRTH='"+self.datevalueChange+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
//                setvaluestoall()
                
                let result1 = purzDB.executeUpdate(update1,
                                                   withArgumentsInArray: nil)
                
                
                if (!result1){
                    
                    print("Error: \(purzDB.lastErrorMessage())")
                    //                    dispatch_async(dispatch_get_main_queue()) {
                    //                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    //
                    //                    }
                }
            }
            
        }
    }
    func setvaluestoall(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            print(Appconstant.customerid)
            let selectSQL = "SELECT * FROM CUSTOMERDETAIL WHERE CUSTOMER_ID=" + Appconstant.customerid
            let result:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                          withArgumentsInArray: nil)
            
            
            
            
            if (result.next()){
                
                Appconstant.dateOfBirth = result.stringForColumn("DATE_OF_BIRTH")
                dateConversion()
            }
                
            else{
                //   status.text = "Failed to add contact"
                print("Error: \(purzDB.lastErrorMessage())")
            }
            
        }
        purzDB.close()
    }
    func dateConversion()
    {
        
        if Appconstant.dateOfBirth != ""
        {
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateFormatter1 = NSDateFormatter()
            dateFormatter1.dateFormat = "ddMMyyyy"
            let date = dateFormatter.dateFromString(Appconstant.dateOfBirth)
            print(date)
            date1 = dateFormatter1.stringFromDate(date!)
            print("Date\(date1)")
        }
        
        
    }
    func kitnoChange_format()
    {
        if Appconstant.KitNo != ""
        {
        var k = 0
         
        print(Appconstant.KitNo)
        print(Appconstant.KitNo.characters.count)
        for (k=0; k < 16 - Appconstant.KitNo.characters.count; k += 1)
        {
            finalKitNo = finalKitNo + "0"
        }
        finalKitNo = finalKitNo + Appconstant.KitNo
        print("FinalPin")
     
        print(finalKitNo)
        }
    }
 
  
    @IBAction func OkBtnAction(sender: AnyObject) {
      
       if newPinTxtField.text == ""
       {
        self.presentViewController(Alert().alert("Please enter the Pin",message: ""), animated: true, completion: nil)
       }
         else if confirmPinTxtField.text == ""
       {
        self.presentViewController(Alert().alert("Please enter confirm Pin",message: ""), animated: true, completion: nil)
       }

       else if(newPinTxtField.text!.characters.count != 4||confirmPinTxtField.text!.characters.count != 4)
       {
        self.presentViewController(Alert().alert("Punch in a 4 digit pin", message: ""),animated: true,completion: nil)
       }
        
       else if newPinTxtField.text != confirmPinTxtField.text
       {
        self.presentViewController(Alert().alert("Looks like password doesn't match",message: ""), animated: true, completion: nil)
       }
        else
        {
//            dispatch_async(dispatch_get_main_queue()) {
        var pin = "04XXXXFFFFFFFFFF"
        var confirmPin = self.confirmPinTxtField.text!
        
        let pinFormatArray = Array(pin.characters)
        let pinArray = Array(confirmPin.characters)
        var FinalPin = ""
       
        var i = 0
        var j = 0
        
        for (i = 0; i<pin.characters.count; i++)
        {
            
            if pinFormatArray[i] == Character("X")
            {
                if j<=4
                {
              FinalPin = FinalPin + String(pinArray[j])
                j++
                }
            }
            else
            {
                  FinalPin = FinalPin + String(pinFormatArray[i])
            }
        }
            print("Pin\(FinalPin)")
            print("kitNo\(self.finalKitNo)")
            var iv = self.PerformXOR(FinalPin, string2: self.finalKitNo)
            print("IV\(iv)")
            print(self.Key)
            
          
          
           self.encryptedString = try! iv.aesEncrypt(self.Key, iv: iv)
            print("Encryption\(self.encryptedString)")
                if self.encryptedString != ""
                {
                     self.setPinFunc(date1)
                    
                }
            do
            {
                let dec = try! self.encryptedString.aesDecrypt(self.Key, iv: iv)
                print("Decryption\(dec)")
            }
            catch let error as exception
            {
                print(error)
            }

        }
        
        
            
            
//        }
    }
    
    
    

    func PerformXOR(string1: String, string2: String) -> String {
        
        var characterArr1 = Array(string1.lowercaseString.characters)
        var characterArr2 = Array(string2.lowercaseString.characters)
        var finalString = ""
        for(var i=0;i<string1.characters.count;i++) {
            // first string - hexa to int
            var str1 = String(characterArr1[i])
            var str2 = String(characterArr2[i])
            print(str1)
            print(str2)
            if str1 == "a"{
                str1 = "10"
            }
            else if str1 == "b"{
                str1 = "11"
            }
            else if str1 == "c"{
                str1 = "12"
            }
            else if str1 == "d"{
                str1 = "13"
            }
            else if str1 == "e"{
                str1 = "14"
            }
            else if str1 == "f"{
                str1 = "15"
            }
            
            // second string - hexa to int
            if str2 == "a"{
                str2 = "10"
            }
            else if str2 == "b"{
                str2 = "11"
            }
            else if str2 == "c"{
                str2 = "12"
            }
            else if str2 == "d"{
                str2 = "13"
            }
            else if str2 == "e"{
                str2 = "14"
            }
            else if str2 == "f"{
                str2 = "15"
            }
            
            var convert: String = String(Int(str1)! ^ Int(str2)!)
            // convert Int to Hexa
            
            if convert == "10"{
                convert = "a"
            }
            else if convert == "11"{
                convert = "b"
            }
            else if convert == "12"{
                convert = "c"
            }
            else if convert == "13"{
                convert = "d"
            }
            else if convert == "14"{
                convert = "e"
            }
            else if convert == "15"{
                convert = "f"
            }
            
            finalString += convert
            print("Final String: \(finalString)")
            
        }
        
        return finalString
    }
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    


}
extension String {
  
    func aesEncrypt(key: String, iv: String) throws -> String{
        let data = self.dataUsingEncoding(NSUTF8StringEncoding)
  
        let enc = try AES(key: key, iv: iv, blockMode:.ECB,padding: PKCS7()).encrypt(data!.arrayOfBytes())
        
        let encData = NSData(bytes: enc, length: Int(enc.count))
        let base64String: String = encData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0));
        let result = String(base64String)
        return result
    }
    
    func aesDecrypt(key: String, iv: String) throws -> String {
        let data = NSData(base64EncodedString: self, options: NSDataBase64DecodingOptions(rawValue: 0))
        let dec = try AES(key: key, iv: iv, blockMode:.ECB,padding: PKCS7()).decrypt(data!.arrayOfBytes())
        let decData = NSData(bytes: dec, length: Int(dec.count))
        let result = NSString(data: decData, encoding: NSUTF8StringEncoding)
        return String(result!)
    }
}
