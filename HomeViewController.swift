
//kCVImageBufferChromaLocation_Center//
//  HomeViewController.swift
//  Cippy
//
//  Created by apple on 16/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit
import MapKit
import Shimmer

class HomeViewController: UIViewController, UINavigationControllerDelegate,CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate, UITabBarControllerDelegate
    , UITextFieldDelegate,UICollectionViewDataSource,UICollectionViewDelegate
{
    
    var shimmer: FBShimmeringView!
     @IBOutlet weak var cardImgWidth: NSLayoutConstraint!
    //    @IBOutlet weak var cvvBtn: UIButton!
    
    @IBOutlet weak var cardImgBack: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var cardImgFront: UIImageView!
    @IBOutlet weak var cardView: UIView!
    //@IBOutlet weak var amtsymbol: UILabel!
    @IBOutlet weak var balancelbl1: UILabel!
    
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var plusCardBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var addMoneyBackView: UIView!
    @IBOutlet weak var validLbl: UILabel!
    @IBOutlet weak var accountNoLbl: UILabel!
    @IBOutlet weak var balancelbl: UILabel!
    var cvvBtn = UIButton()
    var Str = [String]()
    var serializedjson = ""
    @IBOutlet weak var addMoneycardBtn: UIButton!
    
    @IBOutlet weak var cardImgHeight: NSLayoutConstraint!
    
    @IBOutlet weak var card_Lbl_Space: NSLayoutConstraint!
    
    @IBOutlet weak var cardViewWidth: NSLayoutConstraint!
    
    
    var screenWidth: CGFloat = 0.0
    
    
    
    
    var sidemenu: UIBarButtonItem!
    var trans_date = [String]()
    var transactionamt = [String]()
    var beneficiaryname = [String]()
    var balance = [String]()
    var transactiontype = [String]()
    
    var refresh_balnc = 0
    var pre_operatorCode = [String]()
    var pre_operatorName = [String]()
    var pre_operatorId = [String]()
    var pre_operatorType = [String]()
    var pre_special = [Bool]()
    
    var post_operatorCode = [String]()
    var post_operatorName = [String]()
    var post_operatorId = [String]()
    var post_operatorType = [String]()
    var post_special = [Bool]()
    
    var dth_operatorCode = [String]()
    var dth_operatorName = [String]()
    var dth_operatorId = [String]()
    var dth_operatorType = [String]()
    var dth_special = [Bool]()
    
    var viewDatePicker:UIView!
    var datePickerView:UIDatePicker = UIDatePicker()
    var df = NSDateFormatter()
    var toolbarView = UIView()
    var dateString = ""
    var calendar : NSCalendar = NSCalendar.currentCalendar()
    var currentDateTime = NSDate()
    
    var colorlabel:UIColor!
    var selectedindex = 0
    var selectedfromtable = false
    
    var date1 = ""
    var datevalueChange = ""
    var expireDate = ""
    var cvvNO = ""
    var month = ""
    var year = ""
    var selectdate = 0
    var Currentyear = 0
    var nikiImgs = ["Movie_NIKI.png", "Bus_NIKi.png", "Cap_NIKI.png",  "Recharge_NIKI.png", "Hotel_NIKI.png","UtilityBills_NIKI", "Events_NIKI.png", "Laundry_NIKI.png"]
    var nikiList = ["Movie", "Bus", "Cap", "Recharge", "Hotels", "Utility Bills", "Events", "Laundry"]
    var nikimsg = ""
    var tableNameList = [String]()
    var tableNameList_Img = [String]()
    
    var nikiImage_ColorCode = [ UIColor(red: 65.0/255.0, green: 169.0/255.0, blue: 229.0/255.0, alpha: 1).CGColor, UIColor(red: 66.0/255.0, green: 191.0/255.0, blue: 201.0/255.0, alpha: 1).CGColor, UIColor(red: 84.0/255.0, green: 185.0/255.0, blue: 57.0/255.0, alpha: 1).CGColor, UIColor(red: 230.0/255.0, green: 33.0/255.0, blue: 37.0/255.0, alpha: 1).CGColor, UIColor(red: 244.0/255.0, green: 195.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor, UIColor(red: 63.0/255.0, green: 79.0/255.0, blue: 167.0/255.0, alpha: 1).CGColor, UIColor(red: 239.0/255.0, green: 123.0/255.0, blue: 7.0/255.0, alpha: 1).CGColor,UIColor(red: 215.0/255.0, green: 40.0/255.0, blue: 150.0/255.0, alpha: 1).CGColor]
    private let frontImageView: UIImageView! = UIImageView(image: UIImage(named: "card"))
    
    var backImageView: UIImageView! = UIImageView(image: UIImage(named: "card_back"))
    
    
    @IBOutlet weak var cardBackHeight: NSLayoutConstraint!
    
    private var showingBack = false
    
    var count = 0
    var nikiCount = 0
    override func viewDidLoad()
    {
        
        
        super.viewDidLoad()
        self.balancelbl.text = "0.0"
        
        setCVV()
        createDatePickerViewWithAlertController()
        setvaluestoall()
        initialCardFunc()
        initialCollectionFunc()
        initialfunc()
        transactiontable()
        singleStoreInit()
        dateConversion()
        flibInitFunc()
//        InitialSDK()
//         let shimmer = cardView.startShimmering()
        shimmerInit()
        
    }
   
   
    @IBOutlet weak var chat_ShopLbl: UILabel!
   
    @IBOutlet weak var card_Label_spaceConstraint: NSLayoutConstraint!

    @IBOutlet weak var cardBackView: UIView!
    func shimmerInit()
    {
//        let rect1 = CGRectMake(view.frame.origin.x,  cardView.frame.size.height+cardView.frame.origin.y+1000,
//           view.frame.size.width ,75 )
//        chat_ShopLbl.frame = rect1
        
//      
// cardViewWidth.constant = view.frame.size.width
        print(cardView.frame.size.width)
        print(self.view.frame.size.width)
       if self.view.frame.size.width == 375{
        cardImgWidth.constant = 359
        shimmer = FBShimmeringView(frame: CGRectMake(cardImgFront.frame.origin.x,cardView.frame.origin.y, cardImgWidth.constant, cardImgHeight.constant+6))
       

        }
        else if self.view.frame.size.width > 375
       {
        cardImgWidth.constant = 388
        shimmer = FBShimmeringView(frame: CGRectMake(cardImgFront.frame.origin.x,cardView.frame.origin.y, cardImgWidth.constant, cardImgHeight.constant+6))
       }
        else
       {
         cardImgWidth.constant = 302
         shimmer = FBShimmeringView(frame: CGRectMake(cardImgFront.frame.origin.x,cardView.frame.origin.y, cardImgWidth.constant, cardImgHeight.constant+6))
        
        
        }
        cardBackHeight.constant = shimmer.frame.size.height
        shimmer.contentView = self.cardView
        self.cardBackView.addSubview(shimmer)
        shimmer.shimmeringAnimationOpacity = 0.1
        shimmer.shimmeringHighlightLength = 0.1
        shimmer.shimmeringSpeed = 100.0
        shimmer.shimmering = true
        shimmer.translatesAutoresizingMaskIntoConstraints = true
          NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: #selector(self.StopShimmerAnimation), userInfo: nil, repeats: false)
    }
    func StopShimmerAnimation()
    {
        shimmer.shimmering = false
    }
    func flibInitFunc()
        
    {
      
        
        cvvBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
        
       
        frontImageView.contentMode = .ScaleToFill
        backImageView.contentMode = .ScaleToFill
        cardView.addSubview(frontImageView)
        frontImageView.translatesAutoresizingMaskIntoConstraints = false
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(flip))
        cardView.addGestureRecognizer(singleTap)
        
        //        cardView.addSubview(cvvBtn)
        print("cardimgWidth\(cardImgFront.frame.size.width)")
        print("cvvBtn\(view.frame.size.width)")
//        cvvBtn.layer.backgroundColor = UIColor.blueColor().CGColor
        if self.view.frame.size.width == 375{
            let rect1 = CGRectMake(cardView.frame.size.width/1.60 - 39 ,  cardView.frame.size.height/2+15
                , 50, 20)
            cvvBtn.frame = rect1
            
            cardImgHeight.constant = cardImgFront.frame.size.width/1.75
            let rect = CGRectMake(cardView.frame.origin.x, cardImgFront.frame.origin.y-1, view.frame.size.width-16, cardImgHeight.constant+6)
            backImageView.frame = rect
        }
       else if self.view.frame.size.width > 375{
            let rect1 = CGRectMake(cardImgFront.frame.size.width/1.60 - 29 ,  cardImgFront.frame.size.height/2+19
                , 50, 25)
            cvvBtn.frame = rect1
            
            cardImgHeight.constant = cardImgFront.frame.size.width/1.75
            let rect = CGRectMake(cardView.frame.origin.x, cardImgFront.frame.origin.y-1, view.frame.size.width-26, cardImgHeight.constant+6)
            backImageView.frame = rect
        }
        else {
            let rect1 = CGRectMake(cardView.frame.size.width/2 - 35 ,  cardView.frame.size.height/2
                , 50, 20)
            cvvBtn.frame = rect1
//            cardImgWidth.constant = 300
            cardImgHeight.constant = cardImgFront.frame.size.width/2
            let rect = CGRectMake(cardView.frame.origin.x, cardImgFront.frame.origin.y-1, view.frame.size.width-18, cardImgHeight.constant+6)
            backImageView.frame = rect
        }
        
        print("ORGIN1")
     print(backImageView.frame.origin.x,backImageView.frame.origin.y, backImageView.frame.size.width,backImageView.frame.size.height)
        cvvBtn.addTarget(self, action: #selector(showCVVAction), forControlEvents: .TouchUpInside)
        
        cvvBtn.contentHorizontalAlignment = .Right
        cvvBtn.titleLabel?.font = UIFont(name: "Helvetica", size: 12)
        cvvBtn.hidden = true
    }
    
    
    
    
    func flip(sender: AnyObject) {
        let yourTag = sender.view!.tag
        if yourTag == 1
        {
            
        }
        print("BackTag\(yourTag)")
        print("showingBack\(showingBack)")
        
        if showingBack
        {
            cvvBtn.hidden = true
            UIView.transitionFromView(backImageView, toView: frontImageView, duration: 0.75, options: .TransitionFlipFromRight, completion: nil)
            addMoneycardBtn.userInteractionEnabled = true
            plusCardBtn.userInteractionEnabled = true
          
            shimmer.shimmering = true
            NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: #selector(self.StopShimmerAnimation), userInfo: nil, repeats: false)
         
            
   
            
        }
        else
        {
           
            print("showingBack\(backImageView.frame.size.height)")
            print("showingBack\(backImageView.frame.size.width)")
            
            UIView.transitionFromView(frontImageView, toView: backImageView, duration: 0.75, options: .TransitionFlipFromLeft, completion: nil)
            addMoneycardBtn.userInteractionEnabled = false
            plusCardBtn.userInteractionEnabled  = false
            shimmer.shimmering = true
            NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: #selector(self.StopShimmerAnimation), userInfo: nil, repeats: false)
            
            cardView.addSubview(backImageView)
            if Appconstant.CustomerType == "EQWALLET"
            {
                cvvBtn.hidden = false
                cardView.addSubview(cvvBtn)
                
            }
            else
            {
                cvvBtn.hidden = true
            }
            
            //           mmm cvvBtn.addTarget(self, action: #selector(showCVVAction), forControlEvents: .TouchUpInside)
            
        }
        backImageView.translatesAutoresizingMaskIntoConstraints = true
        showingBack = !showingBack
        
        
        
    }
    
    
    
    
    func InitialSDK()
    {
        print("*\(Appconstant.customername)*")
        print("*\(Appconstant.mobileno)*")
        print("*\(Appconstant.customerid)*")
        if !AccountConstant.initiateSDK {
            AccountConstant.initiateSDK = true
            self.view.userInteractionEnabled = false
            do{
                
                let UserDetails = SdkClientApplication.init(userName: Appconstant.customername, appName: "com.equitas.purz", instituteId: "999", channel: "equitas-purz", bankRefUrl: "https://www.equitasbank.com", userMail: Appconstant.mobileno+"@test.com", mobile: Appconstant.mobileno, userIdentity: Appconstant.customerid, transactionID_Prefix: "ESF")
                
                try ApiClient().activateSdk(UserDetails, withCompletion: { (success, error, response) in
                    
                    if success {
                        print(response)
                        self.view.userInteractionEnabled = true
                    }
                    else {
                        self.view.userInteractionEnabled = true
                        if response != nil {
                            print(response)
                            let response_success = response.valueForKey("message")
                            self.presentViewController(Alert().alert(String(response_success!),message: ""), animated: true, completion: nil)
                            
                        }
                    }
                })
                
            }
            catch {
                AccountConstant.initiateSDK = false
                print("Internet connection failed..!")
            }
        }
    }
    
    
    func storeCVV(cvvNo1 : String)
    {
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let insertsql = "INSERT INTO CVV_TABLE (CUSTOMER_ID,CVV_NO) VALUES ('\(Appconstant.customerid)','\(cvvNo1)')"
            print(insertsql)
            let result = purzDB.executeUpdate(insertsql,
                                              withArgumentsInArray: nil)
            
            
            if !result {
                print("Error: \(purzDB.lastErrorMessage())")
            }
            
        }
        
        purzDB.close()
    }
    
    
    func setCVV()
    {
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let select = "SELECT * FROM CVV_TABLE"
            let result:FMResultSet = purzDB.executeQuery(select,
                                                         withArgumentsInArray: nil)
            while(result.next()){
                cvvNO=(result.stringForColumn("CVV_NO"))
                
                
            }
            
            if cvvNO != ""
            {
                self.cvvBtn.setTitle(cvvNO, forState: .Normal)
            }
            else
            {
                self.cvvBtn.setTitle("Get CVV", forState: .Normal)
            }
            self.tableView.reloadData()
            
        }
        purzDB.close()
        
    }
    func singleStoreInit()
    {
        
        if Appconstant.CustomerType == "EQWALLET"
        {
            cvvBtn.hidden = false
            addMoneyBackView.hidden = true
            addMoneycardBtn.hidden = true
            plusCardBtn.hidden = true
            tableNameList = ["Pay @Shop","Unified Payment Interface","Recharge/Pay Bill","Transactions","Expense Management","Offers","Contact Us"]
            tableNameList_Img = ["ic_home_scan_qr.png","UPI_Logo.png","ic_home_recharge.png","ic_home_transfer.png","ic_Expense.png","ic_home_offers.png","ic_home_Contact.png"]
        }
        else
        {
            scrollview.contentSize = CGSizeMake(self.view.frame.size.width, self.cardView.frame.size.height+535)
            cvvBtn.hidden = true
            addMoneyBackView.hidden = true
            addMoneycardBtn.hidden = true
            plusCardBtn.hidden = true
            tableNameList = ["Pay @Shop","Unified Payment Interface","Recharge/Pay Bill","Transactions","Expense Management","Reset Card PIN","Offers","Contact Us"]
            tableNameList_Img = ["ic_home_scan_qr.png","UPI_Logo.png","ic_home_recharge.png","ic_home_transfer.png","ic_Expense.png", "ic_home_reset.png","ic_home_offers.png","ic_home_Contact.png"]
            
        }
        
    }
    func createDatePickerViewWithAlertController()
    {
        let currentdate = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: currentdate)
        
        Currentyear =  Int(components.year)
        //        nodataLbl.hidden = true
        df.dateFormat = "yyyy";
        self.selectdate = Int(df.stringFromDate(currentdate))!
        self.viewDatePicker = UIView(frame: CGRectMake(0, self.view.frame.size.height - 288, self.view.frame.size.width, 288))
        viewDatePicker.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(viewDatePicker)
        
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.maximumDate = NSDate()
        
        var pickerSize : CGSize = datePickerView.sizeThatFits(CGSizeZero)
        datePickerView.frame = CGRectMake(0, self.view.frame.size.height - 244, self.view.frame.size.width, 244)
        datePickerView.backgroundColor = UIColor.clearColor()
        print(datePickerView.frame.origin.y)
        print(datePickerView.frame.size.height)
        self.view.addSubview(datePickerView)
        
        let toolbarView: UIView = UIView(frame: CGRectMake(0,0,self.view.frame.size.width,44))
        toolbarView.backgroundColor = UIColor.grayColor()
        //        let toolBar = UIToolbar(frame: CGRectMake(0,0,self.view.frame.size.width,44))
        let doneButton = UIButton.init(frame: CGRectMake(self.view.frame.size.width - 60,5,50,34))
        doneButton.addTarget(self, action: #selector(self.doneClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        dateString = df.stringFromDate(currentDateTime)
        print("Date"+dateString)
        
        toolbarView.addSubview(doneButton)
        
        
        print(toolbarView.frame.origin.y)
        self.viewDatePicker.addSubview(toolbarView)
        viewDatePicker.hidden = true
        datePickerView.hidden = true
    }
    
    func doneClicked(sender: UIButton)
    {
        
        
        print(Currentyear)
        print(selectdate)
        
        if(selectdate+18 > Currentyear){
            dispatch_async(dispatch_get_main_queue()) {
                self.presentViewController(Alert().alert("Sorry! The minimum age for using Purz is 18 years", message: ""),animated: true,completion: nil)
                
                
            }
            
        }
        else
        {
            
            
            self.viewDatePicker.hidden = true
            self.datePickerView.hidden = true
            if   self.datevalueChange != ""
            {
                
                print(datevalueChange)
                
                
                let profileviewmodel = UpdateProfileViewModel.init(specialDate: self.datevalueChange, entityId: Appconstant.customerid)!
                let serializedjson  = JSONSerializer.toJson(profileviewmodel)
                print(serializedjson)
                self.sendrequesttoserverForUpdateEntity(Appconstant.BASE_URL+Appconstant.URL_UPDATE_ENTITY, values: serializedjson)
                
            }
            
        }
        
    }
    
    
    
    
    func sendrequesttoserverForUpdateEntity(url : String, values: String)
    {
        print(url)
        print(values)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                dispatch_async(dispatch_get_main_queue()) {
                    //                self.activityIndicator.stopAnimating()
                }
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {
                dispatch_async(dispatch_get_main_queue()) {
                    //                self.activityIndicator.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")  // check for http errors
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseStringUpdate = \(responseString)")
                let json = JSON(data: data!)
                
                //                self.activityIndicator.stopAnimating()
                //       }
                if(json["result"].stringValue == "true"){
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        //                        self.presentViewController(Alert().alert("Your Date of Birth is updated successfully!", message: ""),animated: true,completion: nil)
                        
                        
                    }
                    
                    //
                    self.generateCVVFunc()
                    self.saveintoDB()
                    
                    
                    
                }
                
            }
        }
        
        task.resume()
        
    }
    
    func saveintoDB(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let selectSQL = "SELECT * FROM CUSTOMERDETAIL"
            
            let results:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                           withArgumentsInArray: nil)
            if (results.next()){
                let update1 = "UPDATE CUSTOMERDETAIL SET DATE_OF_BIRTH='"+self.datevalueChange+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                self.setvaluestoall()
                let result1 = purzDB.executeUpdate(update1,
                                                   withArgumentsInArray: nil)
                
                
                if (!result1){
                    
                    print("Error: \(purzDB.lastErrorMessage())")
                    //                    dispatch_async(dispatch_get_main_queue()) {
                    //                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    //
                    //                    }
                }
            }
            
        }
    }
    func setvaluestoall(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            print(Appconstant.customerid)
            let selectSQL = "SELECT * FROM CUSTOMERDETAIL WHERE CUSTOMER_ID=" + Appconstant.customerid
            let result:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                          withArgumentsInArray: nil)
            
            
            
            
            if (result.next()){
                
                Appconstant.dateOfBirth = result.stringForColumn("DATE_OF_BIRTH")
                
            }
                
            else{
                //   status.text = "Failed to add contact"
                print("Error: \(purzDB.lastErrorMessage())")
            }
            
        }
        purzDB.close()
    }
    
    
    
    func datePickerValueChanged(sender:UIDatePicker) {
        print("Sender\(sender)")
        print("Date\(sender.date)")
        let dateFormatter1 = NSDateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        datevalueChange = dateFormatter1.stringFromDate(sender.date)
        //        datevalueChange = String(Int64(sender.date.timeIntervalSince1970 * 1000))
        print("Date\(datevalueChange)")
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "ddMMyyyy"
        date1 = dateFormatter.stringFromDate(sender.date)
        let dateFormatter3 = NSDateFormatter()
        dateFormatter3.dateFormat = "yyyy"
        print(dateFormatter3.stringFromDate(sender.date))
        self.selectdate = Int(dateFormatter3.stringFromDate(sender.date))!
        
        
    }
    func showCVVAction(sender: UIButton!) {
        let yourTag = sender.tag
        print("TAG\(yourTag)")
        
        print("DateofBirth")
        print(Appconstant.dateOfBirth)
        print("Cvv\(cvvNO)")
        if cvvNO == ""
        {
            cvvBtn.enabled =  true
            
            if Appconstant.dateOfBirth == ""
            {
                let alert = UIAlertController(title: "Your date of birth is required to get CVV, please fill it!",message: "", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.Default, handler: self.showCalenderFunc))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                generateCVVFunc()
                
            }
        }
        else
        {
            let singleTap = UITapGestureRecognizer(target: self, action: #selector(flip))
            cardView.addGestureRecognizer(singleTap)
            cvvBtn.enabled = false
        }
        
    }
    func showCalenderFunc(action: UIAlertAction) {
        
        viewDatePicker.hidden=false
        datePickerView.hidden=false
        //Use action.title
    }
    func dateConversion()
    {
        
        if Appconstant.dateOfBirth != ""
        {
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateFormatter1 = NSDateFormatter()
            dateFormatter1.dateFormat = "ddMMyyyy"
            let date = dateFormatter.dateFromString(Appconstant.dateOfBirth)
            print(date)
            date1 = dateFormatter1.stringFromDate(date!)
            print("Date\(date1)")
        }
        if Appconstant.expiredate != ""
        {
            print(Appconstant.expiredate)
            
            let expiredate = Array(Appconstant.expiredate.characters)
            if Appconstant.expiredate.characters.count == 4 {
                month = String(expiredate[0])+String(expiredate[1])
                year = String(expiredate[2])+String(expiredate[3])
            }
            print(year)
            print(month)
            expireDate = year+month
            validLbl.text = month + "/" + year
        }
        else
        {
            validLbl.text = ""
        }
        
        
    }
    func generateCVVFunc()
    {
        
        let generateCVV = GenerateCVV.init(entityId: Appconstant.customerid, kitNo: Appconstant.KitNo, expiryDate: self.expireDate, dob: self.date1)!
        
        self.serializedjson  = JSONSerializer.toJson(generateCVV)
        
        self.generateCVVFromServer(Appconstant.BASE_URL+Appconstant.GENERATE_CVV,values: self.serializedjson)
        
    }
    func generateCVVFromServer(url : String, values: String)
    {
        print(url)
        print(values)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                dispatch_async(dispatch_get_main_queue()) {
                    //                    self.activityIndicater.stopAnimating()
                }
                print(error)
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        //                        self.activityIndicater.stopAnimating()
                    }
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                dispatch_async(dispatch_get_main_queue()) {
                    //                    self.activityIndicater.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                
                //                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                //                self.activityIndicater.stopAnimating()
                
            }
            dispatch_async(dispatch_get_main_queue()) {
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseStringCVV = \(responseString)")
                let json = JSON(data: data!)
                
                
                if json["result"].isEmpty{
                    
                    let item1 = json["exception"]
                    print(item1)
                    print( item1["errorCode"])
                    dispatch_async(dispatch_get_main_queue()) {
                        if item1["errorCode"].stringValue == "Y3003" && Appconstant.expiredate == ""
                        {
                            self.presentViewController(Alert().alert("You are unable to get cvv due to expiry date!",message: ""),animated: true, completion: nil)
                        }
                        else
                        {
                            self.presentViewController(Alert().alert(item1["shortMessage"].stringValue,message: ""),animated: true, completion: nil)
                        }
                        
                        
                        
                    }
                }
                else
                {
                    
                    let item = json["result"]
                    self.storeCVV(item["cvv"].stringValue)
                    self.cvvBtn.setTitle(item["cvv"].stringValue, forState: .Normal)
                    self.cvvNO = item["cvv"].stringValue
                    self.presentViewController(Alert().alert("CVV Retrieved Successfully",message: ""),animated: true, completion: nil)
                    print(item["cvv"].stringValue)
                }
                
            }
            
        }
        
        task.resume()
        
    }
    func initialCardFunc()
    {
        addMoneyBackView.layer.cornerRadius = 9
        plusCardBtn.addTarget(self, action: #selector(HomeViewController.addMoneyAction), forControlEvents: .TouchUpInside)
        addMoneycardBtn.addTarget(self, action: #selector(HomeViewController.addMoneyAction), forControlEvents: .TouchUpInside)
    }
    func addMoneyAction()
    {
        self.performSegueWithIdentifier("Home_AddMoney", sender: self)
    }
    func initialCollectionFunc()
    {
        let borderforCollectionView = CALayer()
        let width_CollectionView = CGFloat(2.0)
        borderforCollectionView.opacity = 0.5
        borderforCollectionView.borderColor = UIColor.lightGrayColor().CGColor
        borderforCollectionView.frame = CGRect(x: 0, y: collectionView.frame.size.height - 2, width: collectionView.frame.size.width*3 - 50, height: collectionView.frame.size.height)
        
        borderforCollectionView.borderWidth = width_CollectionView
        collectionView.layer.addSublayer(borderforCollectionView)
        collectionView.layer.masksToBounds = true
        
        
        
    }
    override func viewDidLayoutSubviews() {
        if Appconstant.CustomerType == "EQWALLET"
        {
            scrollview.contentSize = CGSizeMake(self.view.frame.size.width, self.cardView.frame.size.height+490)
        }
        else
        {
            scrollview.contentSize = CGSizeMake(self.view.frame.size.width, self.cardView.frame.size.height+535)
        }
        super.viewDidLayoutSubviews()
        
    }
    
    func sidemenufunction()
    {
        Appconstant.fromsidemenu = false
        if(Appconstant.SideMenu==1)
        {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("Home_Payatstore", sender: self)
            }
        }
        else if(Appconstant.SideMenu==2)
        {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("Home_Recharge", sender: self)
            }
        }
            
        else if(Appconstant.SideMenu==3)
        {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("Home_Transaction", sender: self)
            }
        }
            
        else if(Appconstant.SideMenu==6)
        {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("home_contact", sender: self)
                
            }
        }
        
    }
    func buttonTouched()
    {
        performSegueWithIdentifier("home_notification", sender: nil)
    }
    
    func separater(separateString: [Character]) -> String
    {
        print("separateString.count")
        print(separateString.count)
        var spaceSeparater = ""
        
        var i = 0
        var j = 0
        for (i = 0;i<separateString.count;i++)
        {
            
            if i % 4 == 0
            {
                spaceSeparater = spaceSeparater+" "
                
                print(spaceSeparater)
                
            }
            
            print("IIII\(i)")
            print(spaceSeparater)
            
            if Appconstant.CustomerType != "EQWALLET" {
                if i != 0 && i != 1 && i != 12 && i != 13 && i != 14 && i != 15
                {
                    spaceSeparater = spaceSeparater+"X"
                }
                else {
                    spaceSeparater = spaceSeparater+String(separateString[i])
                }
            }
                
            else
            {
                spaceSeparater = spaceSeparater+String(separateString[i])
                print(spaceSeparater)
            }
        }
        
        return spaceSeparater
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        getbalancefromServer(Appconstant.BASE_URL+Appconstant.URL_FETCH_MULTI_BALANCE_INFO+Appconstant.customerid)
        print(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=3")
        print("Appconstant.customerid\(Appconstant.customerid)")
        sidemenu = UIBarButtonItem(image: UIImage(named: "menu_three_line.png"), style: .Plain, target: self, action: Selector("action"))
        //        sidemenu.title = " Home "
        let leftItem = UIBarButtonItem(title: "Home", style: .Plain, target: nil, action: nil)
        
        navigationItem.leftBarButtonItems = [sidemenu,leftItem]
        
        
        navigationItem.leftBarButtonItem = sidemenu
        sidemenu.target = self.revealViewController()
        sidemenu.action = #selector(SWRevealViewController.revealToggle(_:))
        if Appconstant.fromlogout{
            Appconstant.fromlogout = false
            let alert = UIAlertController(title: "Are you sure you want to logout?", message: "", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: { alertAction in
                exit (0)
            }))
            
            alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { alertAction in
            }))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func initialfunc(){
        
        scrollview.scrollEnabled = true
        tabBarController?.delegate = self
        self.tabBarController?.tabBar.hidden = false
        navigationController?.navigationBarHidden = false
        navigationController?.navigationBarHidden = false
        
        var myString = Appconstant.cardnumber
        let charactersArray = Array(myString.characters)
        print(charactersArray)
        var spaceCustomerNumber = separater(charactersArray)
        accountNoLbl.text = spaceCustomerNumber
        print(year)
        print(month)
        
        if(Appconstant.fromsidemenu == true)
        {
            if Appconstant.SideMenu == 4 {
                Appconstant.SideMenu == 0
                Appconstant.fromsidemenu = false
                self.presentViewController(Alert().alert("Coming Soon...", message: ""),animated: true,completion: nil)
//                self.performSegueWithIdentifier("home_to_upi", sender: self)
            }
            else{
                sidemenufunction()
            }
        }
//        getbalancefromServer(Appconstant.BASE_URL+Appconstant.URL_FETCH_MULTI_BALANCE_INFO+Appconstant.customerid)
        //        print(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=3")
        
        
        sidemenu = UIBarButtonItem(image: UIImage(named: "menu_three_line.png"), style: .Plain, target: self, action: Selector("action"))
        
        let leftItem = UIBarButtonItem(title: "Home", style: .Plain, target: nil, action: nil)
        
        navigationItem.leftBarButtonItems = [sidemenu,leftItem]
        navigationItem.leftBarButtonItem = sidemenu
        sidemenu.target = self.revealViewController()
        sidemenu.action = #selector(SWRevealViewController.revealToggle(_:))
        
        let badgeButton : UIButton = UIButton(frame: CGRectMake(0, 0, 22, 22))
        badgeButton.setImage(UIImage(named: "Notification"), forState: UIControlState.Normal)
        badgeButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        badgeButton.targetForAction(#selector(HomeViewController.buttonTouched), withSender: self)
        badgeButton.addTarget(self, action: #selector(HomeViewController.buttonTouched)
            , forControlEvents: UIControlEvents.TouchUpInside)
        
        //ROUNDED BUTTON Labelin
        if(Appconstant.notificationcount > 0){
            let label = UILabel(frame: CGRectMake(10, -10, 16, 16))
            label.layer.masksToBounds = true
            label.layer.cornerRadius = label.frame.size.height/2
            label.textAlignment = NSTextAlignment.Center
            label.backgroundColor = UIColor.whiteColor()
            label.textColor = UIColor.blackColor()
            label.font = label.font.fontWithSize(12)
            label.text = String(Appconstant.notificationcount)
            badgeButton.addSubview(label)
        }
        
        let barButton : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton
        
        
        if Appconstant.fromweb{
            Appconstant.fromweb = false
            dispatch_async(dispatch_get_main_queue()) {
                self.presentViewController(Alert().alert(Appconstant.alertmsg, message: ""),animated: true,completion: nil)
            }
        }
        
    }
    //    @IBAction func AddmoneyBtnAction(sender: AnyObject) {
    //        dispatch_async(dispatch_get_main_queue()) {self.performSegueWithIdentifier("Home_Addmoney", sender: self)
    //        }
    //        \8
    //
    
    func transactiontable(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let select = "SELECT * FROM TRANSACTIONS"
            let result4:FMResultSet = purzDB.executeQuery(select,
                                                          withArgumentsInArray: nil)
            if(result4.next()){
                
            }
            else{
                dispatch_async(dispatch_get_main_queue()) {
                    self.gettentransaction(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=10")
                }
            }
        }
        purzDB.close()
        
    }
    
    func getbalancefromServer(url: String){
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //        var amountTxt = "\u{20B9}" +   Appconstant.mainbalance
        //        amountTxt="\u{20B9}"+String(10)
        //        //                             self.balancelbl.text! = "\u{20B9}" +   Appconstant.mainbalance
        //        //self.balancelbl = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 50))
        //
        //        let strokeTextAttributes = [NSStrokeColorAttributeName : UIColor.whiteColor(),NSForegroundColorAttributeName : UIColor.blackColor(),NSStrokeWidthAttributeName : -5.0,NSFontAttributeName : UIFont.boldSystemFontOfSize(25)]
        
        //        self.balancelbl.attributedText = NSAttributedString(string: amountTxt, attributes: strokeTextAttributes)
        
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                
                for item in json["result"].arrayValue{
                    dispatch_async(dispatch_get_main_queue()) {
                        let balance_two_decimal = String(format: "%.2f", item["balance"].doubleValue)
                        let checkbalancedecimal = balance_two_decimal.componentsSeparatedByString(".")
                        if(checkbalancedecimal[1].characters.count == 1){
                            self.balancelbl.text =  balance_two_decimal + "0"
                            Appconstant.mainbalance = self.balancelbl.text!
                            let balanceamt = "\u{20B9}" + Appconstant.mainbalance
                            
                            self.balancelbl1.text =  balance_two_decimal + "0"
                            let balanceamt1 = "\u{20B9}" + Appconstant.mainbalance
                            
                            //                                var foo = NSAttributedString(string: balanceamt, attributes: [
                            //                                    NSStrokeColorAttributeName : UIColor.blackColor(),
                            //                                    NSForegroundColorAttributeName : UIColor.whiteColor(),
                            //                                    NSStrokeWidthAttributeName : NSNumber(float: -8),
                            //                                    NSFontAttributeName : UIFont.systemFontOfSize(16.0)
                            //                                    ])
                            //
                            //
                            //                                var foo1 = NSAttributedString(string: balanceamt1, attributes: [
                            //                                    NSStrokeColorAttributeName : UIColor.blackColor(),
                            //                                    NSForegroundColorAttributeName : UIColor.whiteColor(),
                            //                                    NSStrokeWidthAttributeName : NSNumber(float: 9),
                            //                                    NSFontAttributeName : UIFont.systemFontOfSize(25.0)
                            //                                    ])
                            //                                self.balancelbl.text = balanceamt
                            //                                self.balancelbl.attributedText = foo
                            //                                self.balancelbl1.text = balanceamt1
                            //                                self.balancelbl1.attributedText = foo
                            
                            self.balancelbl.text = balanceamt
                            
                            
                            print("AMOUNT\(Appconstant.mainbalance)")
                            
                        }
                        else{
                            self.balancelbl.text = balance_two_decimal
                            Appconstant.mainbalance = self.balancelbl.text!
                            //self.balancelbl1.text = balance_two_decimal
                            var amountTxt = "\u{20B9}" +   Appconstant.mainbalance
                            self.balancelbl.text = amountTxt
                            //let strokeTextAttributes = [NSStrokeColorAttributeName : UIColor.whiteColor(),NSForegroundColorAttributeName : UIColor.blackColor(),NSStrokeWidthAttributeName : -5.0,NSFontAttributeName : UIFont.boldSystemFontOfSize(25)]
                            //
                            //                                self.balancelbl.attributedText = NSAttributedString(string: amountTxt, attributes: strokeTextAttributes)
                            
                            //                                self.balancelbl.backgroundColor = UIColor.cyanColor()
                            //                                self.balancelbl.layer.shadowColor = UIColor.whiteColor().CGColor
                            //                                self.balancelbl.layer.shadowOffset = CGSize(width: 50.0, height: 50.0)
                            //                                self.balancelbl.layer.shadowOpacity = 15.0
                            //                                self.balancelbl.layer.shadowRadius = 18.0
                            //self.balancelbl.text = balanceamt
                            
                            
                            //                                var foo = NSAttributedString(string: balanceamt, attributes: [
                            //                                    NSStrokeColorAttributeName : UIColor.blackColor(),
                            //                                    NSForegroundColorAttributeName : UIColor.whiteColor(),
                            //                                    NSStrokeWidthAttributeName : NSNumber(float: -8),
                            //                                    NSFontAttributeName : UIFont.systemFontOfSize(16.0)
                            //                                    ])
                            //
                            
                            
                        }
                    }
                }
                if(self.refresh_balnc == 1){
                    self.refresh_balnc = 0
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("So Fresh! Balance in your wallet is refreshed.", message: ""),animated: true,completion: nil)
                    }
                }
                
            }
        }
        task.resume()
    }
    
    func getrecenttransaction(url: String){
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                }
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            self.trans_date.removeAll()
            self.transactionamt.removeAll()
            self.beneficiaryname.removeAll()
            for item1 in json["result"].arrayValue{
                let item = item1["transaction"]
                
                let balance_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                let amount = balance_two_decimal.componentsSeparatedByString(".")
                if(amount[1].characters.count == 1){
                    let finalamount = balance_two_decimal + "0"
                    self.transactionamt.append(finalamount)
                }
                else{
                    self.transactionamt.append(balance_two_decimal)
                }
                
                
                let perfectName = item["beneficiaryName"].stringValue
                
                if(perfectName .containsString("null"))
                {
                    perfectName.stringByReplacingOccurrencesOfString("null", withString: "")
                }
                
                self.beneficiaryname.append(perfectName)
                
                
                //                    self.beneficiaryname.append(item["beneficiaryName"].stringValue)
                let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                self.transactiontype.append(item["type"].stringValue)
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd MMM yyyy tt:mm:ss"
                dateFormatter.timeZone = NSTimeZone(name: "UTC")
                let dateString = dateFormatter.stringFromDate(date)
                print(dateString)
                if(!dateString.isEmpty){
                    let datearray = dateString.componentsSeparatedByString(" ")
                    if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                        let correctdate = datearray[0] + "st " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                        let correctdate = datearray[0] + "nd " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                        let correctdate = datearray[0] + "rd " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    else{
                        let correctdate = datearray[0] + "th " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                
                self.tableView.reloadData()                }
            
            self .transactiontable()
        }
        
        task.resume()
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as UITableViewCell!
        let listImg = cell.viewWithTag(1) as! UIImageView
        let namelbl = cell.viewWithTag(2) as! UILabel
        namelbl.text = tableNameList[indexPath.row]
        listImg.image = UIImage(named: tableNameList_Img[indexPath.row])
        
        print("List"+tableNameList[indexPath.row])
        
        //        let border = CALayer()
        //        let width = CGFloat(1.0)
        //        border.borderColor = UIColor.whiteColor().CGColor
        //        border.frame = CGRect(x: 0, y: cell.frame.size.height - width, width:  cell.frame.size.width, height: cell.frame.size.height)
        //
        //        border.borderWidth = width
        //        cell.layer.addSublayer(border)
        //        cell.layer.masksToBounds = true
        //        cell.layer.borderColor = UIColor.whiteColor().CGColor
        //        cell.layer.backgroundColor = UIColor.lightGrayColor().CGColor
        cell.selectionStyle = .None
        //        let whiteRoundedView : UIView = UIView(frame: CGRectMake(5, 5, self.view.frame.size.width - 30, 50))
        if Appconstant.CustomerType=="EQWALLET"
        {
            if count < 8
            {
                cell.layer.backgroundColor = CGColorCreate(CGColorSpaceCreateDeviceRGB(), [1.0, 1.0, 1.0, 1.0])
                cell.layer.masksToBounds = false
                cell.layer.cornerRadius = 2.0
                cell.layer.shadowOffset = CGSizeMake(-1, 1)
                cell.layer.shadowOpacity = 0.5
                count += 1
            }
        }
        else
        {
            if count < 9
            {
                cell.layer.backgroundColor = CGColorCreate(CGColorSpaceCreateDeviceRGB(), [1.0, 1.0, 1.0, 1.0])
                cell.layer.masksToBounds = false
                cell.layer.cornerRadius = 2.0
                cell.layer.shadowOffset = CGSizeMake(-1, 1)
                cell.layer.shadowOpacity = 0.5
                count += 1
            }
            
            
        }
        return cell
        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("tableRowCount:\(tableNameList_Img.count)")
        return tableNameList.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        dispatch_async(dispatch_get_main_queue()) {
        self.selectedindex = indexPath.row
        self.selectedfromtable = true
        if(indexPath.row==0)
        {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("Home_Payatstore", sender: self)
            }
        }
        else if(indexPath.row==1)
        {
            dispatch_async(dispatch_get_main_queue()) {
                self.presentViewController(Alert().alert("Coming Soon...", message: ""),animated: true,completion: nil)
//                self.performSegueWithIdentifier("home_to_upi", sender: self)
            }
        }
        else if(indexPath.row==2)
        {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("Home_Recharge", sender: self)
            }
        }
            
        else if(indexPath.row==3)
        {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("Home_Transaction", sender: self)
            }
        }
        else if(indexPath.row==4)
        {
            dispatch_async(dispatch_get_main_queue()) {
                print("come soon")
                self.performSegueWithIdentifier("To_Piechart", sender: self)
            }
        }
        else if(indexPath.row==5)
        {
            
            
            if Appconstant.CustomerType=="EQWALLET"
            {
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("Home_Offers", sender: self)
                }
            }
                
            else
                
            {
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("Home_SingleStore", sender: self)
                }
            }
        }
        else if(indexPath.row==6)
        {
            
            
            if Appconstant.CustomerType=="EQWALLET"
            {
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("home_contact", sender: self)
                }
            }
                
            else
                
            {
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("Home_Offers", sender: self)
                }
            }
        }
            
        else if(indexPath.row==7)
        {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("home_contact", sender: self)
            }
        }
        }
        
    }
    
    
    
    
    
    func gettentransaction(url: String){
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                }
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseStringtrans = \(responseString)")
            let json = JSON(data: data!)
            
            self.transactionamt.removeAll()
            self.trans_date.removeAll()
            self.beneficiaryname.removeAll()
            var i = 0
            for item1 in json["result"].arrayValue{
                let item = item1["transaction"]
                
                
                
                
                
                let balance_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                let amount = balance_two_decimal.componentsSeparatedByString(".")
                if(amount[1].characters.count == 1){
                    let finalamount = balance_two_decimal + "0"
                    self.transactionamt.append(finalamount)
                }
                else{
                    self.transactionamt.append(balance_two_decimal)
                }
                
                let mainbalance_two_decimal = String(format: "%.2f", item["balance"].doubleValue)
                let mainamount = mainbalance_two_decimal.componentsSeparatedByString(".")
                if(mainamount[1].characters.count == 1){
                    let finalamount = balance_two_decimal + "0"
                    self.balance.append(finalamount)
                }
                else{
                    self.balance.append(mainbalance_two_decimal)
                }
                
                
                let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd MMM"
                dateFormatter.timeZone = NSTimeZone(name: "UTC")
                let dateString = dateFormatter.stringFromDate(date)
                if(!dateString.isEmpty){
                    let datearray = dateString.componentsSeparatedByString(" ")
                    if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                        let correctdate = datearray[0] + "st " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                        let correctdate = datearray[0] + "nd " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                        let correctdate = datearray[0] + "rd " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    else{
                        let correctdate = datearray[0] + "th " + datearray[1]
                        self.trans_date.append(correctdate)
                    }
                    
                }
                print(item["description"].stringValue)
                let desc = ""
                DBHelper().purzDB()
                let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
                let databasePath = databaseURL.absoluteString
                let purzDB = FMDatabase(path: databasePath as String)
                if purzDB.open() {
                    
                    let insert = "INSERT INTO TRANSACTIONS (AMOUNT,BENEFICIARY_ID,TRANSACTION_TYPE,TYPE,TIME,TRANSACTION_STATUS,TX_REF,BENEFICIARY_NAME,DESCRIPTION,OTHER_PARTY_NAME,OTHER_PARTY_ID,TXN_ORIGIN,TRANSACTIONID) VALUES"
                    let value0 =  "('"+self.transactionamt[i]+"','\(item["beneficiaryId"].stringValue)','\(item["transactionType"].stringValue)','\(item["type"].stringValue)',"
                    let value1 = "'"+self.trans_date[i]+"','\(item["transactionStatus"].stringValue)','\(item["txRef"].stringValue)','\(item["beneficiaryName"].stringValue)',"
                    let value2 = "'\(desc)','\(item["otherPartyName"].stringValue)','\(item["otherPartyId"].stringValue)','\(item["txnOrigin"].stringValue)','\(item["externalTransactionId"].stringValue)')"
                    let insertsql = insert+value0+value1+value2
                    let result = purzDB.executeUpdate(insertsql,
                                                      withArgumentsInArray: nil)
                    
                    if !result {
                        //   status.text = "Failed to add contact"
                        print("Error: \(purzDB.lastErrorMessage())")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                    purzDB.close()
                }
                i++
                
            }
        }
        
        task.resume()
    }
    
    
    func getplanlist(url: String){
        print(url)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else
            {
                // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                }
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            for item in json["result"].arrayValue{
                if(item["operatorType"].stringValue == "PREPAID"){
                    self.pre_operatorCode.append(item["operatorCode"].stringValue)
                    self.pre_operatorName.append(item["operatorName"].stringValue)
                    self.pre_operatorId.append(item["operatorId"].stringValue)
                    self.pre_operatorType.append(item["operatorType"].stringValue)
                    self.pre_special.append(item["special"].boolValue)
                }
                else if(item["operatorType"].stringValue == "POSTPAID"){
                    self.post_operatorCode.append(item["operatorCode"].stringValue)
                    self.post_operatorName.append(item["operatorName"].stringValue)
                    self.post_operatorId.append(item["operatorId"].stringValue)
                    self.post_operatorType.append(item["operatorType"].stringValue)
                    self.post_special.append(item["special"].boolValue)
                }
                else if(item["operatorType"].stringValue == "DTH"){
                    self.dth_operatorCode.append(item["operatorCode"].stringValue)
                    self.dth_operatorName.append(item["operatorName"].stringValue)
                    self.dth_operatorId.append(item["operatorId"].stringValue)
                    self.dth_operatorType.append(item["operatorType"].stringValue)
                    self.dth_special.append(item["special"].boolValue)
                }
                
            }
        }
        
        task.resume()
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "home_rechargeorbill") {
            let nextview = segue.destinationViewController as! RechargeorPayBills
        }
        else if segue.identifier == "Home_to_Niki" {
            let nextview = segue.destinationViewController as! NikiWebViewController
            nextview.nikiurl = "http://35.154.219.94:3000?mobile=\(Appconstant.mobileno)&name=\(Appconstant.customername)&email=\(Appconstant.email)&message=\(nikimsg)"

        }
        if(segue.identifier == "from-home") {
            let nextview = segue.destinationViewController as! TransactionsViewController
            nextview.fromhome = true
            nextview.selectedindex = selectedindex
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return nikiList.count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("collectionCell", forIndexPath: indexPath) as UICollectionViewCell
        
        let img = cell.viewWithTag(1) as! UIImageView
        let listname = cell.viewWithTag(2) as! UILabel
        listname.text = nikiList[indexPath.row]
        
        img.image = UIImage(named: nikiImgs[indexPath.row])
        //        if nikiCount <= 7
        //        {
        ////        if nikiImage_ColorCode.count-1 <= indexPath.row
        ////        {
        img.layer.cornerRadius = img.frame.size.height/2
        img.layer.borderColor = nikiImage_ColorCode[indexPath.row]
        img.layer.borderWidth = 1
        ////        }
        //        nikiCount+=1
        //        }
        
        
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        // Compute the dimension of a cell for an NxN layout with space S between
        // cells.  Take the collection view's width, subtract (N-1)*S points for
        // the spaces between the cells, and then divide by N to find the final
        // dimension for the cell's width and height.
        //
        //        let cellsAcross: CGFloat = 4
        //        let spaceBetweenCells: CGFloat = 1
        //        print(collectionView.bounds.width)
        //        let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
        //        print(dim)
        return CGSize(width: 85, height: 100)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
//        if indexPath.row == 0 {
//            nikimsg = "movies near me"
//        }
//        else if indexPath.row == 1 {
//            nikimsg = "Bus near me"
//        }
//        else if indexPath.row == 2 {
//            nikimsg = "Cab near me"
//        }
//        else if indexPath.row == 3 {
//            nikimsg = "Recharge"
//        }
//        else if indexPath.row == 4 {
//            nikimsg = "Hotels near me"
//        }
//        else if indexPath.row == 5 {
//            nikimsg = "Utility Bills"
//        }
//        else if indexPath.row == 6 {
//            nikimsg = "Events"
//        }
//        else if indexPath.row == 7 {
//            nikimsg = "Laundry near me"
//        }
//        CheckNiki()
        
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(Alert().alert("Coming Soon...", message: ""),animated: true,completion: nil)
        }
        
    }
    
    func CheckNiki() {
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let selectSQL = "SELECT * FROM PROFILE_INFO WHERE CUSTOMER_ID = \(Appconstant.customerid)"
            
            let results:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                           withArgumentsInArray: nil)
            if results.next() {
                print(results.stringForColumn("NIKI"))
                if results.stringForColumn("NIKI") == "true" {
                    self.performSegueWithIdentifier("Home_to_Niki", sender: self)
                }
                else {
                    disclaimerAlert()
                }
            }
            else {
                disclaimerAlert()
            }
        }

    }
    
    func disclaimerAlert() {
        var mobileno_char = Array(Appconstant.mobileno.characters)
        var mobileno = ""
        var updatetable = ""
        for i in 0 ..< Appconstant.mobileno.characters.count  {
            if i <= 5 {
                mobileno += "."
            }
            else {
                mobileno += String(mobileno_char[i])
            }
        }
        let alertController = UIAlertController(title: "DISCLAIMER",
                                                message: "I am aware that the Booking related services under “Chat and Shop” in your Purz App are provided by Niki.ai. I hereby give my consent to the Bank to share my below personal information with Niki.ai for sending me booking related information. I am further aware that Niki.ai alone is responsible for such services and I shall not make Bank liable in any manner for the same.\n\nNAME\t:\t\(Appconstant.customername)\nMOBILE\t:\t\(mobileno)" ,
                                                preferredStyle: .Alert)
        
        let action1 = UIAlertAction(title: "I AGREE", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            if purzDB.open() {
                let nikivalue = "true"
                updatetable = "UPDATE PROFILE_INFO SET NIKI = '"+nikivalue+"' WHERE CUSTOMER_ID = " + Appconstant.customerid
                let result = purzDB.executeUpdate(updatetable,
                    withArgumentsInArray: nil)
                print(updatetable)
                if !result {
                    print("Error in create niki")
                    print("Error: \(purzDB.lastErrorMessage())")
                }
                self?.performSegueWithIdentifier("Home_to_Niki", sender: self)
                
            }
            purzDB.close()
            })
        
        let action2 = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            })
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.presentViewController(alertController, animated: true, completion: nil)
    }

    
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        
        let tabBarIndex = tabBarController.selectedIndex
        
        if tabBarIndex == 0 {
            print("Selected item 0")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("HomeView")
            
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        
    }
    
}
extension UIView {
    
    func startShimmering(){
        let light = UIColor.whiteColor().CGColor
        let alpha = UIColor.whiteColor().colorWithAlphaComponent(0.7).CGColor
        
        let gradient = CAGradientLayer()
        gradient.colors = [alpha, light, alpha, alpha, light, alpha]
        gradient.frame = CGRect(x: -self.bounds.size.width, y: 0, width: 3 * self.bounds.size.width, height: self.bounds.size.height)
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.525)
        gradient.locations = [0.4, 0.5, 0.6]
        self.layer.mask = gradient
        
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [0.0, 0.1, 0.2]
        animation.toValue = [0.8, 0.9, 1.0]
        animation.duration = 1.5
        animation.repeatCount = HUGE
        gradient.addAnimation(animation, forKey: "shimmer")
       
    }
    
    func stopShimmering(){
        self.layer.mask = nil
    }
    
}
