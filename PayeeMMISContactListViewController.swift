
//
//  PayeeMMISContactListViewController.swift
//  purZ
//
//  Created by Vertace on 31/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class PayeeMMISContactListViewController: UIViewController,UITableViewDataSource, UITableViewDelegate, UITabBarControllerDelegate {
    
    var registeredName = [String]()
    var MMID = [String]()
    var MobileNo = [String]()
    var contact_PayeeName = ""
    var contact_MobileNo = ""
    var contact_MMID = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.delegate = self
        // Do any additional setup after loading the view.
        PayeeVPAList()

    }
    @IBAction func closeBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "Are you sure you want to delete?",
                                            message: "",
                                            preferredStyle: .Alert)
        
        let action1 = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            
            if purzDB.open() {
                
                let deleteSQL =  "DELETE FROM BENEFICIARY_MMID WHERE MMID = " + self!.MMID[indexPath.row]
                
                let result = purzDB.executeUpdate(deleteSQL,
                    withArgumentsInArray: nil)
                
                if !result {
                    //   status.text = "Failed to add contact"
                    print("Error: \(purzDB.lastErrorMessage())")
                }
                else
                {
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self!.presentViewController(Alert().alert("MMID deleted successfully",message: ""), animated: true, completion: nil)
                        self!.MMID.removeAll()
                        self!.registeredName.removeAll()
                        self!.PayeeVPAList()
                    }
                }
            }
            
            })
        
        let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            })
        
        
        alertController?.addAction(action1)
        alertController?.addAction(action2)
        self.presentViewController(alertController!, animated: true, completion: nil)
        

        
        
    }
    
    @IBAction func backBtnAction(sender: AnyObject) {
        if Appconstant.sendMoney_MMID
        {
            performSegueWithIdentifier("To_SendMMID", sender: self)
            Appconstant.sendMoney_MMID = false
        }
        else
        {
            performSegueWithIdentifier("To_BeneficiaryHome", sender: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func PayeeVPAList()
    {
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let select = "SELECT * FROM BENEFICIARY_MMID"
            let result:FMResultSet = purzDB.executeQuery(select,
                                                         withArgumentsInArray: nil)
            while(result.next()){
                registeredName.append(result.stringForColumn("REGISTERED_NAME"))
                MMID.append(result.stringForColumn("MMID"))
                  MobileNo.append(result.stringForColumn("MOBILE_NO"))
                
            }
            self.tableView.reloadData()
        }
        purzDB.close()
        
        
        
    }
    

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("payeeCreatedMMID", forIndexPath: indexPath) as UITableViewCell!
        
        
        let MMIDLbl = cell.viewWithTag(1) as! UILabel
        let RegisteredName = cell.viewWithTag(2) as! UILabel
        let crossBtn = cell.viewWithTag(3) as! UIButton
        let backView = cell.viewWithTag(4) as UIView!
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, backView.frame.size.height - 1 , backView.frame.size.width,1.0)
        bottomLine1.backgroundColor = UIColor.lightGrayColor().CGColor
        backView.layer.addSublayer(bottomLine1)

        MMIDLbl.text = MMID[indexPath.row] + "/" + MobileNo[indexPath.row]
        RegisteredName.text = registeredName[indexPath.row]
        
//        cell.layer.borderWidth = 3
//        cell.layer.borderColor = UIColor(red: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1).CGColor
        cell.selectionStyle = .None
//
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return MMID.count
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if Appconstant.sendMoney_MMID
        {
            Appconstant.sendMoney_MMID = false
        contact_PayeeName = registeredName[indexPath.row]
        contact_MMID = MMID[indexPath.row]
        contact_MobileNo = MobileNo[indexPath.row]
        performSegueWithIdentifier("To_SendMMID", sender: self)
        }
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "To_SendMMID") {
            let nextview = segue.destinationViewController as! sendMoneyMMIDViewController
            
            nextview.contact_PayeeName = contact_PayeeName
            nextview.contact_MobileNo = contact_MobileNo
            nextview.contact_MMID = contact_MMID
            
        }
        else if segue.identifier == "B_MMID_to_addMMID" {
            let nextvc = segue.destinationViewController as! AddMMIDContactViewController
            nextvc.existing_mmid = self.MMID
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
}
