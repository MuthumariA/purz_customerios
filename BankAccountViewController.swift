//
//  BankAccountViewController.swift
//  purZ
//
//  Created by Vertace on 24/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class BankAccountViewController: UIViewController, UITabBarControllerDelegate, UITextFieldDelegate {
   
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var confirmAccountTxtfield: UITextField!
    @IBOutlet weak var accountTxtfield: UITextField!
    @IBOutlet weak var registeredNameTxtfield: UITextField!
    @IBOutlet weak var beneficiaryIFSCTxtfield: UITextField!
    
    var result = false
    var secure_AccountNo = ""
    var account  = ""
    var existing_accounts = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialFunc()
        // Do any additional setup after loading the view.
    }
    func initialFunc()
    {
        accountTxtfield.secureTextEntry = true
        tabBarController?.delegate = self
        confirmBtn.layer.borderWidth = 2
        confirmBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        confirmBtn.layer.cornerRadius = confirmBtn.frame.size.height/2
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, beneficiaryIFSCTxtfield.frame.size.height - 1 , beneficiaryIFSCTxtfield.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                beneficiaryIFSCTxtfield.borderStyle = UITextBorderStyle.None
        beneficiaryIFSCTxtfield.layer.addSublayer(bottomLine)
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, registeredNameTxtfield.frame.size.height - 1 , registeredNameTxtfield.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                registeredNameTxtfield.borderStyle = UITextBorderStyle.None
        registeredNameTxtfield.layer.addSublayer(bottomLine1)
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, accountTxtfield.frame.size.height - 1 , accountTxtfield.frame.size.width, 1.0)
        bottomLine2.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                accountTxtfield.borderStyle = UITextBorderStyle.None
        accountTxtfield.layer.addSublayer(bottomLine2)
        let bottomLine3 = CALayer()
        bottomLine3.frame = CGRectMake(0.0, confirmAccountTxtfield.frame.size.height - 1 , confirmAccountTxtfield.frame.size.width, 1.0)
        bottomLine3.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                confirmAccountTxtfield.borderStyle = UITextBorderStyle.None
        confirmAccountTxtfield.layer.addSublayer(bottomLine3)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GenerateQRViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

    }
//    func validateIFSC(code : String) -> Bool {
//        let regex1 = try! NSRegularExpression(pattern: "^[A-Za-z]{4}0.{6}$", options: .CaseInsensitive)
//    
//        return regex1.numberOfMatchesInString(code, options: NSMatchingOptions.Anchored, range: NSMakeRange(0, ))
//     
//    }
 
    @IBAction func confirmBtn(sender: AnyObject) {
       
            var alreadyexist = false
        for(var i=0;i<existing_accounts.count;i++) {
            if existing_accounts[i] == accountTxtfield.text! {
                alreadyexist = true
            }
        }
        if !alreadyexist {
        let ifsc = beneficiaryIFSCTxtfield.text!
        let accountNo = accountTxtfield.text!
        let confirmAC = confirmAccountTxtfield.text!
        let ifscValidation = validateIFSC(ifsc)
        if (ifsc == "" )
        {
            self.presentViewController(Alert().alert("Beneficiary VPA can't be empty", message: ""),animated: true,completion: nil)
        }
            
            
        else if(ifsc.characters.count != 11 || !ifscValidation )
        {
            self.presentViewController(Alert().alert("Enter Valid IFSC Code", message: ""),animated: true,completion: nil)
            
        }
        else if (registeredNameTxtfield.text! == "" )
        {
            self.presentViewController(Alert().alert("Registered Name can't be empty", message: ""),animated: true,completion: nil)
        }
        else if (accountNo == "" )
        {
            self.presentViewController(Alert().alert("Account Number can't be empty", message: ""),animated: true,completion: nil)
        }
        else if (confirmAC == "" )
        {
            self.presentViewController(Alert().alert("Confirm Account Number can't be empty", message: ""),animated: true,completion: nil)
        }
        else if (accountNo != confirmAC)
        {
            self.presentViewController(Alert().alert("Account Number can't be different", message: ""),animated: true,completion: nil)
            
        }
        else
        {
            account = accountTxtfield.text!
            
            if account.characters.count > 4
            {
                
                secure_AccountNo = secureAccountNO(account)
            }else{
                secure_AccountNo = confirmAccountTxtfield.text!
            }
            
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            if purzDB.open() {
                let insert = "INSERT INTO BENEFICIARY_IFSC (IFSC_CODE,REGISTERED_NAME, ACCOUNT_NUMBER, SECURE_ACCOUNT_NUMBER) VALUES ('\(beneficiaryIFSCTxtfield.text!)','\(registeredNameTxtfield.text!)','\(confirmAccountTxtfield.text!)','\(secure_AccountNo)')"
                let result = purzDB.executeUpdate(insert, withArgumentsInArray: nil)
                if !result  {
                    //   status.text = "Failed to add contact"
                    print("Error: \(purzDB.lastErrorMessage())")
                }
            }
            self.performSegueWithIdentifier("To_ListIFSC", sender: self)
        }
        }
        else {
            beneficiaryIFSCTxtfield.text = ""
            registeredNameTxtfield.text = ""
            accountTxtfield.text = ""
            confirmAccountTxtfield.text = ""
            self.presentViewController(Alert().alert("Beneficiary Account already exist!", message: ""), animated: true, completion: nil)
        }
    }

    func validateIFSC(ifsc:String)-> Bool
    {
        
        let passwordRegex = "^[A-Z]{4}0.{6}$"
        
        let myString = "\(ifsc)"
        let regex = try! NSRegularExpression(pattern: passwordRegex, options: [])
        let range = NSMakeRange(0, myString.characters.count)
        
        let CountString = myString.characters.count
        
        let modString = regex.stringByReplacingMatchesInString(myString, options: [], range: range, withTemplate: "valid")
        
        if(modString == "valid")
        {
            
            result = true
        }
        else
        {
            result = false
        }
        
        return result
        
    }
     func secureAccountNO(str:String) ->String
    {
        
        var index1 = str.endIndex.advancedBy(-4)
        var substring1 = ""
        let last = String(str.characters.suffix(4))
        
        for(var i=0; i<str.characters.count-4;i++){
            substring1 = substring1 + "*"
        }
        let secureAC = substring1+last
    return secureAC
    }
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
   func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    let startingLength = textFieldToChange.text?.characters.count ?? 0
    let lengthToAdd = string.characters.count
    let lengthToReplace = range.length
    let newLength = startingLength + lengthToAdd - lengthToReplace
   if textFieldToChange == beneficiaryIFSCTxtfield
   {
    
            return newLength <= 11
    
    }
    if textFieldToChange == beneficiaryIFSCTxtfield
    {
        
        return newLength <= 11
     
    }
    else if textFieldToChange == accountTxtfield
    {
        
        return newLength <= 16
        
    }
    else if textFieldToChange == confirmAccountTxtfield
    {
        
        return newLength <= 16
        
    }
    return true
    }
 
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
}
