//
//  PieChartViewController.swift
//  purZ
//
//  Created by Vertace on 13/11/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class PieChartViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var colorLbl1: UILabel!
    @IBOutlet weak var DescribtionLbl1: UILabel!
    @IBOutlet weak var colorLbl2: UILabel!
    @IBOutlet weak var Describtionlbl2: UILabel!
    @IBOutlet weak var colorLbl3: UILabel!
    @IBOutlet weak var DescribtionLbl3: UILabel!
    @IBOutlet weak var colorLbl4: UILabel!
    @IBOutlet weak var DescribtionLbl4: UILabel!
    @IBOutlet weak var nodataLbl: UILabel!
   



    
    internal let PNLightGreen = UIColor(red: 186.0/255.0, green: 255.0/255.0, blue: 122.0/255.0, alpha: 1.0)
    internal let PNFreshBlue = UIColor(red: 125.0/255.0, green: 231.0/255.0, blue: 254.0/255.0, alpha: 1.0)
    internal let PNDeepYellow = UIColor(red: 246.0 / 255.0 , green: 201.0 / 255.0, blue: 122.0 / 255.0, alpha: 1.0)
    internal let PNFreshYellow = UIColor(red: 253.0/255.0, green: 249.0/255.0, blue: 121.0/255.0, alpha: 1.0)
    
    internal let PNGrey = UIColor(red: 186.0/255.0 , green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
    internal let PNLightGrey = UIColor(red: 246.0 / 255.0 , green: 246.0 / 255.0, blue: 246.0 / 255.0, alpha: 1.0)
    internal let PNDeepGrey = UIColor(red: 99.0/255.0, green: 99.0/255.0, blue: 99.0/255.0, alpha: 1.0)
    
    internal let PNLightBlue = UIColor(red: 94.0/255.0, green: 147.0/255.0, blue: 246.0/255.0, alpha: 1.0)
    internal let PNBule = UIColor(red: 82.0/255.0, green: 116.0/255.0, blue: 188.0/255.0, alpha: 1.0)
    internal let PNDarkBlue = UIColor(red: 121.0/255.0, green: 134.0/255.0, blue: 142.0/255.0, alpha: 1.0)
    internal let PNTwitterBlue = UIColor(red: 0.0/255.0, green: 171.0/255.0, blue: 243.0/255.0, alpha: 1.0)
    
    internal let PNRed = UIColor(red: 245.0/255.0, green: 94.0/255.0, blue: 78.0/255.0, alpha: 1.0)
    
    
    @IBOutlet weak var activityIndicater: UIActivityIndicatorView!
    @IBOutlet weak var endDateTxtField: UITextField!
    @IBOutlet weak var startDateTxtField: UITextField!
    var datePickerView:UIDatePicker = UIDatePicker()
    var datePickerView1:UIDatePicker = UIDatePicker()
    var date = ""
    var calendar : NSCalendar = NSCalendar.currentCalendar()
    var currentDateTime = NSDate()
    var df = NSDateFormatter()
    var toolbarView = UIView()
    var dateString = ""
    var from_select=false
    var to_select = false
    var date1 = ""
    var calendar1 : NSCalendar = NSCalendar.currentCalendar()
    var currentDateTime1 = NSDate()
    var df1 = NSDateFormatter()
    var toolbarView1 = UIView()
    var dateString1 = ""
    var amount = [String]()
    var payer = [String]()
    var payee = [String]()
    var rrn = [String]()
    var status = [String]()
    var type = [String]()
    var transDate = [String]()
    var txnType = [String]()
    var serializedjson = ""
    var loopcount = 0
    var TPP_Amount: Float = 0.0
    var Load_Amount: Float = 0.0
    var C2C_Amount: Float = 0.0
    var C2M_Amount: Float = 0.0
//    var pieChart: PNPieChart
    var pnchartview = PNPieChart(frame: CGRect(x: 40, y: 155, width: 0, height: 0), items: [PNPieChartDataItem(dateValue: CGFloat(0), dateColor:  UIColor.whiteColor(), description: "")])
  var bottomLine = CALayer()
    var bottomLine1 = CALayer()
//    bottomLine.frame = CGRectMake(0.0, startDateTxtField.frame.size.height - 2 , startDateTxtField.frame.size.width, 2.0)
//    bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                startDateTxtField.borderStyle = UITextBorderStyle.None
//    startDateTxtField.layer.addSublayer(bottomLine)

    
     var viewDatePicker:UIView!
    
        override func viewDidLoad() {
            super.viewDidLoad()
         nodataLbl.hidden = false
        // Do any additional setup after loading the view.
            initialize()
    }
    
    func initialize() {
         navigationController?.navigationBar.hidden = true
        nodataLbl.hidden = false
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        startDateTxtField.borderStyle = UITextBorderStyle.None
        bottomLine.frame = CGRectMake(0.0, startDateTxtField.frame.size.height - 1 , startDateTxtField.frame.size.width, 1.0)
        startDateTxtField.layer.addSublayer(bottomLine)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        endDateTxtField.borderStyle = UITextBorderStyle.None
        bottomLine1.frame = CGRectMake(0.0, endDateTxtField.frame.size.height - 1 , endDateTxtField.frame.size.width, 1.0)
        endDateTxtField.layer.addSublayer(bottomLine1)
   
        activityIndicater.hidesWhenStopped = true
        startDateTxtField.userInteractionEnabled = false
        endDateTxtField.userInteractionEnabled = false
        createDatePickerViewWithAlertController()
    }

    
    private func setPieChart() -> PNPieChart {
nodataLbl.hidden = true
        print("ChartAmout")
        print(CGFloat(TPP_Amount), CGFloat(C2M_Amount),CGFloat(C2C_Amount),CGFloat(Load_Amount))
        let item1 = PNPieChartDataItem(dateValue: CGFloat(TPP_Amount), dateColor:  PNLightGreen, description: "")
        let item2 = PNPieChartDataItem(dateValue: CGFloat(C2M_Amount), dateColor: PNFreshBlue, description: "")
        let item3 = PNPieChartDataItem(dateValue: CGFloat(C2C_Amount), dateColor: PNDeepYellow , description: "")
        let item4 = PNPieChartDataItem(dateValue: CGFloat(Load_Amount), dateColor: PNFreshYellow, description: "")
        print(self.startDateTxtField.frame.origin.y)
        print(self.startDateTxtField.frame.size.height)
        
        
        let frame = CGRect(x: 40, y: self.startDateTxtField.frame.origin.y+self.startDateTxtField.frame.size.height+20, width: self.view.frame.size.width-80, height: self.view.frame.size.width-80)
        
        var items = [PNPieChartDataItem]()
        var itemsDescribtion = [String]()
        
        for(var i = 0 ; i<=3 ; i++) {
            if i == 0 && CGFloat(TPP_Amount) > 0.0{
                items += [item1]
                itemsDescribtion += ["Recharge"]
            }
            else if i == 1 && CGFloat(C2M_Amount) > 0.0 {
                items += [item2]
                itemsDescribtion += ["Pay Merchant"]
                
            }
            else if i == 2 && CGFloat(C2C_Amount) > 0.0 {
                items += [item3]
                itemsDescribtion += ["Send Money"]
            }
            else if i == 3 && CGFloat(Load_Amount) > 0.0 {
                print(Load_Amount)
                items += [item4]
                itemsDescribtion += ["Load Money"]
                
            }
            print("checkinggggg\(itemsDescribtion)")
            
        }
        print("Itemsssss")
        print(itemsDescribtion)
        
        colorLbl1.backgroundColor = UIColor.clearColor()
        DescribtionLbl1.text = ""
        colorLbl2.backgroundColor = UIColor.clearColor()
        Describtionlbl2.text = ""
        colorLbl3.backgroundColor = UIColor.clearColor()
        DescribtionLbl3.text = ""
        colorLbl4.backgroundColor = UIColor.clearColor()
        DescribtionLbl4.text = ""
        
        for (var j = 0; j<itemsDescribtion.count;j++)
        {
            if itemsDescribtion[j] == "Recharge" {
                
                colorLbl1.backgroundColor = PNLightGreen
                DescribtionLbl1.text = "Recharge"
            }
            else if itemsDescribtion[j] == "Pay Merchant" {
                if j == 0 {
                    colorLbl1.backgroundColor = PNFreshBlue
                    DescribtionLbl1.text = "Pay Merchant"
                }
                else if j == 1 {
                    colorLbl2.backgroundColor = PNFreshBlue
                    Describtionlbl2.text = "Pay Merchant"
                }
            }
            else if itemsDescribtion[j] == "Send Money" {
                if j == 0 {
                    colorLbl1.backgroundColor = PNDeepYellow
                    DescribtionLbl1.text = "Send Money"
                }
                else if j == 1 {
                    colorLbl2.backgroundColor = PNDeepYellow
                    Describtionlbl2.text = "Send Money"
                }
                else if j == 2 {
                    colorLbl3.backgroundColor = PNDeepYellow
                    DescribtionLbl3.text = "Send Money"
                }
            }
            else if itemsDescribtion[j] == "Load Money" {
                if j == 0 {
                    colorLbl1.backgroundColor = PNFreshYellow
                    DescribtionLbl1.text = "Load Money"
                }
                else if j == 1 {
                    colorLbl2.backgroundColor = PNFreshYellow
                    Describtionlbl2.text = "Load Money"
                }
                else if j == 2 {
                    colorLbl3.backgroundColor = PNFreshYellow
                    DescribtionLbl3.text = "Load Money"
                }
                else if j == 3 {
                    colorLbl4.backgroundColor = PNFreshYellow
                    DescribtionLbl4.text = "Load Money"
                }
            }
        }
        
        let pieChart = PNPieChart(frame: frame, items: items)
        print(pieChart.frame.origin.y)
        print(pieChart.frame.size.height)
        pieChart.descriptionTextColor = UIColor.blackColor()
        pieChart.descriptionTextFont = UIFont(name: "Avenir-Medium", size: 12)!
        pieChart.center = self.view.center
        self.activityIndicater.stopAnimating()
        return pieChart
    }

   
    @IBAction func startTxtFieldAction(sender: AnyObject) {
        from_select = true
        to_select = false
       viewDatePicker.hidden = false
        datePickerView.hidden = false
        
        bottomLine.frame = CGRectMake(0.0, startDateTxtField.frame.size.height - 2 , startDateTxtField.frame.size.width, 2.0)
        bottomLine1.frame = CGRectMake(0.0, endDateTxtField.frame.size.height - 1 , endDateTxtField.frame.size.width, 1.0)

        
        startDateTxtField.layer.addSublayer(bottomLine)
        endDateTxtField.layer.addSublayer(bottomLine1)
      
        
    }
    @IBAction func endTxtFieldAction(sender: AnyObject) {
        to_select = true
        from_select = false
        viewDatePicker.hidden = false
        datePickerView.hidden = false
       bottomLine1.frame = CGRectMake(0.0, endDateTxtField.frame.size.height - 2 , endDateTxtField.frame.size.width, 2.0)
         bottomLine.frame = CGRectMake(0.0, startDateTxtField.frame.size.height - 1 , startDateTxtField.frame.size.width, 1.0)
        startDateTxtField.layer.addSublayer(bottomLine)
        endDateTxtField.layer.addSublayer(bottomLine1)

       
    }

   
    func createDatePickerViewWithAlertController()
    {
//        nodataLbl.hidden = true
        df.dateFormat = "dd/MM/yyyy"
        self.viewDatePicker = UIView(frame: CGRectMake(0, self.view.frame.size.height - 288, self.view.frame.size.width, 288))
        viewDatePicker.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(viewDatePicker)
        
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.maximumDate = NSDate()
        
        var pickerSize : CGSize = datePickerView.sizeThatFits(CGSizeZero)
        datePickerView.frame = CGRectMake(0, self.view.frame.size.height - 244, self.view.frame.size.width, 244)
        datePickerView.backgroundColor = UIColor.clearColor()
        print(datePickerView.frame.origin.y)
        print(datePickerView.frame.size.height)
        self.view.addSubview(datePickerView)
        startDateTxtField.inputView = .None
        endDateTxtField.inputView = .None
        let toolbarView: UIView = UIView(frame: CGRectMake(0,0,self.view.frame.size.width,44))
        toolbarView.backgroundColor = UIColor.grayColor()
        //        let toolBar = UIToolbar(frame: CGRectMake(0,0,self.view.frame.size.width,44))
        let doneButton = UIButton.init(frame: CGRectMake(self.view.frame.size.width - 60,5,50,34))
        doneButton.addTarget(self, action: #selector(PieChartViewController.doneClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        
        let cancelButton = UIButton.init(frame: CGRectMake(10,5,70,34))
        cancelButton.addTarget(self, action: #selector(PieChartViewController.cancelClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cancelButton.setTitle("Cancel", forState: UIControlState.Normal)
        datePickerView.addTarget(self, action: #selector(PieChartViewController.datePickerValueChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
         dateString = df.stringFromDate(currentDateTime)
        print("Date"+dateString)
       
        toolbarView.addSubview(doneButton)
        toolbarView.addSubview(cancelButton)
        
        print(toolbarView.frame.origin.y)
        self.viewDatePicker.addSubview(toolbarView)
        viewDatePicker.hidden = true
        datePickerView.hidden = true
    }
    func doneClicked(sender: UIButton)
    {
        bottomLine.frame = CGRectMake(0.0, startDateTxtField.frame.size.height - 1 , startDateTxtField.frame.size.width, 1.0)
        startDateTxtField.layer.addSublayer(bottomLine)
        bottomLine1.frame = CGRectMake(0.0, endDateTxtField.frame.size.height - 1 , endDateTxtField.frame.size.width, 1.0)
        endDateTxtField.layer.addSublayer(bottomLine1)

        
        
        dispatch_async(dispatch_get_main_queue()) {
         self.viewDatePicker.hidden = true
         self.datePickerView.hidden = true
           if  self.from_select && self.datevalueChange != ""
        {
            self.startDateTxtField.text = self.datevalueChange
            self.from_select = false
        
            
        }
        else if self.to_select && self.datevalueChange != ""
        {
            self.viewDatePicker.hidden = true
            self.datePickerView.hidden = true
            self.endDateTxtField.text = self.datevalueChange
            self.to_select = false
           

        }
       if self.startDateTxtField.text == "" && self.from_select
       {
        self.from_select = false
        self.startDateTxtField.text = self.dateString
              }
        else if self.endDateTxtField.text == "" && self.to_select
       {
        self.to_select = false
        self.endDateTxtField.text = self.dateString
              }
        
        if self.startDateTxtField.text != "" && self.endDateTxtField.text != ""
        {

            self.TPP_Amount = 0.0
            self.C2C_Amount = 0.0
            self.C2M_Amount = 0.0
            self.Load_Amount = 0.0
            var i = 0;
         
            var transactionType = ["TPP","C2C","C2M","LOAD"]
                self.loopcount = 0
                self.transaction(transactionType[i])
                
                print("transaction"+transactionType[i])
            self.activityIndicater.startAnimating()
            
        }
        }
        
        
    }
    
    func cancelClicked(sender: UIButton)
    {
        print("cancelClicked")
        viewDatePicker.hidden = true
        datePickerView.hidden = true
        
        //        self.dateSelected()
    }
    var datevalueChange = ""
    func datePickerValueChanged(sender:UIDatePicker) {
        print("Sender\(sender)")
        let currentdate = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: currentdate)
        
        let year =  components.year
        
        
        print("Date\(sender.date)")
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        dispatch_async(dispatch_get_main_queue()) {
//            
//           if self.from_select
//           {
            
           self.datevalueChange = dateFormatter.stringFromDate(sender.date)
//            print("StartDate")
//            print(self.startDateTxtField.text)
//            self.date = dateFormatter.stringFromDate(sender.date)
//            }
//            else if self.to_select
//           {
//           
//            self.endDateTxtField.text = dateFormatter.stringFromDate(sender.date)
//            print("StartDate")
//            print(self.endDateTxtField.text)
//            self.date = dateFormatter.stringFromDate(sender.date)
//            }
            
            
        }
        
    }
   func transaction(txnType : String)
    {
         nodataLbl.hidden = true
     dispatch_async(dispatch_get_main_queue()) {
        
        var transactionDF = NSDateFormatter()
       transactionDF.dateFormat = "yyyy-MM-dd"
        let startDate = self.df.dateFromString(self.startDateTxtField.text!)
        print(self.startDateTxtField.text!)
        let endDate = self.df.dateFromString(self.endDateTxtField.text!)
        
        let startDateString = transactionDF.stringFromDate(startDate!)
        let endDateString = transactionDF.stringFromDate(endDate!)
       
        let PieChartViewmodel = PieChart.init(fromDate: startDateString , toDate: endDateString, txnType: txnType)!
        
        self.serializedjson  = JSONSerializer.toJson(PieChartViewmodel)
        
        self.transactionHistory(Appconstant.BASE_URL+Appconstant.TRANSACTIONHISTORY_PIECHART+"?pageSize=5000" ,values: self.serializedjson)
        }
        
    }
    
    func transactionHistory(url : String, values: String)
    {
        print(url)
        print(values)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
      
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicater.stopAnimating()
                }
                print(error)
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        self.activityIndicater.stopAnimating()
                    }
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicater.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                
//                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    self.activityIndicater.stopAnimating()
                
            }
            dispatch_async(dispatch_get_main_queue()) {
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseStringTransaction = \(responseString)")
                let json = JSON(data: data!)
            
                
                               if json["result"].isEmpty{
                    let item1 = json["exception"]
                                   }
                
                    for item in json["result"].arrayValue
                    {
                                                let item1 = item["transaction"]
                      if item1["transactionType"].stringValue == "TPP"
                        {
                        
                   self.TPP_Amount = item1["amount"].floatValue+self.TPP_Amount
                            
                            
                    
                            
                        }
                      else if item1["transactionType"].stringValue == "C2M"
                        {
                            
                            self.C2M_Amount = item1["amount"].floatValue+self.C2M_Amount
                            
                        }
                        
                      else if item1["transactionType"].stringValue == "C2C"
                        {
                            
                            self.C2C_Amount = item1["amount"].floatValue+self.C2C_Amount
                            
                            
                          
                            
                        }
                       else if item1["transactionType"].stringValue == "Load"
                        {
                            
                            self.Load_Amount = item1["amount"].floatValue+self.Load_Amount
                            
                            
                            
                            
                        }
//                         print("TPP_Amount\(self.TPP_Amount)")
//                        print("C2C_Amount\(self.C2C_Amount)")
//                        print("Load_Amount\(self.Load_Amount)")
//                        print("C2M_Amount\(self.C2M_Amount)")
//                        
                    
                  
                    }
                    var transactionType = ["TPP","C2C","C2M","LOAD"]
            
                    self.loopcount++
            print("***********")
            print(self.loopcount)
                    if self.loopcount <= 3 {
                        
                     self.transaction(transactionType[self.loopcount])
                    }
                    if self.loopcount == 4
                    {
//                         self.viewDatePicker.removeFromSuperview()
//                        self.viewDatePicker.removeFromSuperview()
                        self.viewDatePicker.hidden = true
                        self.datePickerView.hidden = true
                        if self.TPP_Amount == 0.0 && self.C2C_Amount == 0.0 && self.Load_Amount == 0.0 && self.C2M_Amount==0.0
                        {
                            dispatch_async(dispatch_get_main_queue()) {
                                self.colorLbl1.backgroundColor = UIColor.clearColor()
                                self.DescribtionLbl1.text = ""
                                self.colorLbl2.backgroundColor = UIColor.clearColor()
                                self.Describtionlbl2.text = ""
                                self.colorLbl3.backgroundColor = UIColor.clearColor()
                                self.DescribtionLbl3.text = ""
                                self.colorLbl4.backgroundColor = UIColor.clearColor()
                                self.DescribtionLbl4.text = ""
                                self.view.viewWithTag(1)?.removeFromSuperview()
                                
                                self.presentViewController(Alert().alert("No Transactions found!", message: ""),animated: true,completion: nil)
                                self.startDateTxtField.text = ""
                                self.endDateTxtField.text = ""
                                                 self.nodataLbl.hidden = false
                                
                            }
                            
                        }
                        else {
                            self.view.viewWithTag(1)?.removeFromSuperview()
                            let pieChart = self.setPieChart()
                            dispatch_async(dispatch_get_main_queue()) {
                                self.activityIndicater.stopAnimating()
                            }
                            pieChart.tag = 1
                            self.view.addSubview(pieChart);
                            self.view.addSubview(self.viewDatePicker)
                            self.view.addSubview(self.datePickerView)
                        }
                    }
              
        }
        
        }
        task.resume()
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
