//
//  TransactionDetailViewController.swift
//  purZ
//
//  Created by Vertace on 23/11/17.
//  Copyright © 2017 Vertace. All rights reserved.
//



import UIKit

class TransactionDetailViewController: UIViewController, UITabBarControllerDelegate {
    
    @IBOutlet weak var remitter_lbl: UILabel!
    @IBOutlet weak var beneficiary_lbl: UILabel!
    @IBOutlet weak var amount_lbl: UILabel!
    @IBOutlet weak var date_lbl: UILabel!
    @IBOutlet weak var transactionType_lbl: UILabel!
    @IBOutlet weak var transactionID_lbl: UILabel!
    @IBOutlet weak var remarks_lbl: UILabel!
    @IBOutlet weak var status_lbl: UILabel!
    
    @IBOutlet weak var complaintView: UIView!
    @IBOutlet weak var complaintRefNo_lbl: UILabel!
    @IBOutlet weak var complaintStatus_lbl: UILabel!
    
    @IBOutlet weak var raiseQuery_btn: UIButton!
    
    var remitter = ""
    var beneficiary = ""
    var amount = ""
    var date = ""
    var transactionType = ""
    var transactionID = ""
    var remarks = ""
    var status = ""
    var complaintRefNo = ""
    var complaintStatus = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        initialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initialize() {
        tabBarController?.delegate = self
        complaintView.hidden = true
        remitter_lbl.text = remitter
        beneficiary_lbl.text = beneficiary
        amount_lbl.text = amount
        date_lbl.text = date
        transactionType_lbl.text = transactionType
        transactionID_lbl.text = transactionID
        remarks_lbl.text = remarks
        status_lbl.text = status
        if status == "REJECT" {
            status_lbl.textColor = UIColor.redColor()
        }
        //TransDetail_to_Complaint
        ApiClient().getComplaintStatus(transactionID) { (success, error, response) in
            if success {
                
                self.raiseQuery_btn.userInteractionEnabled = false
                self.complaintRefNo_lbl.text = String(response.valueForKey("complaintRefNo")!)
                self.complaintStatus_lbl.text = String(response.valueForKey("complaintStatus")!)
                if self.complaintRefNo_lbl.text! != "" {
                    self.complaintView.hidden = false
                }
            }
        }
    }
    @IBAction func backBtnAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "TransDetail_to_Complaint" {
            let nextview = segue.destinationViewController as! ComplaintViewController
            nextview.amount = self.amount
            nextview.remitter = self.remitter
            nextview.beneficiary = self.beneficiary
            nextview.date = self.date
            nextview.transactionType = self.transactionType
            nextview.transactionID = self.transactionID
            nextview.remarks = self.remarks
            nextview.status = self.status
        }
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    

}
