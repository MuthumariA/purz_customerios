//
//  CardDetailViewController.swift
//  purZ
//
//  Created by Vertace on 31/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class CardDetailViewController: UIViewController, UITextFieldDelegate, UITabBarControllerDelegate {
    
    @IBOutlet var accountNoTxtField: UITextField!
    @IBOutlet var last6digitTxtField: UITextField!
    @IBOutlet var monthtextfield: UITextField!
    @IBOutlet var yeartextfield: UITextField!
    @IBOutlet var accountView: UIView!
    @IBOutlet var lastdigitView: UIView!
    @IBOutlet var dateView: UIView!
    
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var okBtn: UIButton!
   
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    var characterCountLimit = 6
    var fromaccounts = false
    var accountdetail = ""
    var handelvpa = ""
    var fromlinkVPA = false
    var fromcreateVPA = false
    var fromAccountProvider = false
    var fromAvailableAccounts = false
    var fromselectaccount = false
    var fromListVPA = false
    var toastmsg = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.delegate = self
        cancelBtn.layer.cornerRadius = 11
        cancelBtn.layer.borderColor = UIColor.redColor().CGColor
        cancelBtn.layer.borderWidth = 2
        okBtn.layer.borderWidth = 2
        okBtn.layer.cornerRadius = 11
        okBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor

        // Do any additional setup after loading the view.
        
        initial()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: #selector(CardDetailViewController.StopAnimation), userInfo: nil, repeats: false)
    }
    
    func StopAnimation() {
        dispatch_async(dispatch_get_main_queue()) {
            self.activityIndicator.stopAnimating()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initial(){
        activityIndicator.hidesWhenStopped = true
        last6digitTxtField.delegate = self
        monthtextfield.delegate = self
        yeartextfield.delegate = self
        monthtextfield.keyboardType = .NumberPad
        yeartextfield.keyboardType = .NumberPad
        accountNoTxtField.text = AccountConstant.accountno
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CardDetailViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        accountNoTxtField.userInteractionEnabled = false

    }
    
    func dismissKeyboard(){
       self.view.endEditing(true)
    }
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textFieldToChange == last6digitTxtField {
            characterCountLimit = 6
            if last6digitTxtField.text?.characters.count == 6 && string != ""{
                monthtextfield.text = string
                monthtextfield.becomeFirstResponder()
            }
        }
        else{
            characterCountLimit = 2
        }

        if textFieldToChange == monthtextfield {
            if monthtextfield.text?.characters.count == 2 && string != "" {
                yeartextfield.text = string
                yeartextfield.becomeFirstResponder()
            }
        }
        
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func convertStringToDictionary(text: String) -> [NSObject:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [NSObject:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    
    @IBAction func okBtnAction(sender: AnyObject) {
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: date)
        
        let year =  components.year
        
        let twodigit = String(year).substringWithRange(String(year).startIndex.advancedBy(0)..<String(year).startIndex.advancedBy(2))
        let enteredyear = twodigit + yeartextfield.text!
        
        if last6digitTxtField.text! == "" {
          self.presentViewController(Alert().alert("Card number can't be empty",message: ""), animated: true, completion: nil)
        }
        else if last6digitTxtField.text?.characters.count != 6 {
            self.presentViewController(Alert().alert("Invalid card number",message: ""), animated: true, completion: nil)
        }
        else if monthtextfield.text! == "" {
            self.presentViewController(Alert().alert("Month can't be empty",message: ""), animated: true, completion: nil)
        }
        else if monthtextfield.text?.characters.count != 2 || Int(monthtextfield.text!) > 12 || Int(monthtextfield.text!) < 1{
            self.presentViewController(Alert().alert("Invalid month",message: ""), animated: true, completion: nil)
        }
        else if yeartextfield.text! == "" {
            self.presentViewController(Alert().alert("Year can't be empty",message: ""), animated: true, completion: nil)
        }
        else if yeartextfield.text?.characters.count != 2 || Int(enteredyear) < year{
            self.presentViewController(Alert().alert("Invalid year",message: ""), animated: true, completion: nil)
        }
        else {
        let customeraccount = self.convertStringToDictionary(AccountConstant.accountdetail)! as NSDictionary
        
        
        self.activityIndicator.startAnimating()
        let CustomerAccountValue = CustomerAccount.init(accountDetails: customeraccount as! [NSObject : AnyObject])
        
            let carddata = CardData.init(self.last6digitTxtField.text!, expiry_month: self.monthtextfield.text!, expiry_year: self.yeartextfield.text!)
            
            if fromaccounts {
                let account = self.convertStringToDictionary(accountdetail)! as NSDictionary
              
                let VirtualAddr_detail = VirtualAddress.init(account as! [NSObject : AnyObject], handle: handelvpa)
                 ApiClient().resetMPIN(VirtualAddr_detail, custAccount: CustomerAccountValue, carddata: carddata, view: self, { (success, error, response) in
                    dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                    }
                    AccountConstant.vpanames.removeAll()
                    AccountConstant.vparesponses.removeAll()
                    AccountConstant.vpaProviderName.removeAll()
                    if success && response != nil {
                        self.toastmsg = "UPI PIN resetted successfully!"
                        self.performSegueWithIdentifier("back_to_accounts", sender: self)
//                        let response_success = response.valueForKey("success")
//                        print(response_success)
//                        if String(response_success!) == "1" {
//                            self.activityIndicator.stopAnimating()
//                            var alertController:UIAlertController?
//                            alertController?.view.tintColor = UIColor.blackColor()
//                            alertController = UIAlertController(title: "Password resetted successfully!",message: "",preferredStyle: .Alert)
//                            let action1 = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//                                self!.performSegueWithIdentifier("back_to_accounts", sender: self)
//                                })
//                            alertController?.addAction(action1)
//                            self.presentViewController(alertController!, animated: true, completion: nil)
//                            
//                        }
                    }
                    else if response != nil{
                        self.toastmsg = String(response.valueForKey("message")!)
                        if self.toastmsg == "" {
                        self.toastmsg = "UPI PIN failed to reset"
                        }
                        self.performSegueWithIdentifier("back_to_accounts", sender: self)
                        
                    }
                 })
            }
            
            else {
        
        do{
            
            let accountdefault = AccountDefaultStatus.init(defaultDebitCredit: AccountConstant.defaultdebit, default_Credit: AccountConstant.defaultcredit)
            
            try ApiClient().mapAccount(carddata, vpa: AccountConstant.VPA, custAccount: CustomerAccountValue, accountDefaultStatus: accountdefault, view: self, withCompletion: { (success, error, response) in
                dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                }
                if success{
                   
                    let response_success = response.valueForKey("success")
                    
                    if String(response_success!) == "1" {
                        var alertmsg = "VPA created successfully!"
                        if self.fromlinkVPA {
                            alertmsg = "Account added successfully!"
                        }
                        AccountConstant.vpanames.removeAll()
                        AccountConstant.vparesponses.removeAll()
                        AccountConstant.vpaProviderName.removeAll()
                        var alertController:UIAlertController?
                        alertController?.view.tintColor = UIColor.blackColor()
                        alertController = UIAlertController(title: alertmsg,message: "",preferredStyle: .Alert)
                        let action1 = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                            if self!.fromlinkVPA {
                                self!.performSegueWithIdentifier("card_to_linkvpa", sender: self)
                            }
                            else {
                                self!.performSegueWithIdentifier("back_to_createvpa", sender: self)
                            }
                            
                        })
                         alertController?.addAction(action1)
                         self.presentViewController(alertController!, animated: true, completion: nil)
                      
                    }
                    else {
                        if self.fromlinkVPA {
                            self.presentViewController(Alert().alert("Account failed to add",message: ""), animated: true, completion: nil)
                        }
                        else {
                            self.presentViewController(Alert().alert("VPA failed to create",message: ""), animated: true, completion: nil)
                        }
                    }
                }
                else{
                    if response != nil {
                        let message = String(response.valueForKey("message")!)
                        if message != "" {
                            self.presentViewController(Alert().alert(message,message: ""), animated: true, completion: nil)
                        }
                        else {
                            self.presentViewController(Alert().alert("VPA failed to create",message: ""), animated: true, completion: nil)
                        }
                       
                    }
                }
            })
        }
        catch{
            print("ERROR")
        }
       }
      }
        
    }
    
    
    @IBAction func backBtnAction(sender: AnyObject) {
        if fromaccounts {
            self.toastmsg = ""
            self.performSegueWithIdentifier("back_to_accounts", sender: self)
        }
        else if fromlinkVPA {
            self.performSegueWithIdentifier("card_to_linkvpa", sender: self)
        }
        else {
            self.performSegueWithIdentifier("back_to_createvpa", sender: self)
        }
    }
    
    
    @IBAction func cancelBtnAction(sender: AnyObject) {
        self.performSegueWithIdentifier("back_to_createvpa", sender: self)
    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "back_to_createvpa") {
            let nextview = segue.destinationViewController as! CreateVPAViewController
            nextview.fromListVPA = fromListVPA
            nextview.fromselectaccount = fromselectaccount
            nextview.fromAccountProvider = fromAccountProvider
            nextview.fromAvailableAccounts = fromAvailableAccounts
        }
        else if(segue.identifier == "back_to_accounts") {
            let nextview = segue.destinationViewController as! AccountsViewController
            nextview.toastmessage = self.toastmsg
        }
    }
}
