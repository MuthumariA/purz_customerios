


//
//  CropImageViewController.swift
//  purZ
//
//  Created by Vertace on 05/05/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class CropImageViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, ImageCropViewControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    @IBOutlet weak var BackgroungImg: UIImageView!
    
    @IBOutlet weak var chooselbl: UILabel!
    var imageCropView = ImageCropView()
    var image : UIImage? = UIImage()
    let imagePickerController = UIImagePickerController()
    
    var strBase64 = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.imageCropView.image = UIImage(named: "2.jpg")
        
        if Appconstant.cropimage == true
        {
            Appconstant.cropimage = false
                        self.imageCropView.controlColor = UIColor.cyanColor()
                        
                        self.imagePickerController.delegate = self
                        self.presentViewController(self.imagePickerController, animated: true, completion: nil)
        }
      else if Appconstant.takephoto == true
        {
            Appconstant.takephoto = false
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
                imagePickerController.delegate = self
                imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera;
                imagePickerController.allowsEditing = true
                self.presentViewController(imagePickerController, animated: true, completion: nil)
                
            }        }
        
    }
    
    @IBAction func takephotoAction(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePickerController.delegate = self
            imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera;
            imagePickerController.allowsEditing = true
            self.presentViewController(imagePickerController, animated: true, completion: nil)
            
        }
    }
    @IBAction func openBarButtonClick(sender: AnyObject) {
        
        self.imageCropView.controlColor = UIColor.cyanColor()
        
        self.imagePickerController.delegate = self
        self.presentViewController(self.imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])  {
        
        self.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imageView.image = self.image
        self.imageView.contentMode = UIViewContentMode.ScaleAspectFit
        self.imageView.clipsToBounds = true
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cropBarButtonClick(sender: AnyObject) {
        //        self.dismissViewControllerAnimated(true, completion: nil)
        if self.image != nil{
            
            var controller = ImageCropViewController1(image: self.image)
            controller.delegate = self
            controller.blurredBackground = true
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    @IBAction func cancelBtnAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func ImageCropViewController(controller: UIViewController!, didFinishCroppingImage croppedImage: UIImage!) {
        
        self.image = croppedImage
        self.imageView.image = self.image
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func ImageCropViewControllerDidCancel(controller: UIViewController!) {
        
        self.imageView.image = self.image
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func thisImage(image : UIImage, hasBeenSavedInPhotoAlbumWithError error : NSError?, usingContextInfo ctxInfo : ()){
        if let actualError = error {
            
            var alert = UIAlertController(title: "Fail!", message: "Saved with error \(actualError.description).", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            var alert = UIAlertController(title: "Succes!", message: "Saved to camera roll.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
    }
    
    
    @IBAction func saveBarButtonClick(sender: AnyObject) {
        
        if self.imageView.image != nil{
            
            UIImageWriteToSavedPhotosAlbum(self.image!, self, Selector("image:didFinishSavingWithError:contextInfo:"), nil)
        }
        else
        {
            UIAlertView(title: "Please select the photo from gallery ", message: "", delegate: nil, cancelButtonTitle: "Ok").show()
        }
    }
    
    
    
    
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo: UnsafePointer<()>) {
        if self.imageView.image != nil
        {
            
            self.imageView.image = image
            self.imageView.contentMode = UIViewContentMode.ScaleAspectFill
            self.imageView.clipsToBounds = true
            
            
            let image : UIImage = image
            //            let imageData:NSData = UIImageJPEGRepresentation(image,1)!
            if self.imageView.image?.imageOrientation == UIImageOrientation.Up {
                
            }
            else{
                UIGraphicsBeginImageContextWithOptions((self.imageView.image?.size)!, false, (self.imageView.image?.scale)!)
                self.imageView.image?.drawInRect(CGRectMake(0, 0, (self.imageView.image?.size.width)!, (self.imageView.image?.size.height)!))
                self.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
            }
            let imageData:NSData =  UIImagePNGRepresentation(self.imageView.image!)!
            self.strBase64 = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
            
            
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            if purzDB.open() {
                let selectSQL = "SELECT * FROM IMAGETABLE WHERE CUSTOMER_ID=" + Appconstant.customerid
                
                let results:FMResultSet! = purzDB.executeQuery(selectSQL,
                                                               withArgumentsInArray: nil)
                
                
                
                if (results.next()){
                    
                    let insertSQL = "UPDATE IMAGETABLE SET IMAGE_PATH = '"+self.strBase64+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                    
                    let result = purzDB.executeUpdate(insertSQL,
                                                      withArgumentsInArray: nil)
                    if !result {
                        //   status.text = "Failed to add contact"
                        print("Error: \(purzDB.lastErrorMessage())")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                }
                    
                    
                    
                else{
                    let insertsql = "INSERT INTO IMAGETABLE (CUSTOMER_ID,IMAGE_PATH) VALUES ('\(Appconstant.customerid)','"+self.strBase64+"')"
                    let result = purzDB.executeUpdate(insertsql,
                                                      withArgumentsInArray: nil)
                    if !result {
                        //   status.text = "Failed to add contact"
                        print("Error: \(purzDB.lastErrorMessage())")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                    
                }
            }
            purzDB.close()
            
            UIAlertView(title: "Success", message: "This image has been saved to your Camera Roll successfully", delegate: nil, cancelButtonTitle: "Close").show()
            
            Appconstant.fromcropimage = true
            self.dismissViewControllerAnimated(true, completion: nil)
            
        }
        //    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //        if(segue.identifier == "image_profile") {
        //            let nextview = segue.destinationViewController as! ProfileSettingsViewController
        //            nextview.cropimage = image
        //                        nextview.fromcropimage = true
        //           
        //        }}
        
    }
}




