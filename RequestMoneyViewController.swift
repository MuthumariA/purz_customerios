//
//  RequestMoneyViewController.swift
//  purZ
//
//  Created by Vertace on 25/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class RequestMoneyViewController: UIViewController,UITabBarControllerDelegate,UITextFieldDelegate {

    @IBOutlet weak var VPAListTxtField: UITextField!
    @IBOutlet weak var remarkTxtField: UITextField!
    @IBOutlet weak var amountTxtField: UITextField!
    @IBOutlet weak var payerVPATxtField: UITextField!
    @IBOutlet weak var validDateTxtField: UITextField!
    
    @IBOutlet weak var refreshbtn: UIButton!
    @IBOutlet weak var sendBtn: UIButton!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    var request_Contact_VPA = ""
    var datePickerView:UIDatePicker!
    var date = ""
    var calendar : NSCalendar = NSCalendar.currentCalendar()
    var currentDate = NSDate()
    var df = NSDateFormatter()
    var toolbarView = UIView()
    var dateString = ""
    var vpa = [String]()
    var VPAResponse = [String]()
    var validDate = 7
    var customeraccount=NSDictionary()
    var status = ""
    var descriptiontxt = ""
    var characterCountLimit = 0
    var transactionID = ""
    let alertView: UIAlertView = UIAlertView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickContact()
        VPAList()
        initialFunc()
    }
    @IBAction func VPAListBtnAction(sender: AnyObject) {
        self.view.endEditing(true)
        toolbarView.hidden = true
        if vpa.count != 0
        {
        alertView.show()
        }
        
    }
    
    @IBAction func requestBtnAction(sender: AnyObject) {
        if payerVPATxtField.text == ""
        {
            self.presentViewController(Alert().alert("VPA can't be empty", message: ""), animated: true, completion: nil)
            
        }
        else if amountTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Amount can't be empty", message: ""), animated: true, completion: nil)
        }
        else if Double(amountTxtField.text!)! < 1
        {
            self.presentViewController(Alert().alert("Amount can't be zero",message: ""), animated: true, completion: nil)
        }
        else if VPAListTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Please select the VPA", message: ""), animated: true, completion: nil)
        }
        else
        {
          
            let virtualAddress = VirtualAddress.init(customeraccount as! [NSObject : AnyObject], handle: VPAListTxtField.text!)
            
            self.activityIndicator.startAnimating()
            
            ApiClient().collect(payerVPATxtField.text!, payee: virtualAddress, amount: NSDecimalNumber(string: amountTxtField.text!), remark: remarkTxtField.text!, expireAfter: Int32(validDate), view: self, { (success, Error, response) in
                
                if response != nil
                {
                    
                    let message = response.valueForKey("message")
                    UIAlertView(title: String(message!), message: "", delegate: nil, cancelButtonTitle: "Ok").show()
                    self.transactionID = String(response.valueForKey("BankRRN")!)
                    
                    let success1 = (response.valueForKey("success"))
                   
                    
                    if (String(success1!) == "1")
                    {
                        self.status = "Success"
                        self.descriptiontxt = String(message!)
                        self.performSegueWithIdentifier("To_CollectSuccess", sender: self)
                    }
                    else{
                        self.status = "Failed"
                        self.descriptiontxt = String(message!)
                        self.performSegueWithIdentifier("To_CollectSuccess", sender: self)
                    }
                    
                    
                }
                else {
                    self.status = "Failed"
                    self.descriptiontxt = "Transaction Failed"
                    self.performSegueWithIdentifier("To_CollectSuccess", sender: self)
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
            })
        }
        
    }
    
    @IBAction func refreshBtnAction(sender: AnyObject) {
        UIView.animateWithDuration(0.5, animations: {
            let angle: CGFloat? = CGFloat((self.refreshbtn.valueForKeyPath("layer.transform.rotation.z") as? NSNumber)!)
            let transform = CGAffineTransformMakeRotation(angle! + CGFloat(M_PI))
            self.refreshbtn.transform = transform
        })
        
        payerVPATxtField.text = ""
        amountTxtField.text = ""
        remarkTxtField.text = ""
        VPAListTxtField.text = ""
        

    }
   
    @IBAction func validDateTxtFieldAction(sender: AnyObject)
    {
        
        df.dateFormat = "dd.MM.yyyy"
        let datePickerView:UIDatePicker = UIDatePicker()
        //        datePickerView.maximumDate[NSDate, date]
        datePickerView.minimumDate = NSDate()
        let sevenDaysAfter = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value:7,
                                                                           toDate: NSDate(), options: NSCalendarOptions(rawValue: 0))
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.maximumDate = sevenDaysAfter!
        
        validDateTxtField.inputView = datePickerView
        
        var date = df.dateFromString(dateString)
        
        dateString = df.stringFromDate(currentDate)
        
        date = df.dateFromString(dateString)
        
        datePickerView.setDate(date!, animated: false)
        
        toolbarView = UIView(frame: CGRectMake(0,self.view.frame.size.height-datePickerView.frame.size.height-44+25,self.view.frame.size.width,44))
        toolbarView.backgroundColor = UIColor.lightGrayColor()
        
        let doneButton = UIButton.init(frame: CGRectMake(self.view.frame.size.width - 60,5,50,34))
        doneButton.addTarget(self, action: "Clicked", forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        NSTimer.scheduledTimerWithTimeInterval(0.2, target:self, selector: Selector("timerfunc"), userInfo: nil, repeats: false)
        toolbarView.addSubview(doneButton)
        
        
        datePickerView.addTarget(self, action: "datePickerValueChanged:", forControlEvents: UIControlEvents.ValueChanged)
        
        
    }

    func initialFunc()
    {
         payerVPATxtField.userInteractionEnabled = true
        validDateTxtField.delegate = self
        validDateTxtField.text = "Your collect request Valid for 7 days"
        tabBarController?.delegate = self
        sendBtn.layer.borderWidth = 2
        sendBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        sendBtn.layer.cornerRadius = sendBtn.frame.size.height/2
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, payerVPATxtField.frame.size.height - 1 , payerVPATxtField.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                payerVPATxtField.borderStyle = UITextBorderStyle.None
        payerVPATxtField.layer.addSublayer(bottomLine)
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, amountTxtField.frame.size.height - 1 , amountTxtField.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                amountTxtField.borderStyle = UITextBorderStyle.None
        amountTxtField.layer.addSublayer(bottomLine1)
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, remarkTxtField.frame.size.height - 1 , remarkTxtField.frame.size.width, 1.0)
        bottomLine2.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                remarkTxtField.borderStyle = UITextBorderStyle.None
        remarkTxtField.layer.addSublayer(bottomLine2)
        let bottomLine3 = CALayer()
        bottomLine3.frame = CGRectMake(0.0, VPAListTxtField.frame.size.height - 1 , VPAListTxtField.frame.size.width, 1.0)
        bottomLine3.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                VPAListTxtField.borderStyle = UITextBorderStyle.None
        VPAListTxtField.layer.addSublayer(bottomLine3)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RequestMoneyViewController.dismissKeyboard))
        view.addGestureRecognizer(tap);

    }
    func VPAList()
    {
        self.activityIndicator.startAnimating()
        var error: NSError?
        ApiClient().getMappedVirtualAddresses
            { (success, error, response) in
            if success
            {
                
                if response != nil
                {
                    
                    var jsonData: NSData?
                    
                    do
                        
                    {
                        jsonData = try NSJSONSerialization.dataWithJSONObject(response, options:NSJSONWritingOptions.PrettyPrinted)
                        let json = JSON(data: jsonData!)
//                        for item in json.arrayValue {
//                            self.vpa.append(item["va"].stringValue)
//                            self.VPAResponse.append(String(item))
//                            print("VPA:\(self.vpa)")
//                        }
                        
                        for item1 in json.arrayValue {
                            let item2 = item1["VirtualAddress"]
                            let items = item2["accounts"]
                            
                            self.vpa.append(items["handle"].stringValue)
                            for item in items["CustomerAccount"].arrayValue {
                                if item["default-credit"].stringValue == "D"{
                                    self.VPAResponse.append(String(item))
                                }
                            }
                        }
                        
                        
                        
                    }
                    catch
                    {
                        jsonData = nil
                        
                    }
                    let jsonDataLength = "\(jsonData!.length)"
                }
              
                self.alertView.delegate = self
          
                self.alertView.title = "Credit VPA"
                for(var i = 0; i<self.vpa.count; i += 1){
                    self.alertView.addButtonWithTitle(self.vpa[i])
                }
                self.alertView.addButtonWithTitle("Cancel")
            }
                self.activityIndicator.stopAnimating()
        }
    }
    func convertStringToDictionary(text: String) -> [NSObject:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [NSObject:AnyObject]
            }
            catch
            {
                print(error)
            }
            catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
func pickContact()
{
    
    payerVPATxtField.text = request_Contact_VPA
    payerVPATxtField.userInteractionEnabled = false
    }
    @IBAction func contactBtnAction(sender: AnyObject) {
         Appconstant.collect_Money = true
        performSegueWithIdentifier("collectMoney_VPAList", sender: self)
       
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard(){
        toolbarView.hidden = true
        self.view.endEditing(true)
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        if buttonIndex != VPAResponse.count
        {
            customeraccount = self.convertStringToDictionary(VPAResponse[buttonIndex])! as NSDictionary
            VPAListTxtField.text = vpa[buttonIndex]
            
        }
    }
    

    
    
    func timerfunc(){
        toolbarView.hidden = false
        
        self.view.addSubview(toolbarView)
        
    }
    func Clicked(){
        
        toolbarView.hidden = true
        
        self.view.endEditing(true)
    }
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: currentDate)
        
        let year =  components.year
        
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd"
        
     self.date = dateFormatter.stringFromDate(sender.date)
        
        let selectDate = Int(self.date)
        
        let currentdate = Int(dateFormatter.stringFromDate(currentDate))
        
        
        if currentdate! <= selectDate!
        {
        validDate = selectDate!-currentdate!+1
        }
        else
        {
            let addDate = currentdate!+selectDate!
             validDate = addDate-currentdate!
        }
        
        if self.validDate != 0
        {
            validDateTxtField.text = "Your collect request Valid for " + String(validDate) + " days"
            
        }
        else
        {
            validDateTxtField.text = "Your collect request Valid for 7 days"
            //            dateString = df.stringFromDate(currentDate)
        }
        
        }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "To_CollectSuccess") {
            let nextview = segue.destinationViewController as! CollectMoneySuccessViewController
            nextview.transactionID = transactionID
            nextview.status = status
            nextview.descriptiontxt = descriptiontxt

        }
    }
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textFieldToChange == amountTxtField
        {
            characterCountLimit = 6
            if((amountTxtField.text?.rangeOfString(".")) != nil){
                characterCountLimit = 9
                let strcount = amountTxtField.text! + string
                let strarray = strcount.componentsSeparatedByString(".")
                
                for(var i = 0; i<strarray.count; i++){
                    if i == 1{
                        if strarray[1].isEmpty{
                            
                        }
                        else{
                            if strarray[1].characters.count == 3{
                                return false
                            }
                            else{
                                return true
                            }
                        }
                    }
                }
                
            }
            else if string == "." && amountTxtField.text?.characters.count == 6{
                return true
            }
        }
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if(textField == validDateTxtField) {
            animateViewMoving(true, moveValue: 25)
        }
        
    }
    func textFieldDidEndEditing(textField: UITextField) {
        
        if(textField == validDateTxtField) {
            animateViewMoving(false, moveValue: 25)
        }
        
    }
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
        
    }
    
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
}
