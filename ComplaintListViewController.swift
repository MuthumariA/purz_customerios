//
//  ComplaintListViewController.swift
//  purZ
//
//  Created by Vertace on 23/11/17.
//  Copyright © 2017 Vertace. All rights reserved.
//


import UIKit

class ComplaintListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITabBarControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var complaintRefNo = [String]()
    var complaintStatus = [String]()
    var indexPath = 0
    var remitter = [String]()
    var beneficiary = [String]()
    var amount = [String]()
    var type = [String]()
    var complaintComments = [String]()
    var transactionID = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initialize() {
        tabBarController?.delegate = self
        ApiClient().getComplaintList { (success, error, response) in
            if response != nil {
                
                do
                    
                {
                    
                    let jsonData:NSData = try NSJSONSerialization.dataWithJSONObject(response, options:NSJSONWritingOptions.PrettyPrinted)
                    let json = JSON(data: jsonData)
                    for item in json.arrayValue {
                        
                        self.complaintRefNo.append(item["complaintRefNo"].stringValue)
                        self.complaintStatus.append(item["complaintStatus"].stringValue)
                        self.remitter.append(item["payerVPA"].stringValue)
                        self.beneficiary.append(item["payeeVPA"].stringValue)
                        self.amount.append(item["amount"].stringValue)
                        self.type.append(item["txnType"].stringValue)
                        self.complaintComments.append(item["txnComment"].stringValue)
                        self.transactionID.append(item["txnReferenceNo"].stringValue)
                    }
                    
                    if self.complaintRefNo.count == 0 {
                        self.tableView.hidden = true
                        let msg = response.valueForKey("message")
                        self.presentViewController(Alert().alert(String(msg!), message: ""), animated: true, completion: nil)
                    }
                    self.tableView.reloadData()
                }
                catch {
                    print("error")
                }
                
                
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell!
        
        let sno_lbl = cell.viewWithTag(1) as! UILabel
        let complaintRefNo_lbl = cell.viewWithTag(2) as! UILabel
        let complaintstatus_lbl = cell.viewWithTag(3) as! UILabel
        
        if indexPath.row == 0 {
            sno_lbl.text = "S.No"
            complaintRefNo_lbl.text = "Complaint Ref No."
            complaintstatus_lbl.text = "Complaint Status"
            sno_lbl.font = UIFont.systemFontOfSize(12, weight: UIFontWeightBold)
            sno_lbl.textAlignment = .Center
            complaintRefNo_lbl.font = UIFont.systemFontOfSize(12, weight: UIFontWeightBold)
            complaintRefNo_lbl.textAlignment = .Left
            complaintstatus_lbl.font = UIFont.systemFontOfSize(12, weight: UIFontWeightBold)
            complaintstatus_lbl.textAlignment = .Center
        }
        else {
            
            sno_lbl.text = "\(indexPath.row)"
            complaintRefNo_lbl.text = self.complaintRefNo[indexPath.row-1]
            complaintstatus_lbl.text = self.complaintStatus[indexPath.row-1]
            sno_lbl.font = UIFont.systemFontOfSize(12, weight: UIFontWeightRegular)
            sno_lbl.textAlignment = .Center
            complaintRefNo_lbl.font = UIFont.systemFontOfSize(12, weight: UIFontWeightRegular)
            complaintRefNo_lbl.textAlignment = .Left
            complaintstatus_lbl.font = UIFont.systemFontOfSize(12, weight: UIFontWeightRegular)
            complaintstatus_lbl.textAlignment = .Center
        }
        
        cell.selectionStyle = .None
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.complaintRefNo.count+1
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row != 0 {
            self.indexPath = indexPath.row - 1
            self.performSegueWithIdentifier("To_ComplaintDetails", sender: self)
        }
    }
    
    
    @IBAction func backBtnAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     */
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "To_ComplaintDetails" {
            let nextview = segue.destinationViewController as! ComplaintDetailsViewController
            nextview.remitter = remitter[self.indexPath]
            nextview.beneficiary = beneficiary[self.indexPath]
            nextview.amount = amount[self.indexPath]
            nextview.type = type[self.indexPath]
            nextview.complaintRefNo = complaintRefNo[self.indexPath]
            nextview.complaintComments = complaintComments[self.indexPath]
            nextview.complaintStatus = complaintStatus[self.indexPath]
            nextview.transactionID = transactionID[self.indexPath]
            
            
        }
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
  
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
