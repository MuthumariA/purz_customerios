//
//  TransactionHistoryViewController.swift
//  purZ
//
//  Created by Vertace on 26/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class TransactionHistoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITabBarControllerDelegate, UITextFieldDelegate{
    
    @IBOutlet weak var activityIndicater: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var endDateTxtField: UITextField!
    @IBOutlet weak var startDateTxtField: UITextField!
    
    var datePickerView:UIDatePicker = UIDatePicker()
    var datePickerView1:UIDatePicker = UIDatePicker()
    var date = ""
    var calendar : NSCalendar = NSCalendar.currentCalendar()
    var currentDateTime = NSDate()
    var df = NSDateFormatter()
    var toolbarView = UIView()
    var dateString = ""
    
    var date1 = ""
    var calendar1 : NSCalendar = NSCalendar.currentCalendar()
    var currentDateTime1 = NSDate()
    var df1 = NSDateFormatter()
    var toolbarView1 = UIView()
    var dateString1 = ""
    var amount = [String]()
    var payer = [String]()
    var payee = [String]()
    var rrn = [String]()
    var status = [String]()
    var type = [String]()
    var transDate = [String]()
    var txnType = [String]()
    var remarks = [String]()
    var indexPath = 0
    
    //    var success = false
    //    var reject = false
    //    var credit = false
    //    var debit = false
    //
    var filter_StartDate = ""
    var filter_EndDate = ""
    var selected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
           df.dateFormat = "dd.MM.yyyy"
        if Appconstant.filter {
            self.startDateTxtField.text = filter_StartDate
            self.endDateTxtField.text = filter_EndDate
            if self.startDateTxtField.text!.isEmpty || self.endDateTxtField.text!.isEmpty {
                
            }
            self.transHistory()
        }
        if self.date != ""{
            dateString = self.date
            
        }
        else{
            let sevenDaysAgo = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -7,
                                                                             toDate: NSDate(), options: NSCalendarOptions(rawValue: 0))
            
            
            dateString = df.stringFromDate(sevenDaysAgo!)
        }
        
        
        let date = df.dateFromString(dateString)
       
         startDateTxtField.text = dateString
        if self.date1 != ""
        {
            dateString1 = self.date1
            
        }
        else
        {
            
            dateString1 = df.stringFromDate(currentDateTime)
        }
        
        let date1 = df1.dateFromString(dateString1)
        endDateTxtField.text = dateString1
        tabBarController?.delegate = self
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, self.startDateTxtField.frame.size.height - 1 , self.startDateTxtField.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        self.startDateTxtField.borderStyle = UITextBorderStyle.None
        self.startDateTxtField.layer.addSublayer(bottomLine1)
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, self.endDateTxtField.frame.size.height - 1 , self.endDateTxtField.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;
        self.endDateTxtField.borderStyle = UITextBorderStyle.None
        self.endDateTxtField.layer.addSublayer(bottomLine)
        startDateTxtField.inputView = datePickerView
        endDateTxtField.inputView = datePickerView1
        transHistory()

        
        
        
        
        // Do any additional setup after loading the view.
        
        
        
        
    }
    
    @IBAction func fromDateBtnAction(sender: AnyObject)
    {
        selected = true
        srtTxtfieldAction()
        
    }
    @IBAction func toDateBtnAction(sender: AnyObject)
    {
        selected = true
        EndtxtFieldAction()
    }
    
    
    func transHistory()
    {
        activityIndicater.startAnimating()
        if Appconstant.filter
        {
                     filter_EndDate = self.endDateTxtField.text!
            filter_StartDate = self.startDateTxtField.text!
            
        }
            
        else
        {
            Appconstant.filter = false
            filter_EndDate = self.endDateTxtField.text!
            filter_StartDate = self.startDateTxtField.text!
        }
        let start_date = filter_StartDate.stringByReplacingOccurrencesOfString(".", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        let end_date =  filter_EndDate.stringByReplacingOccurrencesOfString(".", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        ApiClient().miniStatement(start_date, toDate: end_date) { (Success, error, response) in
            
            if response != nil
            {
                let response_success = String(response.valueForKey("success")!)
                if response_success == "0"
                    
                {
                    self.activityIndicater.stopAnimating()
                    let message = response.valueForKey("message")!
                    if !String(message).isEmpty {
                        self.presentViewController(Alert().alert(String(message),message: ""), animated: true, completion: nil)
                    }
                }
                else
                {
                    var jsonData: NSData?
                    
                    do
                        
                    {
                        self.amount.removeAll()
                        self.transDate.removeAll()
                        self.payer.removeAll()
                        self.payee.removeAll()
                        self.rrn.removeAll()
                        self.status.removeAll()
                        self.type.removeAll()
                        self.txnType.removeAll()
                        
                        jsonData = try NSJSONSerialization.dataWithJSONObject(response, options:NSJSONWritingOptions.PrettyPrinted)
                        let json = JSON(data: jsonData!)
                        
                        for item in json.arrayValue
                        {
                            var append = true
                            
                            // Filter Condition
                            if Appconstant.filter {
                                append = false
                                
                                // SUCCESS and REJECT filter condition with CREDIT or DEBIT
                                if Appconstant.debitCheckBox || Appconstant.creditCheckBox {
                                    if Appconstant.debitCheckBox && item["txn-type"].stringValue == "DEBIT" {
                                        if Appconstant.successCheckBox && Appconstant.rejectCheckBox {
                                            append = true
                                        }
                                        else if Appconstant.successCheckBox && item["status"].stringValue == "SUCCESS" {
                                            append = true
                                        }
                                        else if Appconstant.rejectCheckBox && item["status"].stringValue == "REJECT" {
                                            append = true
                                        }
                                        else if !Appconstant.successCheckBox && !Appconstant.rejectCheckBox{
                                            append = true
                                        }
                                    }
                                    else if Appconstant.creditCheckBox && item["txn-type"].stringValue == "CREDIT" {
                                        if Appconstant.successCheckBox && Appconstant.rejectCheckBox {
                                            append = true
                                        }
                                        else if Appconstant.successCheckBox && item["status"].stringValue == "SUCCESS" {
                                            append = true
                                        }
                                        else if Appconstant.rejectCheckBox && item["status"].stringValue == "REJECT" {
                                            append = true
                                        }
                                        else if !Appconstant.successCheckBox && !Appconstant.rejectCheckBox{
                                            append = true
                                        }
                                    }
                                }
                                    
                                    // SUCCESS and REJECT filter alone condition
                                else if Appconstant.successCheckBox && Appconstant.rejectCheckBox {
                                    append = true
                                }
                                else if Appconstant.successCheckBox && item["status"].stringValue == "SUCCESS" {
                                    append = true
                                }
                                else if Appconstant.rejectCheckBox && item["status"].stringValue == "REJECT" {
                                    append = true
                                }
                                
                                
                            }    // End of filter condition
                            
                            
                            if append {
                                
                                self.txnType.append((item["txn-type"].stringValue))
                                self.amount.append(item["amount"].stringValue)
                                self.payer.append((item["payer"].stringValue))
                                self.payee.append((item["payee"].stringValue))
                                self.rrn.append((item["rrn"].stringValue))
                                self.status.append((item["status"].stringValue))
                                self.type.append((item["type"].stringValue))
                                self.remarks.append((item["note"].stringValue))
                                
                                let dateString = item["date"].stringValue
                                self.df1.dateFormat = "yyyy-MM-ddHH:mm:ss"
                                let dateFormat = self.df1.dateFromString(dateString)
                                self.df1.dateFormat = "dd-MMM-yy"
                                let dateFormat1 = self.df1.stringFromDate(dateFormat!)
                                self.transDate.append(dateFormat1)
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                        
                    catch
                    {
                        jsonData = nil
                        
                    }
                    let jsonDataLength = "\(jsonData!.length)"
                }
                if self.amount.count == 0{
                    self.presentViewController(Alert().alert("No transaction available", message: ""), animated: true, completion: nil)
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicater.stopAnimating()
                    self.tableView.reloadData()
                }
            }
            else {
                self.activityIndicater.stopAnimating()
                self.presentViewController(Alert().alert("No transaction available", message: ""), animated: true, completion: nil)
            }
            
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "To_filter") {
            let nextview = segue.destinationViewController as! TransactionFilterViewController
            nextview.filter_StartDate = self.startDateTxtField.text!
            nextview.filter_EndDate = self.endDateTxtField.text!
        }
        else if(segue.identifier == "To_TransactionDetails") {
            let nextview = segue.destinationViewController as! TransactionDetailViewController
            nextview.amount = self.amount[self.indexPath]
            nextview.remitter = self.payer[self.indexPath]
            nextview.beneficiary = self.payee[self.indexPath]
            nextview.date = self.transDate[self.indexPath]
            nextview.transactionType = self.txnType[self.indexPath]
            nextview.transactionID = self.rrn[self.indexPath]
            nextview.remarks = self.remarks[self.indexPath]
            nextview.status = self.status[self.indexPath]
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("historyListCell", forIndexPath: indexPath) as UITableViewCell!
        let img = cell.viewWithTag(1)! as! UIImageView
        let amountLbl = cell.viewWithTag(2) as! UILabel
        let dateLbl = cell.viewWithTag(4) as! UILabel
        let userDetailLbl = cell.viewWithTag(3) as! UILabel
        let backView = cell.viewWithTag(5)! as UIView
  
        
            amountLbl.text = "Rs "+amount[indexPath.row]
            userDetailLbl.text = payer[indexPath.row]+" to "+payee[indexPath.row]+" | "+rrn[indexPath.row]+" | "+status[indexPath.row]
    
            dateLbl.text = transDate[indexPath.row]
            if type[indexPath.row] == "COLLECT"
            {
                img.image = UIImage(named: "In.png")
                
            }
            else{
                img.image = UIImage(named: "Out.png")
            }

        
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, backView.frame.size.height - 1 , view.frame.size.width-10, 2.0)
        bottomLine.backgroundColor = UIColor.lightGrayColor().CGColor
        cell.selectionStyle = .None
        bottomLine.opacity = 0.3
        backView.layer.addSublayer(bottomLine)
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.indexPath = indexPath.row
        self.performSegueWithIdentifier("To_TransactionDetails", sender: self)
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return amount.count
    }
    func srtTxtfieldAction()
    {
        startDateTxtField.becomeFirstResponder()
        df.dateFormat = "dd.MM.yyyy"
        
        datePickerView.maximumDate = NSDate()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        
        
        if self.date != ""
        {
            dateString = self.date
            
        }
        else{
            let sevenDaysAgo = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -7,
                                                                             toDate: NSDate(), options: NSCalendarOptions(rawValue: 0))
           
            dateString = df.stringFromDate(sevenDaysAgo!)
            
        }
        
        let date = df.dateFromString(dateString)
        
        datePickerView.setDate(date!, animated: false)
       
        
        if toolbarView.backgroundColor != UIColor.lightGrayColor() {
        toolbarView = UIView(frame: CGRectMake(0,self.view.frame.size.height-datePickerView.frame.size.height-44,self.view.frame.size.width,44))
             toolbarView.backgroundColor = UIColor.lightGrayColor()
            let doneButton = UIButton.init(frame: CGRectMake(self.view.frame.size.width - 60,5,50,34))
            doneButton.addTarget(self, action: "Clicked", forControlEvents: UIControlEvents.TouchUpInside)
            doneButton.setTitle("Done", forState: UIControlState.Normal)
            NSTimer.scheduledTimerWithTimeInterval(0.2, target:self, selector: Selector("timerfunc"), userInfo: nil, repeats: false)
            toolbarView.addSubview(doneButton)
        }
        toolbarView.hidden = false
         startDateTxtField.inputView = datePickerView
        datePickerView.addTarget(self, action: "datePickerValueChanged:", forControlEvents: UIControlEvents.ValueChanged)
        
        
    }
    
    
    func timerfunc(){
        toolbarView.hidden = false
        
        self.view.addSubview(toolbarView)
        
    }
    func Clicked(){
        
        if startDateTxtField.text == ""
        {
            toolbarView.hidden = true
            self.view.endEditing(true)
            startDateTxtField.text = dateString
            EndtxtFieldAction()
            
        }
        else{
            //            endDateTxtField.text =
            if endDateTxtField.text! == "" {
                endDateTxtField.text = dateString1
                 toolbarView.hidden = true
            }
            
            toolbarView.hidden = true
            
            self.view.endEditing(true)
            if startDateTxtField.text != "" && endDateTxtField.text != ""
            {
                toolbarView.hidden = true
                transHistory()
            }
        }
        
        
    }
    func datePickerValueChanged(sender:UIDatePicker) {
        let currentdate = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: currentdate)
        
        let year =  components.year
        
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        dispatch_async(dispatch_get_main_queue()) {
            self.startDateTxtField.text = dateFormatter.stringFromDate(sender.date)
            self.date = dateFormatter.stringFromDate(sender.date)
        }
        
    }
    func EndtxtFieldAction()
    {
        
        endDateTxtField.inputView = datePickerView1
        endDateTxtField.becomeFirstResponder()
        df1.dateFormat = "dd.MM.yyyy"
        datePickerView1.maximumDate = NSDate()
        datePickerView1.datePickerMode = UIDatePickerMode.Date
        if self.date1 != ""
        {
            dateString1 = self.date1
            
        }
        else
        {
            
            dateString1 = df1.stringFromDate(currentDateTime)
        }
        endDateTxtField.inputView = datePickerView1
        let date1 = df1.dateFromString(dateString1)
        datePickerView1.setDate(date1!, animated: false)
        
        if toolbarView.backgroundColor != UIColor.lightGrayColor() {
            toolbarView = UIView(frame: CGRectMake(0,self.view.frame.size.height-datePickerView.frame.size.height-44,self.view.frame.size.width,44))
            toolbarView.backgroundColor = UIColor.lightGrayColor()
            let doneButton = UIButton.init(frame: CGRectMake(self.view.frame.size.width - 60,5,50,34))
            doneButton.addTarget(self, action: "Clicked", forControlEvents: UIControlEvents.TouchUpInside)
            doneButton.setTitle("Done", forState: UIControlState.Normal)
            NSTimer.scheduledTimerWithTimeInterval(0.2, target:self, selector: Selector("timerfunc"), userInfo: nil, repeats: false)
            toolbarView.addSubview(doneButton)
        }
        NSTimer.scheduledTimerWithTimeInterval(0.2, target:self, selector: Selector("timerfunc1"), userInfo: nil, repeats: false)
        //    toolbarView1.addSubview(doneButton1)
        
            toolbarView.hidden = false
        datePickerView1.addTarget(self, action: "datePickerValueChanged1:", forControlEvents: UIControlEvents.ValueChanged)
        
        
    }
    
    
    
    func timerfunc1()
    {
        toolbarView.hidden = false
        
        self.view.addSubview(toolbarView1)
        
    }
    func Clicked1()
    {
        
        toolbarView1.hidden = true
        
        self.view.endEditing(true)
        
    }
    func datePickerValueChanged1(sender:UIDatePicker)
    {
        let currentdate = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: currentdate)
        
        let year =  components.year
        
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        dispatch_async(dispatch_get_main_queue())
        {
            self.endDateTxtField.text = dateFormatter.stringFromDate(sender.date)
            self.date1 = dateFormatter.stringFromDate(sender.date)
        }
        
    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
 
}
