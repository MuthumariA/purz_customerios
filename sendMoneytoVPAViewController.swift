//
//  sendMoneytoVPAViewController.swift
//  purZ
//
//  Created by Vertace on 21/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit
import AVFoundation
import Contacts
import ContactsUI

class sendMoneytoVPAViewController: UIViewController,UITabBarControllerDelegate, QRCodeReaderViewControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var scanQRview: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scanQRLbl: UILabel!
    @IBOutlet weak var payeeVPATxtField: UITextField!
    @IBOutlet weak var AmountTxtField: UITextField!
    @IBOutlet weak var remarkTxtfield: UITextField!
    @IBOutlet weak var VPAListTxtfield: UITextField!
    
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var contactSearchBtn: UIButton!
    @IBOutlet weak var refreshBtn: UIButton!
    @IBOutlet weak var scanQRIcon: UIButton!
    @IBOutlet weak var dropdownBtn: UIButton!
    
    var alertView = UIAlertView()
    
    var characterCountLimit = 0
    var send_Contact_PayeeName = ""
    var send_Contact_VPA = ""
    var payerAccount = NSDictionary()
    var payerAccountObj = NSObject()
    var VPAResponse = [String]()
    var vpa = [String]()
    var phonenumber = [String]()
    var status = ""
    var descriptiontxt = ""
    var transactionid = ""
    var fromPayatStore = false
    var qrvalue: QRCodeReaderResult!
    
    lazy var reader: QRCodeReaderViewController = {
        let builder = QRCodeViewControllerBuilder { builder in
            builder.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode])
            builder.showTorchButton = true
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Initialize()
        pick_contact()
        VPAList()
        if fromPayatStore {
           setQRdetailstoField(qrvalue)
        }
    }
    @IBAction func refreshBtnAction(sender: AnyObject) {
        UIView.animateWithDuration(0.5, animations: {
            let angle: CGFloat? = CGFloat((self.refreshBtn.valueForKeyPath("layer.transform.rotation.z") as? NSNumber)!)
            let transform = CGAffineTransformMakeRotation(angle! + CGFloat(M_PI))
            self.refreshBtn.transform = transform
        })
        
        payeeVPATxtField.text = ""
        AmountTxtField.text = ""
        remarkTxtfield.text = ""
        VPAListTxtfield.text = ""
        
    }
    @IBAction func VPAListBtnAction(sender: AnyObject) {
        
            self.payeeVPATxtField.resignFirstResponder()
            self.AmountTxtField.resignFirstResponder()
            self.remarkTxtfield.resignFirstResponder()
            self.VPAListTxtfield.resignFirstResponder()
            self.view.endEditing(true)
        if self.vpa.count != 0
        {
            dispatch_async(dispatch_get_main_queue(), {
            self.alertView.show()
        })
        }
        else {
            let alertController = UIAlertController(title: "No VPA created or linked. Please create/link  VPA to continue your payment",
                                                    message: "" ,
                                                    preferredStyle: .Alert)
            
            let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                self?.performSegueWithIdentifier("To_UPIHome", sender: self)
                })
            
            alertController.addAction(action)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
    }
    @IBAction func contact_pickerBtnAtn(sender: AnyObject) {
        Appconstant.send_Money = true
        
    }
    @IBAction func sendBtnAction(sender: AnyObject) {
        
      
        if payeeVPATxtField.text == ""
        {
            self.presentViewController(Alert().alert("VPA can't be empty",message: ""), animated: true, completion: nil)
            
            
        }
        else if AmountTxtField.text == ""
        {
            self.presentViewController(Alert().alert("Amount can't be empty",message: ""), animated: true, completion: nil)
            
        }
           else if Double(AmountTxtField.text!)! < 1
        
        {
           self.presentViewController(Alert().alert("Amount can't be zero",message: ""), animated: true, completion: nil)
        }
        else if VPAListTxtfield.text  == ""
        {
            
            self.presentViewController(Alert().alert("Please select the VPA",message: ""), animated: true, completion: nil)
        }
            
        else
        {  
            let payeeAccount = PayeeParam.init(VPA: payeeVPATxtField.text!)
            let payerVirtualAccount = VirtualAddress.init(payerAccount as! [NSObject : AnyObject], handle: VPAListTxtfield.text!)
            let amount = NSDecimalNumber(string: AmountTxtField.text!)
            var remark = remarkTxtfield.text!
            if remark == "" {
                remark = "NA"
            }
            
            let transaction = TransactionParameters.init(transaction: payerVirtualAccount, payee: payeeAccount, amount: amount, remark: remark)
            
            self.activityIndicator.startAnimating()
            self.sendBtn.userInteractionEnabled = false
            ApiClient().pay(transaction!, view: self, withCompletion: { (success, error, response) in
                
                if success
                {
                    let response_success = response.valueForKey("success")
                    if String(response_success!) == "1" {
                        self.status = "Success"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("sendmoneyVPA_to_success", sender: self)
                    }
                    else {
                        self.status = "Failed"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("sendmoneyVPA_to_success", sender: self)
                    }
                }
                else
                {
                    if response != nil
                    {
                        self.status = "Failed"
                        self.descriptiontxt = String(response.valueForKey("message")!)
                        self.transactionid = String(response.valueForKey("BankRRN")!)
                        self.performSegueWithIdentifier("sendmoneyVPA_to_success", sender: self)
                    }
                }
                self.activityIndicator.stopAnimating()
                self.sendBtn.userInteractionEnabled = true
            })
            
        }
    }
    
    func Initialize()
    {
        self.payeeVPATxtField.userInteractionEnabled = true
        self.remarkTxtfield.userInteractionEnabled = true
        self.AmountTxtField.userInteractionEnabled = true
        AmountTxtField.delegate = self
        tabBarController?.delegate = self
        sendBtn.layer.borderWidth = 2
        sendBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        sendBtn.layer.cornerRadius = sendBtn.frame.size.height/2
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, payeeVPATxtField.frame.size.height - 1 , payeeVPATxtField.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                payeeVPATxtField.borderStyle = UITextBorderStyle.None
        payeeVPATxtField.layer.addSublayer(bottomLine)
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, AmountTxtField.frame.size.height - 1 , AmountTxtField.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                AmountTxtField.borderStyle = UITextBorderStyle.None
        AmountTxtField.layer.addSublayer(bottomLine1)
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, remarkTxtfield.frame.size.height - 1 , remarkTxtfield.frame.size.width, 1.0)
        bottomLine2.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                remarkTxtfield.borderStyle = UITextBorderStyle.None
        remarkTxtfield.layer.addSublayer(bottomLine2)
        let bottomLine3 = CALayer()
        bottomLine3.frame = CGRectMake(0.0, VPAListTxtfield.frame.size.height - 1 , VPAListTxtfield.frame.size.width, 1.0)
        bottomLine3.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                VPAListTxtfield.borderStyle = UITextBorderStyle.None
        VPAListTxtfield.layer.addSublayer(bottomLine3)
        
        // Do any additional setup after loading the view.
        
        let scanqrtap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(sendMoneytoVPAViewController.scanQRAction))
        scanQRview.addGestureRecognizer(scanqrtap)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GenerateQRViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // check QR value
        
    }
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textFieldToChange == AmountTxtField
        {
            characterCountLimit = 6
            if((AmountTxtField.text?.rangeOfString(".")) != nil){
                characterCountLimit = 9
                let strcount = AmountTxtField.text! + string
                let strarray = strcount.componentsSeparatedByString(".")
                
                for(var i = 0; i<strarray.count; i++){
                    if i == 1{
                        if strarray[1].isEmpty{
                            
                        }
                        else{
                            if strarray[1].characters.count == 3{
                                return false
                            }
                            else{
                                return true
                            }
                        }
                    }
                }
                
            }
            else if string == "." && AmountTxtField.text?.characters.count == 6{
                return true
            }
        }
        else {
            characterCountLimit = 50
        }
        
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
        
        
    }
    
    func VPAList()
    {
        self.activityIndicator.startAnimating()
        ApiClient().getMappedVirtualAddresses
            { (success, error, response) in
                self.activityIndicator.stopAnimating()
                if success
                {
                    
                    if response != nil
                    {
                        var jsonData: NSData?
                        
                        do
                            
                        {
                            jsonData = try NSJSONSerialization.dataWithJSONObject(response, options:NSJSONWritingOptions.PrettyPrinted)
                            let json = JSON(data: jsonData!)
                            
//                            for item in json.arrayValue
//                            {
//                                self.vpa.append(item["va"].stringValue)
//                                self.VPAResponse.append(String(item))
//                                print("VPA:\(self.vpa)")
//                                print("vparesponse;\(self.VPAResponse)")
//                            }
                            for item1 in json.arrayValue {
                                
                                let item2 = item1["VirtualAddress"]
                                let items = item2["accounts"]
                                
                                self.vpa.append(items["handle"].stringValue)
                                for item in items["CustomerAccount"].arrayValue {
                                    if item["default-debit"].stringValue == "D"{
                                        self.VPAResponse.append(String(item))
                                    }
                                }
                            }
                            
                        }
                        catch let error as NSError {
                            
                        }
                        catch
                        {
                            jsonData = nil
                            
                        }
                        let jsonDataLength = "\(jsonData!.length)"
                    }
                    self.alertView.delegate = self
                    self.alertView.title = "Debit VPA"
                    for(var i = 0; i<self.vpa.count; i += 1)
                    {
                        self.alertView.addButtonWithTitle(self.vpa[i])
                    }
                    self.alertView.addButtonWithTitle("Cancel")
                    
                    
                }
                
        }
    }
    
    func pick_contact()
    {
        payeeVPATxtField.text = send_Contact_VPA
        if payeeVPATxtField.text! != "" {
        payeeVPATxtField.userInteractionEnabled = false
        }
        
    }
    func dismissKeyboard(){
        self.view.endEditing(true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func contactPickerBtnAction(sender: AnyObject) {
        
    }
    
    
    
    
    func convertStringToDictionary(text: String) -> [NSObject:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do
            {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [NSObject:AnyObject]
            }
            catch
            {
                print(error)
            }
            catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        if buttonIndex != VPAResponse.count
        {
            
            
            let PA  = self.convertStringToDictionary(VPAResponse[buttonIndex])! as NSDictionary
            payerAccount = PA
            
            VPAListTxtfield.text = vpa[buttonIndex]
            
        }
    }
    func scanQRAction(){
        
        if QRCodeReader.supportsMetadataObjectTypes() {
            reader.modalPresentationStyle = .FormSheet
            reader.delegate = self
            dispatch_async(dispatch_get_main_queue()) {
                self.reader.completionBlock = { (result: QRCodeReaderResult?) in
                    if let result = result {
                        print("Completion with result: \(result.value) of type \(result.metadataType)")
                    }
                }
            }
            presentViewController(reader, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Purz", message: "Reader not supported by the current device", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            
            presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func reader(reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult)
    {
        self.dismissViewControllerAnimated(true, completion: { [weak self] in
            
            self!.setQRdetailstoField(result)
            })
    }
    
    
    
    func setQRdetailstoField(result: QRCodeReaderResult) {
        
        let splitURL = LinkableTransaction().parseIntentData(result.value)
        let QR_remarks = splitURL.getRemarks()
        let QR_amount = splitURL.getAmount()
        let QR_payee = splitURL.getPayeeVPA()
        if QR_remarks != nil
        {
            self.payeeVPATxtField.text = QR_payee
            self.remarkTxtfield.text = QR_remarks
            self.AmountTxtField.text = String(QR_amount)
            self.payeeVPATxtField.userInteractionEnabled = false
            self.remarkTxtfield.userInteractionEnabled = false
            self.AmountTxtField.userInteractionEnabled = false
        }
        else
        {
            self.presentViewController(Alert().alert("Invalid QR", message: ""), animated: true, completion: nil)
        }
    }
    
    
    func readerDidCancel(reader: QRCodeReaderViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
 */
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "sendmoneyVPA_to_success") {
            let nextview = segue.destinationViewController as! CollectMoneySuccessViewController
            nextview.transactionID = transactionid
            nextview.status = status
            nextview.descriptiontxt = descriptiontxt
            
        }
     }

    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
}
//extension NSURL {
//    var fragments: [String: String] {
//        var results = [String: String]()
//        if let pairs = self.fragment?.componentsSeparatedByString("&") where pairs.count > 0 {
//            for pair: String in pairs {
//                if let keyValue = pair.componentsSeparatedByString("=") as [String]? {
//                    results.updateValue(keyValue[1], forKey: keyValue[0])
//                }
//            }
//        }
//        return results
//    }
//}
