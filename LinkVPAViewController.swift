//
//  LinkVPAViewController.swift
//  purZ
//
//  Created by Vertace on 25/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class LinkVPAViewController: UIViewController,UITabBarControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var linlVPAList1TxtField: UITextField!
    @IBOutlet weak var linkVPAList2TxtField: UITextField!
    
    @IBOutlet var defaultsendbtn: UIButton!
    
    @IBOutlet var defaultreceivebtn: UIButton!
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    
    var bankname = ""
    var accountdetails = ""
    var vpa = ""
    var vpadetails = ""
    var defaultdebit = false
    var defaultcredit = false
    var accountno = ""
    var upi = ""
    var alertView = UIAlertView()
    var vpaname = [String]()
    var VPAresponse = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialFunc()
                // Do any additional setup after loading the view.
    }
    func initialFunc()
    {
        tabBarController?.delegate = self
        self.activityIndicator.hidesWhenStopped = true
        submitBtn.layer.borderWidth = 2
        submitBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        submitBtn.layer.cornerRadius = submitBtn.frame.size.height/2
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, linlVPAList1TxtField.frame.size.height - 1 , linlVPAList1TxtField.frame.size.width, 1.0)
        bottomLine.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                linlVPAList1TxtField.borderStyle = UITextBorderStyle.None
        linlVPAList1TxtField.layer.addSublayer(bottomLine)
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, linkVPAList2TxtField.frame.size.height - 1 , linkVPAList2TxtField.frame.size.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0).CGColor;                linkVPAList2TxtField.borderStyle = UITextBorderStyle.None
        linkVPAList2TxtField.layer.addSublayer(bottomLine1)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GenerateQRViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        linlVPAList1TxtField.delegate = self
        linkVPAList2TxtField.delegate = self
        linlVPAList1TxtField.allowsEditingTextAttributes = false
        linkVPAList2TxtField.allowsEditingTextAttributes = false
        
        if vpa != "" {
            self.linlVPAList1TxtField.text = vpa
        }
        if bankname != "" {
            self.linkVPAList2TxtField.text = bankname+" "+accountno
        }
        self.alertView.delegate = self
        self.alertView.title = "Select VPA"
        
        
            initi()


    }
    func initi(){
        self.activityIndicator.startAnimating()
        ApiClient().getMappedVirtualAddresses { (success, error, response) in
            self.activityIndicator.stopAnimating()
            if success {
                if response != nil {
                    
                    do{
                        // do whatever with jsonResult
                        let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(response, options: NSJSONWritingOptions.PrettyPrinted)
                        let json = JSON(data: jsonData)
//                        for item in json.arrayValue {
//                            print(String(item))
//                            self.vpaname.append(item["va"].stringValue)
//                            self.alertView.addButtonWithTitle(item["va"].stringValue)
//                            self.VPAresponse.append(String(item))
//                            AccountConstant.vpaProviderName.append(item["account-provider-name"].stringValue)
//                        }
                        AccountConstant.vpaProviderName.removeAll()
                        for item1 in json.arrayValue {
                            
                            let item2 = item1["VirtualAddress"]
                            let items = item2["accounts"]
                           
                            
                            self.vpaname.append(items["handle"].stringValue)
                            self.alertView.addButtonWithTitle(items["handle"].stringValue)
                            for item in items["CustomerAccount"].arrayValue {
                                if item["default-credit"].stringValue == "D"{
                                    AccountConstant.vpaProviderName.append(item["account-provider-name"].stringValue)
                                    self.VPAresponse.append(String(item))
                                }
                            }
                        }
                        
                        
                        self.alertView.addButtonWithTitle("Cancel")
                        
                    }
                    catch let error as NSError {
                        print("error")
                    }
                    
                }
                
            }
            
        }
    }

    @IBAction func accountTxtAction(sender: AnyObject) {
        
        self.performSegueWithIdentifier("To_SelectAccount", sender: self)
        
    }
    
    @IBAction func submitBtnAction(sender: AnyObject) {
        if linlVPAList1TxtField.text! == "" || linlVPAList1TxtField.text! == "Select VPA"{
            self.presentViewController(Alert().alert("Please select the VPA",message: ""), animated: true, completion: nil)
        }
        else if linkVPAList2TxtField.text! == ""{
            self.presentViewController(Alert().alert("Please select the Bank Account",message: ""), animated: true, completion: nil)
        }
        else{
            AccountDefaultStatus.init(defaultDebitCredit: defaultdebit, default_Credit: defaultcredit)
            AccountConstant.defaultdebit = defaultdebit
            AccountConstant.defaultcredit = defaultcredit
            let email = Appconstant.mobileno + "@test.com"
            AccountConstant.accountdetail = self.accountdetails
            AccountConstant.VPA = self.linlVPAList1TxtField.text!
            AccountConstant.accountno = self.accountno
            if self.upi == "N" {
                self.performSegueWithIdentifier("popover_to_card", sender: self)
            }
            else{
                do {
                    
                    let userdetail = SdkClientApplication.init(userName: Appconstant.customername, appName: "com.equitas.purz", instituteId: "999", channel: "equitas-purz", bankRefUrl: "https://www.equitasbank.com", userMail: email, mobile: Appconstant.mobileno, userIdentity: Appconstant.customerid, transactionID_Prefix: "ESF")
                    
                    //            let data = accountdetail.dataUsingEncoding(NSUTF8StringEncoding)
                  
                    let customeracc = self.convertStringToDictionary(self.accountdetails)! as NSDictionary
                   
                    
                    let CustomerAcc_detail = CustomerAccount.init(accountDetails: customeracc as! [NSObject : AnyObject])
                    
                    
                    let defaultstatus = AccountDefaultStatus.init(defaultDebitCredit: defaultdebit, default_Credit: defaultcredit)
                    
                    try ApiClient().mapAccount(userdetail, vpa: self.linlVPAList1TxtField.text!, custAccount: CustomerAcc_detail, accountDefaultStatus: defaultstatus, withCompletion: { (success, error, response) in
                       
                        AccountConstant.vpanames.removeAll()
                        AccountConstant.vparesponses.removeAll()
                        AccountConstant.vpaProviderName.removeAll()
                        self.view.userInteractionEnabled = true
                        
                        if success {
                            
                            let response_success = response.valueForKey("success")
                            if String(response_success!) == "1" {
                                self.presentViewController(Alert().alert("Account added successfully!",message: ""), animated: true, completion: nil)
                                self.linlVPAList1TxtField.text = ""
                                self.linkVPAList2TxtField.text = ""
                                
                            }
                        }
                        else{
                           
                            self.presentViewController(Alert().alert("Account failed to add",message: ""), animated: true, completion: nil)
                        }
                
                    })
                }
                catch let error as NSError{
                    self.view.userInteractionEnabled = true
                }
            }
            
        }
    }
    
    
    @IBAction func senddefaultBtnAction(sender: AnyObject) {
        if defaultdebit {
            defaultdebit = false
            defaultsendbtn.setImage(UIImage(named: "unchecked.png"), forState: .Normal)
        }
        else{
            defaultdebit = true
            defaultsendbtn.setImage(UIImage(named: "checked_box.png"), forState: .Normal)
        }
    }
    
    
    @IBAction func receivedefaultBtnAction(sender: AnyObject) {
        if defaultcredit {
            defaultcredit = false
            defaultreceivebtn.setImage(UIImage(named: "unchecked.png"), forState: .Normal)
        }
        else{
            defaultcredit = true
            defaultreceivebtn.setImage(UIImage(named: "checked_box.png"), forState: .Normal)
        }
    }
    
    
    @IBAction func selectVPAbtnAction(sender: AnyObject) {
        if self.vpaname.count != 0 {
            alertView.show()
        }
    }
    
    
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    
  
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
         if buttonIndex != vpaname.count {
            linlVPAList1TxtField.text = vpaname[buttonIndex]
            vpa = vpaname[buttonIndex]
            vpadetails = VPAresponse[buttonIndex]
        }
    }
    func textFieldShouldBeginEditing(state: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        view.endEditing(true)
        return true
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func convertStringToDictionary(text: String) -> [NSObject:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [NSObject:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "To_SelectAccount") {
            let nextview = segue.destinationViewController as! SelectAccountViewController
            nextview.vpa = vpa
            nextview.vpadetails = vpadetails
            nextview.fromLinkVPA = true
            nextview.vpaname = vpaname
            nextview.VPAresponse = VPAresponse
        }
        else if(segue.identifier == "popover_to_card") {
            let nextview = segue.destinationViewController as! CardDetailViewController
            nextview.fromlinkVPA = true
        
        }
    }
    
    
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    

}
