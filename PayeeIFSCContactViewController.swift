
//
//  PayeeIFSCContactViewController.swift
//  purZ
//
//  Created by Vertace on 29/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class PayeeIFSCContactViewController: UIViewController,UITableViewDataSource, UITableViewDelegate, UITabBarControllerDelegate
{
   
    @IBOutlet weak var tableView: UITableView!
    
    var accountList = [String]()
    var secure_accountList = [String]()
    var ifsc = [String]()
    var payeeName = [String]()
    var contact_PayeeName = ""
    var contact_IFSC = ""
    var contact_AccountNo = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
 tabBarController?.delegate = self
        PayeeAccountList()

        // Do any additional setup after loading the view.
    }
    @IBAction func closeBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        print("Index\(indexPath.row)")
       
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "Are you sure you want to delete?",
                                            message: "",
                                            preferredStyle: .Alert)
        
        let action1 = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            
            if purzDB.open() {
                
                let deleteSQL =  "DELETE FROM BENEFICIARY_IFSC WHERE ACCOUNT_NUMBER = " + self!.accountList[indexPath.row]
                
                let result = purzDB.executeUpdate(deleteSQL,
                    withArgumentsInArray: nil)
                
                if !result {
                    //   status.text = "Failed to add contact"
                    print("Error: \(purzDB.lastErrorMessage())")
                }
                else
                {
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self!.presentViewController(Alert().alert("IFSC deleted successfully",message: ""), animated: true, completion: nil)
                        self!.ifsc.removeAll()
                        self!.payeeName.removeAll()
                        self!.accountList.removeAll()
                        self!.secure_accountList.removeAll()
                        self!.PayeeAccountList()
                    }
                }
            }

         })
        
        let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            })
        
       
        alertController?.addAction(action1)
        alertController?.addAction(action2)
        self.presentViewController(alertController!, animated: true, completion: nil)
        
    }

    @IBAction func backBtnAction(sender: AnyObject) {
        if Appconstant.sendMoney_Account
        {
            Appconstant.sendMoney_Account = false
     performSegueWithIdentifier("To_SendIFSC", sender: self)
        }
        else
        {
       performSegueWithIdentifier("To_BeneficiaryHome", sender: self)
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func PayeeAccountList()
    {
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            accountList.removeAll()
            payeeName.removeAll()
            ifsc.removeAll()
        secure_accountList.removeAll();
            let select = "SELECT * FROM BENEFICIARY_IFSC"
            let result:FMResultSet = purzDB.executeQuery(select,
                                                         withArgumentsInArray: nil)
            while(result.next()){
                payeeName.append(result.stringForColumn("REGISTERED_NAME"))
                ifsc.append(result.stringForColumn("IFSC_CODE"))
                accountList.append(result.stringForColumn("ACCOUNT_NUMBER"))
                secure_accountList.append(result.stringForColumn("SECURE_ACCOUNT_NUMBER"))
            }
            self.tableView.reloadData()
        }
        purzDB.close()
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("payeeCreatedIFSC", forIndexPath: indexPath) as UITableViewCell!
    
    
    let bankImg = cell.viewWithTag(1) as! UIImageView
    let accountNo = cell.viewWithTag(2) as! UILabel
        let closeBtn = cell.viewWithTag(3) as! UIButton
    let registeredName = cell.viewWithTag(4) as! UILabel
      let ifscCode  = cell.viewWithTag(5) as! UILabel
        
       let account = accountList[indexPath.row]
          let count = account.characters.count
//                let first = String(account.characters.prefix(count))
     
//  print("Fisrst:\(first)")
        
   
    accountNo.text = secure_accountList[indexPath.row]
    registeredName.text = ": "+payeeName[indexPath.row]
    ifscCode.text = ": "+ifsc[indexPath.row]
        
    cell.layer.borderWidth = 3
    cell.layer.borderColor = UIColor(red: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1).CGColor
    cell.selectionStyle = .None
    
    return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ifsc.count
    }
   
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if Appconstant.sendMoney_Account
        {
              Appconstant.sendMoney_Account = false
               contact_PayeeName = payeeName[indexPath.row]
        contact_IFSC = ifsc[indexPath.row]
        contact_AccountNo = accountList[indexPath.row]
        performSegueWithIdentifier("To_SendIFSC", sender: self)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "To_SendIFSC") {
            let nextview = segue.destinationViewController as! paytoAccountViewController
           
            nextview.contact_PayeeName = contact_PayeeName
            nextview.contact_AccountNo = contact_AccountNo
            nextview.contact_IFSC = contact_IFSC
           
        }
        else if segue.identifier == "B_IFSC_to_addIFSC" {
            let nextvc = segue.destinationViewController as! BankAccountViewController
            nextvc.existing_accounts = self.accountList
        }
    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    

}
