//  UPIHomeViewController.swift
//  purZ
//
//  Created by Vertace on 13/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class UPIHomeViewController: UIViewController,UITabBarControllerDelegate {
    
    
    @IBOutlet var addaccountview: UIView!
    
    @IBOutlet var sendmoneyview: UIView!
    
    @IBOutlet var collectrequestview: UIView!
    
    @IBOutlet var vpamanagerview: UIView!
    
    @IBOutlet var beneficiaryview: UIView!
    
    @IBOutlet var transactionsview: UIView!
    
    @IBOutlet var generateQRview: UIView!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    var accountNo = [String]()
    
    override func viewDidLoad() {
     
        
        intialfunction()
        
        getUserBankAccountList()
        
        // 559279563 -> Pradeepa UserID
        
        //        do{
        //            let UserDetailsViewModel = SdkClientApplication.init(userName: "Bhuvanesh", appName: "com.equitas.PurZ", instituteId: "999", channel: "equitas-purz", bankRefUrl: "https://www.equitasbank.com", userMail: "8489207008@test.com", mobile: "8489207008", userIdentity: "476496615")
        //            print(UserDetailsViewModel)
        //            try ApiClient().activateSdk(UserDetailsViewModel, withCompletion: { (success, error, response) in
        //                if success {
        //                        print(response)
        //                }
        //                else {
        //                    if response != nil {
        //                        print(response)
        //                    }
        //                }
        //            })
        //
        //        }
        //        catch {
        //            print("Internet connection failed..!")
        //        }
        
        //        do{
        //            let UserDetailsViewModel = SdkClientApplication.init(userName: "Pradeepa", appName: "com.equitas.PurZ", instituteId: "999", channel: "equitas-purz", bankRefUrl: "https://www.equitasbank.com", userMail: "7402552366@test.com", mobile: "7402552366", userIdentity: "559279563")
        //            print(UserDetailsViewModel)
        //            try ApiClient().activateSdk(UserDetailsViewModel, withCompletion: { (true, error, reponse) in
        //                print(reponse)
        //            })
        //
        //        }
        //        catch {
        //            print("Internet connection failed..!")
        //        }
        
        
        
        
    }

    
//    func getVPAList() {
//        ApiClient()..getMappedVirtualAddresses { (success, error, response) in
//            if success {
//                if response != nil {
//                    print(response)
//                    do{
//                        // do whatever with jsonResult
//                        let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(response, options: NSJSONWritingOptions.PrettyPrinted)
//                        let json = JSON(data: jsonData)
//                        var vpaname = [String]()
//                        var vpaaccountdetail = [String]()
//                        var vpaprovidernames = [String]()
//                        print(json.arrayValue)
//                        for item in json.arrayValue {
//                            print(String(item))
//                            vpaname.append(item["va"].stringValue)
//                            vpaaccountdetail.append(String(item))
//                            vpaprovidernames.append(item["account-provider-name"].stringValue)
//                        }
//                        AccountConstant.vpanames = vpaname
//                        AccountConstant.vparesponses = vpaaccountdetail
//                        AccountConstant.vpaProviderName = vpaprovidernames
//                    }
//                    catch {
//                        print("error")
//                    }
//                    
//                }
//                else{
//                    if response != nil {
//                        print(response)
//                    }
//                }
//            }
//            self.activityIndicator.stopAnimating()
//            self.view.userInteractionEnabled = true
//        }
//    }
    
    func intialfunction(){
        tabBarController?.delegate = self
        self.navigationController?.navigationBarHidden = true
        addaccountview.layer.cornerRadius = 7
        addaccountview.clipsToBounds = true
        sendmoneyview.layer.cornerRadius = 7
        sendmoneyview.clipsToBounds = true
        collectrequestview.layer.cornerRadius = 7
        collectrequestview.clipsToBounds = true
        vpamanagerview.layer.cornerRadius = 7
        vpamanagerview.clipsToBounds = true
        beneficiaryview.layer.cornerRadius = 7
        beneficiaryview.clipsToBounds = true
        transactionsview.layer.cornerRadius = 7
        transactionsview.clipsToBounds = true
        generateQRview.layer.cornerRadius = 7
        generateQRview.clipsToBounds = true
        
        addaccountview.layer.borderWidth = 1
        addaccountview.layer.borderColor = UIColor.lightGrayColor().CGColor
        sendmoneyview.layer.borderWidth = 1
        sendmoneyview.layer.borderColor = UIColor.lightGrayColor().CGColor
        collectrequestview.layer.borderWidth = 1
        collectrequestview.layer.borderColor = UIColor.lightGrayColor().CGColor
        vpamanagerview.layer.borderWidth = 1
        vpamanagerview.layer.borderColor = UIColor.lightGrayColor().CGColor
        beneficiaryview.layer.borderWidth = 1
        beneficiaryview.layer.borderColor = UIColor.lightGrayColor().CGColor
        transactionsview.layer.borderWidth = 1
        transactionsview.layer.borderColor = UIColor.lightGrayColor().CGColor
        generateQRview.layer.borderWidth = 1
        generateQRview.layer.borderColor = UIColor.lightGrayColor().CGColor
        let addaccounttap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UPIHomeViewController.addaccountfunction))
        addaccountview.addGestureRecognizer(addaccounttap)
        let sendmoneytap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UPIHomeViewController.sendmoneyfunction))
        sendmoneyview.addGestureRecognizer(sendmoneytap)
        let collectrequesttap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UPIHomeViewController.collectMoneyFunc))
        collectrequestview.addGestureRecognizer(collectrequesttap)
        let vpamanagertap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UPIHomeViewController.VPAManagerFunc))
        vpamanagerview.addGestureRecognizer(vpamanagertap)
        let beneficiarytap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UPIHomeViewController.beneficiaryFunc))
        beneficiaryview.addGestureRecognizer(beneficiarytap)
        let transactionstap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UPIHomeViewController.transactionFunc))
        transactionsview.addGestureRecognizer(transactionstap)
        let generateQRtap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UPIHomeViewController.generateQRFunc))
        generateQRview.addGestureRecognizer(generateQRtap)
    }
    
    func getUserBankAccountList(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let select = "SELECT * FROM ACCOUNTS"
            let result:FMResultSet = purzDB.executeQuery(select,
                                                         withArgumentsInArray: nil)
            while(result.next()){
                accountNo.append(result.stringForColumn("ACCOUNT_NO"))
                
            }
            
        }
        purzDB.close()
        
    }
    
    func collectMoneyFunc(){
        if self.accountNo.count == 0 {
            AddAccountAlert()
        }
        else {
            self.performSegueWithIdentifier("UPI_CollectMoney", sender: self)
        }
        
    }
    func VPAManagerFunc(){
        if self.accountNo.count == 0 {
            AddAccountAlert()
        }
        else {
            self.performSegueWithIdentifier("UPIHome_VPAManager", sender: self)
        }
        
    }
    func generateQRFunc(){
        if self.accountNo.count == 0 {
            AddAccountAlert()
        }
        else {
            self.performSegueWithIdentifier("UPI_GenerateQR", sender: self)
        }
        
    }
    
    func beneficiaryFunc(){
        if self.accountNo.count == 0 {
            AddAccountAlert()
        }
        else {
            self.performSegueWithIdentifier("UPIHome_Beneficiary", sender: self)
        }
        
    }
    
    
    func addaccountfunction(){
        
                   self.performSegueWithIdentifier("AddAccount_to_accounts", sender: self)
          }
    func transactionFunc(){
        if self.accountNo.count == 0 {
            AddAccountAlert()
        }
        else {
            self.performSegueWithIdentifier("UPIHome_Transaction", sender: self)
        }
    
    }
    
    func sendmoneyfunction() {
        if self.accountNo.count == 0 {
            AddAccountAlert()
        }
        else {
            self.performSegueWithIdentifier("UPI_SendMoney", sender: self)
        }
    }
    
    func AddAccountAlert(){
                                        var alertController:UIAlertController?
                                alertController?.view.tintColor = UIColor.blackColor()
                                alertController = UIAlertController(title: "New user?",
                                                                    message: "You have no accounts yet!\nPlease add a bank account to continue...!",
                                                                    preferredStyle: .Alert)
                                let action1 = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
        
                                    })
                                let action2 = UIAlertAction(title: "ADD ACCOUNT", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
        
                                    self!.performSegueWithIdentifier("AddAccount_to_accounts", sender: self)
        
                                    })
        
        
                                alertController?.addAction(action2)
                                alertController?.addAction(action1)
                                self.presentViewController(alertController!, animated: true, completion: nil)
        
        
        

        
    }
    

    
    
    
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    
    
}
