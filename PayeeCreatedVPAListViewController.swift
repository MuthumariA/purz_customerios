//
//  PayeeCreatedVPAListViewController.swift
//  purZ
//
//  Created by Vertace on 28/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class PayeeCreatedVPAListViewController: UIViewController,UITableViewDataSource, UITableViewDelegate, UITabBarControllerDelegate {
   
    var registeredName = [String]()
    var vpa = [String]()
    var contact_PayeeName = ""
    var contact_VPA = ""
    var vpa_id = [String]()
    
    @IBOutlet weak var tableView: UITableView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.delegate = self
        PayeeVPAList()
    }
    
    @IBAction func backBtnAction(sender: AnyObject) {
        if Appconstant.send_Money
        {
            self.performSegueWithIdentifier("To_SendVPA", sender: self)
            Appconstant.send_Money = false
        }
        else if Appconstant.collect_Money
        {
            self.performSegueWithIdentifier("To_CollectMoney", sender: self)
        Appconstant.collect_Money = false
        }
        else
        {
            self.performSegueWithIdentifier("back_to_Beneficiary", sender: self)
            
        }
        
    }
    
    @IBAction func closeBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "Are you sure you want to delete?",
                                            message: "",
                                            preferredStyle: .Alert)
        
        let action1 = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            
            if purzDB.open() {
            var deleteSQL = ""
            if self!.vpa.count == 1 {
                deleteSQL = "DELETE FROM BENEFICIARY_VPA"
            }
            else {
//                deleteSQL =  "DELETE FROM BENEFICIARY_VPA WHERE VPA = " + self.vpa[indexPath.row]
                deleteSQL =  "DELETE FROM BENEFICIARY_VPA WHERE ID = " + self!.vpa_id[indexPath.row]
            }            
           
                let result = purzDB.executeUpdate(deleteSQL,
                    withArgumentsInArray: nil)
                
                if !result {
                    //   status.text = "Failed to add contact"
                    print("Error: \(purzDB.lastErrorMessage())")
                    self!.presentViewController(Alert().alert("VPA not deleted",message: ""), animated: true, completion: nil)
                }
                else
                {
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self!.presentViewController(Alert().alert("VPA deleted successfully",message: ""), animated: true, completion: nil)
                        self!.vpa.removeAll()
                        self!.registeredName.removeAll()
                        self!.PayeeVPAList()
                    }
                }
            }
            
            })
            
            let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                
                })
            
            
            alertController?.addAction(action1)
            alertController?.addAction(action2)
            self.presentViewController(alertController!, animated: true, completion: nil)
            
            
            
        }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func PayeeVPAList()
    {
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        if purzDB.open() {
            let select = "SELECT * FROM BENEFICIARY_VPA"
            let result:FMResultSet = purzDB.executeQuery(select,
                                                         withArgumentsInArray: nil)
            while(result.next()){
                registeredName.append(result.stringForColumn("REGISTERED_NAME"))
                vpa.append(result.stringForColumn("VPA"))
                vpa_id.append(result.stringForColumn("ID"))
            }
            self.tableView.reloadData()
        }
        purzDB.close()
        
        
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("payeeCreatedVPA", forIndexPath: indexPath) as UITableViewCell!
        
        
        let VPA = cell.viewWithTag(1) as! UILabel
        let RegisteredName = cell.viewWithTag(2) as! UILabel
        
        VPA.text = vpa[indexPath.row]
        RegisteredName.text = registeredName[indexPath.row]
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return vpa.count
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        contact_PayeeName = registeredName[indexPath.row]
        contact_VPA = vpa[indexPath.row]
        if Appconstant.send_Money
        {
         Appconstant.send_Money = false
            performSegueWithIdentifier("To_SendVPA", sender: self)
            
        }
        else if Appconstant.collect_Money

        {
             Appconstant.collect_Money = false
           
            performSegueWithIdentifier("To_CollectMoney", sender: self)
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "B_VPA_to_createVPA" {
            let nextvc = segue.destinationViewController as! AddVPAcontactViewController
            nextvc.existingVPA = vpa
        }
      else if(segue.identifier == "To_SendVPA") {
                let nextview = segue.destinationViewController as! sendMoneytoVPAViewController
                
                nextview.send_Contact_PayeeName = contact_PayeeName
                nextview.send_Contact_VPA = contact_VPA
                
            }
        
        else if(segue.identifier == "To_CollectMoney") {
                let nextview = segue.destinationViewController as! RequestMoneyViewController
                nextview.request_Contact_VPA = contact_VPA
                
            }
        }
 
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    
}
