//
//  BeneficiaryHomeViewController.swift
//  purZ
//
//  Created by Vertace on 24/07/17.
//  Copyright © 2017 Vertace. All rights reserved.
//

import UIKit

class BeneficiaryHomeViewController: UIViewController, UITabBarControllerDelegate {

    @IBOutlet weak var VPAView: UIView!
    @IBOutlet weak var AccountView: UIView!
    @IBOutlet weak var AadharView: UIView!
    @IBOutlet weak var MMIDView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialFunc()
    }
    func initialFunc()
    {
        tabBarController?.delegate = self
        VPAView.userInteractionEnabled = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "addVPAcontact")
        VPAView.addGestureRecognizer(tap)
        
        AccountView.userInteractionEnabled = true
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "addBankAccount")
        AccountView.addGestureRecognizer(tap1)
        
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "addMMIDContact")
        MMIDView.addGestureRecognizer(tap2)
        
        let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "addAadharContact")
        AadharView.addGestureRecognizer(tap3)
        
        // Do any additional setup after loading the view.
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, VPAView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine.backgroundColor = UIColor.lightGrayColor().CGColor
        VPAView.layer.addSublayer(bottomLine)
        
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, AccountView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine1.backgroundColor = UIColor.lightGrayColor().CGColor
        AccountView.layer.addSublayer(bottomLine1)
        
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, MMIDView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine2.backgroundColor = UIColor.lightGrayColor().CGColor
        MMIDView.layer.addSublayer(bottomLine2)
        
        let bottomLine3 = CALayer()
        bottomLine3.frame = CGRectMake(0.0, AadharView.frame.size.height - 1 , view.frame.size.width-35, 1.0)
        bottomLine3.backgroundColor = UIColor.lightGrayColor().CGColor
        AadharView.layer.addSublayer(bottomLine3)
    }
    func addAadharContact()
    {
        print("Tapped")
        self.performSegueWithIdentifier("To_AadharContact", sender: self)
        
        
    }
    func addMMIDContact()
    {
        print("Tapped")
        self.performSegueWithIdentifier("To_MMIDContact", sender: self)
        
        
    }
    
    func addVPAcontact()
    {
        print("Tapped")
        self.performSegueWithIdentifier("To_CreatedVPAList", sender: self)
        
        
    }
    func addBankAccount()
    {
        print("Tapped")
        self.performSegueWithIdentifier("To_BankAccountList", sender: self)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }

}
