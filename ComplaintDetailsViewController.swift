//
//  ComplaintDetailsViewController.swift
//  purZ
//
//  Created by Vertace on 27/11/17.
//  Copyright © 2017 Vertace. All rights reserved.
//



import UIKit

class ComplaintDetailsViewController: UIViewController, UITabBarControllerDelegate {
    
    @IBOutlet weak var remitter_lbl: UILabel!
    @IBOutlet weak var beneficiary_lbl: UILabel!
    @IBOutlet weak var amount_lbl: UILabel!
    @IBOutlet weak var type_lbl: UILabel!
    @IBOutlet weak var complaintRefNo_lbl: UILabel!
    @IBOutlet weak var complaintComments_lbl: UILabel!
    @IBOutlet weak var complaintStatus_lbl: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var remitter = ""
    var beneficiary = ""
    var amount = ""
    var type = ""
    var complaintRefNo = ""
    var complaintComments = ""
    var complaintStatus = ""
    var transactionID = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialize()
    }
    
    func initialize() {
        tabBarController?.delegate = self
        remitter_lbl.text = remitter
        beneficiary_lbl.text = beneficiary
        amount_lbl.text = amount
        type_lbl.text = type
        complaintRefNo_lbl.text = complaintRefNo
        complaintComments_lbl.text = complaintComments
        complaintStatus_lbl.text = complaintStatus
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func checkStatusBtnAction(sender: AnyObject) {
        activityIndicator.startAnimating()
        ApiClient().getComplaintStatus(transactionID) { (success, error, response) in
            self.activityIndicator.stopAnimating()
            if success {
                self.complaintStatus_lbl.text = String(response.valueForKey("complaintStatus")!)
                self.presentViewController(Alert().alert("Complaint Status updated successfully!",message: ""), animated: true, completion: nil)
            }
            else {
                self.presentViewController(Alert().alert("Complaint Status failed to update!",message: ""), animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func backBtnAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                //   Appconstant.pushhome = true
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            //            self.performSegueWithIdentifier("To_Home", sender: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if tabBarIndex == 1{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
