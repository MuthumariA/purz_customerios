
//
//  RechargeorPayBills.swift
//  Purz
//
//  Created by Vertace on 17/02/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class RechargeorPayBills: UIViewController, CNContactPickerDelegate, UITextFieldDelegate, UITextViewDelegate, UITabBarControllerDelegate ,UIPickerViewDelegate, UIPickerViewDataSource
{
    var recentTransaction = 0
    
    var billingUnits = [String]()
    
    //    var baseViewPicker = UIView()
    // var pickerView = UIPickerView()
    var selectedRowInBillingUnit = 0
    
    @IBOutlet weak var billingUnitText: UIButton!
    var pickerView = UIPickerView()
    var rechargeorPayBillItems1 = [rechargeorPayBill]()
    
    var mobileSeriesItems1 = [mobileSeries]()
    
    var rechargeItem = [ModelViewforRecharge]()
    var segmentno_provider = 0
    var provider = "Select Provider"
    var from_provider = false
    
    var segmentNumberSelectedInProvider = 0
    var segmentNumberSelectedInRecharge = 0
    var provider_Name = ""
    // var segmentno = ""
    var lan_provider = ""
    var ins_provider = ""
    var index_prov = 0
    
    var validString = ""
    var mobileno = ""
    var contactalert = false
    var pre_index = 0
    var webchecktxtFieldAmt = ""
    var repeat1_clicked = false
    var repeat2_clicked = false
    var tableView = UITableView()

    
    @IBOutlet weak var downArrowImage7: UIImageView!
    @IBOutlet weak var downArrowImage1: UIImageView!
    
    
    
    @IBOutlet weak var downArrowImage2: UIImageView!
    @IBOutlet weak var downArrowImage3: UIImageView!
    @IBOutlet weak var downArrowImage4: UIImageView!
    @IBOutlet weak var downArrowImage5: UIImageView!
    @IBOutlet weak var downArrowImage6: UIImageView!
    
    @IBOutlet weak var constraintgas: UIButton!
    @IBOutlet weak var constraintsEle: UIButton!
    @IBOutlet weak var constraintins: UIButton!
    @IBOutlet weak var constraintlan: UIButton!
    
    @IBOutlet weak var constraintIns2: UIButton!
    @IBOutlet weak var constraintele2: UIButton!
    
    @IBOutlet weak var View_pre: UIView!
    @IBOutlet weak var View_post: UIView!
    @IBOutlet weak var View_dth: UIView!
    @IBOutlet weak var View_elec: UIView!
    @IBOutlet weak var View_gas: UIView!
    @IBOutlet weak var View_ins: UIView!
    @IBOutlet weak var View_lan: UIView!
    
    @IBOutlet weak var wholeScrollview: UIScrollView!
    
    @IBOutlet weak var recentView: UIView!
    @IBOutlet weak var recentlbl: UILabel!
    @IBOutlet weak var recentView1: UIView!
    @IBOutlet weak var recentView2: UIView!
    
    @IBOutlet weak var repeatbtn1: UIButton!
    @IBOutlet weak var repeatbtn2: UIButton!
    
    @IBOutlet weak var recentMobileNolbl1: UILabel!
    @IBOutlet weak var recentAmtlbl1: UILabel!
    @IBOutlet weak var recentMobileNolbl2: UILabel!
    @IBOutlet weak var recentAmtlbl2: UILabel!
    
    var scrollHeight:CGFloat = 450
    var recent_prepaidNo = [String]()
    var recent_prepaidAmt = [String]()
    var recent_prepaidProvider = [String]()
    var recent_prepaidOperator = [String]()
   
    var recent_postpaidNo = [String]()
    var recent_postpaidAmt = [String]()
    var recent_postpaidProvider = [String]()
    var recent_postpaidOperator = [String]()
    
    var recent_dthNo = [String]()
    var recent_dthAmt = [String]()
    var recent_dthProvider = [String]()
    var recent_dthOperator = [String]()
   
   
    var recent_electricityNo = [String]()
    var recent_electricityAmt = [String]()
    var recent_electricityProvider = [String]()
    var recent_electricityAtherization2 = [String]()
    var recent_electricityAtherization3 = [String]()

    var recent_gasNo = [String]()
    var recent_gasAmt = [String]()
    var recent_gasProvider = [String]()
    var recent_gasAtherization2 = [String]()
    var recent_gasAtherization3 = [String]()

    var recent_landlineNo = [String]()
    var recent_landlineAmt = [String]()
    var recent_landlineProvider = [String]()
    var recent_landlineAtherization2 = [String]()
    var recent_landlineAtherization3 = [String]()

    var recent_insuranceNo = [String]()
    var recent_insuranceAmt = [String]()
    var recent_insuranceProvider = [String]()
    var recent_insuranceAtherization2 = [String]()
    var recent_insuranceAtherization3 = [String]()

    
    
    
    var selectedRow = 0
    var transactionId = ""
    var currentdate = ""
    var fetchedAmount = 0
    var fetchedNumber = 0
    var fetchedDate = 0
    var fetchedDueDate = 0
    
    var DateandTimeselected = ""
    
    var temp_lastDigitCharacterLength = 0
    var temp1_lastDigitCharacterLength = 0
    var temp2_lastDigitCharacterLength = 0
    var temp3_lastDigitCharacterLength = 0
    var temp22_lastDigitCharacterLength = 0
    
    
    @IBOutlet weak var Badgebtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var notificationBtn: UIButton!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var segmentController: UISegmentedControl!
    
    @IBOutlet weak var contactnotxtfield1: UITextField!
    @IBOutlet weak var providerbtn1: UIButton!
    @IBOutlet weak var operatortxtfield1: UIButton!
    
    @IBOutlet weak var amounttxtField1: UITextField!
    @IBOutlet weak var amounttxtField1_1: UITextField!
    @IBOutlet weak var browsePlanbtn1: UIButton!
    @IBOutlet weak var browsePlanbtn1_1: UIButton!
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label1_1: UILabel!
    
    
    
    @IBOutlet weak var contactnotxtfield2: UITextField!
    @IBOutlet weak var providerbtn2: UIButton!
    @IBOutlet weak var amounttxtField2: UITextField!
    @IBOutlet weak var amounttxtField2_2: UITextField!
    
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label2_2: UILabel!
    
    @IBOutlet weak var customerIdtxtfield3: UITextField!
    @IBOutlet weak var provider3: UIButton!
    @IBOutlet weak var operatortxtfield3: UIButton!
    @IBOutlet weak var amounttxtField3: UITextField!
    @IBOutlet weak var browsePlanbtn3: UIButton!
    
    
    @IBOutlet weak var providerbtn4: UIButton!
    @IBOutlet weak var customer_no4: UITextField!
    @IBOutlet weak var amounttxtField4: UITextField!
    @IBOutlet weak var phone_no4: UITextField!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var amounttxtfield4_4: UITextField!
    
    
    @IBOutlet weak var providerbtn5: UIButton!
    @IBOutlet weak var customeraccnoo5: UITextField!
    @IBOutlet weak var amounttxtField5: UITextField!
    
    
    @IBOutlet weak var providerbtn6: UIButton!
    @IBOutlet weak var policyno6: UITextField!
    @IBOutlet weak var dobtxtField6: UITextField!
    
    @IBOutlet weak var amounttxtField6: UITextField!
    @IBOutlet weak var amounttxtField6_6: UITextField!
    @IBOutlet weak var label6_6: UILabel!
    
    
    @IBOutlet weak var providerbtn7: UIButton!
    @IBOutlet weak var phonenotxtField7: UITextField!
    @IBOutlet weak var amounttxtField7: UITextField!
    @IBOutlet weak var amounttxtField7_7: UITextField!
    
    @IBOutlet weak var account_no7: UITextField!
    @IBOutlet weak var label7_7: UILabel!
    
    
    @IBOutlet weak var payBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var topupBtn: UIButton!
    @IBOutlet weak var specialBtn: UIButton!
    @IBOutlet weak var radioView: UIView!
    
    
    @IBOutlet weak var serviceType7: UITextField!
    @IBOutlet weak var amounttxtFieldAfterServiceType: UITextField!
    
    @IBOutlet weak var labelLineForAmount3: UILabel!
    @IBOutlet weak var constriantButtonAccount7: UIButton!
    @IBOutlet weak var constriantButtonServiceType7: UIButton!
    
    
    @IBOutlet weak var viewBottomHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var IsPostpaid = "N"
    var specialselected = false
    var fromoperatorview = false
    var selectedindex = 0
    var operator_amt = ""
    var operator_mobile = ""
    var operator_provider = ""
    var operatorname = ""
    var operator_operatorcode = ""
    var isradioviewhide = true
    var isradiospecial = false
    
    var toolbarView = UIView()
    var browsebtn1_hide = false
    var browsebtn3_hide = false
    
    var plan_mobileno = ""
    var plan_provider = ""
    var plan_operator = ""
    var plan_amt = ""
    var plan_radioview = false
    var plan_selectedspecial = false
    var plan_selectedindex = 0
    var fromplan = false
    
    var fromotp = false
    
    
    var characterCountLimit = 10
    var datePickerView:UIDatePicker!
    var phonenumber = [String]()
    var contact_number = ""
    var amount_value = ""
    var radioview = false
    var special = false
    var radio = 0
    
    
    var prepaid = false
    var postpaid = false
    var dth = false
    var electricity = false
    var gas = false
    var insurance = false
    var landline = false
    
    var recharge = true
    var pay = false
    
    
    
    var providername = ""
    var operatorcode = ""
    
    var fromoperatorview_Prepaid = false
    var fromoperatorview_Dth = false
    var isPrepaidClicked = false
    
    var toastLabel:UILabel!
    
    
    var segmentNumberforRecharge = 0
    
    var message = ""
    var segementNumberThroughSegue = false
    
    
    var prepaidbottomLabel = UILabel()
    var postpaidbottomLabel = UILabel()
    var dthbottomLabel = UILabel()
    var electricitybottomLabel = UILabel()
    var insurancebottomLabel = UILabel()
    var gasbottomLabel = UILabel()
    var landlinebottomLabel = UILabel()
    
    
    func getBillingUnit()
    {
        
        let location = NSBundle.mainBundle().pathForResource("Providers", ofType: "txt")
        
        //            let location = NSString(string:"/Users/vertace/Desktop/untitled folder/sample/Purz/Cippy/Providers.txt").stringByExpandingTildeInPath
        let fileContent = try? NSString(contentsOfFile: location!, encoding: NSUTF8StringEncoding)
        let data = fileContent!.dataUsingEncoding(NSUTF8StringEncoding)
        let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
        
        let json = JSON(data: data!)
        
        
        for provider in json.arrayValue {
            
            let providerlist = rechargeorPayBill(OperatorName: provider["OperatorName"].stringValue, OperatorCode: provider["OperatorCode"].stringValue,  OperatorType: provider["OperatorType"].stringValue,
                                                 IsSpecial: String(provider["IsSpecial"].doubleValue), MinimumBillAmount: provider["MinimumBillAmount"].stringValue, Authenticator1: provider["Authenticator1"].stringValue, Authenticator2: String(provider["Authenticator2"].doubleValue), Authenticator3: provider["Authenticator3"].stringValue)!
            self.rechargeorPayBillItems1 += [providerlist];
            if (provider["OperatorType"].stringValue == "ELECTRICITY" && provider["OperatorName"].stringValue == "MSEDCL Ltd.")
            {
                let authComponent1:NSArray = (provider["Authenticator2"].stringValue).componentsSeparatedByString("!!")
                //                print(authComponent1[0])
                //                print(authComponent1[1])
                //                print(authComponent1[2])
                //                print("billing Units")
                //                print(authComponent1[3])
                print(authComponent1[3].componentsSeparatedByString("~"))
                let units = authComponent1[3].componentsSeparatedByString("~")
                for(var i = 0; i<units.count; i++)
                {
                    billingUnits.append(units[i])
                }
                pickerView.reloadAllComponents()
                //                                billingUnits.append(authComponent1[3].componentsSeparatedByString("~"))
                //                               print(authComponent1[3].componentsSeparatedByString("~"))
                //                billingUnits.append(authComponent1[3].componentsSeparatedByString("~"))
                //                print("billing Units Array==")
                //                print(billingUnits)
                
            }
            
            
        }
    }
    
    @IBAction func billingUnitValueClicked(sender: UIButton) {
        
        //        billingUnitText.inputView = pickerView
        //        baseViewPicker.hidden = false
        pickerView.hidden = false
        toolbarView.hidden = false
        
        //        let barButtonDone = UIBarButtonItem.init(title: "done", style: UIBarButtonItemStyle.Plain, target: self, action: "doneClicked:")
        //        toolBar.setItems([barButtonDone], animated: true)
        //        barButtonDone.tintColor = UIColor.blackColor()
        print(toolbarView.frame.origin.y)
        
        //        self.baseViewPicker.addSubview(pickerView)
        
        
    }
    
    
    func doneClicked(sender: UIButton)
    {
        print("Clicked done")
        //        baseViewPicker.hidden = true
        pickerView.hidden = true
        toolbarView.hidden = true
        self.SelectedBillNumber()
    }
    
    func cancelClicked(sender: UIButton)
    {
        print("cancelClicked")
        //        baseViewPicker.hidden = true
        pickerView.hidden = true
        toolbarView.hidden = true
        //        self.dateSelected()
    }
    
    
    func SelectedBillNumber()
    {
        
        print("billingUnits[selectedRowInBillingUnit]")
        print(billingUnits[selectedRowInBillingUnit])
        billingUnitText.titleLabel?.text = billingUnits[selectedRowInBillingUnit]
        
        print(Operators.elect_operatorName)
        print(Operators.elect_operatorCode)
        print(selectedRow)
        print(Operators.elect_operatorCode[index_prov])
        var selectedBillerId = Operators.elect_operatorCode[index_prov]
        
        if(validString == "valid")
        {
            
            dispatch_async(dispatch_get_main_queue())
            {
                let fetchBill = FetchBillModel.init(entityId: Appconstant.customerid, dateTime: self.currentdate, billerId:selectedBillerId, authenticator1: self.customer_no4.text! , authenticator2: (self.billingUnitText.titleLabel?.text!)!, authenticator3:  "", transactionId:self.transactionId)!
                let serializedjson  = JSONSerializer.toJson(fetchBill)
                print(serializedjson)
                self.activityIndicator.startAnimating()
                self.sendrequesttoserverForFetchAmount(Appconstant.BASE_URL+Appconstant.URL_FETCH_BILL, values: serializedjson)
                self.activityIndicator.stopAnimating()
            }
            
        }
        else
        {
            
        }
        
    }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return billingUnits.count
        //billingUnits.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return billingUnits[row]
    }
    
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedRowInBillingUnit = row
        
        //        billingUnitText.text = "check"
        //billingUnits[row]
    }
    
    
    
    
    func setvalueforForgot()
    {
        print("Seg_value forgot==>>")
        print(Appconstant.segmentValueWhileClickingForgot)
        if(Appconstant.segmentValueWhileClickingForgot == 0)
        {
            self.segmentController.selectedSegmentIndex = 0
            
            
            self.showviewAndSegmentLabelBasedOnSegmentSelection(0)
            
            self.contactnotxtfield1.text = Appconstant.r_mobileNo
            self.providerbtn1.setTitle(Appconstant.r_provider, forState: .Normal)
            self.operatortxtfield1.setTitle(Appconstant.r_operator, forState: .Normal)
            self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Normal)
                        self.amounttxtField1.text = Appconstant.r_amount
            self.segementNumberThroughSegue = true
            Appconstant.fromsegue = false
            self.callserverforRecharge()
        }
            
        else if(Appconstant.segmentValueWhileClickingForgot == 1)
        {
            self.segmentController.selectedSegmentIndex = 1
            self.showviewAndSegmentLabelBasedOnSegmentSelection(1)
            
            self.contactnotxtfield2.text = Appconstant.r_mobileNo
            self.providerbtn2.setTitle(Appconstant.r_provider, forState: .Normal)
            self.amounttxtField2.text = Appconstant.r_amount
            self.segementNumberThroughSegue = true
            Appconstant.fromsegue = false
            self.callserverforRecharge()
        }
        else if(Appconstant.segmentValueWhileClickingForgot == 2)
        {
            self.segmentController.selectedSegmentIndex = 2
            self.showviewAndSegmentLabelBasedOnSegmentSelection(2)
            print("Second Appcons_values==>>")
            print(Appconstant.r_customerID)
            print(Appconstant.r_amount)
            self.customerIdtxtfield3.text = Appconstant.r_customerID
            self.provider3.setTitle(Appconstant.r_provider, forState: .Normal)
            self.operatortxtfield3.setTitle(Appconstant.r_operator, forState: .Normal)
            self.amounttxtField3.text = Appconstant.r_amount
            self.segementNumberThroughSegue = true
            Appconstant.fromsegue = false
            self.callserverforRecharge()
        }
        else if(Appconstant.segmentValueWhileClickingForgot == 3)
        {
            self.segmentController.selectedSegmentIndex = 3
            self.showviewAndSegmentLabelBasedOnSegmentSelection(3)
            print("electricity Appcons_values==>>")
            print(Appconstant.r_customerID)
            print(Appconstant.r_amount)
            print(Appconstant.r_provider)
            
            self.customer_no4.text = Appconstant.r_customerID
            self.providerbtn4.setTitle(Appconstant.r_provider, forState: .Normal)
            self.amounttxtField4.text = Appconstant.r_amount
            
            self.segementNumberThroughSegue = true
            Appconstant.fromsegue = false
            self.payBillForElectricity()
        }
        else if(Appconstant.segmentValueWhileClickingForgot == 4)
        {
            self.segmentController.selectedSegmentIndex = 4
            self.showviewAndSegmentLabelBasedOnSegmentSelection(4)
            print("gas Appcons_values==>>")
            print(Appconstant.r_customerID)
            print(Appconstant.r_amount)
            print(Appconstant.r_provider)
            
            self.customeraccnoo5.text = Appconstant.r_customerID
            self.providerbtn5.setTitle(Appconstant.r_provider, forState: .Normal)
            self.amounttxtField5.text = Appconstant.r_amount
            
            self.segementNumberThroughSegue = true
            Appconstant.fromsegue = false
            self.payBillForGas()
        }
        else if(Appconstant.segmentValueWhileClickingForgot == 5)
        {
            self.segmentController.selectedSegmentIndex = 5
            self.showviewAndSegmentLabelBasedOnSegmentSelection(5)
            print("insurance Appcons_values==>>")
            print(Appconstant.r_customerID)
            print(Appconstant.r_amount)
            print(Appconstant.r_provider)
            
            self.customeraccnoo5.text = Appconstant.r_customerID
            self.providerbtn6.setTitle(Appconstant.r_provider, forState: .Normal)
            self.amounttxtField6.text = Appconstant.r_amount
            self.segementNumberThroughSegue = true
            Appconstant.fromsegue = false
            self.payBillForInsurance()
        }
        else
        {
            self.segmentController.selectedSegmentIndex = 6
            self.showviewAndSegmentLabelBasedOnSegmentSelection(6)
            self.account_no7.text = Appconstant.r_mobileNo
            self.phonenotxtField7.text = Appconstant.r_phoneNo
            self.providerbtn7.setTitle(Appconstant.r_provider, forState: .Normal)
            self.amounttxtField7.text = Appconstant.r_amount
            Appconstant.fromsegue = false
            self.payBillForLandline()
        }
        //
        
    }
    
    
    func showviewAndSegmentLabelBasedOnSegmentSelection(value:Int)
    {
        print("Value-==>")
        print(value)
        prepaidbottomLabel.frame = CGRect(x:0,y:29,width: self.view.frame.size.width/4+1,height:1.5)
        print("Prepaid values segment==>>")
        print(prepaidbottomLabel.frame.size.width)
        print(prepaidbottomLabel.frame.origin.x)
        
        scrollView.addSubview(prepaidbottomLabel)
        
        postpaidbottomLabel.frame = CGRect(x:prepaidbottomLabel.frame.origin.x+prepaidbottomLabel.frame.size.width,y:29,width: self.view.frame.size.width/4,height:1.5)
        print("Postpaid values segment==>>")
        print(postpaidbottomLabel.frame.size.width)
        print(postpaidbottomLabel.frame.origin.x)
        scrollView.addSubview(postpaidbottomLabel)
        
        dthbottomLabel.frame = CGRect(x:postpaidbottomLabel.frame.origin.x+postpaidbottomLabel.frame.size.width,y:29,width: self.view.frame.size.width/4+1,height:1.5)
        print("Dth values segment==>>")
        print(dthbottomLabel.frame.size.width)
        print(dthbottomLabel.frame.origin.x)
        scrollView.addSubview(dthbottomLabel)
        
        electricitybottomLabel.frame = CGRect(x:dthbottomLabel.frame.origin.x+dthbottomLabel.frame.size.width,y:29,width: self.view.frame.size.width/4+21,height:1.5)
        print("Electricity values segment==>>")
        print(electricitybottomLabel.frame.size.width)
        print(electricitybottomLabel.frame.origin.x)
        scrollView.addSubview(electricitybottomLabel)
        
        insurancebottomLabel.frame = CGRect(x:electricitybottomLabel.frame.origin.x+electricitybottomLabel.frame.size.width,y:29,width: self.view.frame.size.width/4+1,height:1.5)
        scrollView.addSubview(insurancebottomLabel)
        
        gasbottomLabel.frame = CGRect(x:insurancebottomLabel.frame.origin.x+insurancebottomLabel.frame.size.width,y:29,width: self.view.frame.size.width/4+1,height:1.5)
        scrollView.addSubview(gasbottomLabel)
        
        landlinebottomLabel.frame = CGRect(x:gasbottomLabel.frame.origin.x+gasbottomLabel.frame.size.width,y:29,width: self.view.frame.size.width/4+1,height:1.5)
        scrollView.addSubview(landlinebottomLabel)
        
        
        if(value == 0)
        {
            segmentController.selectedSegmentIndex = 0
            payBtn.tag = 0
            prepaidbottomLabel.backgroundColor = UIColor.whiteColor()
            self.segmentController.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Selected)
            
            postpaidbottomLabel.backgroundColor  = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);
            dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            electricitybottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            
            View_pre.hidden = false
            View_post.hidden = true
            View_dth.hidden = true
            View_elec.hidden = true
            View_gas.hidden = true
            View_ins.hidden = true
            View_lan.hidden = true
            
        }
        else if(value == 1)
        {
            segmentController.selectedSegmentIndex = 1
            payBtn.tag = 1
            prepaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);
            postpaidbottomLabel.backgroundColor  = UIColor.whiteColor()
            dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            electricitybottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            
            View_pre.hidden = true
            View_post.hidden = false
            View_dth.hidden = true
            View_elec.hidden = true
            View_gas.hidden = true
            View_ins.hidden = true
            View_lan.hidden = true
            
        }
        else if(value == 2)
        {
            segmentController.selectedSegmentIndex = 2
            payBtn.tag = 2
            prepaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);
            postpaidbottomLabel.backgroundColor  = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            dthbottomLabel.backgroundColor = UIColor.whiteColor()
            electricitybottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            
            View_pre.hidden = true
            View_post.hidden = true
            View_dth.hidden = false
            View_elec.hidden = true
            View_gas.hidden = true
            View_ins.hidden = true
            View_lan.hidden = true
            
        }
        else if(value == 3)
        {
            segmentController.selectedSegmentIndex = 3
            payBtn.tag = 3
            prepaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);
            postpaidbottomLabel.backgroundColor  = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            electricitybottomLabel.backgroundColor = UIColor.whiteColor()
            insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            
            View_pre.hidden = true
            View_post.hidden = true
            View_dth.hidden = true
            View_elec.hidden = false
            View_gas.hidden = true
            View_ins.hidden = true
            View_lan.hidden = true
            
        }
        else if(value == 4)
        {
            segmentController.selectedSegmentIndex = 4
            payBtn.tag = 4
            prepaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);
            postpaidbottomLabel.backgroundColor  = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            electricitybottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            insurancebottomLabel.backgroundColor = UIColor.whiteColor()
            gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            
            View_pre.hidden = true
            View_post.hidden = true
            View_dth.hidden = true
            View_elec.hidden = true
            View_gas.hidden = false
            View_ins.hidden = true
            View_lan.hidden = true
            
        }
        else if(value == 5)
        {
            segmentController.selectedSegmentIndex = 5
            payBtn.tag = 5
            prepaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);
            postpaidbottomLabel.backgroundColor  = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            electricitybottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            gasbottomLabel.backgroundColor = UIColor.whiteColor()
            landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            
            View_pre.hidden = true
            View_post.hidden = true
            View_dth.hidden = true
            View_elec.hidden = true
            View_gas.hidden = true
            View_ins.hidden = false
            View_lan.hidden = true
            
        }
        else{
            segmentController.selectedSegmentIndex = 6
            payBtn.tag = 6
            prepaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);
            postpaidbottomLabel.backgroundColor  = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            electricitybottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
            landlinebottomLabel.backgroundColor = UIColor.whiteColor()
            
            View_pre.hidden = true
            View_post.hidden = true
            View_dth.hidden = true
            View_elec.hidden = true
            View_gas.hidden = true
            View_ins.hidden = true
            View_lan.hidden = false
        }
        
}

    func tappedMe1()
    {
        print("Tapped on Image")
        self.providerbtn1.sendActionsForControlEvents(.TouchUpInside);
        
    }
    func tappedMe2()
    {
        print("Tapped on Image")
        self.providerbtn2.sendActionsForControlEvents(.TouchUpInside);
        
    }
    func tappedMe3()
    {
        print("Tapped on Image")
        self.provider3.sendActionsForControlEvents(.TouchUpInside);
        
    }
    func tappedMe4()
    {
        print("Tapped on Image")
        self.providerbtn4.sendActionsForControlEvents(.TouchUpInside);
        
    }
    func tappedMe5()
    {
        print("Tapped on Image")
        self.providerbtn5.sendActionsForControlEvents(.TouchUpInside);
        
    }
    func tappedMe6()
    {
        print("Tapped on Image")
        self.providerbtn6.sendActionsForControlEvents(.TouchUpInside);
        
    }
    func tappedMe7()
    {
        print("Tapped on Image")
        self.providerbtn7.sendActionsForControlEvents(.TouchUpInside);
        
    }
    
    
    func someSelector()
    {
        
        self.activityIndicator.stopAnimating()
    }
    
    
    override func viewDidLoad() {
        //Intial Loading Symbol - 3 Seconds
        
        initialize()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
   //******
        if Appconstant.fromweb {
            Appconstant.fromweb = false
            self.activityIndicator.startAnimating()
            getbalancefromServer(Appconstant.BASE_URL+Appconstant.URL_FETCH_MULTI_BALANCE_INFO+Appconstant.customerid)
        }
        
        
        
        
    }
    
    func initialize() {
        amounttxtField1.delegate = self
        constraintele2.hidden = true
        constraintIns2.hidden = true
        constraintgas.hidden = true
        constraintins.hidden = true
        constraintlan.hidden = true
        constraintsEle.hidden = true
        pickerView.delegate = self
        pickerView.dataSource = self
        if segmentController.selectedSegmentIndex == 0
        {
            var recentTransaction = 0
           recentTransactionSetValue("PREPAID")
        }
        
        //        baseViewPicker = UIView(frame: CGRectMake(0, self.view.frame.size.height - 288, self.view.frame.size.width, 288))
        //        baseViewPicker.backgroundColor = UIColor.whiteColor()
        //        self.view.addSubview(baseViewPicker)
        
        pickerView.frame = CGRectMake(0,self.view.frame.size.height-244, self.view.frame.size.width, 244)
        pickerView.backgroundColor = UIColor.lightGrayColor()
        print(pickerView.frame.origin.y)
        print(pickerView.frame.size.height)
        self.view.addSubview(pickerView)
        
        
        toolbarView = UIView(frame: CGRectMake(0,self.view.frame.size.height-288,self.view.frame.size.width,44))
        toolbarView.backgroundColor = UIColor.grayColor()
        
        
        
        self.view.addSubview(toolbarView)
        
        
        let doneButton = UIButton.init(frame: CGRectMake(self.view.frame.size.width - 60,5,50,34))
        doneButton.addTarget(self, action: "doneClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        
        let cancelButton = UIButton.init(frame: CGRectMake(10,5,70,34))
        cancelButton.addTarget(self, action: "cancelClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        cancelButton.setTitle("Cancel", forState: UIControlState.Normal)
        toolbarView.addSubview(doneButton)
        toolbarView.addSubview(cancelButton)
        
        toolbarView.hidden = true
        pickerView.hidden = true
        if (Operators.post_operatorName.isEmpty || Operators.pre_operatorName.isEmpty || Operators.dth_operatorName.isEmpty || Operators.elect_operatorName.isEmpty || Operators.gas_operatorName.isEmpty || Operators.lan_operatorName.isEmpty || Mobilelist.mbl_Location.isEmpty || Mobilelist.mbl_Mobile.isEmpty || Mobilelist.mbl_Operator.isEmpty || Mobilelist.mbl_Srno.isEmpty)
        {
            
            self.activityIndicator.startAnimating()
            var timer = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: Selector("someSelector"), userInfo: nil, repeats: false)
            
        }
        
        
        //Provider Arrow Image Tap action
        
        let tapImage1 = UITapGestureRecognizer(target: self, action: "tappedMe1")
        downArrowImage1.addGestureRecognizer(tapImage1)
        downArrowImage1.userInteractionEnabled = true
        
        
        let tapImage2 = UITapGestureRecognizer(target: self, action: "tappedMe2")
        downArrowImage2.addGestureRecognizer(tapImage2)
        downArrowImage2.userInteractionEnabled = true
        
        
        let tapImage3 = UITapGestureRecognizer(target: self, action: "tappedMe3")
        downArrowImage3.addGestureRecognizer(tapImage3)
        downArrowImage3.userInteractionEnabled = true
        
        let tapImage4 = UITapGestureRecognizer(target: self, action: "tappedMe4")
        downArrowImage4.addGestureRecognizer(tapImage4)
        downArrowImage4.userInteractionEnabled = true
        
        let tapImage5 = UITapGestureRecognizer(target: self, action: "tappedMe5")
        downArrowImage5.addGestureRecognizer(tapImage5)
        downArrowImage5.userInteractionEnabled = true
        
        let tapImage6 = UITapGestureRecognizer(target: self, action: "tappedMe6")
        downArrowImage6.addGestureRecognizer(tapImage6)
        downArrowImage6.userInteractionEnabled = true
        
        let tapImage7 = UITapGestureRecognizer(target: self, action: "tappedMe7")
        downArrowImage7.addGestureRecognizer(tapImage7)
        downArrowImage7.userInteractionEnabled = true
        
        
        
        
        
        //For FORGGOT PIN WHILE TRANSACTION
        
        
        Appconstant.segmentedIndex = segmentNumberforRecharge
        print("PP_SEMENT Index ==>>")
        print( Appconstant.segmentedIndex)
        
        
        
        
        if (fromotp)
        {
            setvalueforForgot()
        }
        
        toastLabel = UILabel()
        toastLabel.frame = CGRectMake(self.view.frame.size.width/2 - 150, self.view.frame.size.height-150, 300, 105)
        toastLabel.backgroundColor = UIColor.blackColor()
        toastLabel.textColor = UIColor.whiteColor()
        toastLabel.textAlignment = NSTextAlignment.Center;
        //                        toastLabel.sizeToFit()
        toastLabel.numberOfLines = 5
        self.view.addSubview(toastLabel)
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        
        toastLabel.hidden = true
        self.segmentController.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.darkGrayColor()], forState: UIControlState.Normal)
        self.segmentController.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Selected)
        Appconstant.SideMenu = 0;
        super.viewDidLoad()
        
        self.currentDate()
        self.getUDID()
        
        customeraccnoo5.delegate = self
        phonenotxtField7.delegate = self
        
        dobtxtField6.delegate = self
        policyno6.delegate = self
        customer_no4.delegate = self
        contactnotxtfield1.delegate = self
        contactnotxtfield2.delegate = self
        phone_no4.delegate = self
        
        
        
        account_no7.delegate = self
        serviceType7.delegate = self
        
        
        print("Selected VAl==>>")
        print(segmentNumberSelectedInRecharge)
        self.segmentController.selectedSegmentIndex = segmentNumberSelectedInRecharge
        tabBarController?.delegate = self
        navigationController?.navigationBarHidden = true
        contactnotxtfield1.delegate = self
        contactnotxtfield2.delegate = self
        // segmentController.selectedSegmentIndex = 0
        
        
        
        if fromoperatorview_Prepaid
        {
            operatortxtfield1.userInteractionEnabled = true
            
            
        }
        else if fromoperatorview_Dth
        {
            operatortxtfield3.userInteractionEnabled = true
        }
            
        else
        {
            operatortxtfield1.userInteractionEnabled = false
            operatortxtfield3.userInteractionEnabled = false
        }
        
        
        
        activityIndicator.hidden = true
        
        if (segementNumberThroughSegue)
        {
            
        }
        else
        {
            
            View_post.hidden = true
            View_dth.hidden = true
            View_elec.hidden = true
            View_gas.hidden = true
            View_ins.hidden = true
            View_lan.hidden = true
        }
        
        amounttxtField1_1.hidden = true
        label1_1.hidden = true
        browsePlanbtn1.hidden = true
        browsePlanbtn1_1.hidden = true
        
        amounttxtField2_2.hidden = true
        label2_2.hidden = true
        
        browsePlanbtn3.hidden = true
        
        billingUnitText.hidden = true
        amounttxtfield4_4.hidden = true
        label4.hidden = true
        phone_no4.hidden = true
        
        amounttxtField4.userInteractionEnabled = false
        amounttxtfield4_4.userInteractionEnabled = false
        
        amounttxtField5.userInteractionEnabled = false
        
        
        dobtxtField6.hidden = true
        amounttxtField6.hidden = false
        amounttxtField6_6.hidden = true
        
        amounttxtField6.userInteractionEnabled = false
        amounttxtField6_6.userInteractionEnabled = false
        label6_6.hidden = true
        
        account_no7.hidden = true
        constriantButtonAccount7.hidden = true
        amounttxtField7.hidden = false
        amounttxtField7_7.hidden = true
        
        amounttxtField7_7.userInteractionEnabled = false
        amounttxtField7_7.userInteractionEnabled = false
        
        label7_7.hidden = true
        
        
        radioView.hidden = true
        
        if(radioview){
            if special{
                specialBtn.setImage(UIImage(named: "radio-on-button-1.png"), forState: .Normal)
                topupBtn.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
            }
            else{
                topupBtn.setImage(UIImage(named: "radio-on-button-1.png"), forState: .Normal)
                specialBtn.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
            }
        }
        
        Badgebtn.layer.cornerRadius  = self.Badgebtn.frame.size.height/2
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        self.view.addGestureRecognizer(tap)
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width*7 + 20 , height: 35)
        wholeScrollview.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        payBtn.layer.cornerRadius = 18
        payBtn.layer.borderWidth = 2.3;
        payBtn.layer.borderColor = UIColor(red: 98.0/255.0, green: 154.0/255.0, blue: 14.0/255.0, alpha: 1).CGColor
        
        cancelBtn.layer.cornerRadius = 18
        cancelBtn.layer.borderWidth = 2.3;
        cancelBtn.layer.borderColor = UIColor.redColor().CGColor
        if !fromoperatorview{
            print("========")
            print(selectedindex)
            
        }
        view.endEditing(true)
        payBtn.setTitle("RECHARGE", forState: .Normal)
        
        segmentController.setWidth(self.view.frame.size.width/4, forSegmentAtIndex: 0)
        
        prepaidbottomLabel.frame = CGRect(x:0,y:29,width: self.view.frame.size.width/4+1,height:1)
        
        if fromoperatorview_Prepaid
        {
            prepaidbottomLabel.backgroundColor = UIColor.whiteColor()
        }
        else{
            prepaidbottomLabel.backgroundColor  = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);
            
        }
        
        
        if selectedindex == 0
        {
            prepaidbottomLabel.backgroundColor = UIColor.whiteColor()
        }
        else
        {
            prepaidbottomLabel.backgroundColor  = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);
            
        }
        
        scrollView.addSubview(prepaidbottomLabel)
        
        print("prepaidblab==>>")
        print(prepaidbottomLabel.frame.origin.x)
        
        segmentController.setWidth(self.view.frame.size.width/4+1, forSegmentAtIndex: 1)
        postpaidbottomLabel.frame = CGRect(x:prepaidbottomLabel.frame.origin.x+prepaidbottomLabel.frame.size.width,y:29,width: self.view.frame.size.width/4,height:1)
        postpaidbottomLabel.backgroundColor  = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);         scrollView.addSubview(postpaidbottomLabel)
        print("postpaidlab==>>")
        print(postpaidbottomLabel.frame.origin.x)
        print(prepaidbottomLabel.frame.size.width)
        
        segmentController.setWidth(self.view.frame.size.width/4, forSegmentAtIndex: 2)
        dthbottomLabel.frame = CGRect(x:postpaidbottomLabel.frame.origin.x+postpaidbottomLabel.frame.size.width,y:29,width: self.view.frame.size.width/4+1,height:1)
        
        
        if fromoperatorview_Dth
        {
            dthbottomLabel.backgroundColor = UIColor.whiteColor()
        }
        else
        {
            dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        }
        
        if selectedindex == 2
        {
            dthbottomLabel.backgroundColor = UIColor.whiteColor()
        }
        else
        {
            dthbottomLabel.backgroundColor  = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);
        }
        
        scrollView.addSubview(dthbottomLabel)
        print("dthlab==>>")
        print(dthbottomLabel.frame.origin.x)
        
        print("Y values==>>")
        print(postpaidbottomLabel.frame.origin.x+postpaidbottomLabel.frame.size.height)
        
        segmentController.setWidth(self.view.frame.size.width/4+20, forSegmentAtIndex: 3)
        electricitybottomLabel.frame = CGRect(x:dthbottomLabel.frame.origin.x+dthbottomLabel.frame.size.width,y:29,width: self.view.frame.size.width/4+21,height:1)
        electricitybottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        scrollView.addSubview(electricitybottomLabel)
        print("eleclab==>>")
        print(electricitybottomLabel.frame.origin.x)
        
        
        segmentController.setWidth(self.view.frame.size.width/4, forSegmentAtIndex: 4)
        insurancebottomLabel.frame = CGRect(x:electricitybottomLabel.frame.origin.x+electricitybottomLabel.frame.size.width,y:29,width: self.view.frame.size.width/4+1,height:1)
        insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        scrollView.addSubview(insurancebottomLabel)
        
        
        segmentController.setWidth(self.view.frame.size.width/4, forSegmentAtIndex: 5)
        gasbottomLabel.frame = CGRect(x:insurancebottomLabel.frame.origin.x+insurancebottomLabel.frame.size.width,y:29,width: self.view.frame.size.width/4+1,height:1)
        gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        scrollView.addSubview(gasbottomLabel)
        
        segmentController.setWidth(self.view.frame.size.width/4, forSegmentAtIndex: 6)
        landlinebottomLabel.frame = CGRect(x:gasbottomLabel.frame.origin.x+gasbottomLabel.frame.size.width,y:29,width: self.view.frame.size.width/4+1,height:1)
        landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        scrollView.addSubview(landlinebottomLabel)
        
        if(Appconstant.notificationcount > 0){
            Badgebtn.hidden = false
            Badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
            Badgebtn.userInteractionEnabled = false
        }
        else{
            Badgebtn.hidden = true
        }
        
        if fromoperatorview{
            setvalues()
        }
        if fromplan{
            setvalue()
            
        }
        //        if segementNumberThroughSegue
        //        {
        //            setForgotValues()
        //        }
        
        if from_provider
        {
            if (segmentno_provider == 3)
            {
                segmentController.selectedSegmentIndex = 3
                thirdsegment()
                
            }
            else if (segmentno_provider == 0)
            {
                segmentController.selectedSegmentIndex = 0
                recentTransaction = 0
                zerosegment()
                
            }
            else if (segmentno_provider == 1)
            {
                segmentController.selectedSegmentIndex = 1
                 recentTransaction = 1
                firstsegment()
                
            }
            else if (segmentno_provider == 2)
            {
                segmentController.selectedSegmentIndex = 2
                 recentTransaction = 2
                secondsegment()
                
            }
            else if (segmentno_provider == 4)
            {
                segmentController.selectedSegmentIndex = 4
                 recentTransaction = 4
                fourthsegment()
                
            }
            else if (segmentno_provider == 5)
            {
                segmentController.selectedSegmentIndex = 5
                 recentTransaction = 5
                fifthsegment()
                
            }
            else if (segmentno_provider == 6)
            {
                segmentController.selectedSegmentIndex = 6
                 recentTransaction = 6
                sixthsegment()
            }
            
        }
        
        
    }
    

    
    
    @IBAction func textFieldEditing(sender: UITextField)
    {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.maximumDate = NSDate()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action:Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        
    }
    
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        print(sender.date)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEE MM yyyy"
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let dateFormatter1 = NSDateFormatter()
        dateFormatter1.dateFormat = "yyyyMMddHHmmss"///this is you want to convert format
        dateFormatter1.timeZone = NSTimeZone(name: "UTC")
        let DateandTimeselected = dateFormatter1.stringFromDate(sender.date)
        
        print("DateandTimeSelected")
        print(DateandTimeselected)
        
        dobtxtField6.text = dateFormatter.stringFromDate(sender.date)
        if(dobtxtField6.text != ""){
            datepickerfunction()
        }
        
    }
    
    func datepickerfunction()
    {
        constraintIns2.hidden = false
        // textFieldToChange.keyboardType = UIKeyboardType.NumberPad
        if(policyno6 == "")
        {
        }
        else
        {
            validString = ""
            
            
            characterCountLimit = 50
            let userEnteredString11 = dobtxtField6.text
            //        let range = dobtxtField6.text?.characters.count
            //                var newString11 = (userEnteredString11! as NSString).stringByReplacingCharactersInRange((range,0), withString: dobtxtField6.text) as NSString
            
            
            
            if(dobtxtField6.text?.characters.count >= 1 )
            {
                validString = ""
                print(Operators.ins_Authenticator2[index_prov])
                
                let authComponent2:NSArray = Operators.ins_Authenticator2[index_prov].componentsSeparatedByString("!!")
                print(authComponent2[0])
                print(authComponent2[1])
                print(authComponent2[2])
                //                    validString = self.checkValidExpression(dobtxtField6.text! as String,myPattern: authComponent2[1] as! String)
                
                print(dobtxtField6.text?.characters.count)
                
                if(dobtxtField6.text?.characters.count == temp1_lastDigitCharacterLength)
                {
                    
                }
                else
                {
                    
                    if(validString == "")
                    {
                        print("valid")
                        constraintIns2.hidden = true
                    }
                    else
                    {
                        constraintIns2.hidden = false
                        temp_lastDigitCharacterLength = (dobtxtField6.text?.characters.count)!
                        
                    }
                }
                
            }
            if(dobtxtField6.text?.characters.count == 10)
            {
                print(Operators.ins_operatorName)
                print(Operators.ins_operatorCode)
                print(selectedRow)
                print(Operators.ins_operatorCode[index_prov])
                var selectedBillerId = Operators.ins_operatorCode[index_prov]
                
                dispatch_async(dispatch_get_main_queue())
                {
                    let fetchBill = FetchBillModel.init(entityId: Appconstant.customerid, dateTime: self.currentdate, billerId:selectedBillerId, authenticator1: self.policyno6.text!, authenticator2: self.dobtxtField6.text! as String, authenticator3:  "", transactionId:self.transactionId)!
                    let serializedjson  = JSONSerializer.toJson(fetchBill)
                    print(serializedjson)
                    self.activityIndicator.startAnimating()
                    self.sendrequesttoserverForFetchAmount(Appconstant.BASE_URL+Appconstant.URL_FETCH_BILL, values: serializedjson)
                }
                
            }
            
        }
        
        
        
    }
    
    
    func getbalancefromServer(url: String){
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                
                for item in json["result"].arrayValue{
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        
                        if item["balance"].doubleValue >= Double(self.webchecktxtFieldAmt) {
                            if self.payBtn.tag == 0 || self.payBtn.tag == 1 || self.payBtn.tag == 2 {
                                self.callserverforRecharge()
                            }
                            else if self.payBtn.tag == 3
                            {
                                self.payBillForElectricity()
                            }
                            else if self.payBtn.tag == 4
                            {
                                self.payBillForGas()
                            }
                            else if self.payBtn.tag == 5
                            {
                                self.payBillForInsurance()
                            }
                            else if self.payBtn.tag == 6
                            {
                                self.payBillForLandline()
                            }

                        }
                        else {
                            dispatch_async(dispatch_get_main_queue()) {
                                self.activityIndicator.stopAnimating()
                                self.presentViewController(Alert().alert("Oops! Insufficient Funds..Add some money now, it’s simple!", message: ""),animated: true,completion: nil)
                            }
                        }
                        
                        
                    }
                }
                
            }
        }
        task.resume()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("Selected VAl==>>")
        print(segmentNumberSelectedInRecharge)
        //        self.segmentController.selectedSegmentIndex = segmentNumberSelectedInRecharge
        getplanlistfunc()
    }
    
    
    //    func setForgotValues(){
    //
    //
    //        segmentController.selectedSegmentIndex = Appconstant.segmentedIndex
    //
    //        if segmentController.selectedSegmentIndex == 0{
    //            segmentfuncZero()
    //            payBtn.tag = 0
    ////            self.amounttxtField1.text = operator_amt
    ////            self.contactnotxtfield1.text = operator_mobile
    ////            self.providerbtn1.setTitle(operator_provider, forState: .Normal)
    ////            operatortxtfield1.setTitle(operatorname, forState: .Normal)
    ////            browsePlanbtn3.hidden = true
    ////            browsePlanbtn1.hidden = browsebtn1_hide
    //            //            (segmentController.subviews[2] as UIView).tintColor = UIColor(red: 250.0/255.0, green: 194.0/255.0, blue: 35.0/255.0, alpha: 1)
    //        }
    //        else if segmentController.selectedSegmentIndex == 1{
    //            segmentfuncTwo()
    //             payBtn.tag = 1
    //            self.amounttxtField3.text = operator_amt
    //            self.customerIdtxtfield3.text = operator_mobile
    //            self.provider3.setTitle(operator_provider, forState: .Normal)
    //            operatortxtfield3.setTitle(operatorname, forState: .Normal)
    //            browsePlanbtn3.hidden = false
    //            browsePlanbtn3.hidden = browsebtn3_hide
    //            browsePlanbtn1.hidden = true
    //        }
    //
    //        else if segmentController.selectedSegmentIndex == 2{
    //            segmentfuncTwo()
    //             payBtn.tag = 2
    //            self.amounttxtField3.text = operator_amt
    //            self.customerIdtxtfield3.text = operator_mobile
    //            self.provider3.setTitle(operator_provider, forState: .Normal)
    //            operatortxtfield3.setTitle(operatorname, forState: .Normal)
    //            browsePlanbtn3.hidden = false
    //            browsePlanbtn3.hidden = browsebtn3_hide
    //            browsePlanbtn1.hidden = true
    //        }
    //
    //
    //
    //        if isradioviewhide{
    //            radioView.hidden = true
    //        }
    //        else{
    //            radioView.hidden = false
    //        }
    //    }
    //
    
    
    func dismissKeyboard(){
        //        self.view.frame.origin.y = 0
        self.view.endEditing(true)
        toolbarView.hidden = true
        pickerView.hidden = true
        //        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 400)
    }
    
    func setvalues(){
        
        
        segmentController.selectedSegmentIndex = selectedindex
        print("checking\n\(segmentController.selectedSegmentIndex)\n\(selectedindex)")
        
        if segmentController.selectedSegmentIndex == 0{
            segmentfuncZero()
            payBtn.tag = 0
            self.amounttxtField1.text = operator_amt
            self.contactnotxtfield1.text = operator_mobile
            self.providerbtn1.setTitle(operator_provider, forState: .Normal)
            operatortxtfield1.setTitle(operatorname, forState: .Normal)
            self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Normal)
            self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Selected)
            browsePlanbtn3.hidden = true
            browsePlanbtn1.hidden = browsebtn1_hide
            
            //            (segmentController.subviews[2] as UIView).tintColor = UIColor(red: 250.0/255.0, green: 194.0/255.0, blue: 35.0/255.0, alpha: 1)
        }
        else if segmentController.selectedSegmentIndex == 2{
            segmentfuncTwo()
            self.recentTransaction = 2
            
            if self.recent_dthNo.count == 1
            {
                self.setSingleRecentTransaction()
            }
            else if self.recent_dthNo.count > 1
            {
                self.setTwoRecentTransaction();
                
            }
            else
            {
                self.recentTransactionSetValue("DTH")
            }
            payBtn.tag = 2
            self.amounttxtField3.text = operator_amt
            self.customerIdtxtfield3.text = operator_mobile
            self.provider3.setTitle(operator_provider, forState: .Normal)
            operatortxtfield3.setTitle(operatorname, forState: .Normal)
            self.operatortxtfield3.setTitleColor(UIColor.blackColor(), forState: .Normal)
            self.operatortxtfield3.setTitleColor(UIColor.blackColor(), forState: .Selected)
            browsePlanbtn3.hidden = false
            browsePlanbtn3.hidden = browsebtn3_hide
            browsePlanbtn1.hidden = true
        }
        if isradioviewhide{
            radioView.hidden = true
        }
        else{
            
            radioView.hidden = false
            if isradiospecial {
                specialfunc()
            }
            else {
                topupfunction()
            }
            
        }
}
    func setvalue(){
        
        segmentController.selectedSegmentIndex = plan_selectedindex
        if isradioviewhide{
            radioView.hidden = true
            
        }
        else{
            
            radioView.hidden = false
            if isradiospecial {
                specialfunc()
            }
            else {
                topupfunction()
            }
        }
        
        if segmentController.selectedSegmentIndex == 0{
            payBtn.tag = 0
            browsePlanbtn1.hidden = false
            self.amounttxtField1.text = plan_amt
            self.contactnotxtfield1.text = plan_mobileno
            self.providerbtn1.setTitle(plan_provider, forState: .Normal)
            operatortxtfield1.setTitle(plan_operator, forState: .Normal)
            self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Normal)
            self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Selected)
            //            self.segmentno = segmentno
            if plan_radioview{
                if plan_selectedspecial{
                    
                    radio = 1
                    topupBtn.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
                    specialBtn.setImage(UIImage(named: "radio-on-button-1.png"), forState: .Normal)
                }
                else{
                    radio = 0
                    topupBtn.setImage(UIImage(named: "radio-on-button-1.png"), forState: .Normal)
                    specialBtn.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
                }
            }
        }
        else if segmentController.selectedSegmentIndex == 2{
            payBtn.tag = 2
            segmentfuncTwo()
            browsePlanbtn3.hidden = false
            self.amounttxtField3.text = plan_amt
            self.customerIdtxtfield3.text = plan_mobileno
            self.provider3.setTitle(plan_provider, forState: .Normal)
            operatortxtfield3.setTitle(plan_operator, forState: .Normal)
            self.operatortxtfield3.setTitleColor(UIColor.blackColor(), forState: .Normal)
            self.operatortxtfield3.setTitleColor(UIColor.blackColor(), forState: .Selected)
        }
        
        
        
    }
    
    func getplanlistfunc(){
        if (Operators.post_operatorName.isEmpty || Operators.pre_operatorName.isEmpty || Operators.dth_operatorName.isEmpty || Operators.elect_operatorName.isEmpty || Operators.gas_operatorName.isEmpty || Operators.lan_operatorName.isEmpty)
        {
            
            let location = NSBundle.mainBundle().pathForResource("Providers", ofType: "txt")
            
            //            let location = NSString(string:"/Users/vertace/Desktop/untitled folder/sample/Purz/Cippy/Providers.txt").stringByExpandingTildeInPath
            let fileContent = try? NSString(contentsOfFile: location!, encoding: NSUTF8StringEncoding)
            let data = fileContent!.dataUsingEncoding(NSUTF8StringEncoding)
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            
            let json = JSON(data: data!)
            
            
            for provider in json.arrayValue {
                
                let providerlist = rechargeorPayBill(OperatorName: provider["OperatorName"].stringValue, OperatorCode: provider["OperatorCode"].stringValue,  OperatorType: provider["OperatorType"].stringValue,
                                                     IsSpecial: String(provider["IsSpecial"].doubleValue), MinimumBillAmount: provider["MinimumBillAmount"].stringValue, Authenticator1: provider["Authenticator1"].stringValue, Authenticator2: String(provider["Authenticator2"].doubleValue), Authenticator3: provider["Authenticator3"].stringValue)!
                self.rechargeorPayBillItems1 += [providerlist];
                
                
                if provider["OperatorType"].stringValue == "PREPAID"{
                    Operators.pre_operatorName.append(provider["OperatorName"].stringValue)
                    Operators.pre_operatorCode.append(provider["OperatorCode"].stringValue)
                    Operators.pre_operatorType.append(provider["OperatorType"].stringValue)
                    Operators.pre_special.append(provider["IsSpecial"].boolValue)
                    Operators.pre_MinBillAmount.append(provider["MinimumBillAmount"].stringValue)
                    Operators.pre_Authenticator1.append(provider["Authenticator1"].stringValue)
                    Operators.pre_Authenticator2.append(provider["Authenticator2"].stringValue)
                    Operators.pre_Authenticator3.append(provider["Authenticator3"].stringValue)
                    
                    
                    
                }
                else if provider["OperatorType"].stringValue == "POSTPAID"
                {
                    Operators.post_operatorName.append(provider["OperatorName"].stringValue)
                    Operators.post_operatorCode.append(provider["OperatorCode"].stringValue)
                    Operators.post_operatorType.append(provider["OperatorType"].stringValue)
                    Operators.post_special.append(provider["IsSpecial"].boolValue)
                    Operators.post_MinBillAmount.append(provider["MinimumBillAmount"].stringValue)
                    Operators.post_Authenticator1.append(provider["Authenticator1"].stringValue)
                    Operators.post_Authenticator2.append(provider["Authenticator2"].stringValue)
                    Operators.post_Authenticator3.append(provider["Authenticator3"].stringValue)
                    
                }
                else if provider["OperatorType"].stringValue == "DTH"
                {
                    Operators.dth_operatorName.append(provider["OperatorName"].stringValue)
                    Operators.dth_operatorCode.append(provider["OperatorCode"].stringValue)
                    Operators.dth_operatorType.append(provider["OperatorType"].stringValue)
                    Operators.dth_special.append(provider["IsSpecial"].boolValue)
                    Operators.dth_MinBillAmount.append(provider["MinimumBillAmount"].stringValue)
                    Operators.dth_Authenticator1.append(provider["Authenticator1"].stringValue)
                    Operators.dth_Authenticator2.append(provider["Authenticator2"].stringValue)
                    Operators.dth_Authenticator3.append(provider["Authenticator3"].stringValue)
                    
                    
                }
                else if provider["OperatorType"].stringValue == "ELECTRICITY"
                {
                    Operators.elect_operatorName.append(provider["OperatorName"].stringValue)
                    Operators.elect_operatorCode.append(provider["OperatorCode"].stringValue)
                    Operators.elect_operatorType.append(provider["OperatorType"].stringValue)
                    Operators.elect_special.append(provider["IsSpecial"].boolValue)
                    Operators.elect_MinBillAmount.append(provider["MinimumBillAmount"].stringValue)
                    Operators.elect_Authenticator1.append(provider["Authenticator1"].stringValue)
                    Operators.elect_Authenticator2.append(provider["Authenticator2"].stringValue)
                    Operators.elect_Authenticator3.append(provider["Authenticator3"].stringValue)
                    
                    
                }
                else if provider["OperatorType"].stringValue == "GAS"
                {
                    Operators.gas_operatorName.append(provider["OperatorName"].stringValue)
                    Operators.gas_operatorCode.append(provider["OperatorCode"].stringValue)
                    Operators.gas_operatorType.append(provider["OperatorType"].stringValue)
                    Operators.gas_special.append(provider["IsSpecial"].boolValue)
                    Operators.gas_MinBillAmount.append(provider["MinimumBillAmount"].stringValue)
                    Operators.gas_Authenticator1.append(provider["Authenticator1"].stringValue)
                    Operators.gas_Authenticator2.append(provider["Authenticator2"].stringValue)
                    Operators.gas_Authenticator3.append(provider["Authenticator3"].stringValue)
                }
                else if provider["OperatorType"].stringValue == "INSURANCE"
                {
                    Operators.ins_operatorCode.append(provider["OperatorCode"].stringValue)
                    Operators.ins_operatorName.append(provider["OperatorName"].stringValue)
                    Operators.ins_operatorType.append(provider["OperatorType"].stringValue)
                    Operators.ins_special.append(provider["IsSpecial"].boolValue)
                    Operators.ins_MinBillAmount.append(provider["MinimumBillAmount"].stringValue)
                    Operators.ins_Authenticator1.append(provider["Authenticator1"].stringValue)
                    Operators.ins_Authenticator2.append(provider["Authenticator2"].stringValue)
                    Operators.ins_Authenticator3.append(provider["Authenticator3"].stringValue)
                }
                else if provider["OperatorType"].stringValue == "LANDLINE"
                {
                    Operators.lan_operatorCode.append(provider["OperatorCode"].stringValue)
                    Operators.lan_operatorName.append(provider["OperatorName"].stringValue)
                    Operators.lan_operatorType.append(provider["OperatorType"].stringValue)
                    Operators.lan_special.append(provider["IsSpecial"].boolValue)
                    Operators.lan_MinBillAmount.append(provider["MinimumBillAmount"].stringValue)
                    Operators.lan_Authenticator1.append(provider["Authenticator1"].stringValue)
                    Operators.lan_Authenticator2.append(provider["Authenticator2"].stringValue)
                    Operators.pre_Authenticator3.append(provider["Authenticator3"].stringValue)
                }
                
                
            }
        }
            
        else{
            
        }
        
        if (Mobilelist.mbl_Location.isEmpty || Mobilelist.mbl_Mobile.isEmpty || Mobilelist.mbl_Operator.isEmpty || Mobilelist.mbl_Srno.isEmpty)
        {
            let location1 = NSBundle.mainBundle().pathForResource("Mobileseries", ofType: "txt")
            
            //            let location = NSString(string:"/Users/vertace/Downloads/Mobileseries.txt").stringByExpandingTildeInPath
            let fileContent1 = try? NSString(contentsOfFile: location1!, encoding: NSUTF8StringEncoding)
            //            print(fileContent1)
            let data1 = fileContent1!.dataUsingEncoding(NSUTF8StringEncoding)
            let responseString1 = NSString(data: data1!, encoding: NSUTF8StringEncoding)
            let jsonMbl = JSON(data: data1!)
            
            for mobiles in jsonMbl.arrayValue  {
                
                let mobilelist = mobileSeries(Srno: mobiles["Srno"].stringValue, Mobile: mobiles["Mobile"].stringValue,  Operator: mobiles["Operator"].stringValue,
                                              Location: String(mobiles["Location"].doubleValue))!
                self.mobileSeriesItems1 += [mobilelist];
                
                
                Mobilelist.mbl_Srno.append(mobiles["Srno"].stringValue)
                Mobilelist.mbl_Mobile.append(mobiles["Mobile"].stringValue)
                Mobilelist.mbl_Operator.append(mobiles["Operator"].stringValue)
                Mobilelist.mbl_Location.append(mobiles["Location"].stringValue)
                
            }
            
            
            
        }
    }
    
    
    
    
    func callserverforRecharge(){
        if segmentController.selectedSegmentIndex == 0{
            if self.contactnotxtfield1.text!.isEmpty{
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Seems you forgot to fill some field.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let desc = "Prepaid Recharge for "+self.contactnotxtfield1.text!
                var passcode = Appconstant.pwd
                let checkpwd: Int = Int(passcode.substringWithRange(passcode.startIndex.advancedBy(0)..<passcode.startIndex.advancedBy(1)))!
                print(checkpwd)
                if checkpwd == 0 {
                    passcode = "1" + passcode
                }
                
                let rechargeItem = ModelViewforRecharge.init(isPostpaid:IsPostpaid,amount:self.amounttxtField1.text!,business:"EQWALLET",businessEntityId:"EQWALLET",comment:"Recharge",description:desc,externalTransactionId:"123456",fromEntityId:Appconstant.customerid,mobile:self.contactnotxtfield1.text!,productId:"GENERAL",provider:(providerbtn1.titleLabel?.text)!, serviceType:"M",sessionId:"",special:specialselected,toEntityId:Appconstant.TOENTITYID,transactionOrigin:"MOBILE",transactionType:"TPP",yapcode:passcode)!
                let serializedjson  = JSONSerializer.toJson(rechargeItem)
                print(serializedjson)
                self.activityIndicator.startAnimating()
                print("Recharge URL")
                print(Appconstant.BASE_URL+Appconstant.URL_RECHARGE_PAY_BILL, separator: serializedjson)
                self.sendrequesttoserverForRecharge(Appconstant.BASE_URL+Appconstant.URL_RECHARGE_PAY_BILL, values: serializedjson)
            }
        }
        else if segmentController.selectedSegmentIndex == 1{
            if self.contactnotxtfield2.text!.isEmpty{
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Seems you forgot to fill some field.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                var passcode = Appconstant.pwd
                let checkpwd: Int = Int(passcode.substringWithRange(passcode.startIndex.advancedBy(0)..<passcode.startIndex.advancedBy(1)))!
                print(checkpwd)
                if checkpwd == 0 {
                    passcode = "1" + passcode
                }
                
                let desc = "Postpaid Recharge for "+self.contactnotxtfield2.text!
                let rechargeItem = ModelViewforRecharge.init(isPostpaid:IsPostpaid,amount:self.amounttxtField2.text!,business:"EQWALLET",businessEntityId:"EQWALLET",comment:"Recharge",description:desc,externalTransactionId:"123456",fromEntityId:Appconstant.customerid,mobile:self.contactnotxtfield2.text!,productId:"GENERAL",provider:(providerbtn2.titleLabel?.text)!,serviceType:"M",sessionId:"",special:specialselected,toEntityId:Appconstant.TOENTITYID,transactionOrigin:"MOBILE",transactionType:"TPP",yapcode:passcode)!
                let serializedjson  = JSONSerializer.toJson(rechargeItem)
                print(serializedjson)
                self.activityIndicator.startAnimating()
                self.sendrequesttoserverForRecharge(Appconstant.BASE_URL+Appconstant.URL_RECHARGE_PAY_BILL, values: serializedjson)
            }
        }
        else if segmentController.selectedSegmentIndex == 2{
            if self.customerIdtxtfield3.text!.isEmpty{
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Seems you forgot to fill some field.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let desc = "Postpaid Recharge for "+self.customerIdtxtfield3.text!
                var passcode = Appconstant.pwd
                let checkpwd: Int = Int(passcode.substringWithRange(passcode.startIndex.advancedBy(0)..<passcode.startIndex.advancedBy(1)))!
                print(checkpwd)
                if checkpwd == 0 {
                    passcode = "1" + passcode
                }
                
                let rechargeItem = ModelViewforRecharge.init(isPostpaid:IsPostpaid,amount:self.amounttxtField3.text!,business:"EQWALLET",businessEntityId:"EQWALLET",comment:"Recharge",description:desc,externalTransactionId:"123456",fromEntityId:Appconstant.customerid,mobile:self.customerIdtxtfield3.text!,productId:"GENERAL",provider:(provider3.titleLabel?.text)!,serviceType:"D",sessionId:"",special:false,toEntityId:Appconstant.TOENTITYID,transactionOrigin:"MOBILE",transactionType:"TPP",yapcode:passcode)!
                let serializedjson  = JSONSerializer.toJson(rechargeItem)
                print(serializedjson)
                self.activityIndicator.startAnimating()
                self.sendrequesttoserverForRecharge(Appconstant.BASE_URL+Appconstant.URL_RECHARGE_PAY_BILL, values: serializedjson)
            }
        }
        
        
    }
    
    
    func payBillForElectricity()
        
    {
        print(Operators.elect_operatorName)
        print(Operators.elect_operatorCode)
        print(selectedRow)
        print(Operators.elect_operatorCode[selectedRow])
        let selectedBillerId1 = Operators.elect_operatorCode[selectedRow]
        var passcode = Appconstant.pwd
        let checkpwd: Int = Int(passcode.substringWithRange(passcode.startIndex.advancedBy(0)..<passcode.startIndex.advancedBy(1)))!
        print(checkpwd)
        if checkpwd == 0 {
            passcode = "1" + passcode
        }
        
        let payBill = PayBillMode.init(entityId: Appconstant.customerid, dateTime: currentdate, billerId:selectedBillerId1, authenticator1: self.customer_no4.text!, authenticator2:  "", authenticator3:  "", transactionId: transactionId, fromEntityId: Appconstant.customerid, toEntityId: Appconstant.TOENTITYID, comment:"Bill Pay", businessEntityId: "EQWALLET", productId:  "GENERAL", transactionType:  "TPP", transactionOrigin: "MOBILE", yapcode: passcode, business: "EQWALLET", amount:String(self.fetchedAmount), refTxnId:transactionId, billId: "NA", billNumber:  String(self.fetchedNumber), billDate: String(self.fetchedDate), billDueDate:String(self.fetchedDueDate), billAmount: String(self.fetchedAmount), payWithOutBill:  "Y", partialPayment:  "N", filler1: "NA", filler2:  "NA", filler3: "NA!NA")!
        let serializedjson  = JSONSerializer.toJson(payBill)
        print(serializedjson)
        self.activityIndicator.startAnimating()
        self.sendrequesttoserverForRecharge(Appconstant.BASE_URL+Appconstant.URL_PAY_BILL, values: serializedjson)
        
    }
    
    func payBillForInsurance()
        
    {
        print(Operators.ins_operatorName)
        print(Operators.ins_operatorCode)
        print(selectedRow)
        print(Operators.ins_operatorCode[selectedRow])
        var selectedBillerId1 = Operators.ins_operatorCode[selectedRow]
        var passcode = Appconstant.pwd
        let checkpwd: Int = Int(passcode.substringWithRange(passcode.startIndex.advancedBy(0)..<passcode.startIndex.advancedBy(1)))!
        print(checkpwd)
        if checkpwd == 0 {
            passcode = "1" + passcode
        }
        
        let payBill = PayBillMode.init(entityId: Appconstant.customerid, dateTime: currentdate, billerId:selectedBillerId1, authenticator1: self.policyno6.text!, authenticator2:  "", authenticator3:  "", transactionId: transactionId, fromEntityId: Appconstant.customerid, toEntityId: Appconstant.TOENTITYID, comment:"Bill Pay", businessEntityId: "EQWALLET", productId:  "GENERAL", transactionType:  "TPP", transactionOrigin: "MOBILE", yapcode: passcode, business: "EQWALLET", amount:String(self.fetchedAmount), refTxnId:transactionId, billId: "NA", billNumber:  String(self.fetchedNumber), billDate: String(self.fetchedDate), billDueDate:String(self.fetchedDueDate), billAmount: String(self.fetchedAmount), payWithOutBill:  "Y", partialPayment:  "N", filler1: "NA", filler2:  "NA", filler3: "NA!NA")!
        let serializedjson  = JSONSerializer.toJson(payBill)
        print(serializedjson)
        self.activityIndicator.startAnimating()
        self.sendrequesttoserverForRecharge(Appconstant.BASE_URL+Appconstant.URL_PAY_BILL, values: serializedjson)
        
        
    }
    func payBillForLandline()
        
    {
        print(Operators.lan_operatorName)
        print(Operators.lan_operatorCode)
        print(selectedRow)
        print(Operators.lan_operatorCode[selectedRow])
        var selectedBillerId1 = Operators.lan_operatorCode[selectedRow]
        var passcode = Appconstant.pwd
        let checkpwd: Int = Int(passcode.substringWithRange(passcode.startIndex.advancedBy(0)..<passcode.startIndex.advancedBy(1)))!
        print(checkpwd)
        if checkpwd == 0 {
            passcode = "1" + passcode
        }
        
        
        let payBill = PayBillMode.init(entityId: Appconstant.customerid, dateTime: currentdate, billerId:selectedBillerId1, authenticator1: self.phonenotxtField7.text!, authenticator2:  "", authenticator3:  "", transactionId: transactionId, fromEntityId: Appconstant.customerid, toEntityId: Appconstant.TOENTITYID, comment:"Bill Pay", businessEntityId: "EQWALLET", productId:  "GENERAL", transactionType:  "TPP", transactionOrigin: "MOBILE", yapcode: passcode, business: "EQWALLET", amount:String(self.fetchedAmount), refTxnId:transactionId, billId: "NA", billNumber:  String(self.fetchedNumber), billDate: String(self.fetchedDate), billDueDate:String(self.fetchedDueDate), billAmount: String(self.fetchedAmount), payWithOutBill:  "Y", partialPayment:  "N", filler1: "NA", filler2:  "NA", filler3: "NA!NA")!
        let serializedjson  = JSONSerializer.toJson(payBill)
        print(serializedjson)
        self.activityIndicator.startAnimating()
        self.sendrequesttoserverForRecharge(Appconstant.BASE_URL+Appconstant.URL_PAY_BILL, values: serializedjson)
        
        
    }
    func payBillForGas()
        
    {
        print(Operators.gas_operatorName)
        print(Operators.gas_operatorCode)
        print(selectedRow)
        print(Operators.gas_operatorCode[selectedRow])
        var selectedBillerId1 = Operators.gas_operatorCode[selectedRow]
        var passcode = Appconstant.pwd
        let checkpwd: Int = Int(passcode.substringWithRange(passcode.startIndex.advancedBy(0)..<passcode.startIndex.advancedBy(1)))!
        print(checkpwd)
        if checkpwd == 0 {
            passcode = "1" + passcode
        }
        
        let payBill = PayBillMode.init(entityId: Appconstant.customerid, dateTime: currentdate, billerId:selectedBillerId1, authenticator1: self.customeraccnoo5.text!, authenticator2:  "", authenticator3:  "", transactionId: transactionId, fromEntityId: Appconstant.customerid, toEntityId: Appconstant.TOENTITYID, comment:"Bill Pay", businessEntityId: "EQWALLET", productId:  "GENERAL", transactionType:  "TPP", transactionOrigin: "MOBILE", yapcode: passcode, business: "EQWALLET", amount:String(self.fetchedAmount), refTxnId:transactionId, billId: "NA", billNumber:  String(self.fetchedNumber), billDate: String(self.fetchedDate), billDueDate:String(self.fetchedDueDate), billAmount: String(self.fetchedAmount), payWithOutBill:  "Y", partialPayment:  "N", filler1: "NA", filler2:  "NA", filler3: "NA!NA")!
        let serializedjson  = JSONSerializer.toJson(payBill)
        print(serializedjson)
        self.activityIndicator.startAnimating()
        self.sendrequesttoserverForRecharge(Appconstant.BASE_URL+Appconstant.URL_PAY_BILL, values: serializedjson)
        
        
    }
    
    
    
    
    func sendrequesttoserverForFetchAmount(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("Basic NzY2NzE4OTI5MToxMjM0", forHTTPHeaderField: "Authorization")
        request.addValue("EQWALLET", forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                self.activityIndicator.stopAnimating()
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                return
            }
            if self.segmentController.selectedSegmentIndex == 3
            {
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                else {
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseStringooooo = \(responseString)")
                    
                    let json = JSON(data: data!)
                    self.activityIndicator.stopAnimating()
                    let item = json["result"]
                    print(item)
                    
                    //Bill Amount
                    
                    let item3 = json["result"]["billAmount"]
                    print(item3)
                    
                    if item3 == nil
                    {
                        
                        self.fetchedAmount = 0
                        self.activityIndicator.stopAnimating()
                        if (self.providerbtn4.titleLabel?.text == "West Bengal State Electricity Distribution Company Limited" || self.providerbtn4.titleLabel?.text == "MSEDCL Ltd.")
                        {
                            self.amounttxtfield4_4.text = String(self.fetchedAmount)
                            //                            self.amounttxtField4.text = String(self.fetchedAmount)
                        }
                        else
                        {
                            self.amounttxtField4.text = String(self.fetchedAmount)
                            //                            self.amounttxtfield4_4.text = String(self.fetchedAmount)
                        }
                        
                        //                        self.amounttxtField4.text = String(self.fetchedAmount)
                        // Set fetchedAmount to Text Field
                        

                    }
                    else
                    {
                        self.fetchedAmount = Int(item3.stringValue)!
                        self.activityIndicator.stopAnimating()
                        if (self.providerbtn4.titleLabel?.text == "West Bengal State Electricity Distribution Company Limited" || self.providerbtn4.titleLabel?.text == "MSEDCL Ltd.")
                        {
                            self.amounttxtfield4_4.text = String(self.fetchedAmount)
                            
                        }
                        else
                        {
                            self.amounttxtField4.text = String(self.fetchedAmount)
                            //                            self.amounttxtfield4_4.text = String(self.fetchedAmount)
                        }
                        
                        
                    }
                    
                    //Bill Number
                    let item4 = json["result"]["billNumber"]
                    print(item3)
                    if item4 == nil
                    {
                        self.fetchedNumber = 0
                    }
                    else
                    {
                        self.fetchedNumber = Int(item4.stringValue)!
                    }
                    
                    //Bill Date
                    let item5 = json["result"]["billDate"]
                    print(item5)
                    if item5 == nil
                    {
                        self.fetchedDate = 0
                    }
                    else
                    {
                        self.fetchedDate = Int(item5.stringValue)!
                        
                    }
                    
                    //Bill Due Date
                    let item6 = json["result"]["billDueDate"]
                    print(item6)
                    if item6 == nil
                    {
                        self.fetchedDueDate = 0
                    }
                    else
                    {
                        self.fetchedDueDate = Int(item6.stringValue)!
                        
                    }
                    
                }
                
            }
            if self.segmentController.selectedSegmentIndex == 4
                
            {
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                else {
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseStringooooo = \(responseString)")
                    
                    let json = JSON(data: data!)
                    self.activityIndicator.stopAnimating()
                    let item = json["result"]
                    print(item)
                    
                    //Bill Amount
                    
                    let item3 = json["result"]["billAmount"]
                    print(item3)
                    if item3 == nil
                    {
                        
                        self.fetchedAmount = 0
                        self.amounttxtField5.text = String(self.fetchedAmount)
                        // Set fetchedAmount to Text Field
                        
                    }
                    else
                    {
                        self.fetchedAmount = Int(item3.stringValue)!
                        self.amounttxtField5.text = String(self.fetchedAmount)
                        
                    }
                    
                    //Bill Number
                    let item4 = json["result"]["billNumber"]
                    print(item3)
                    if item4 == nil
                    {
                        self.fetchedNumber = 0
                    }
                    else
                    {
                        self.fetchedNumber = Int(item4.stringValue)!
                    }
                    
                    //Bill Date
                    let item5 = json["result"]["billDate"]
                    print(item5)
                    if item5 == nil
                    {
                        self.fetchedDate = 0
                    }
                    else
                    {
                        self.fetchedDate = Int(item5.stringValue)!
                        
                    }
                    
                    //Bill Due Date
                    let item6 = json["result"]["billDueDate"]
                    print(item6)
                    if item6 == nil
                    {
                        self.fetchedDueDate = 0
                    }
                    else
                    {
                        self.fetchedDueDate = Int(item6.stringValue)!
                        
                    }
                    
                }
                
            }
            
            
            if self.segmentController.selectedSegmentIndex == 5
            {
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                else {
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseStringooooo = \(responseString)")
                    
                    let json = JSON(data: data!)
                    self.activityIndicator.stopAnimating()
                    let item = json["result"]
                    print(item)
                    
                    //Bill Amount
                    
                    let item3 = json["result"]["billAmount"]
                    print(item3)
                    if item3 == nil
                    {
                        
                        self.fetchedAmount = 0
                        
                        if(self.ins_provider == "INDIA FIRST Life Insurance" || self.ins_provider == "PNB Metlife")
                        {
                            self.amounttxtField6.text = String(self.fetchedAmount)
                            // Set fetchedAmount to Text Field
                            
                        }
                        else
                        {
                            self.amounttxtField6_6.text = String(self.fetchedAmount)
                            // Set fetchedAmount to Text Field
                        }
                        
                    }
                    else
                    {
                        self.fetchedAmount = Int(item3.stringValue)!
                        if(self.ins_provider == "INDIA FIRST Life Insurance" || self.ins_provider == "PNB Metlife")
                        {
                            self.amounttxtField6.text = String(self.fetchedAmount)
                            // Set fetchedAmount to Text Field
                            
                        }
                        else
                        {
                            self.amounttxtField6_6.text = String(self.fetchedAmount)
                            // Set fetchedAmount to Text Field
                        }
                        //                        self.amounttxtField7_7.text = String(self.fetchedAmount)
                        
                    }
                    //Bill Number
                    let item4 = json["result"]["billNumber"]
                    print(item3)
                    if item4 == nil
                    {
                        self.fetchedNumber = 0
                    }
                    else
                    {
                        self.fetchedNumber = Int(item4.stringValue)!
                    }
                    
                    //Bill Date
                    let item5 = json["result"]["billDate"]
                    print(item5)
                    if item5 == nil
                    {
                        self.fetchedDate = 0
                    }
                    else
                    {
                        self.fetchedDate = Int(item5.stringValue)!
                        
                    }
                    
                    //Bill Due Date
                    let item6 = json["result"]["billDueDate"]
                    print(item6)
                    if item6 == nil
                    {
                        self.fetchedDueDate = 0
                    }
                    else
                    {
                        self.fetchedDueDate = Int(item6.stringValue)!
                        
                    }
                    
                }
                
            }
            if(self.segmentController.selectedSegmentIndex == 6)
            {
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                else {
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseStringooooo = \(responseString)")
                    
                    let json = JSON(data: data!)
                    self.activityIndicator.stopAnimating()
                    let item = json["result"]
                    print(item)
                    
                    //Bill Amount
                    
                    let item3 = json["result"]["billAmount"]
                    print(item3)
                    if item3 == nil
                    {
                        
                        self.fetchedAmount = 0
                        
                        if(self.serviceType7.hidden == false)
                        {
                            self.amounttxtFieldAfterServiceType.text = String(self.fetchedAmount)
                            self.amounttxtField7_7.hidden = true
                            // Set fetchedAmount to Text Field
                            
                        }
                            
                        else
                        {
                            
                            if(self.lan_provider == "BSNL landline" || self.lan_provider == "MTNL Delhi")
                            {
                                self.amounttxtField7_7.text = String(self.fetchedAmount)
                                // Set fetchedAmount to Text Field
                                self.amounttxtFieldAfterServiceType.hidden = true
                                self.labelLineForAmount3.hidden = true
                            }
                                
                            else
                            {
                                self.amounttxtField7.text = String(self.fetchedAmount)
                                // Set fetchedAmount to Text Field
                            }
                            
                        }
                        
                        
                        
                    }
                    else
                    {
                        self.fetchedAmount = Int(item3.stringValue)!
                        if(self.lan_provider == "BSNL landline" || self.lan_provider == "MTNL Delhi")
                        {
                            self.amounttxtField7_7.text = String(self.fetchedAmount)
                            // Set fetchedAmount to Text Field
                            
                        }
                        else
                        {
                            self.amounttxtField7.text = String(self.fetchedAmount)
                            // Set fetchedAmount to Text Field
                        }
                        //                        self.amounttxtField7_7.text = String(self.fetchedAmount)
                        
                    }
                    
                    //Bill Number
                    let item4 = json["result"]["billNumber"]
                    print(item3)
                    if item4 == nil
                    {
                        self.fetchedNumber = 0
                    }
                    else
                    {
                        self.fetchedNumber = Int(item4.stringValue)!
                    }
                    
                    //Bill Date
                    let item5 = json["result"]["billDate"]
                    print(item5)
                    if item5 == nil
                    {
                        self.fetchedDate = 0
                    }
                    else
                    {
                        self.fetchedDate = Int(item5.stringValue)!
                        
                    }
                    
                    //Bill Due Date
                    let item6 = json["result"]["billDueDate"]
                    print(item6)
                    if item6 == nil
                    {
                        self.fetchedDueDate = 0
                    }
                    else
                    {
                        self.fetchedDueDate = Int(item6.stringValue)!
                        
                    }
                    
                }
                
            }        }
        
        task.resume()
        
}


    @IBAction func constraintBtn_elect(sender: AnyObject) {
        let authComponent1:NSArray = Operators.elect_Authenticator1[index_prov].componentsSeparatedByString("!!")
        print(authComponent1[0])
        print(authComponent1[1])
        print(authComponent1[2])
        
        
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title:  authComponent1[2] as? String,
                                            message: "",
                                            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            
            
            })
        
        alertController?.addAction(action)
        self.presentViewController(alertController!, animated: true, completion: nil)
        
        
    }
    
    @IBAction func constraintBtn_gas(sender: AnyObject) {
        
        let authComponent1:NSArray = Operators.gas_Authenticator1[index_prov].componentsSeparatedByString("!!")
        print(authComponent1[0])
        print(authComponent1[1])
        print(authComponent1[2])
        
        
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title:  authComponent1[2] as? String,
                                            message: "",
                                            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            
            
            })
        
        alertController?.addAction(action)
        self.presentViewController(alertController!, animated: true, completion: nil)
        
    }
    
    
    @IBAction func constraintBtn_ins(sender: AnyObject) {
        
        let authComponent1:NSArray = Operators.ins_Authenticator1[index_prov].componentsSeparatedByString("!!")
        print(authComponent1[0])
        print(authComponent1[1])
        print(authComponent1[2])
        
        
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title:  authComponent1[2] as? String,
                                            message: "",
                                            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            
            
            })
        
        alertController?.addAction(action)
        self.presentViewController(alertController!, animated: true, completion: nil)
    }
    
    @IBAction func constraintBtn_lan(sender: AnyObject) {
        //constriantButtonAccount7.hidden = true
        let authComponent1:NSArray = Operators.lan_Authenticator1[index_prov].componentsSeparatedByString("!!")
        print(authComponent1[0])
        print(authComponent1[1])
        print(authComponent1[2])
        
        
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title:  authComponent1[2] as? String,
                                            message: "",
                                            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            
            
            })
        
        alertController?.addAction(action)
        self.presentViewController(alertController!, animated: true, completion: nil)
        
    }
    
    @IBAction func constraintBtnAccount_lan(sender: AnyObject) {
        
        let authValue = Operators.lan_Authenticator2[index_prov] as NSString
        print("authValue.length==>>")
        print(authValue.length)
        var authComponent1:NSArray = NSArray()
        if(authValue.length == 0)
        {
            constriantButtonAccount7.hidden = true
        }
        else
        {
            constriantButtonAccount7.hidden = false
            let authComponent1:NSArray = Operators.lan_Authenticator2[index_prov].componentsSeparatedByString("!!")
            print(authComponent1[0])
            print(authComponent1[1])
            print(authComponent1[2])
            
            
            var alertController:UIAlertController?
            alertController?.view.tintColor = UIColor.blackColor()
            alertController = UIAlertController(title:  authComponent1[2] as? String,
                                                message: "",
                                                preferredStyle: .Alert)
            
            let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                
                
                
                })
            
            alertController?.addAction(action)
            self.presentViewController(alertController!, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func constraintBtnService_lan(sender: AnyObject) {
        
        let authComponent1:NSArray = Operators.pre_Authenticator3[index_prov].componentsSeparatedByString("!!")
        print(authComponent1[0])
        print(authComponent1[1])
        print(authComponent1[2])
        
        
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title:  authComponent1[2] as? String,
                                            message: "",
                                            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            
            
            })
        
        alertController?.addAction(action)
        self.presentViewController(alertController!, animated: true, completion: nil)
        
    }
    
    @IBAction func constraintele2_BtnAction(sender: AnyObject) {
        
        let authComponent2:NSArray = Operators.elect_Authenticator2[index_prov].componentsSeparatedByString("!!")
        print(authComponent2[0])
        print(authComponent2[1])
        print(authComponent2[2])
        
        
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title:  authComponent2[2] as? String,
                                            message: "",
                                            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            
            
            })
        
        alertController?.addAction(action)
        self.presentViewController(alertController!, animated: true, completion: nil)
        
        
        
    }
    
    @IBAction func constraintBtn_ins2(sender: AnyObject) {
        
        let authComponent2:NSArray = Operators.ins_Authenticator2[index_prov].componentsSeparatedByString("!!")
        print(authComponent2[0])
        print(authComponent2[1])
        print(authComponent2[2])
        
        
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title:  authComponent2[2] as? String,
                                            message: "",
                                            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            
            
            })
        
        alertController?.addAction(action)
        self.presentViewController(alertController!, animated: true, completion: nil)
    }
    
    
    
    func sendrequesttoserverForRecharge(url : String, values: String)
    {
        print(url)
        print(values)
        self.view.userInteractionEnabled = false
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("Basic NzY2NzE4OTI5MToxMjM0", forHTTPHeaderField: "Authorization")
        request.addValue("EQWALLET", forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        print(values)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                dispatch_async(dispatch_get_main_queue()) {
                    self.view.userInteractionEnabled = true
                }
                self.activityIndicator.stopAnimating()
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                //                    dispatch_async(dispatch_get_main_queue()) {
                //                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                //                    }
                
            }
            
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("Latest responseString = \(responseString)")
            
            let json = JSON(data: data!)
            dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                self.view.userInteractionEnabled = true
            }
            let item = json["result"]
            if(json["result"] != nil){
                let item1 = item["response"]
                if item["txnId"].stringValue != ""{
                    self.updatetransactionDB()
                    
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        var alertController:UIAlertController?
                        alertController?.view.tintColor = UIColor.blackColor()
                    print(Appconstant.segmentedIndex)
                        print(self.segmentController.selectedSegmentIndex)
                        if(Appconstant.segmentedIndex == 0||Appconstant.segmentedIndex == 2||self.segmentController.selectedSegmentIndex == 0||self.segmentController.selectedSegmentIndex == 2)
                        {
                            self.activityIndicator.stopAnimating()
                            if self.recentTransaction == 0
                            {
                      self.recentTransactionAddValue("PREPAID")
                            }
                                                       else if self.recentTransaction == 2
                            {
                                self.recentTransactionAddValue("DTH")
                            }
                            self.message = "Yey! Recharge Done."
                            Appconstant.fromsegue = false
                        }
                        else  if(Appconstant.segmentedIndex == 1||Appconstant.segmentedIndex == 3||Appconstant.segmentedIndex == 4||Appconstant.segmentedIndex == 5||Appconstant.segmentedIndex == 6||self.segmentController.selectedSegmentIndex == 1||self.segmentController.selectedSegmentIndex == 3||self.segmentController.selectedSegmentIndex == 4||self.segmentController.selectedSegmentIndex == 5||self.segmentController.selectedSegmentIndex == 6)
                        {
                            self.activityIndicator.stopAnimating()
                            
                            self.message = "Yey! Payment Done."
                            Appconstant.fromsegue = false
                           if self.recentTransaction == 1
                            {
                                self.recentTransactionAddValue("POSTPAID")
                            }

                            else if self.recentTransaction == 3
                            {
                                self.recentTransactionAddValue("ELECTRICITY")
                            }
                            else if self.recentTransaction == 4
                            {
                                self.recentTransactionAddValue("GAS")
                            }
                            else if self.recentTransaction == 5
                            {
                                self.recentTransactionAddValue("INSURANCE")
                            }
                            else if self.recentTransaction == 6
                            {
                                self.recentTransactionAddValue("LANDLINE")
                            }
                            
                        }
                        else
                        {
                            self.activityIndicator.stopAnimating()
                            self.message = "Yey! Money Sent."
                            Appconstant.fromsegue = false
                            
                        }
                        
                        
                        alertController = UIAlertController(title: "Payment",
                                                            message: self.message ,
                                                            preferredStyle: .Alert)
                        
                        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                            dispatch_async(dispatch_get_main_queue()) {
                                self!.performSegueWithIdentifier("ask_home", sender: self)
                            }
                            
                            })
                        
                        alertController?.addAction(action)
                        self.presentViewController(alertController!, animated: true, completion: nil)
                    }
                }
                else{
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Recharge failed. Please try again later", message: ""),animated: true,completion: nil)
                    }
                }
                
            }
            else{
                
                let item1 = json["exception"]
                if item1["errorCode"].stringValue == "Y2004"{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("You performed a transaction with the same data very recently. Please try again after sometime", message: ""),animated: true,completion: nil)
                    }
                }
                else if item1["errorCode"].stringValue == "Y110"{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Password is Invalid", message: ""),animated: true,completion: nil)
                    }
                }
                else if item1["errorCode"].stringValue == "Y202"{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Invalid Data", message: ""),animated: true,completion: nil)
                    }
                }
                    
                else if item1["errorCode"].stringValue == "Y102"{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Your maximum attempt exhausted", message: ""),animated: true,completion: nil)
                    }
                }
                else if item1["errorCode"].stringValue != ""{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert(item1["shortMessage"].stringValue, message: ""),animated: true,completion: nil)
                    }
                }
                    
                else{
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Recharge failed. Please try again later", message: ""),animated: true,completion: nil)
                    }
                }
            self.performSegueWithIdentifier("ask_home", sender: self)}
            
        }
        
        task.resume()
        
    }
   
    func recentTransactionAddValue(utilityType: String)
    {
        
    if utilityType == "PREPAID"
    {
        
  let recentViewModel = AddRecentViewModel.init(fromEntityId: Appconstant.customerid, toContactNo: "", amount: amounttxtField1.text!, txnType: "TPP", operatorCode: (operatortxtfield1.titleLabel?.text)!, serviceProvider: (providerbtn1.titleLabel?.text)!, utilityType: utilityType, qrCode: "", authorization1: contactnotxtfield1.text!, authorization2: "", authorization3: "", tip: "")!
         let serializedjson  = JSONSerializer.toJson(recentViewModel)
        AddRecentTransactions(Appconstant.BASE_URL+Appconstant.ADD_RECENT_TRANSACTION, values: serializedjson)
        print(serializedjson)
       
        }
       else if utilityType == "POSTPAID"
        {
            let recentViewModel = AddRecentViewModel.init(fromEntityId: Appconstant.customerid, toContactNo: "", amount: amounttxtField2.text!, txnType: "TPP", operatorCode: "", serviceProvider: (providerbtn2.titleLabel?.text)!, utilityType: utilityType, qrCode: "", authorization1: contactnotxtfield2.text!, authorization2: "", authorization3: "", tip: "")!
            let serializedjson  = JSONSerializer.toJson(recentViewModel)
            AddRecentTransactions(Appconstant.BASE_URL+Appconstant.ADD_RECENT_TRANSACTION, values: serializedjson)
            print(serializedjson)
        }
    else if utilityType == "DTH"
    {
        
        let recentViewModel = AddRecentViewModel.init(fromEntityId: Appconstant.customerid, toContactNo: "", amount: amounttxtField3.text!, txnType: "TPP", operatorCode: (operatortxtfield3.titleLabel?.text)!, serviceProvider: (provider3.titleLabel?.text)!, utilityType: utilityType, qrCode: "", authorization1: customerIdtxtfield3.text!, authorization2: "", authorization3: "", tip: "")!
        let serializedjson  = JSONSerializer.toJson(recentViewModel)
        AddRecentTransactions(Appconstant.BASE_URL+Appconstant.ADD_RECENT_TRANSACTION, values: serializedjson)
        print(serializedjson)


       
        
        }
    else if utilityType == "ELECTRICITY"
    {
        var Amount = ""
        var Atherization2 = ""
        if amounttxtfield4_4.hidden == false{
           Amount = amounttxtfield4_4.text!
           Atherization2 = (billingUnitText.titleLabel?.text)!
        }
        else
        {
            Amount = amounttxtField4.text!
            Atherization2 = ""
        }
        let recentViewModel = AddRecentViewModel.init(fromEntityId: Appconstant.customerid, toContactNo: "", amount: Amount, txnType: "TPP", operatorCode: "", serviceProvider: (providerbtn4.titleLabel?.text)!, utilityType: utilityType, qrCode: "", authorization1: customer_no4.text!, authorization2: Atherization2, authorization3: "", tip: "")!
        let serializedjson  = JSONSerializer.toJson(recentViewModel)
        AddRecentTransactions(Appconstant.BASE_URL+Appconstant.ADD_RECENT_TRANSACTION, values: serializedjson)
        print(serializedjson)
        
        }

    else if utilityType == "GAS"
    {
        
        let recentViewModel = AddRecentViewModel.init(fromEntityId: Appconstant.customerid, toContactNo: "", amount: amounttxtField5.text!, txnType: "TPP", operatorCode: "", serviceProvider: (providerbtn5.titleLabel?.text)!, utilityType: utilityType, qrCode: "", authorization1: customeraccnoo5.text!, authorization2: "", authorization3: "", tip: "")!
        let serializedjson  = JSONSerializer.toJson(recentViewModel)
        AddRecentTransactions(Appconstant.BASE_URL+Appconstant.ADD_RECENT_TRANSACTION, values: serializedjson)
        print(serializedjson)
      
        
        
        
        }
        
    else if utilityType == "INSURANCE"
    {
        var amountIns = ""
        var atherization2Ins = ""
        if amounttxtField6_6.hidden == false{
            amountIns = amounttxtField6_6.text!
            atherization2Ins = (dobtxtField6.text)!
        }
        else
        {
            amountIns = amounttxtField6.text!
            atherization2Ins = ""
        }
        let recentViewModel = AddRecentViewModel.init(fromEntityId: Appconstant.customerid, toContactNo: "", amount: amountIns, txnType: "TPP", operatorCode: "", serviceProvider: (providerbtn6.titleLabel?.text)!, utilityType: utilityType, qrCode: "", authorization1: policyno6.text!, authorization2: atherization2Ins, authorization3: "", tip: "")!
        let serializedjson  = JSONSerializer.toJson(recentViewModel)
        AddRecentTransactions(Appconstant.BASE_URL+Appconstant.ADD_RECENT_TRANSACTION, values: serializedjson)
        print(serializedjson)
     
                        }
    else if utilityType == "LANDLINE"
    {
        var amountLan = ""
        var atherization3Lan = ""
        
        if amounttxtFieldAfterServiceType.hidden == false{
            amountLan = amounttxtFieldAfterServiceType.text!
            atherization3Lan = (serviceType7.text)!
        }
        else
        {
            amountLan = amounttxtField7_7.text!
            atherization3Lan = ""
        }
        let recentViewModel = AddRecentViewModel.init(fromEntityId: Appconstant.customerid, toContactNo: "", amount: amountLan, txnType: "TPP", operatorCode: "", serviceProvider: (providerbtn7.titleLabel?.text)!, utilityType: utilityType, qrCode: "", authorization1: phonenotxtField7.text!, authorization2: account_no7.text!, authorization3: atherization3Lan, tip: "")!
        let serializedjson  = JSONSerializer.toJson(recentViewModel)
        AddRecentTransactions(Appconstant.BASE_URL+Appconstant.ADD_RECENT_TRANSACTION, values: serializedjson)
        print(serializedjson)
     
        }
    }
    func AddRecentTransactions(url : String, values: String)
    {
        print(url)
        print(values)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                 print("responseStringAddRecent = \(response)")
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseStringAddRecent = \(responseString)")
            let json = JSON(data: data!)
            print(json)
            dispatch_async(dispatch_get_main_queue()) {
                
            }
            
        }
        
        task.resume()
        
        
    }
    
    @IBAction func dobTxtField(sender: AnyObject) {
        
        
        createDatePickerViewWithAlertController()
    }
    func createDatePickerViewWithAlertController()
    {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.Date
        let dateString = "01-01-1999"
        let df = NSDateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let date = df.dateFromString(dateString)
        datePickerView.setDate(date!, animated: false)
        print(datePickerView.frame.size.height)
        dobtxtField6.inputView = datePickerView
        toolbarView = UIView(frame: CGRectMake(0,self.view.frame.size.height-datePickerView.frame.size.height-44,self.view.frame.size.width,44))
        toolbarView.backgroundColor = UIColor.lightGrayColor()
        //        let toolBar = UIToolbar(frame: CGRectMake(0,0,self.view.frame.size.width,44))
        let doneButton = UIButton.init(frame: CGRectMake(self.view.frame.size.width - 60,5,50,34))
        doneButton.addTarget(self, action: "Clicked", forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        
        //        let cancelButton = UIButton.init(frame: CGRectMake(10,5,70,34))
        //        cancelButton.addTarget(self, action: "Clicked", forControlEvents: UIControlEvents.TouchUpInside)
        //        cancelButton.setTitle("Cancel", forState: UIControlState.Normal)
        
        NSTimer.scheduledTimerWithTimeInterval(0.2, target:self, selector: Selector("timerfunc"), userInfo: nil, repeats: false)
        toolbarView.addSubview(doneButton)
        //        toolbarView.addSubview(cancelButton)
        
        datePickerView.addTarget(self, action: "datePickerValueChanged:", forControlEvents: UIControlEvents.ValueChanged)
    }
    
    
    
    @IBAction func segmentControllerActn(sender: AnyObject) {
        
        switch segmentController.selectedSegmentIndex {
        case 0:
            dispatch_async(dispatch_get_main_queue()) {
             self.recentTransaction = 0
            if self.recent_prepaidNo.count == 1
            {
                self.setSingleRecentTransaction()
            }
            else if self.recent_prepaidNo.count > 1
            {
                
                self.setTwoRecentTransaction();
                
            }
            else
            {
                
                self.recentTransactionSetValue("PREPAID")
            }
            }
            zerosegment()
            clearAction()
           
            case 1:
                dispatch_async(dispatch_get_main_queue()) {
                self.recentTransaction = 1
                if self.recent_postpaidNo.count == 1
                {
                    self.setSingleRecentTransaction()
                }
                else if self.recent_postpaidNo.count > 1
                {
                    self.setTwoRecentTransaction();
                    
                }
                else
                {self.recentTransactionSetValue("POSTPAID")
                }
                }
            firstsegment()
            clearAction()
           
        case 2:
            dispatch_async(dispatch_get_main_queue()) {
            self.recentTransaction = 2
            
            if self.recent_dthNo.count == 1
            {
                self.setSingleRecentTransaction()
            }
            else if self.recent_dthNo.count > 1
            {
                self.setTwoRecentTransaction();
                
            }
            else
            {
                self.recentTransactionSetValue("DTH")
            }
            }

            secondsegment()
            clearAction()
          case 3:
            dispatch_async(dispatch_get_main_queue()) {
            self.recentTransaction = 3
            if self.recent_electricityNo.count == 1
            {
                self.setSingleRecentTransaction()
            }
            else if self.recent_electricityNo.count > 1
            {
                self.setTwoRecentTransaction();
                
            }
            else
            {
                self.recentTransactionSetValue("ELECTRICITY")
            }
            }

            thirdsegment()
            clearAction()
            
        case 4:
            dispatch_async(dispatch_get_main_queue()) {
            self.recentTransaction = 4
            if self.recent_gasNo.count == 1
            {
                self.setSingleRecentTransaction()
            }
            else if self.recent_gasNo.count > 1
            {
                self.setTwoRecentTransaction();
                
            }
            else
            {
                self.recentTransactionSetValue("GAS")
            }

            }
            fourthsegment()
            clearAction()
            
        case 5:1
        dispatch_async(dispatch_get_main_queue()) {
        self.recentTransaction = 5
        if self.recent_insuranceNo.count == 1
        {
            self.setSingleRecentTransaction()
        }
        else if self.recent_insuranceNo.count > 1
        {
            self.setTwoRecentTransaction();
            
        }
        else
        {
            self.recentTransactionSetValue("INSURANCE")
        }
        }
        fifthsegment()
        clearAction()
            
        case 6:
            dispatch_async(dispatch_get_main_queue()) {
            self.recentTransaction = 6
            if self.recent_landlineNo.count == 1
            {
                self.setSingleRecentTransaction()
            }
            else if self.recent_landlineNo.count > 1
            {
                self.setTwoRecentTransaction();
                
            }
            else
            {
                self.recentTransactionSetValue("LANDLINE")
            }
            }
            sixthsegment()
            clearAction()
            
        default:
            break;
        }
        
    }
    
    
    func recentTransactionSetValue(utilityType: String)
{
    dispatch_async(dispatch_get_main_queue())
    {
    self.wholeScrollview.scrollEnabled = false
    self.recentView.hidden = true
    self.viewBottomHeight.constant = 152
        self.hiderecent()
        let getrecentViewModel = GetRecentViewModel.init(fromEntityId: Appconstant.customerid, utilityType: utilityType)!
        let serializedjson  = JSONSerializer.toJson(getrecentViewModel)
        
        self.getRecentTransactions(Appconstant.BASE_URL+Appconstant.GET_RECENT_TRANSACTIONS, values: serializedjson)
    }
      }

    func getRecentTransactions(url : String, values: String)
    {
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                
            }
            dispatch_async(dispatch_get_main_queue()) {
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            
            let result = json["result"]
           if self.recentTransaction == 0
                {
            for item in result["content"].arrayValue {
              result["content"]["utilityType"].stringValue
                print((result["content"]["utilityType"].stringValue))
                
                print((item["utilityType"].stringValue).lowercaseString)
                if (item["utilityType"].stringValue).lowercaseString == "prepaid"
                {
                self.recent_prepaidNo.append(item["authorization1"].stringValue)
                self.recent_prepaidAmt.append(item["amount"].stringValue)
                self.recent_prepaidProvider.append(item["serviceProvider"].stringValue)
                 self.recent_prepaidOperator.append(item["operatorCode"].stringValue)
                    
                    print("RECENET_PrepaidNo\(self.recent_prepaidNo)")
                            }
            }
                    if self.recent_prepaidNo.count == 1 {
                        self.setSingleRecentTransaction()
                    }
                    else if self.recent_prepaidNo.count > 1 {
                        self.setTwoRecentTransaction()
                    }
                    else
                    {
                         self.viewBottomHeight.constant = 137
                        self.wholeScrollview.scrollEnabled = false
                        self.recentView.hidden = true
                        
                        
                    }
           }
            else if self.recentTransaction == 1
            {
                 for item in result["content"].arrayValue {
                if (item["utilityType"].stringValue).lowercaseString=="postpaid"
                {
                    
                    self.recent_postpaidNo.append(item["authorization1"].stringValue)
                    self.recent_postpaidAmt.append(item["amount"].stringValue)
                    self.recent_postpaidProvider.append(item["serviceProvider"].stringValue)
                   self.recent_postpaidOperator.append(item["operatorCode"].stringValue)
                }
                }
                
                    if self.recent_postpaidNo.count == 1 {
                        self.setSingleRecentTransaction()
                    }
                    else if self.recent_postpaidNo.count > 1 {
                        self.setTwoRecentTransaction()
                    }
                    else
                    {
                         self.viewBottomHeight.constant = 152
                        self.wholeScrollview.scrollEnabled = false
                        self.recentView.hidden = true
                       
                        
                    }
                            }
           else if self.recentTransaction == 2
           {
            for item in result["content"].arrayValue {
                if (item["utilityType"].stringValue).lowercaseString == "dth"
                {
                self.recent_dthNo.append(item["authorization1"].stringValue)
                self.recent_dthAmt.append(item["amount"].stringValue)
                self.recent_dthProvider.append(item["serviceProvider"].stringValue)
                    self.recent_dthOperator.append(item["operatorCode"].stringValue)
                   
                
                }
            }
            if self.recent_dthNo.count == 1 {
                self.setSingleRecentTransaction()
            }
            else if self.recent_dthNo.count > 1 {
                self.setTwoRecentTransaction()
            }
            else
            {
                self.viewBottomHeight.constant = 152
                self.wholeScrollview.scrollEnabled = false
                self.recentView.hidden = true
                
                
            }
            }
           else if self.recentTransaction == 3
           {
            for item in result["content"].arrayValue {
                if (item["utilityType"].stringValue).lowercaseString == "electricity"
                {
                    self.recent_electricityNo.append(item["authorization1"].stringValue)
                    self.recent_electricityAmt.append(item["amount"].stringValue)
                    self.recent_electricityProvider.append(item["serviceProvider"].stringValue)
                    self.recent_electricityAtherization2.append(item["authorization2"].stringValue)
                    self.recent_electricityAtherization3.append(item["authorization3"].stringValue)
                    
                    
                }
            }
            if self.recent_electricityNo.count == 1 {
                self.setSingleRecentTransaction()
            }
            else if self.recent_electricityNo.count > 1 {
                self.setTwoRecentTransaction()
            }
            else
            {
                self.viewBottomHeight.constant = 152
                self.wholeScrollview.scrollEnabled = false
                self.recentView.hidden = true
                
                
            }

            }
           else if self.recentTransaction == 4
           {
            for item in result["content"].arrayValue {
                if (item["utilityType"].stringValue).lowercaseString == "gas"
                {
                    self.recent_gasNo.append(item["authorization1"].stringValue)
                    self.recent_gasAmt.append(item["amount"].stringValue)
                    self.recent_gasProvider.append(item["serviceProvider"].stringValue)
                    self.recent_gasAtherization2.append(item["authorization2"].stringValue)
                    self.recent_gasAtherization3.append(item["authorization3"].stringValue)
                    
                    
                }
            }
            if self.recent_gasNo.count == 1 {
                self.setSingleRecentTransaction()
            }
            else if self.recent_gasNo.count > 1 {
                self.setTwoRecentTransaction()
            }
            else
            {
                self.viewBottomHeight.constant = 152
                self.wholeScrollview.scrollEnabled = false
                self.recentView.hidden = true
              }
           
            }
           else if self.recentTransaction == 5
           {
            for item in result["content"].arrayValue {
                if (item["utilityType"].stringValue).lowercaseString == "insurance"
                {
                    self.recent_insuranceNo.append(item["authorization1"].stringValue)
                    self.recent_insuranceAmt.append(item["amount"].stringValue)
                    self.recent_insuranceProvider.append(item["serviceProvider"].stringValue)
                    self.recent_insuranceAtherization2.append(item["authorization2"].stringValue)
                    self.recent_insuranceAtherization3.append(item["authorization3"].stringValue)
                    
                    print("Insurance\(self.recent_insuranceNo)")
                }
            }
           print("INSURANCECount++\(self.recent_insuranceNo.count)")
            print(self.recent_insuranceNo)
                if self.recent_insuranceNo.count == 1 {
                    self.setSingleRecentTransaction()
                }
                else if self.recent_insuranceNo.count > 1 {
                    self.setTwoRecentTransaction()
                }
                else
                {
                    self.viewBottomHeight.constant = 152
                    self.wholeScrollview.scrollEnabled = false
                    self.recentView.hidden = true
                    
                }
            

            }
           else if self.recentTransaction == 6
           {
            for item in result["content"].arrayValue {
                if (item["utilityType"].stringValue).lowercaseString == "landline"
                {
                    self.recent_landlineNo.append(item["authorization1"].stringValue)
                    self.recent_landlineAmt.append(item["amount"].stringValue)
                    self.recent_landlineProvider.append(item["serviceProvider"].stringValue)
                    self.recent_landlineAtherization2.append(item["authorization2"].stringValue)
                    self.recent_landlineAtherization3.append(item["authorization3"].stringValue)
                    print("self.recent_landlineAtherization3")
                    print(self.recent_landlineAtherization3)
                    print(self.recent_landlineAtherization3.count)
                    
                }
            }
            if self.recent_landlineNo.count == 1 {
                self.setSingleRecentTransaction()
            }
            else if self.recent_landlineNo.count > 1 {
                self.setTwoRecentTransaction()
            }
            else
            {
                self.viewBottomHeight.constant = 152
                self.wholeScrollview.scrollEnabled = false
                self.recentView.hidden = true
                
            }
            
            }
            
            
            
        }
        }
        
        
        task.resume()
        
        
    }
    func setSingleRecentTransaction()
    {
        dispatch_async(dispatch_get_main_queue()) {
    self.wholeScrollview.scrollEnabled = true
        self.recentView.hidden = false
        self.recentView2.hidden = true
        if Int(self.viewBottomHeight.constant) == 152
        {

    self.viewBottomHeight.constant = self.viewBottomHeight.constant+35
        }
        else if Int(self.viewBottomHeight.constant) == 257
        {
            
            self.viewBottomHeight.constant = self.viewBottomHeight.constant-70
        }

  
    if self.recentTransaction == 0 && self.recent_prepaidNo.count == 1
    {
        
        self.recentMobileNolbl1.text = self.recent_prepaidNo[0]
        self.recentAmtlbl1.text = "\u{20B9}" + self.recent_prepaidAmt[0] + " | PREPAID"
    }
       else if self.recentTransaction == 1 && self.recent_postpaidNo.count == 1
    {
            self.recentMobileNolbl1.text = self.recent_postpaidNo[0]
            self.recentAmtlbl1.text = "\u{20B9}" + self.recent_postpaidAmt[0] + " | POSTPAID"
      }
    else if self.recentTransaction == 2 && self.recent_dthNo.count == 1
    {
       self.recentMobileNolbl1.text = self.recent_dthNo[0]
        self.recentAmtlbl1.text = "\u{20B9}" + self.recent_dthAmt[0] + " | DTH"
        
    }
    else if self.recentTransaction == 3 && self.recent_electricityNo.count == 1
    {

        self.recentMobileNolbl1.text = self.recent_electricityNo[0]
        self.recentAmtlbl1.text = "\u{20B9}" + self.recent_electricityAmt[0] + " | ELECTRICITY"
    }
    else if self.recentTransaction == 4 && self.recent_gasNo.count == 1
    {
        self.recentMobileNolbl1.text = self.recent_gasNo[0]
        self.recentAmtlbl1.text = "\u{20B9}" + self.recent_gasAmt[0] + " | GAS"
     }
    else if self.recentTransaction == 5 && self.recent_insuranceNo.count == 1
    {
        self.recentMobileNolbl1.text = self.recent_insuranceNo[0]
        self.recentAmtlbl1.text = "\u{20B9}" + self.recent_insuranceAmt[0] + " | INSURANCE"
    }
    else if self.recentTransaction == 6 && self.recent_landlineNo.count == 1
    {
        self.recentMobileNolbl1.text = self.recent_landlineNo[0]
        self.recentAmtlbl1.text = "\u{20B9}" + self.recent_landlineAmt[0] + " | LANDLINE"
     }
        else
    {
         self.wholeScrollview.scrollEnabled = false
        self.recentView.hidden = true
        self.viewBottomHeight.constant = 152
        }
        }
    }
   
    func setTwoRecentTransaction() {
        dispatch_async(dispatch_get_main_queue()) {
        self.recentView.hidden = false
        self.recentView2.hidden = false
                self.wholeScrollview.scrollEnabled = true
        print(Int(self.viewBottomHeight.constant+105))
        print(self.viewBottomHeight.constant)
        if Int(self.viewBottomHeight.constant) == 152
        {
        self.viewBottomHeight.constant = self.viewBottomHeight.constant+105
        }
        else if Int(self.viewBottomHeight.constant) == 187
        {
            self.viewBottomHeight.constant = self.viewBottomHeight.constant+70
        }
      
     if self.recentTransaction == 0 && self.recent_prepaidNo.count > 0 {
          self.recentMobileNolbl1.text = self.recent_prepaidNo[self.recent_prepaidNo.count-2]
            self.recentAmtlbl1.text = "\u{20B9}" + self.recent_prepaidAmt[self.recent_prepaidNo.count-2] + " | PREPAID"
            self.recentMobileNolbl2.text = self.recent_prepaidNo[self.recent_prepaidNo.count-1]
            self.recentAmtlbl2.text = "\u{20B9}" + self.recent_prepaidAmt[self.recent_prepaidNo.count-1] + " | PREPAID"
                   }
     else if  self.recentTransaction == 1  && self.recent_postpaidNo.count > 0{
        self.recentMobileNolbl1.text = self.recent_postpaidNo[self.recent_postpaidNo.count-2]
        self.recentAmtlbl1.text = "\u{20B9}" + self.recent_postpaidAmt[self.recent_postpaidAmt.count-2] + " | POSTAID"
        self.recentMobileNolbl2.text = self.recent_postpaidNo[self.recent_postpaidNo.count-1]
        self.recentAmtlbl2.text = "\u{20B9}" + self.recent_postpaidAmt[self.recent_postpaidAmt.count-1] + " | POSTAID"
              }
     else if  self.recentTransaction == 2 && self.recent_dthNo.count > 0 {
        self.recentMobileNolbl1.text = self.recent_dthNo[self.recent_dthNo.count-2]
        self.recentAmtlbl1.text = "\u{20B9}" + self.recent_dthAmt[self.recent_dthAmt.count-2] + " | DTH"
        self.recentMobileNolbl2.text = self.recent_dthNo[self.recent_dthNo.count-1]
        self.recentAmtlbl2.text = "\u{20B9}" + self.recent_dthAmt[self.recent_dthAmt.count-1] + " | DTH"
        }
     else if  self.recentTransaction == 3  && self.recent_electricityNo.count > 0{
        self.recentMobileNolbl1.text = self.recent_electricityNo[self.recent_electricityNo.count-2]
        self.recentAmtlbl1.text = "\u{20B9}" + self.recent_electricityAmt[self.recent_electricityAmt.count-2] + " | ELECTRICITY"
        self.recentMobileNolbl2.text = self.recent_electricityNo[self.recent_electricityNo.count-1]
        self.recentAmtlbl2.text = "\u{20B9}" + self.recent_electricityAmt[self.recent_electricityAmt.count-1] + " | ELECTRICITY"
        }
     else if  self.recentTransaction == 4 && self.recent_gasNo.count > 0{
        self.recentMobileNolbl1.text = "\u{20B9}" + self.recent_gasNo[self.recent_gasNo.count-2]
        self.recentAmtlbl1.text = self.recent_gasAmt[self.recent_gasAmt.count-2] + " | GAS"
        self.recentMobileNolbl2.text = self.recent_gasNo[self.recent_gasNo.count-1]
        self.recentAmtlbl2.text = "\u{20B9}" + self.recent_gasAmt[self.recent_gasAmt.count-1] + " | GAS"
     }
     else if  self.recentTransaction == 5 && self.recent_insuranceNo.count > 0{
        self.recentMobileNolbl1.text = self.recent_insuranceNo[self.recent_insuranceNo.count-2]
        self.recentAmtlbl1.text = "\u{20B9}" + self.recent_insuranceAmt[self.recent_insuranceAmt.count-2] + " | INSURANCE"
        self.recentMobileNolbl2.text = self.recent_insuranceNo[self.recent_insuranceNo.count-1]
        self.recentAmtlbl2.text = "\u{20B9}" + self.recent_insuranceAmt[self.recent_insuranceAmt.count-1] + " | INSURANCE"
        
        }
     else if  self.recentTransaction == 6 && self.recent_landlineNo.count > 0{
        self.recentMobileNolbl1.text = self.recent_landlineNo[self.recent_landlineNo.count-2]
        self.recentAmtlbl1.text = "\u{20B9}" + self.recent_landlineAmt[self.recent_landlineAmt.count-2] + " | LANDLINE"
        self.recentMobileNolbl2.text = self.recent_landlineNo[self.recent_landlineNo.count-1]
        self.recentAmtlbl2.text = "\u{20B9}" + self.recent_landlineAmt[self.recent_landlineAmt.count-1] + " | LANDLINE"
        }
     else
     {
        self.wholeScrollview.scrollEnabled = false
        self.recentView.hidden = true
        self.viewBottomHeight.constant = 152
        
        }

        }
    }
    @IBAction func repeatBtn1Action(sender: AnyObject) {
        repeat1_clicked = true
        
        if recentTransaction == 0
        {
           
     contactnotxtfield1.text = recent_prepaidNo[0].stringByReplacingOccurrencesOfString("+91", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
//.componentsSeparatedByString("+91")
            providerbtn1.setTitle(recent_prepaidProvider[0], forState: .Normal)
            self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Normal)
    operatortxtfield1.setTitle(recent_prepaidOperator[0], forState: .Normal)
        amounttxtField1.text = recent_prepaidAmt[0]
            
        }
       else if recentTransaction == 1
        {
            contactnotxtfield2.text = recent_postpaidNo[0].stringByReplacingOccurrencesOfString("+91", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)

            providerbtn2.setTitle(recent_postpaidProvider[0], forState: .Normal)
           
            amounttxtField2.text = recent_postpaidAmt[0]
            
        }
        else if recentTransaction == 2
        {
            customerIdtxtfield3.text = recent_dthNo[0]
            provider3.setTitle(recent_dthProvider[0], forState: .Normal)
            operatortxtfield3.setTitle(recent_dthOperator[0], forState: .Normal)
            amounttxtField3.text = recent_dthAmt[0]
            
        }
        else if recentTransaction == 3
        {
            amounttxtField4.hidden = false
            billingUnitText.hidden = true
            amounttxtfield4_4.hidden = true
            label4.hidden = true
            customer_no4.text = recent_electricityNo[0]
            providerbtn4.setTitle(recent_electricityProvider[0], forState: .Normal)
             amounttxtField4.text = recent_electricityAmt[0]
            if recent_electricityAtherization2[0] !=   ""
            {
                amounttxtField4.hidden = true
                billingUnitText.hidden = false
                amounttxtfield4_4.hidden = false
                label4.hidden = false
     billingUnitText.setTitle(recent_electricityAtherization2[0], forState: .Normal)
            amounttxtfield4_4.text = recent_electricityAmt[0]
            }
            
        }
        else if recentTransaction == 4

        {
            customeraccnoo5.text = recent_gasNo[0]
            providerbtn5.setTitle(recent_gasProvider[0], forState: .Normal)
            amounttxtField5.text = recent_gasAmt[0]
          
            
        }
   
            
        else if recentTransaction == 5
        {
            amounttxtField6.hidden = false
            dobtxtField6.hidden = true
            amounttxtField6_6.hidden = true
            label6_6.hidden = true
            policyno6.text = recent_insuranceNo[0]
            providerbtn6.setTitle(recent_insuranceProvider[0], forState: .Normal)
            amounttxtField6.text = recent_insuranceAmt[0]
            if recent_insuranceAtherization2[0] !=   ""
            {
                amounttxtField6.hidden = true
                dobtxtField6.hidden = false
                amounttxtField6_6.hidden = false
                label6_6.hidden = false
                dobtxtField6.text = recent_insuranceAtherization2[0]
                amounttxtField6_6.text = recent_insuranceAmt[0]
            }
        }
        else if recentTransaction == 6
        {
            amounttxtField7.hidden = true
            amounttxtField7_7.hidden = false
            label7_7.hidden = false
            account_no7.hidden = false
            serviceType7.hidden = true
            amounttxtFieldAfterServiceType.hidden = true
            labelLineForAmount3.hidden = true
            phonenotxtField7.text = recent_landlineNo[0].stringByReplacingOccurrencesOfString("+91", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)

            providerbtn7.setTitle(recent_landlineProvider[0], forState: .Normal)
            amounttxtField7_7.text = recent_landlineAmt[0]
             account_no7.text = recent_landlineAtherization2[0]
            if recent_landlineAtherization3[0] != ""
            {
                 amounttxtField7.hidden = true
                amounttxtField7_7.hidden = true
                serviceType7.hidden = false
amounttxtFieldAfterServiceType.hidden = false
                labelLineForAmount3.hidden = false
           serviceType7.text = recent_landlineAtherization3[0]
                amounttxtFieldAfterServiceType.text = recent_landlineAmt[0]
            }
        }
      
    }
    @IBAction func repeatBtn2Action(sender: AnyObject) {
        repeat2_clicked = true
        print(recentTransaction)
        if recentTransaction == 0
        {
            print(recentTransaction)
            contactnotxtfield1.text = recent_prepaidNo[self.recent_prepaidNo.count-1].stringByReplacingOccurrencesOfString("+91", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)

            providerbtn1.setTitle(recent_prepaidProvider[self.recent_prepaidProvider.count-1], forState: .Normal)
            self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Normal)
            operatortxtfield1.setTitle(recent_prepaidOperator[self.recent_prepaidOperator.count-1], forState: .Normal)
            amounttxtField1.text = recent_prepaidAmt[self.recent_prepaidAmt.count-1]
            
        }
        else if recentTransaction == 1
        {
            contactnotxtfield2.text = recent_postpaidNo[self.recent_postpaidNo.count-1].stringByReplacingOccurrencesOfString("+91", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)

            providerbtn2.setTitle(recent_postpaidProvider[self.recent_postpaidProvider.count-1], forState: .Normal)
            
            amounttxtField2.text = recent_postpaidAmt[self.recent_postpaidAmt.count-1]
            
        }
        else if recentTransaction == 2
        {
            customerIdtxtfield3.text = recent_dthNo[self.recent_dthNo.count-1]
            provider3.setTitle(recent_dthProvider[self.recent_dthProvider.count-1], forState: .Normal)
            operatortxtfield3.setTitle(recent_dthOperator[self.recent_dthOperator.count-1], forState: .Normal)
            amounttxtField3.text = recent_dthAmt[self.recent_dthAmt.count-1]
            
        }
        else if recentTransaction == 3
        {
            amounttxtField4.hidden = false
            billingUnitText.hidden = true
            amounttxtfield4_4.hidden = true
            label4.hidden = true
            customer_no4.text = recent_electricityNo[self.recent_electricityNo.count-1]
            providerbtn4.setTitle(recent_electricityProvider[self.recent_electricityProvider.count-1], forState: .Normal)
            amounttxtField4.text = recent_electricityAmt[self.recent_electricityAmt.count-1]
            if recent_electricityAtherization2[recent_electricityAtherization2.count-1] !=   ""
            {
                amounttxtField4.hidden = true
                billingUnitText.hidden = false
                amounttxtfield4_4.hidden = false
                label4.hidden = false
                billingUnitText.setTitle(recent_electricityAtherization2[self.recent_electricityAtherization2.count-1], forState: .Normal)
                amounttxtfield4_4.text = recent_electricityAmt[self.recent_electricityAmt.count-1]
            }
            
        }
        else if recentTransaction == 4
            
        {
            customeraccnoo5.text = recent_gasNo[self.recent_gasNo.count-1]
            providerbtn5.setTitle(recent_gasProvider[self.recent_gasProvider.count-1], forState: .Normal)
            amounttxtField5.text = recent_gasAmt[self.recent_gasAmt.count-1]
            
            
        }
            
            
        else if recentTransaction == 5
        {
            amounttxtField6.hidden = false
            dobtxtField6.hidden = true
            amounttxtField6_6.hidden = true
            label6_6.hidden = true
            policyno6.text = recent_insuranceNo[self.recent_insuranceNo.count-1]
            providerbtn6.setTitle(recent_insuranceProvider[self.recent_insuranceProvider.count-1], forState: .Normal)
            amounttxtField6.text = recent_insuranceAmt[self.recent_insuranceAmt.count-1]
            if recent_insuranceAtherization2[recent_insuranceAtherization2.count-1] !=   ""
            {
                amounttxtField6.hidden = true
                dobtxtField6.hidden = false
                amounttxtField6_6.hidden = false
                label6_6.hidden = false
                dobtxtField6.text = recent_insuranceAtherization2[self.recent_insuranceAtherization2.count-1]
                amounttxtField6_6.text = recent_insuranceAmt[self.recent_insuranceAmt.count-1]
            }
        }
        else if recentTransaction == 6
        {
            amounttxtField7.hidden = true
            amounttxtField7_7.hidden = false
            label7_7.hidden = false
            account_no7.hidden = false
            serviceType7.hidden = true
            amounttxtFieldAfterServiceType.hidden = true
            labelLineForAmount3.hidden = true
            phonenotxtField7.text = recent_landlineNo[self.recent_landlineNo.count-1].stringByReplacingOccurrencesOfString("+91", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)

            providerbtn7.setTitle(recent_landlineProvider[self.recent_landlineProvider.count-1], forState: .Normal)
            amounttxtField7_7.text = recent_landlineAmt[self.recent_landlineAmt.count-1]
            account_no7.text = recent_landlineAtherization2[self.recent_landlineAtherization2.count-1]
            if recent_landlineAtherization3[self.recent_landlineAtherization3.count-1] != ""
            {
                amounttxtField7.hidden = true
                amounttxtField7_7
                    .hidden = true
                serviceType7.hidden = false
                amounttxtFieldAfterServiceType.hidden = false
                labelLineForAmount3.hidden = false
                serviceType7.text = recent_landlineAtherization3[self.recent_landlineAtherization3.count-1]
                amounttxtFieldAfterServiceType.text = recent_landlineAmt[self.recent_landlineAmt.count-1]
            }
        }
        
    }
    
    func hiderecent() {
        recentView.hidden = true
        repeatbtn1.layer.cornerRadius = 5
        repeatbtn2.layer.cornerRadius = 5
        setShadow(recentView1)
        setShadow(recentView2)
    }
    
    func setShadow(view: UIView) {
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowOffset = CGSizeZero
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 3
        view.layer.cornerRadius = 5
    }
    func clearAction()
    {
        contactnotxtfield1.text = ""
        providerbtn1.setTitle("Select Provider", forState: .Normal)
        operatortxtfield1.setTitle("Select Operator Circle", forState: .Normal)
        amounttxtField1.text  = ""
        amounttxtField1_1.text  = ""
        contactnotxtfield2.text  = ""
        providerbtn2.setTitle("Select Provider", forState: .Normal)
        amounttxtField2.text  = ""
        amounttxtField2_2.text  = ""
        radioView.hidden = true
        browsePlanbtn1.hidden = true
        browsePlanbtn3.hidden = true
        customerIdtxtfield3.text  = ""
        provider3.setTitle("Select Provider", forState: .Normal)
        operatortxtfield3.titleLabel?.text = "Select Operator Circle"
        amounttxtField3.text  = ""
        providerbtn4.setTitle("Select Electricity board", forState: .Normal)
        customer_no4.text  = ""
        amounttxtField4.text  = ""
        phone_no4.text  = ""
        amounttxtfield4_4.text  = ""
        
        providerbtn5.setTitle("Select Gas Company", forState: .Normal)
        customeraccnoo5.text  = ""
        amounttxtField5.text  = ""
        
        providerbtn6.setTitle("Select Company", forState: .Normal)
        policyno6.text  = ""
        dobtxtField6.text  = ""
        
        amounttxtField6.text  = ""
        amounttxtField6_6.text  = ""
        providerbtn7.setTitle("Select Provider", forState: .Normal)
        phonenotxtField7.text  = ""
        amounttxtField7.text  = ""
        amounttxtField7_7.text  = ""
        
        account_no7.text  = ""
        
        
        serviceType7.text  = ""
        amounttxtFieldAfterServiceType.text  = ""
        
    }
    
    func segmentfuncZero(){
        IsPostpaid = "N"
        view.endEditing(true)
        payBtn.setTitle("RECHARGE", forState: .Normal)
        
        View_pre.hidden = false
        View_post.hidden = true
        View_dth.hidden = true
        View_elec.hidden = true
        View_gas.hidden = true
        View_ins.hidden = true
        View_lan.hidden = true
        radioView.hidden = true
        if (from_provider == true && segmentno_provider == 0)
        {
            if(provider_Name == "")
            {
                   contactnotxtfield1.text = mobileno
                providerbtn1.titleLabel?.text = "Select Provider"
                amounttxtField1.becomeFirstResponder()
                amounttxtField1_1.becomeFirstResponder()

                special = false
            }
            else{
                providerbtn1.setTitle(provider_Name, forState: .Normal)
                print("mobileno")
                print(mobileno)
                contactnotxtfield1.text = mobileno
                amounttxtField1.becomeFirstResponder()
                amounttxtField1_1.becomeFirstResponder()
                operatortxtfield1.userInteractionEnabled = true
                providername = Operators.pre_operatorName[index_prov]
                providerbtn1.setTitle(Operators.pre_operatorName[index_prov], forState: .Normal)
                amounttxtField1.text = ""
                operatorcode = Operators.pre_operatorCode[index_prov]
                if Operators.pre_special[index_prov]{
                    radioView.hidden = false
                }
                else{
                    radioView.hidden = true
                }
                
                special = false
                
            }
        }
        
    }
    
    func segmentfuncTwo(){
        print("SEL Index Colour==>>")
        print(selectedindex)
        IsPostpaid = "N"
        view.endEditing(true)
        payBtn.setTitle("RECHARGE", forState: .Normal)
        
        View_pre.hidden = true
        View_post.hidden = true
        View_dth.hidden = false
        View_elec.hidden = true
        View_gas.hidden = true
        View_ins.hidden = true
        View_lan.hidden = true
        
        radioView.hidden = true
    }
    func zerosegment()
    {
        
        contactalert = false
        
        
        prepaidbottomLabel.backgroundColor = UIColor.whiteColor()
        
        //                prepaidbottomLabel.hidden = false
        //                postpaidbottomLabel.hidden = true
        //                dthbottomLabel.hidden = true
        //                electricitybottomLabel.hidden = true
        //                insurancebottomLabel.hidden = true
        //                gasbottomLabel.hidden = true
        //                landlinebottomLabel.hidden = true
        
        
        
        postpaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        electricitybottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        
        
        segmentfuncZero()
        payBtn.tag = 0
        Appconstant.segmentedIndex = 0
        
        
    }
    func firstsegment()
    {
        Appconstant.segmentedIndex = 1
        contactalert = false
        //                prepaidbottomLabel.hidden = true
        //                postpaidbottomLabel.hidden = false
        //                dthbottomLabel.hidden = true
        //                electricitybottomLabel.hidden = true
        //                insurancebottomLabel.hidden = true
        //                gasbottomLabel.hidden = true
        //                landlinebottomLabel.hidden = true
        
        prepaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        postpaidbottomLabel.backgroundColor = UIColor.whiteColor()
        dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        electricitybottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        
        
        
        IsPostpaid = "Y"
       
        view.endEditing(true)
        payBtn.setTitle("PAY", forState: .Normal)
        
        View_pre.hidden = true
        View_post.hidden = false
        View_dth.hidden = true
        View_elec.hidden = true
        View_gas.hidden = true
        View_ins.hidden = true
        View_lan.hidden = true
        
        radioView.hidden = true
        payBtn.tag = 1
        segmentController.selectedSegmentIndex = 1
        if self.recent_postpaidNo.count == 1
        {
            self.setSingleRecentTransaction()
        }
        else if self.recent_postpaidNo.count > 1
        {
            self.setTwoRecentTransaction();
            
        }
        else
        {self.recentTransactionSetValue("POSTPAID")
        }
        if (from_provider == true && segmentno_provider == 1)
        {
            if(provider_Name == "")
            {
                contactnotxtfield2.text = mobileno
                providerbtn2.titleLabel?.text = "Select Provider"
                radioView.hidden = true
                
            }
            else
            {
                providerbtn2.setTitle(provider_Name, forState: .Normal)
                contactnotxtfield2.text = mobileno
                special = false
                providername = Operators.post_operatorName[index_prov]
                providerbtn2.setTitle(Operators.post_operatorName[index_prov], forState: .Normal)
                
                amounttxtField2.text = ""
                operatorcode = Operators.post_operatorCode[index_prov]
                
                if Operators.post_special[index_prov]{
                    radioView.hidden = false
                }
                else{
                    radioView.hidden = true
                }
                
                
            }
        }
        
    }
    func secondsegment()
    {
        contactalert = false
        //                prepaidbottomLabel.hidden = true
        //                postpaidbottomLabel.hidden = true
        //                dthbottomLabel.hidden = false
        //                electricitybottomLabel.hidden = true
        //                insurancebottomLabel.hidden = true
        //                gasbottomLabel.hidden = true
        //                landlinebottomLabel.hidden = true
        
        
        prepaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        postpaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        dthbottomLabel.backgroundColor = UIColor.whiteColor()
        electricitybottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);                landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        
        segmentfuncTwo()
        payBtn.tag = 2
        print( "payBtn.tag =====")
        Appconstant.segmentedIndex = 2
        print( payBtn.tag)
        segmentController.selectedSegmentIndex = 2
        if (from_provider == true && segmentno_provider == 2)
        {
            if(provider_Name == "")
            {
                 customerIdtxtfield3.text = mobileno
                providername = "Select Provider"
                provider3.setTitle("Select Provider", forState: .Normal)
                operatortxtfield3.userInteractionEnabled = false
                special = false
            }
            else
            {
                provider3.setTitle(provider_Name, forState: .Normal)
                customerIdtxtfield3.text = mobileno
                special = false
                providername = Operators.dth_operatorName[index_prov]
                provider3.setTitle(Operators.dth_operatorName[index_prov], forState: .Normal)
                amounttxtField3.text = ""
                operatorcode = Operators.dth_operatorCode[index_prov]
                operatortxtfield3.userInteractionEnabled = true
            }
        }
        
    }
    
    
    func thirdsegment(){
        contactalert = false
        prepaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        postpaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        electricitybottomLabel.backgroundColor = UIColor.whiteColor()
        insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        
        view.endEditing(true)
        payBtn.setTitle("PAY", forState: .Normal)
        
        View_pre.hidden = true
        View_post.hidden = true
        View_dth.hidden = true
        View_elec.hidden = false
        View_gas.hidden = true
        View_ins.hidden = true
        View_lan.hidden = true
        radioView.hidden = true
        payBtn.tag = 3
        Appconstant.segmentedIndex = 3
        
        segmentController.selectedSegmentIndex = 3
        if (from_provider == true && segmentno_provider == 3)
        {
            
            if(provider_Name == "")
            {
                providerbtn4.titleLabel?.text = "Select Electricity board"
            }
            else{
                providerbtn4.setTitle(provider_Name, forState: .Normal)
                constraintsEle.hidden = false
                special = false
                if(provider_Name == "West Bengal State Electricity Distribution Company Limited" || provider_Name == "Torrent Power")
                {
                    amounttxtField4.hidden = true
                    amounttxtfield4_4.hidden = false
                    label4.hidden = false
                    phone_no4.hidden = false
                    constraintele2.hidden = false
                    billingUnitText.hidden = true
                }
                else if(provider_Name == "MSEDCL Ltd.")
                {
                    self.getBillingUnit()
                    
                    amounttxtField4.hidden = true
                    amounttxtfield4_4.hidden = false
                    label4.hidden = false
                    phone_no4.hidden = true
                    constraintele2.hidden = true
                    billingUnitText.hidden = false
                }
                else{
                    amounttxtField4.hidden = false
                    amounttxtfield4_4.hidden = true
                    label4.hidden = true
                    phone_no4.hidden = true
                    constraintele2.hidden = true
                    billingUnitText.hidden = true
                }
            }
        }
        else
        {
            self.providername = "Select Electricity board"
            self.providerbtn4.setTitle("Select Electricity board", forState: .Normal)
            self.special = false
        }
        
        customer_no4.text = ""
        amounttxtField4.text = ""
        toastLabel.hidden = true
        
    }
    func fourthsegment()
    {
        contactalert = false
        //                prepaidbottomLabel.hidden = true
        //                postpaidbottomLabel.hidden = true
        //                dthbottomLabel.hidden = true
        //                electricitybottomLabel.hidden = true
        //                insurancebottomLabel.hidden = false
        //                gasbottomLabel.hidden = true
        //                landlinebottomLabel.hidden = true
        //
        prepaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        postpaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);                electricitybottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);                insurancebottomLabel.backgroundColor = UIColor.whiteColor()
        gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        
        //                (segmentController.subviews[2] as UIView).backgroundColor = UIColor.whiteColor()
        //                (segmentController.subviews[2] as UIView).tintColor = UIColor.whiteColor()
        //                (segmentController.subviews[1] as UIView).tintColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        //                (segmentController.subviews[2] as UIView).tintColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        //                (segmentController.subviews[3] as UIView).tintColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        //                (segmentController.subviews[4] as UIView).tintColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        //                (segmentController.subviews[5] as UIView).tintColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        //                (segmentController.subviews[0] as UIView).tintColor =  UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        //                (segmentController.subviews[1] as UIView).backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        //                (segmentController.subviews[2] as UIView).backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        //                (segmentController.subviews[3] as UIView).backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        //                (segmentController.subviews[4] as UIView).backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        //                (segmentController.subviews[5] as UIView).backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        //                (segmentController.subviews[0] as UIView).backgroundColor =  UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        view.endEditing(true)
        payBtn.setTitle("PAY", forState: .Normal)
        
        View_pre.hidden = true
        View_post.hidden = true
        View_dth.hidden = true
        View_elec.hidden = true
        View_gas.hidden = false
        View_ins.hidden = true
        View_lan.hidden = true
        
        radioView.hidden = true
        payBtn.tag = 4
        Appconstant.segmentedIndex = 4
        segmentController.selectedSegmentIndex = 4
        if (from_provider == true && segmentno_provider == 4)
        {
            if(provider_Name == "")
            {providername = "Select Gas Company"
                providerbtn5.setTitle("Select Gas Company", forState: .Normal)
                special = false            }
            else
            {
                constraintgas.hidden = false
                special = false
                providername = Operators.gas_operatorName[index_prov]
                providerbtn5.setTitle(Operators.gas_operatorName[index_prov], forState: .Normal)
                customeraccnoo5.text = ""
                amounttxtField5.text = ""
                operatorcode = Operators.gas_operatorCode[index_prov]            }
        }
        
    }
    func fifthsegment()
    {
        contactalert = false
        prepaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        postpaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        electricitybottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        gasbottomLabel.backgroundColor = UIColor.whiteColor()
        landlinebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        
        view.endEditing(true)
        payBtn.setTitle("PAY", forState: .Normal)
        View_pre.hidden = true
        View_post.hidden = true
        View_dth.hidden = true
        View_elec.hidden = true
        View_gas.hidden = true
        View_ins.hidden = false
        View_lan.hidden = true
        
        radioView.hidden = true
        payBtn.tag = 5
        Appconstant.segmentedIndex = 5
        segmentController.selectedSegmentIndex = 5
        if (from_provider == true && segmentno_provider == 5)
        {
            if(provider_Name == "")
            {
                providername = "Select Company"
                providerbtn6.setTitle("Select Company", forState: .Normal)
                special = false
            }
            else
            {
                constraintins.hidden = false
                special = false
                providername = Operators.ins_operatorName[index_prov]
                providerbtn6.setTitle(Operators.ins_operatorName[index_prov], forState: .Normal)
                policyno6.text = ""
                amounttxtField6_6.text = ""
                amounttxtField6.text == ""
                dobtxtField6.text = ""
                operatorcode = Operators.ins_operatorCode[index_prov]
                ins_provider = Operators.ins_operatorName[index_prov]
                
                if(ins_provider == "INDIA FIRST Life Insurance" || ins_provider == "PNB Metlife")
                {
                    
                    dobtxtField6.hidden = true
                    amounttxtField6.hidden = false
                    amounttxtField6_6.hidden = true
                    label6_6.hidden = true
                    
                    
                }
            }
        }
}

    func sixthsegment()
    {
        contactalert = false
        prepaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        postpaidbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);            dthbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        electricitybottomLabel.backgroundColor = UIColor(red:250/255.0, green:194/255.0, blue:35/255.0, alpha:1.0)
        insurancebottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1);            gasbottomLabel.backgroundColor = UIColor(red: 252.0/255.0, green: 207.0/255.0, blue: 80.0/255.0, alpha: 1)
        
        landlinebottomLabel.backgroundColor = UIColor.whiteColor()
        view.endEditing(true)
        
        payBtn.setTitle("PAY", forState: .Normal)
        View_pre.hidden = true
        View_post.hidden = true
        View_dth.hidden = true
        View_elec.hidden = true
        View_gas.hidden = true
        View_ins.hidden = true
        View_lan.hidden = false
        
        radioView.hidden = true
        payBtn.tag = 6
        Appconstant.segmentedIndex = 6
        serviceType7.hidden = true
        amounttxtFieldAfterServiceType.hidden = true
        labelLineForAmount3.hidden = true
        constriantButtonAccount7.hidden = true
        constriantButtonServiceType7.hidden = true
        segmentController.selectedSegmentIndex = 6
        if (from_provider == true && segmentno_provider == 6)
        {
            if(provider_Name == "")
            {providername = "Select Provider"
                providerbtn5.setTitle("Select Provider", forState: .Normal)
                special = false
            }
            else
            {
                special = false
                constraintlan.hidden = false
                providername = Operators.lan_operatorName[index_prov]
                providerbtn7.setTitle(Operators.lan_operatorName[index_prov], forState: .Normal)
                self.activityIndicator.stopAnimating()
                phonenotxtField7.text = ""
                account_no7.text = ""
                amounttxtField7_7.text = ""
                amounttxtField7.text = ""
                operatorcode = Operators.lan_operatorCode[index_prov]
                lan_provider = Operators.lan_operatorName[index_prov]
                
                if(lan_provider != "BSNL landline" || lan_provider != "MTNL Delhi")
                {
                    account_no7.hidden = true
                    constriantButtonAccount7.hidden = true
                    amounttxtField7.hidden = false
                    amounttxtField7_7.hidden = true
                    label7_7.hidden = true
                    amounttxtFieldAfterServiceType.hidden = true
                    serviceType7.hidden = true
                    constriantButtonServiceType7.hidden = true
                    labelLineForAmount3.hidden = true
                }
                else
                {
                    
                }
            }
        }
    }
    @IBAction func topupBtn(sender: AnyObject) {
        topupfunction()
        
    }
    
    func topupfunction(){
        specialselected = false
        radio = 0
        topupBtn.setImage(UIImage(named: "radio-on-button-1.png"), forState: .Normal)
        specialBtn.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
    }
    
    @IBAction func specialBtn(sender: AnyObject) {
        specialfunc()
        
    }
    
    func specialfunc(){
        specialselected = true
        radio = 1
        topupBtn.setImage(UIImage(named: "circle-outline (1).png"), forState: .Normal)
        specialBtn.setImage(UIImage(named: "radio-on-button-1.png"), forState: .Normal)
    }
    
    @IBAction func contactpickerBtnAction1(sender: AnyObject) {
        
        
        let contactPickerViewController = CNContactPickerViewController()
        
        contactPickerViewController.predicateForEnablingContact = NSPredicate(format: "Phonenumber != nil")
        
        contactPickerViewController.delegate = self
        
        presentViewController(contactPickerViewController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func contactpickerBtnAction2(sender: AnyObject) {
        
        let contactPickerViewController = CNContactPickerViewController()
        
        contactPickerViewController.predicateForEnablingContact = NSPredicate(format: "Phonenumber != nil")
        
        contactPickerViewController.delegate = self
        
        presentViewController(contactPickerViewController, animated: true, completion: nil)
        
        
    }
    
    
    func contactPicker(picker: CNContactPickerViewController, didSelectContact contact: CNContact) {
        phonenumber.removeAll()
        var phonenos = [String]()
        for phoneno:CNLabeledValue in contact.phoneNumbers{
            let numVal = phoneno.value as! CNPhoneNumber
            phonenos.append(numVal.stringValue)
            
        }
        setcontactnumber(phonenos)
        //        navigationController?.popViewControllerAnimated(true)
    }
    
    func setcontactnumber(numbers: [String]){
        for number1 in numbers {
            var number = ""
            number = number1.stringByReplacingOccurrencesOfString("+", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            number = number.stringByReplacingOccurrencesOfString(") ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            number = number.stringByReplacingOccurrencesOfString("(", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            number = number.stringByReplacingOccurrencesOfString(")", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            number = number.stringByReplacingOccurrencesOfString("-", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            number = number.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            number = number.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            number = number.stringByReplacingOccurrencesOfString("[^0-9+]", withString: "", options: NSStringCompareOptions.RegularExpressionSearch, range:nil)
            if(number.characters.count == 10){
                phonenumber.append(number)
            }
            else{
                var str = number.componentsSeparatedByString("91")
                print(str.count)
                if(str.count == 1 || str.count > 2){
                    phonenumber.append(number)
                }
                else if(str[1].characters.count > 5){
                    
                    phonenumber.append(str[1])
                }
                else{
                    phonenumber.append(number)
                }
            }
            
            
            
        }
        if(phonenumber.count == 0){
            self.presentViewController(Alert().alert("Number not found", message: ""),animated: true,completion: nil)
        }
        else if(phonenumber.count == 1){
            if(segmentController.selectedSegmentIndex == 0)
            {
                contactnotxtfield1.text = phonenumber[0]
                amounttxtField1.becomeFirstResponder()
                amounttxtField1_1.becomeFirstResponder()
                operatortxtfield1.userInteractionEnabled = true
                for(var i = 0; i<Mobilelist.mbl_Mobile.count; i++){
                    //                    print("MBL_COUNT:\(Mobilelist.mbl_Mobile.count)")
                    let contactno = phonenumber[0].substringWithRange(phonenumber[0].startIndex.advancedBy(0)..<phonenumber[0].startIndex.advancedBy(4))
                    if Mobilelist.mbl_Mobile[i].containsString(contactno){
                        providerbtn1.setTitle(Mobilelist.mbl_Operator[i], forState: .Normal)
                        operatortxtfield1.setTitle(Mobilelist.mbl_Location[i], forState: .Normal)
                        self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Normal)
                        self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Selected)
                        for(var j = 0; j<Operators.pre_operatorName.count; j++){
                            if Operators.pre_operatorName[j] == Mobilelist.mbl_Operator[i]{
                                if Operators.pre_special[j]{
                                    radioView.hidden = false
                                }
                                else{
                                    radioView.hidden = true
                                }
                            }
                            
                        }
                        browsePlanbtn1.hidden = false
                    }
                }
            }
            else
            {
                amounttxtField2_2.becomeFirstResponder()
                
                contactnotxtfield2.text = phonenumber[0]
                amounttxtField2.becomeFirstResponder()
                operatortxtfield1.userInteractionEnabled = true
                for(var i = 0; i<Mobilelist.mbl_Mobile.count; i++){
                    print("MBL_COUNT:\(Mobilelist.mbl_Mobile.count)")
                    let contactno = phonenumber[0].substringWithRange(phonenumber[0].startIndex.advancedBy(0)..<phonenumber[0].startIndex.advancedBy(4))
                    if Mobilelist.mbl_Mobile[i].containsString(contactno){
                        providerbtn2.setTitle(Mobilelist.mbl_Operator[i], forState: .Normal)
                        operatortxtfield1.setTitle(Mobilelist.mbl_Location[i], forState: .Normal)
                        self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Normal)
                        self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Selected)
                        for(var j = 0; j<Operators.pre_operatorName.count; j++){
                            if Operators.pre_operatorName[j] == Mobilelist.mbl_Operator[i]{
                                if Operators.pre_special[j]{
                                    radioView.hidden = false
                                }
                                else{
                                    radioView.hidden = true
                                }
                            }
                            
                        }
                        browsePlanbtn1.hidden = false
                    }
                }
            }
            
            
            
            
        }
        else{
            let alertView: UIAlertView = UIAlertView()
            alertView.delegate = self
            alertView.title = "Pick a number.."
            for(var i = 0; i<phonenumber.count; i++){
                alertView.addButtonWithTitle(phonenumber[i])
            }
            alertView.addButtonWithTitle("Cancel")
            self.contactalert = true
            alertView.show()
        }
    }
    
    func currentDate()
    {
        
        let date = NSDate()
        print(date)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC")
        //        let date = dateFormatter.dateFromString(date)
        
        
        dateFormatter.dateFormat = "yyyyMMddHHmmss"///this is you want to convert format
        dateFormatter.timeZone = NSTimeZone(name: "UTC")
        let DateandTime = dateFormatter.stringFromDate(date)
        currentdate = DateandTime
        print("DateandTime")
        print(DateandTime)
        print(currentdate)
    }
    
    @IBAction func textfieldEdit(sender: UITextField) {
        
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.Date
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action:Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        
        
    }
    
    
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textFieldToChange == amounttxtField1 
        {
            let currentText = amounttxtField1.text ?? ""
            let replacementText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Validate
            return replacementText.isValidDecimal(2)
        }
      if textFieldToChange == amounttxtField2
        {
            let currentText = amounttxtField2.text ?? ""
            let replacementText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Validate
            return replacementText.isValidDecimal(2)
        }
        if textFieldToChange == amounttxtField3
        {
            let currentText = amounttxtField3.text ?? ""
            let replacementText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Validate
            return replacementText.isValidDecimal(2)
        }
    if textFieldToChange == amounttxtField4
        {
            let currentText = amounttxtField4.text ?? ""
            let replacementText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Validate
            return replacementText.isValidDecimal(2)
        }
      if textFieldToChange == amounttxtfield4_4
        {
            let currentText = amounttxtfield4_4.text ?? ""
            let replacementText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Validate
            return replacementText.isValidDecimal(2)
        }
        if textFieldToChange == amounttxtField5
        {
            let currentText = amounttxtField5.text ?? ""
            let replacementText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Validate
            return replacementText.isValidDecimal(2)
        }
        if textFieldToChange == amounttxtField6
        {
            let currentText = amounttxtField6.text ?? ""
            let replacementText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Validate
            return replacementText.isValidDecimal(2)
        }
        if textFieldToChange == amounttxtField6_6
        {
            let currentText = amounttxtField6_6.text ?? ""
            let replacementText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Validate
            return replacementText.isValidDecimal(2)
        }
        if textFieldToChange == amounttxtField7
        {
            let currentText = amounttxtField7.text ?? ""
            let replacementText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Validate
            return replacementText.isValidDecimal(2)
        }
        if textFieldToChange == amounttxtField7_7
        {
            let currentText = amounttxtField7_7.text ?? ""
            let replacementText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Validate
            return replacementText.isValidDecimal(2)
        }
        if textFieldToChange == amounttxtFieldAfterServiceType
        {
            let currentText = amounttxtFieldAfterServiceType.text ?? ""
            let replacementText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Validate
            return replacementText.isValidDecimal(2)
        }

    
        if(textFieldToChange.placeholder == "Enter 4 digit Password"){
            characterCountLimit = 4
            textFieldToChange.borderStyle = .None
        }
        
        if(textFieldToChange == contactnotxtfield1){
            characterCountLimit = 10
            
            if ((contactnotxtfield1.text?.characters.count == 3 && string != "") || (contactnotxtfield1.text?.characters.count == 4 && string != "")) {
                let contactno = contactnotxtfield1.text! + string
                //                print("CONTACT_NO:\(contactno)")
                //                print("MBL_COUNT:\(Mobilelist.mbl_Mobile.count)")
                for(var i = 0; i<Mobilelist.mbl_Mobile.count; i++){
                    print("MBL_COUNT:\(Mobilelist.mbl_Mobile.count)")
                    
                    if Mobilelist.mbl_Mobile[i].containsString(contactno) {
                        providerbtn1.setTitle(Mobilelist.mbl_Operator[i], forState: .Normal)
                        operatortxtfield1.setTitle(Mobilelist.mbl_Location[i], forState: .Normal)
                        self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Normal)
                        self.operatortxtfield1.setTitleColor(UIColor.blackColor(), forState: .Selected)
                        for(var j = 0; j<Operators.pre_operatorName.count; j++){
                            if Operators.pre_operatorName[j] == Mobilelist.mbl_Operator[i]{
                                if Operators.pre_special[j]{
                                    radioView.hidden = false
                                }
                                else{
                                    radioView.hidden = true
                                }
                            }
                            
                        }
                        browsePlanbtn1.hidden = false
                    }
                }
            }
            if (contactnotxtfield1.text?.characters.count == 0 || (contactnotxtfield1.text?.characters.count == 1 && string == ""))  {
                browsePlanbtn1.hidden = true
            }
            if contactnotxtfield1.text?.characters.count >= 3 && string != ""{
                operatortxtfield1.userInteractionEnabled = true
            }
            else{
                operatortxtfield1.userInteractionEnabled = false
            }
            
            
            if contactnotxtfield1.text?.characters.count == 9 && string != ""
            {
                if(contactnotxtfield1.text!+string == "0000000000" || contactnotxtfield1.text!+string == "9999999999" || contactnotxtfield1.text!+string == "8888888888" || contactnotxtfield1.text!+string == "7777777777" || contactnotxtfield1.text!+string == "6666666666" || contactnotxtfield1.text!+string == "5555555555" || contactnotxtfield1.text!+string == "4444444444" || contactnotxtfield1.text!+string == "3333333333" || contactnotxtfield1.text!+string == "2222222222" || contactnotxtfield1.text!+string == "1111111111")
                {
                    self.presentViewController(Alert().alert("Please enter the valid mobile number!", message: ""),animated: true,completion: nil)
                }
            }
            
            
        }

        if(textFieldToChange == contactnotxtfield2){
            characterCountLimit = 10
            
            if (contactnotxtfield2.text?.characters.count == 3 || contactnotxtfield2.text?.characters.count == 4) && string != ""{
                let contactno = contactnotxtfield2.text! + string
                //                print("CONTACT_NO:\(contactno)")
                //                print("MBL_COUNT:\(Mobilelist.mbl_Mobile.count)")
                for(var i = 0; i<Mobilelist.mbl_Mobile.count; i++){
                    //                    print("MBL_COUNT:\(Mobilelist.mbl_Mobile.count)")
                    if Mobilelist.mbl_Mobile[i].containsString(contactno) {
                        providerbtn2.setTitle(Mobilelist.mbl_Operator[i], forState: .Normal)
                        for(var j = 0; j<Operators.pre_operatorName.count; j++){
                            if Operators.pre_operatorName[j] == Mobilelist.mbl_Operator[i]{
                                if Operators.pre_special[j]{
                                    radioView.hidden = false
//                                    radioView.frame.origin.y = 208
                                }
                                else{
                                    radioView.hidden = true
//                                    radioView.frame.origin.y = 208
                                }
                            }
                        }
                        
                    }
                }
            }
            if contactnotxtfield2.text?.characters.count == 9 && string != ""
            {
                if(contactnotxtfield2.text!+string == "0000000000" || contactnotxtfield2.text!+string == "9999999999" || contactnotxtfield2.text!+string == "8888888888" || contactnotxtfield2.text!+string == "7777777777" || contactnotxtfield2.text!+string == "6666666666" || contactnotxtfield2.text!+string == "5555555555" || contactnotxtfield2.text!+string == "4444444444" || contactnotxtfield2.text!+string == "3333333333" || contactnotxtfield2.text!+string == "2222222222" || contactnotxtfield2.text!+string == "1111111111")
                {
                    self.presentViewController(Alert().alert("Oops! Please enter the valid mobile number!", message: ""),animated: true,completion: nil)
                }
            }        }
        
        
        //ELECTRICITY
        if(textFieldToChange == customer_no4)
        {
            constraintsEle.hidden = false
            textFieldToChange.keyboardType = UIKeyboardType.NumberPad
            //            self.view.frame.origin.y = -170
            print(index_prov)
            if(providerbtn4 == "")
            {
                self.presentViewController(Alert().alert("Please Select Electricity Board", message: ""),animated: true,completion: nil)
                //                attributedText.addAttribute(NSFontAttributeName, value: UIFont(name: "ProximaNova-Semibold", size: 20.0)!, range: range)
                //
                //
            }
                
            else
            {
                validString = ""
                
                
                characterCountLimit = 50
                print(range)
                print("range")
                let userEnteredString = customer_no4.text
                
                let newString = (userEnteredString! as NSString).stringByReplacingCharactersInRange(range, withString: string) as NSString
                
                
                
                if(newString.length >= 1 )
                {
                    print("index_prov")
                    print(Operators.elect_Authenticator1[index_prov])
                    
                    let authComponent1:NSArray = Operators.elect_Authenticator1[index_prov].componentsSeparatedByString("!!")
                    print(authComponent1[0])
                    print(authComponent1[1])
                    print(authComponent1[2])
                    validString = self.checkValidExpression(newString as String,myPattern: authComponent1[1] as! String)
                    
                    print(customer_no4.text?.characters.count)
                    
                    if(newString.length == temp_lastDigitCharacterLength)
                    {
                        
                    }
                        
                    else
                    {
                        
                        if(validString == "valid")
                        {
                            print("valid")
                            constraintsEle.hidden = true
                            
                            if(newString.length >= 9 && validString == "valid")
                            {
                                if(customer_no4.text!+string == "00000000" || customer_no4.text!+string == "999999999" || customer_no4.text!+string == "888888888" || customer_no4.text!+string == "777777777" || customer_no4.text!+string == "666666666" || customer_no4.text!+string == "555555555" || customer_no4.text!+string == "444444444" || customer_no4.text!+string == "333333333" || customer_no4.text!+string == "222222222" || customer_no4.text!+string == "111111111")
                                {
                                    self.presentViewController(Alert().alert("Please enter the valid customer number!", message: ""),animated: true,completion: nil)
                                }
                                    
                                else
                                {
                                    print("self.providerbtn4.titleLabel?.text")
                                    print(self.providerbtn4.titleLabel?.text!)
                                    print(self.providerbtn4.titleLabel?.text)
                                    print(self.providerbtn4.titleLabel?.text)
                                    if (self.providerbtn4.titleLabel?.text != "West Bengal State Electricity Distribution Company Limited" && self.providerbtn4.titleLabel?.text != "Torrent Power")
                                    {
                                        var selectedBillerId = Operators.elect_operatorCode[index_prov]
                                        
                                        let fetchBill = FetchBillModel.init(entityId: Appconstant.customerid, dateTime: self.currentdate, billerId:selectedBillerId, authenticator1: newString as String, authenticator2: "", authenticator3:  "", transactionId:self.transactionId)!
                                        let serializedjson  = JSONSerializer.toJson(fetchBill)
                                        print(serializedjson)
                                        self.activityIndicator.startAnimating()
                                        self.sendrequesttoserverForFetchAmount(Appconstant.BASE_URL+Appconstant.URL_FETCH_BILL, values: serializedjson)
                                        
                                        
                                        
                                        print(Operators.elect_operatorName)
                                        print(Operators.elect_operatorCode)
                                        print(index_prov)
                                        print(Operators.elect_operatorCode[index_prov])
                                    }
                                        
                                    else
                                    {
                                        
                                        //                        var selectedBillerId = Operators.elect_operatorCode[index_prov]
                                        //
                                        //                        dispatch_async(dispatch_get_main_queue())
                                        //                        {
                                        //                            let fetchBill = FetchBillModel.init(entityId: Appconstant.customerid, dateTime: self.currentdate, billerId:selectedBillerId, authenticator1: newString as String, authenticator2: "", authenticator3:  "", transactionId:self.transactionId)!
                                        //                            let serializedjson  = JSONSerializer.toJson(fetchBill)
                                        //                            print(serializedjson)
                                        //                            self.activityIndicator.startAnimating()
                                        //                            self.sendrequesttoserverForFetchAmount(Appconstant.BASE_URL+Appconstant.URL_FETCH_BILL, values: serializedjson)
                                        //
                                        //                        }
                                        
                                    }
                                    
                                }
                            }
                            else
                            {
                                constraintsEle.hidden = false
                                /*
                                 Changed for constriant button  alert View
                                 toastLabel.text = authComponent1[2] as? String
                                 
                                 toastLabel.hidden = false
                                 */
                                
                                //                             toastLabel = UILabel(frame: CGRectMake(self.view.frame.size.width/2 - 150, self.view.frame.size.height-150, 300, 105))
                                //                            toastLabel.backgroundColor = UIColor.blackColor()
                                //                            toastLabel.textColor = UIColor.whiteColor()
                                //                            toastLabel.textAlignment = NSTextAlignment.Center;
                                //                            //                        toastLabel.sizeToFit()
                                //                            toastLabel.numberOfLines = 5
                                //
                                //                            self.view.addSubview(toastLabel)
                                //                            toastLabel.text = authComponent1[2] as? String
                                //                            toastLabel.alpha = 1.0
                                //                            toastLabel.layer.cornerRadius = 10;
                                //                            toastLabel.clipsToBounds  =  true
                                
                                //                            UIView.animateWithDuration(4.0, delay: 0.1, options: .CurveEaseOut, animations: {
                                //
                                //                                self.toastLabel.alpha = 0.0
                                //
                                //                                }, completion: nil)
                                
                                
                                //                        self.presentViewController(Alert().alert("\(authComponent1[2])", message: ""),animated: true,completion: nil)
                                temp_lastDigitCharacterLength = (newString.length)
                                
                            }
                            
                        }
                        
                    }
                }
                
                
            }
            
        }
        
        if(textFieldToChange == phone_no4)
        {
            constraintele2.hidden = false
            textFieldToChange.keyboardType = UIKeyboardType.NumberPad
            //            self.view.frame.origin.y = -170
            print(index_prov)
            if(customer_no4 == "")
            {
            }
                
            else
            {
                validString = ""
                
                
                characterCountLimit = 50
                print(range)
                let userEnteredString1 = phone_no4.text
                
                var newString2 = (userEnteredString1! as NSString).stringByReplacingCharactersInRange(range, withString: string) as NSString
                
                
                
                if(newString2.length >= 1 )
                {
                    print("index_prov")
                    print(Operators.elect_Authenticator2[index_prov])
                    
                    let authComponent1:NSArray = Operators.elect_Authenticator2[index_prov].componentsSeparatedByString("!!")
                    print(authComponent1[0])
                    print(authComponent1[1])
                    print(authComponent1[2])
                    validString = self.checkValidExpression(newString2 as String,myPattern: authComponent1[1] as! String)
                    
                    print(phone_no4.text?.characters.count)
                    
                    if(newString2.length == temp_lastDigitCharacterLength)
                    {
                        
                    }
                        
                    else
                    {
                        
                        if(validString == "valid")
                        {
                            print("valid")
                            constraintele2.hidden = true
                            if(newString2.length >= 9 && validString == "valid")
                            { print("self.providerbtn4.titleLabel?.text")
                                print(self.providerbtn4.titleLabel?.text!)
                                print(self.providerbtn4.titleLabel?.text)
                                print(self.providerbtn4.titleLabel?.text)
                                if (self.providerbtn4.titleLabel?.text == "West Bengal State Electricity Distribution Company Limited" || self.providerbtn4.titleLabel?.text == "Torrent Power")
                                {
                                    var selectedBillerId = Operators.elect_operatorCode[index_prov]
                                    
                                    let fetchBill = FetchBillModel.init(entityId: Appconstant.customerid, dateTime: self.currentdate, billerId:selectedBillerId, authenticator1: customer_no4.text!, authenticator2: newString2 as String,authenticator3:  "", transactionId:self.transactionId)!
                                    let serializedjson  = JSONSerializer.toJson(fetchBill)
                                    print(serializedjson)
                                    self.activityIndicator.startAnimating()
                                    self.sendrequesttoserverForFetchAmount(Appconstant.BASE_URL+Appconstant.URL_FETCH_BILL, values: serializedjson)
                                    
                                    
                                    
                                    print(Operators.elect_operatorName)
                                    print(Operators.elect_operatorCode)
                                    print(index_prov)
                                    print(Operators.elect_operatorCode[index_prov])
                                }
                                    
                                else
                                {
                                    //                        var selectedBillerId = Operators.elect_operatorCode[index_prov]
                                    //
                                    //                        dispatch_async(dispatch_get_main_queue())
                                    //                        {
                                    //                            let fetchBill = FetchBillModel.init(entityId: Appconstant.customerid, dateTime: self.currentdate, billerId:selectedBillerId, authenticator1: newString as String, authenticator2: "", authenticator3:  "", transactionId:self.transactionId)!
                                    //                            let serializedjson  = JSONSerializer.toJson(fetchBill)
                                    //                            print(serializedjson)
                                    //                            self.activityIndicator.startAnimating()
                                    //                            self.sendrequesttoserverForFetchAmount(Appconstant.BASE_URL+Appconstant.URL_FETCH_BILL, values: serializedjson)
                                    //
                                    //                        }
                                    
                                }
                                
                                
                            }
                            else
                            {
                                constraintele2.hidden = false
                                /*
                                 Changed for constriant button  alert View
                                 toastLabel.text = authComponent1[2] as? String
                                 
                                 toastLabel.hidden = false
                                 */
                                
                                //                             toastLabel = UILabel(frame: CGRectMake(self.view.frame.size.width/2 - 150, self.view.frame.size.height-150, 300, 105))
                                //                            toastLabel.backgroundColor = UIColor.blackColor()
                                //                            toastLabel.textColor = UIColor.whiteColor()
                                //                            toastLabel.textAlignment = NSTextAlignment.Center;
                                //                            //                        toastLabel.sizeToFit()
                                //                            toastLabel.numberOfLines = 5
                                //
                                //                            self.view.addSubview(toastLabel)
                                //                            toastLabel.text = authComponent1[2] as? String
                                //                            toastLabel.alpha = 1.0
                                //                            toastLabel.layer.cornerRadius = 10;
                                //                            toastLabel.clipsToBounds  =  true
                                
                                //                            UIView.animateWithDuration(4.0, delay: 0.1, options: .CurveEaseOut, animations: {
                                //
                                //                                self.toastLabel.alpha = 0.0
                                //
                                //                                }, completion: nil)
                                
                                
                                //                        self.presentViewController(Alert().alert("\(authComponent1[2])", message: ""),animated: true,completion: nil)
                                temp_lastDigitCharacterLength = (newString2.length)
                                
                            }
                        }
                    }
                    
                    
                }
            }
            
}
        if(textFieldToChange == policyno6){
            constraintins.hidden = false
            textFieldToChange.keyboardType = UIKeyboardType.NumberPad
            if(providerbtn6.titleLabel?.text == "")
            {
                self.presentViewController(Alert().alert("Please select Company", message: ""),animated: true,completion: nil)
                
            }
            else
            {
                validString = ""
                
                
                characterCountLimit = 50
                print(range)
                let userEnteredString1 = policyno6.text
                
                var newStringins = (userEnteredString1! as NSString).stringByReplacingCharactersInRange(range, withString: string) as NSString
                
                
                
                if(newStringins.length >= 1 )
                {
                    
                    print(Operators.ins_Authenticator1[index_prov])
                    
                    let authComponent1:NSArray = Operators.ins_Authenticator1[index_prov].componentsSeparatedByString("!!")
                    print(authComponent1[0])
                    print(authComponent1[1])
                    print(authComponent1[2])
                    validString = self.checkValidExpression(newStringins as String,myPattern: authComponent1[1] as! String)
                    
                    print(policyno6.text?.characters.count)
                    
                    if(newStringins.length == temp1_lastDigitCharacterLength)
                    {
                        
                    }
                    else
                    {
                        
                        if(validString == "valid")
                        {
                            print("valid")
                            constraintins.hidden = true
                            //                let temp_lastDigitCharacterLength = customer_no4.text?.characters.count
                            
                        }
                        else
                        {
                            constraintins.hidden = false
                            
                            temp_lastDigitCharacterLength = (newStringins.length)
                            
                        }
                    }
                    
                }
                if(newStringins.length >= 1 && validString == "valid")
                {
                    print(Operators.ins_operatorName)
                    print(Operators.ins_operatorCode)
                    print(selectedRow)
                    print(Operators.ins_operatorCode[index_prov])
                    if (self.providerbtn6.titleLabel?.text == "INDIA FIRST Life Insurance" || self.providerbtn6.titleLabel?.text ==  "PNB Metlife" )
                    {var selectedBillerId = Operators.ins_operatorCode[index_prov]
                        
                        dispatch_async(dispatch_get_main_queue())
                        {
                            let fetchBill = FetchBillModel.init(entityId: Appconstant.customerid, dateTime: self.currentdate, billerId:selectedBillerId, authenticator1: newStringins as String, authenticator2: "", authenticator3:  "", transactionId:self.transactionId)!
                            let serializedjson  = JSONSerializer.toJson(fetchBill)
                            print(serializedjson)
                            self.activityIndicator.startAnimating()
                            self.sendrequesttoserverForFetchAmount(Appconstant.BASE_URL+Appconstant.URL_FETCH_BILL, values: serializedjson)
                        }
                    }
                }
                
            }
        }
        if(textFieldToChange == dobtxtField6){
            datepickerfunction()

        }
        //LANDLINE
        
        if(textFieldToChange == phonenotxtField7)
        {
            constraintlan.hidden = false
            textFieldToChange.keyboardType = UIKeyboardType.NumberPad
            if(providerbtn7.titleLabel?.text == "")
            {
                self.presentViewController(Alert().alert("Please select Company", message: ""),animated: true,completion: nil)
                
            }
            else
            {
                validString = ""
                
                characterCountLimit = 50
                print(range)
                let userEnteredString_phone = phonenotxtField7.text
                
                var newString_landline = (userEnteredString_phone! as NSString).stringByReplacingCharactersInRange(range, withString: string) as NSString
                
                if(newString_landline.length >= 1 )
                {
                    
                    print(Operators.lan_Authenticator1[index_prov])
                    
                    
                    let authComponent1:NSArray = Operators.lan_Authenticator1[index_prov].componentsSeparatedByString("!!")
                    print(authComponent1[0])
                    print(authComponent1[1])
                    print(authComponent1[2])
                    validString = self.checkValidExpression(newString_landline as String,myPattern: authComponent1[1] as! String)
                    
                    print(newString_landline.length)
                    
                    if(newString_landline.length == temp2_lastDigitCharacterLength)
                    {
                        
                    }
                    else
                    {
                        
                        if(validString == "valid")
                        {
                            print("valid")
                            constraintlan.hidden = true
                            if(lan_provider == "BSNL landline" || lan_provider == "MTNL Delhi")
                            {
                                account_no7.hidden = false
                                constriantButtonAccount7.hidden = false
                                amounttxtField7.hidden = true
                                amounttxtField7_7.hidden = false
                                amounttxtFieldAfterServiceType.hidden = true
                                labelLineForAmount3.hidden = true
                                amounttxtFieldAfterServiceType.text = ""
                                amounttxtField7.text = ""
                                label7_7.hidden = false
                            }
                            else if (lan_provider == "Airtel Landline"){
                                
                                account_no7.hidden = false
                                constriantButtonAccount7.hidden = true
                                amounttxtField7.hidden = true
                                amounttxtFieldAfterServiceType.hidden = true
                                labelLineForAmount3.hidden = true
                                amounttxtFieldAfterServiceType.text = ""
                                amounttxtField7.text = ""
                                amounttxtField7_7.hidden = false
                                label7_7.hidden = false
                                
                                
                            }
                                
                            else{
                                
                                account_no7.hidden = false
                                constriantButtonAccount7.hidden = true
                                amounttxtField7.hidden = true
                                amounttxtFieldAfterServiceType.hidden = true
                                amounttxtField7_7.hidden = false
                                label7_7.hidden = false
                                amounttxtFieldAfterServiceType.text = ""
                                amounttxtField7.text = ""
                                
                            }
                            
                            
                        }
                        else
                        {
                            constraintlan.hidden = false
                            temp_lastDigitCharacterLength = (newString_landline.length)
                            
                        }
                    }
                    
                }
                
                
                
                if(newString_landline.length >= 5 && validString == "valid")
                {
                    
                    if Operators.lan_Authenticator2[index_prov] == "" && account_no7.hidden == true
                    {
                        print(Operators.lan_operatorName)
                        print(Operators.lan_operatorCode)
                        print(selectedRow)
                        print(Operators.lan_operatorCode[index_prov])
                        var selectedBillerId = Operators.lan_operatorCode[index_prov]
                        
                        let fetchBill = FetchBillModel.init(entityId: Appconstant.customerid, dateTime: currentdate, billerId:selectedBillerId, authenticator1: newString_landline as String, authenticator2: "", authenticator3:  "", transactionId: "")!
                        let serializedjson  = JSONSerializer.toJson(fetchBill)
                        print(serializedjson)
                        self.activityIndicator.startAnimating()
                        self.sendrequesttoserverForFetchAmount(Appconstant.BASE_URL+Appconstant.URL_FETCH_BILL, values: serializedjson)
                        //                        self.sendrequesttoserverForRecharge(Appconstant.BASE_URL+Appconstant.URL_PAY_BILL, values: serializedjson)
                        
                    }
                    else
                    {
                        //                account_no7.becomeFirstResponder()
                        
                    }
                }
                
            }
            
        }
        
        if(textFieldToChange == account_no7)
        {
            if(lan_provider == "Airtel Landline")
            {
                constriantButtonAccount7.hidden = true
            }
                
                
            else
            {
                
                constriantButtonAccount7.hidden = false
                textFieldToChange.keyboardType = UIKeyboardType.NumberPad
                if(phonenotxtField7.text == "")
                {
                }
                else
                {
                    validString = ""
                    
                    characterCountLimit = 50
                    print(range)
                    let userEnteredString_phone2 = account_no7.text
                    
                    var newString_landline2 = (userEnteredString_phone2! as NSString).stringByReplacingCharactersInRange(range, withString: string) as NSString
                    
                    if(newString_landline2.length >= 1 )
                    {
                        
                        let authValue = Operators.lan_Authenticator2[index_prov] as NSString
                        print("authValue.length==>>")
                        print(authValue.length)
                        var authComponent1:NSArray = NSArray()
                        if(authValue.length == 0)
                        {
                            authComponent1 = ["","",""]
                            if(newString_landline2.length == 10)
                            {
                                
                                if Operators.pre_Authenticator3[index_prov] == ""
                                {
                                    print(Operators.lan_operatorName)
                                    print(Operators.lan_operatorCode)
                                    print(selectedRow)
                                    print(Operators.lan_operatorCode[index_prov])
                                    var selectedBillerId = Operators.lan_operatorCode[index_prov]
                                    
                                    let fetchBill = FetchBillModel.init(entityId: Appconstant.customerid, dateTime: currentdate, billerId:selectedBillerId, authenticator1: phonenotxtField7.text!, authenticator2: newString_landline2 as String , authenticator3:  "", transactionId: "")!
                                    let serializedjson  = JSONSerializer.toJson(fetchBill)
                                    print(serializedjson)
                                    self.activityIndicator.startAnimating()
                                    self.sendrequesttoserverForFetchAmount(Appconstant.BASE_URL+Appconstant.URL_FETCH_BILL, values: serializedjson)
                                    // self.sendrequesttoserverForRecharge(Appconstant.BASE_URL+Appconstant.URL_PAY_BILL, values: serializedjson)
                                    
                                }
                                else
                                {
                                    serviceType7.hidden = false
                                    constriantButtonServiceType7.hidden = false
                                    amounttxtField7_7.hidden = true
                                    label7_7.hidden = false
                                    //                        labelLineForAmount3.hidden = false
                                    //                        account_no7.becomeFirstResponder()
                                    
                                }
                                
                            }
                            
                            
                            
                        }
                        else
                        {
                            print("Operators.lan_Authenticator2[index_prov]==>>")
                            print(Operators.lan_Authenticator2[index_prov])
                            authComponent1 = Operators.lan_Authenticator2[index_prov].componentsSeparatedByString("!!")
                            print(authComponent1[0])
                            print(authComponent1[1])
                            print(authComponent1[2])
                            
                            
                            validString = self.checkValidExpression(newString_landline2 as String,myPattern: authComponent1[1] as! String)
                            
                            print(newString_landline2.length)
                            
                            if(newString_landline2.length == temp22_lastDigitCharacterLength)
                            {
                                
                            }
                            else
                            {
                                
                                if(validString == "valid")
                                {
                                    print("valid")
                                    constriantButtonAccount7.hidden = true
                                    
                                    
                                }
                                else
                                {
                                    
                                    temp_lastDigitCharacterLength = (newString_landline2.length)
                                    
                                }
                            }
                            
                        }
                    }
                    
                    if(newString_landline2.length >= 5 && validString == "valid")
                        //                 if(newString_landline2.length == 10)
                        
                    {
                        
                        if Operators.pre_Authenticator3[index_prov] == ""
                        {
                            print(Operators.lan_operatorName)
                            print(Operators.lan_operatorCode)
                            print(selectedRow)
                            print(Operators.lan_operatorCode[index_prov])
                            var selectedBillerId = Operators.lan_operatorCode[index_prov]
                            
                            let fetchBill = FetchBillModel.init(entityId: Appconstant.customerid, dateTime: currentdate, billerId:selectedBillerId, authenticator1: phonenotxtField7.text!, authenticator2: newString_landline2 as String , authenticator3:  "", transactionId: "")!
                            let serializedjson  = JSONSerializer.toJson(fetchBill)
                            print(serializedjson)
                            self.activityIndicator.startAnimating()
                            self.sendrequesttoserverForFetchAmount(Appconstant.BASE_URL+Appconstant.URL_FETCH_BILL, values: serializedjson)
                            //                        self.sendrequesttoserverForRecharge(Appconstant.BASE_URL+Appconstant.URL_PAY_BILL, values: serializedjson)
                            
                        }
                        else
                        {
                            serviceType7.hidden = false
                            constriantButtonServiceType7.hidden = false
                            amounttxtField7_7.hidden = true
                            label7_7.hidden = false
                            labelLineForAmount3.hidden = false
                            amounttxtFieldAfterServiceType.hidden = false
                            
                            
                            //                        account_no7.becomeFirstResponder()
                            
                        }
                        
                    }
                    else
                    {
                        serviceType7.text = ""
                        amounttxtFieldAfterServiceType.text = ""
                        
                        serviceType7.hidden = true
                        constriantButtonServiceType7.hidden = true
                        amounttxtField7_7.hidden = false
                        label7_7.hidden = false
                        labelLineForAmount3.hidden = true
                        amounttxtFieldAfterServiceType.hidden = true
                        //                        account_no7.becomeFirstResponder()
                        
                    }
                    
                }
                
                
            }
        }
        
        
        if(textFieldToChange == serviceType7)
        {
            constriantButtonServiceType7.hidden = false
            textFieldToChange.keyboardType = UIKeyboardType.NumberPad
            
            if(account_no7.text == "")
            {
                
                
            }
            else
            {
                
                validString = ""
                
                characterCountLimit = 50
                print(range)
                let userEnteredString_phone2 = serviceType7.text
                
                var newString_landline3 = (userEnteredString_phone2! as NSString).stringByReplacingCharactersInRange(range, withString: string) as NSString
                
                if(newString_landline3.length >= 1 )
                {
                    
                    print(Operators.pre_Authenticator3[index_prov])
                    
                    
                    let authComponent1:NSArray = Operators.pre_Authenticator3[index_prov].componentsSeparatedByString("!!")
                    //                    print(authComponent1[0])
                    //                    print(authComponent1[1])
                    //                    print(authComponent1[2])
                    validString = self.checkValidExpression(newString_landline3 as String,myPattern: authComponent1[1] as! String)
                    
                    print(newString_landline3.length)
                    
                    if(newString_landline3.length == temp22_lastDigitCharacterLength)
                    {
                        
                    }
                    else
                    {
                        
                        if(validString == "valid")
                        {
                            print("valid")
                            
                            constriantButtonServiceType7.hidden = true                        }
                        else
                        {
                            constriantButtonServiceType7.hidden = false
                            temp_lastDigitCharacterLength = (newString_landline3.length)
                            
                        }
                    }
                    
                    
                }
                
                if(newString_landline3.length >= 1 && validString == "valid")
                {
                    self.amounttxtFieldAfterServiceType.hidden = false
                    labelLineForAmount3.hidden = false
                    self.amounttxtField7_7.hidden = true
                    
                    print(Operators.lan_operatorName)
                    print(Operators.lan_operatorCode)
                    print(selectedRow)
                    print(Operators.lan_operatorCode[index_prov])
                    var selectedBillerId = Operators.lan_operatorCode[index_prov]
                    
                    let fetchBill = FetchBillModel.init(entityId: Appconstant.customerid, dateTime: currentdate, billerId:selectedBillerId, authenticator1: phonenotxtField7.text!, authenticator2: account_no7.text! , authenticator3:  newString_landline3 as String, transactionId: "")!
                    let serializedjson  = JSONSerializer.toJson(fetchBill)
                    print(serializedjson)
                    self.activityIndicator.startAnimating()
                    self.sendrequesttoserverForFetchAmount(Appconstant.BASE_URL+Appconstant.URL_FETCH_BILL, values: serializedjson)
                    //                    self.sendrequesttoserverForRecharge(Appconstant.BASE_URL+Appconstant.URL_PAY_BILL, values: serializedjson)
                    
                    
                    
                }
                    
                else
                {
                    self.amounttxtField7_7.hidden = true
                }
                
                
                
                
                
                
            }
            
            
            
            
            
            
            
        }
        
        
        
        //GAS
        
        if(textFieldToChange == customeraccnoo5){
            constraintgas.hidden = false
            textFieldToChange.keyboardType = UIKeyboardType.NumberPad
            print(selectedRow)
            if(providerbtn5.titleLabel?.text == "")
            {
                self.presentViewController(Alert().alert("Please Select Gas Company", message: ""),animated: true,completion: nil)
                
            }
            else
            {
                validString = ""
                
                
                characterCountLimit = 50
                print(range)
                let userEnteredString_gas = customeraccnoo5.text
                
                var newString_gas = (userEnteredString_gas! as NSString).stringByReplacingCharactersInRange(range, withString: string) as NSString
                
                
                
                if(newString_gas.length >= 1 )
                {
                    
                    print(Operators.gas_Authenticator1[index_prov])
                    
                    let authComponent1:NSArray = Operators.gas_Authenticator1[index_prov].componentsSeparatedByString("!!")
                    print(authComponent1[0])
                    print(authComponent1[1])
                    print(authComponent1[2])
                    validString = self.checkValidExpression(newString_gas as String,myPattern: authComponent1[1] as! String)
                    
                    print(newString_gas.length)
                    
                    if(newString_gas.length == temp3_lastDigitCharacterLength)
                    {
                        
                    }
                    else
                    {
                        
                        if(validString == "valid")
                        {
                            print("valid")
                            constraintgas.hidden = true
                            
                        }
                        else
                        {
                            constraintgas.hidden = false
                            temp_lastDigitCharacterLength = (newString_gas.length)
                            
                        }
                    }
                    
                }
                if(newString_gas.length >= 8 && validString == "valid")
                {
                    print(Operators.gas_operatorName)
                    print(Operators.gas_operatorCode)
                    print(selectedRow)
                    print(Operators.gas_operatorCode[index_prov])
                    var selectedBillerId = Operators.gas_operatorCode[index_prov]
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let fetchBill = FetchBillModel.init(entityId: Appconstant.customerid, dateTime: self.currentdate, billerId:selectedBillerId, authenticator1: newString_gas as String, authenticator2: "", authenticator3:  "", transactionId:self.transactionId)!
                        let serializedjson  = JSONSerializer.toJson(fetchBill)
                        print(serializedjson)
                        self.activityIndicator.startAnimating()
                        self.sendrequesttoserverForFetchAmount(Appconstant.BASE_URL+Appconstant.URL_FETCH_BILL, values: serializedjson)
                        self.activityIndicator.stopAnimating()
                    }
                    
                }
            }
            
        }
        
        //            if(contactnotxtfield.text?.characters.count == 9){
        //                let location = NSString(string:"/Users/vertace/Desktop/untitled folder/sample/Purz/Cippy/Providers.txt").stringByExpandingTildeInPath
        //                let fileContent = try? NSString(contentsOfFile: location, encoding: NSUTF8StringEncoding)
        //                print(fileContent)
        //                let data = fileContent!.dataUsingEncoding(NSUTF8StringEncoding)
        //                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
        //                print("responseString = \(responseString)")
        //                let json = JSON(data: data!)                //  sendrequesttoserver(Appconstant.BASE_URL+Appconstant.MANAGER_BUSINESS_ENTITY+Appconstant.URL_CHECK_CUSTOMER+number)
        //
        //            }
        
        
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
}
    func checkValidExpression(numberText:String, myPattern:String)-> String
    {
        print(numberText)
        var myString = "\(numberText)"
        let regex = try! NSRegularExpression(pattern: myPattern, options: NSRegularExpressionOptions.CaseInsensitive)
        let range = NSMakeRange(0, myString.characters.count)
        print(myString.characters.count)
        let CountString = myString.characters.count
        let modString = regex.stringByReplacingMatchesInString(myString, options: [], range: range, withTemplate: "valid")
        print("modstrrrrr")
        print(modString)
        if(modString == "valid")
        {
            if segmentController.selectedSegmentIndex == 5 {
                
                if(ins_provider == "INDIA FIRST Life Insurance" || ins_provider == "PNB Metlife")
                {
                    dobtxtField6.hidden = true
                    amounttxtField6.hidden = false
                    amounttxtField6_6.hidden = true
                    label6_6.hidden = true
                }
                else{
                    constraintIns2.hidden = false
                    dobtxtField6.hidden = false
                    amounttxtField6.hidden = true
                    amounttxtField6_6.hidden = false
                    label6_6.hidden = false
                    
                    
                    
                }
            }
            
            
            
        }
        return modString
}

    func getUDID()
    {
        self.transactionId = NSUUID().UUIDString
        print("TransID")
        print(transactionId)
    }
    
    
    @IBAction func providerbtnAction1(sender: AnyObject) {
        
        let alertView: UIAlertView = UIAlertView()
        alertView.delegate = self
        
        
        //alertView.textFieldAtIndex(1)?.font = UIFont.systemFontOfSize(20.0)
        //        alertView. // let font = UIFont.systemFontOfSize(12.0)
        
        if segmentController.selectedSegmentIndex == 0{
            mobileno = contactnotxtfield1.text!
            segmentno_provider = 0
            self.performSegueWithIdentifier("from_provider", sender: self)
            
        }
        else if segmentController.selectedSegmentIndex == 1 {
            mobileno = contactnotxtfield2.text!
            segmentno_provider = 1
            self.performSegueWithIdentifier("from_provider", sender: self)
            
        }
        else if segmentController.selectedSegmentIndex == 2
        {
            mobileno = customerIdtxtfield3.text!
            segmentno_provider = 2
            self.performSegueWithIdentifier("from_provider", sender: self)
        }
        else  if segmentController.selectedSegmentIndex == 3
        {
            segmentno_provider = 3
            self.performSegueWithIdentifier("from_provider", sender: self)
            
        }
            
        else if segmentController.selectedSegmentIndex == 4 {
            segmentno_provider = 4
            self.performSegueWithIdentifier("from_provider", sender: self)
            
        }
        else if segmentController.selectedSegmentIndex == 5 {
            segmentno_provider = 5
            self.performSegueWithIdentifier("from_provider", sender: self)
        }
        else if segmentController.selectedSegmentIndex == 6 {
            segmentno_provider = 6
            self.performSegueWithIdentifier("from_provider", sender: self)
        }
        
        if segmentController.selectedSegmentIndex != 3{
            //            alertView.show()
            //             alertView.addSubview(tableView)
            
        }
    }
    
    
    
    func showtransactionAlert()
    {
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "PASSWORD",
                                            message: "Please enter your 4 digit secret password.This is necessary to initiate every transaction",
                                            preferredStyle: .Alert)
        alertController!.addTextFieldWithConfigurationHandler(
            {(textField: UITextField!) in
                
                textField.placeholder = "Enter 4 digit Password"
                textField.delegate = self
                textField.secureTextEntry  = true
                textField.keyboardType = UIKeyboardType.NumberPad
                textField.layer.borderColor = UIColor.clearColor().CGColor
                textField.borderStyle = .None
                
        })
        let action = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            
            
            if let textFields = alertController?.textFields{
                let theTextFields = textFields as [UITextField]
                let enteredText = theTextFields[0].text
                //        theTextFields[0].borderStyle = .None
                print("Entered Text==>")
                print(enteredText!)
                print(Appconstant.pwd)
                if((enteredText! == "") || (enteredText!.characters.count < 4) || (enteredText! != Appconstant.pwd)){
                    self!.presentViewController(Alert().alert("INVALID PASSWORD", message: ""),animated: true,completion: nil)
                    print("Wrong PWD")
                    
                }
                else{
                    self!.callserverforRecharge()                }
                
                //                if(enteredText == Appconstant.pwd)
                //                {
                //                    self!.callserverforRecharge()
                //                }
                //                else
                //                {
                //                    self!.presentViewController(Alert().alert("INVALID PASSWORD", message: ""),animated: true,completion: nil)
                //                    print("Wrong PWD")
                //                }
                //
                
            }
            })
        let action1 = UIAlertAction(title: "Forgot", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            print(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
            self!.sendrequesttoserverForForgotPassword(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
            
            })
        let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            })
        
        alertController?.addAction(action)
        alertController?.addAction(action1)
        alertController?.addAction(action2)
        self.presentViewController(alertController!, animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func rechargeBtnAction(sender: AnyObject) {
        print(payBtn.tag)
        
        if(payBtn.tag == 0)
        {
            if(contactnotxtfield1.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
                
                
                
            else if(contactnotxtfield1.text?.characters.count != 10)
            {
                
                self.presentViewController(Alert().alert("Oops! Something is wrong, please check the number and try again!", message: ""),animated: true,completion: nil)
                
            }
                
            else if(providerbtn1.titleLabel?.text == "" || providerbtn1.titleLabel?.text == "Select Provider" || operatortxtfield1.titleLabel?.text == "Select Operator Circle")
            {
                self.presentViewController(Alert().alert("Oops! Something is wrong, please check the mobile number and provider name again!", message: ""),animated: true,completion: nil)
            }
                
                
            else if(amounttxtField1.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
                
            else if(Double(amounttxtField1.text!) <= 7)
            {
                self.presentViewController(Alert().alert("Please pick the minimum amount of your Provider 8", message: ""),animated: true,completion: nil)
            }
            else if(Double(Appconstant.mainbalance) < Double(self.amounttxtField1.text!))
            {
                print("Appconstant.mainbalance")
                print(Appconstant.mainbalance)
                print(amounttxtField1.text!)
//                LoadMoneyFromCard(self.amounttxtField1.text!)
               
                self.presentViewController(Alert().alert("Oops! Insufficient Funds..", message: ""),animated: true,completion: nil)
            }
            else
            {
                Appconstant.r_mobileNo = contactnotxtfield1.text!
                Appconstant.r_provider = (providerbtn1.titleLabel?.text)!
                Appconstant.r_operator = (operatortxtfield1.titleLabel?.text)!
                Appconstant.r_amount = amounttxtField1.text!
                Appconstant.fromsegue = true
                Appconstant.segmentValueWhileClickingForgot = 0
                print(" recharge number")
                print(Appconstant.r_mobileNo)
                print(Appconstant.r_provider)
                print(Appconstant.r_operator)
                print(Appconstant.r_amount)
                self.showtransactionAlert()
                //callserverforRecharge()
            }
        }
        if(payBtn.tag == 1)
        {
            if(contactnotxtfield2.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if(contactnotxtfield2.text! == "0000000000" || contactnotxtfield2.text! == "9999999999" || contactnotxtfield2.text! == "8888888888" || contactnotxtfield2.text! == "7777777777" || contactnotxtfield2.text! == "6666666666" || contactnotxtfield2.text! == "5555555555" || contactnotxtfield2.text! == "4444444444" || contactnotxtfield2.text! == "3333333333" || contactnotxtfield2.text! == "2222222222" || contactnotxtfield2.text! == "1111111111")
            {
                self.presentViewController(Alert().alert("Please enter the valid mobile number!", message: ""),animated: true,completion: nil)
            }
            else if contactnotxtfield2.text!.characters.count != 10 {
                self.presentViewController(Alert().alert("Oops! Something is wrong, please check the number and try again!", message: ""),animated: true,completion: nil)
            }
                
            else if(amounttxtField2.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if(providerbtn2.titleLabel?.text == "" || providerbtn2.titleLabel?.text == "Select Provider")
            {
                self.presentViewController(Alert().alert("Oops! Something is wrong, please check the mobile number and provider name again!", message: ""),animated: true,completion: nil)
            }
            else if(Double(amounttxtField2.text!) <= 9)
            {
                self.presentViewController(Alert().alert("Please pick the minimum amount of your Provider 10", message: ""),animated: true,completion: nil)
                
            }
                
            else if(Double(Appconstant.mainbalance) < Double(self.amounttxtField2.text!))
            {
                print("Appconstant.mainbalance")
                print(Appconstant.mainbalance)
                print(amounttxtField2.text!)
//                LoadMoneyFromCard(self.amounttxtField2.text!)
                self.presentViewController(Alert().alert("Oops! Insufficient Funds..", message: ""),animated: true,completion: nil)
                
            }
            else
            {
                Appconstant.r_mobileNo = contactnotxtfield2.text!
                Appconstant.r_provider = (providerbtn2.titleLabel?.text)!
                Appconstant.r_amount = amounttxtField2.text!
                Appconstant.fromsegue = true
                Appconstant.segmentValueWhileClickingForgot = 1
                print(" recharge number")
                print( Appconstant.r_mobileNo)
                print(  Appconstant.r_provider)
                print( Appconstant.r_amount)
                
                self.showtransactionAlert()
                
                //                callserverforRecharge()
            }
            
        }
        if(payBtn.tag == 2)
        {
            if(customerIdtxtfield3.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if(provider3.titleLabel?.text == "" || provider3.titleLabel?.text == "Select Provider")
            {
                self.presentViewController(Alert().alert("Oops! Something is wrong, please check the DTH provider name again!", message: ""),animated: true,completion: nil)
            }
                
            else if(operatortxtfield3.titleLabel?.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if(amounttxtField3.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if(Double(amounttxtField3.text!) <= 9)
            {
                self.presentViewController(Alert().alert("Please pick the minimum amount of your Provider 10", message: ""),animated: true,completion: nil)
                
            }
                
            else if(Double(Appconstant.mainbalance) < Double(self.amounttxtField3.text!))
            {
                
                print("Appconstant.mainbalance")
                print(Appconstant.mainbalance)
                print(amounttxtField3.text!)
//                LoadMoneyFromCard(self.amounttxtField3.text!)
                self.presentViewController(Alert().alert("Oops! Insufficient Funds..", message: ""),animated: true,completion: nil)
                
            }
                
            else
            {
                
                Appconstant.r_customerID = customerIdtxtfield3.text!
                Appconstant.r_provider = (provider3.titleLabel?.text)!
                Appconstant.r_operator = (operatortxtfield3.titleLabel?.text)!
                Appconstant.r_amount = amounttxtField3.text!
                Appconstant.fromsegue = true
                Appconstant.segmentValueWhileClickingForgot = 2
                
                print(" recharge number")
                print( Appconstant.r_mobileNo)
                print(  Appconstant.r_provider)
                print( Appconstant.r_amount)
                
                
                
                self.showtransactionAlert()
                //                callserverforRecharge()
            }
            
        }
        
        
        
        if(payBtn.tag == 3)
        {
            print(validString)
            if(customer_no4.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if (amounttxtField4.text == "" && amounttxtField4.hidden == false)
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if (amounttxtfield4_4.text == "" && amounttxtfield4_4.hidden == false)
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
                
            else if(providerbtn4.titleLabel?.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
                
            else if (validString != "valid")
            {
                self.presentViewController(Alert().alert("Entered ID is not valid", message: ""),animated: true,completion: nil)
            }
            else if(amounttxtField4.text! == "0")
            {
                self.presentViewController(Alert().alert("You dont have any due bill on your account", message: ""),animated: true,completion: nil)
            }
            else if(amounttxtfield4_4.text! == "0" && amounttxtfield4_4.hidden == false)
            {
                self.presentViewController(Alert().alert("You dont have any due bill on your account", message: ""),animated: true,completion: nil)
            }
                
            else if(Double(Appconstant.mainbalance) <= Double(self.amounttxtField4.text!))
            {
                print("Appconstant.mainbalance")
                print(Appconstant.mainbalance)
                print(amounttxtField4.text!)
//                 LoadMoneyFromCard(self.amounttxtField4.text!)
                self.presentViewController(Alert().alert("Oops! Insufficient Funds..", message: ""),animated: true,completion: nil)
                
            }
                
            else if(Double(Appconstant.mainbalance) < Double(self.amounttxtfield4_4.text!) && amounttxtfield4_4.hidden == false)
            {
//                 LoadMoneyFromCard(self.amounttxtfield4_4.text!)
                
                self.presentViewController(Alert().alert("Oops! Insufficient Funds..", message: ""),animated: true,completion: nil)
                
            }
                
            else
            {
                
                Appconstant.r_customerNo = customer_no4.text!
                Appconstant.r_provider = (providerbtn4.titleLabel?.text)!
                
                Appconstant.r_amount = amounttxtField4.text!
                Appconstant.fromsegue = true
                Appconstant.segmentValueWhileClickingForgot = 3
                
                print(" recharge number")
                print( Appconstant.r_mobileNo)
                print(  Appconstant.r_provider)
                print( Appconstant.r_amount)
                
                
                
                var alertController:UIAlertController?
                alertController?.view.tintColor = UIColor.blackColor()
                alertController = UIAlertController(title: "PASSWORD",
                                                    message: "Please enter your 4 digit secret password.This is necessary to initiate every transaction",
                                                    preferredStyle: .Alert)
                alertController!.addTextFieldWithConfigurationHandler(
                    {(textField: UITextField!) in
                        
                        textField.placeholder = "Enter 4 digit Password"
                        textField.delegate = self
                        textField.secureTextEntry  = true
                        textField.keyboardType = UIKeyboardType.NumberPad
                        textField.layer.borderColor = UIColor.clearColor().CGColor
                        textField.borderStyle = .None
                        
                        
                })
                let action = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    
                    
                    if let textFields = alertController?.textFields{
                        let theTextFields = textFields as [UITextField]
                        let enteredText = theTextFields[0].text
                        //                        theTextFields[0].borderStyle = .None
                        print("Entered Text==>")
                        print(enteredText!)
                        if(enteredText == Appconstant.pwd)
                        {
                            self?.payBillForElectricity()
                        }
                        else
                        {
                            self!.presentViewController(Alert().alert("INVALID PASSWORD", message: ""),animated: true,completion: nil)
                            print("Wrong PWD")
                        }
                        
                        
                    }
                    })
                let action1 = UIAlertAction(title: "Forgot", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    print(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
                    self!.sendrequesttoserverForForgotPassword(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
                    
                    })
                let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    })
                
                alertController?.addAction(action)
                alertController?.addAction(action1)
                alertController?.addAction(action2)
                self.presentViewController(alertController!, animated: true, completion: nil)
                
            }
        }
        if(payBtn.tag == 4)
        {
            
            if(customeraccnoo5.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if(amounttxtField5.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
                
            else if(providerbtn5.titleLabel?.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
                
                
            else if(amounttxtField5.text == "0")
            {
                self.presentViewController(Alert().alert("You dont have any due bill on your account", message: ""),animated: true,completion: nil)
            }
            else if(Double(Appconstant.mainbalance) < Double(self.amounttxtField5.text!))
            {
                print("Appconstant.mainbalance")
                print(Appconstant.mainbalance)
                print(amounttxtField5.text!)
//                 LoadMoneyFromCard(self.amounttxtField5.text!)
                self.presentViewController(Alert().alert("Oops! Insufficient Funds..", message: ""),animated: true,completion: nil)
                
            }
                
            else
            {
                Appconstant.r_customerNo = customeraccnoo5.text!
                Appconstant.r_provider = (providerbtn5.titleLabel?.text)!
                
                Appconstant.r_amount = amounttxtField5.text!
                Appconstant.fromsegue = true
                Appconstant.segmentValueWhileClickingForgot = 4
                
                print(" recharge number")
                print( Appconstant.r_mobileNo)
                print(  Appconstant.r_provider)
                print( Appconstant.r_amount)
                
                
                
                
                var alertController:UIAlertController?
                alertController?.view.tintColor = UIColor.blackColor()
                alertController = UIAlertController(title: "PASSWORD",
                                                    message: "Please enter your 4 digit secret password.This is necessary to initiate every transaction",
                                                    preferredStyle: .Alert)
                alertController!.addTextFieldWithConfigurationHandler(
                    {(textField: UITextField!) in
                        
                        textField.placeholder = "Enter 4 digit Password"
                        textField.delegate = self
                        textField.secureTextEntry  = true
                        textField.keyboardType = UIKeyboardType.NumberPad
                        textField.layer.borderColor = UIColor.clearColor().CGColor
                        textField.borderStyle = .None
                })
                let action = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    
                    
                    if let textFields = alertController?.textFields{
                        let theTextFields = textFields as [UITextField]
                        let enteredText = theTextFields[0].text
                        //                        theTextFields[0].borderStyle = .None
                        print("Entered Text==>")
                        print(enteredText!)
                        if(enteredText == Appconstant.pwd)
                        {
                            self?.payBillForGas()
                        }
                        else
                        {
                            self!.presentViewController(Alert().alert("INVALID PASSWORD", message: ""),animated: true,completion: nil)
                            print("Wrong PWD")
                        }
                        
                        
                    }
                    })
                let action1 = UIAlertAction(title: "Forgot", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    print(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
                    self!.sendrequesttoserverForForgotPassword(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
                    
                    })
                let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    })
                
                alertController?.addAction(action)
                alertController?.addAction(action1)
                alertController?.addAction(action2)
                self.presentViewController(alertController!, animated: true, completion: nil)
                
            }
        }
        if(payBtn.tag == 5)
        {
            if(policyno6.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if(amounttxtField6_6.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if(dobtxtField6.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
                
            else if(providerbtn6.titleLabel?.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if(amounttxtField6_6.text == "0")
            {
                self.presentViewController(Alert().alert("You dont have any due bill on your account", message: ""),animated: true,completion: nil)
            }
                
            else if(Double(Appconstant.mainbalance) < Double(self.amounttxtField6_6.text!))
            {
                print("Appconstant.mainbalance")
                print(Appconstant.mainbalance)
                print(amounttxtField6_6.text!)
//                 LoadMoneyFromCard(self.amounttxtField6_6.text!)
                
                self.presentViewController(Alert().alert("Oops! Insufficient Funds..", message: ""),animated: true,completion: nil)
                
            }
                
            else
            {
                Appconstant.r_customerNo = policyno6.text!
                Appconstant.r_provider = (providerbtn6.titleLabel?.text)!
                Appconstant.r_dob = dobtxtField6.text!
                
                Appconstant.r_amount = amounttxtField6.text!
                Appconstant.fromsegue = true
                Appconstant.segmentValueWhileClickingForgot = 5
                
                print(" recharge number")
                print( Appconstant.r_mobileNo)
                print(  Appconstant.r_provider)
                print( Appconstant.r_amount)
                
                
                var alertController:UIAlertController?
                alertController?.view.tintColor = UIColor.blackColor()
                alertController = UIAlertController(title: "PASSWORD",
                                                    message: "Please enter your 4 digit secret password.This is necessary to initiate every transaction",
                                                    preferredStyle: .Alert)
                alertController!.addTextFieldWithConfigurationHandler(
                    {(textField: UITextField!) in
                        
                        textField.placeholder = "Enter 4 digit Password"
                        textField.delegate = self
                        textField.secureTextEntry  = true
                        textField.keyboardType = UIKeyboardType.NumberPad
                        textField.layer.borderColor = UIColor.clearColor().CGColor
                        textField.borderStyle = .None
                        
                })
                let action = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    
                    
                    if let textFields = alertController?.textFields{
                        let theTextFields = textFields as [UITextField]
                        let enteredText = theTextFields[0].text
                        //                        theTextFields[0].borderStyle = .None
                        print("Entered Text==>")
                        print(enteredText!)
                        if(enteredText == Appconstant.pwd)
                        {
                            self?.payBillForInsurance()
                        }
                        else
                        {
                            self!.presentViewController(Alert().alert("INVALID PASSWORD", message: ""),animated: true,completion: nil)
                            print("Wrong PWD")
                        }
                        
                        
                    }
                    })
                let action1 = UIAlertAction(title: "Forgot", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    print(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
                    self!.sendrequesttoserverForForgotPassword(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
                    
                    })
                let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    })
                
                alertController?.addAction(action)
                alertController?.addAction(action1)
                alertController?.addAction(action2)
                self.presentViewController(alertController!, animated: true, completion: nil)
            }
        }
        
        
        
        
        if (payBtn.tag == 6)
        {
            if(phonenotxtField7.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if(account_no7.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
                
                
            else if(providerbtn5.titleLabel?.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if(amounttxtField7_7.text == "0")
            {
                self.presentViewController(Alert().alert("You dont have any due bill on your account", message: ""),animated: true,completion: nil)
            }
                
            else if(amounttxtFieldAfterServiceType.text == "0")
            {
                self.presentViewController(Alert().alert("You dont have any due bill on your account", message: ""),animated: true,completion: nil)
            }
            else if(amounttxtField7_7.text == "")
            {
                self.presentViewController(Alert().alert("Oops! seems you forgot to fill some fields.. We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
                
            else if(Double(Appconstant.mainbalance) < Double(self.amounttxtField7_7.text!))
            {
                print("Appconstant.mainbalance")
                print(Appconstant.mainbalance)
                print(amounttxtField7_7.text!)
//                 LoadMoneyFromCard(self.amounttxtField7_7.text!)
                self.presentViewController(Alert().alert("Oops! Insufficient Funds..", message: ""),animated: true,completion: nil)
                
            }
                
                
                
                
            else
            {
                
                Appconstant.r_phoneNo = phonenotxtField7.text!
                Appconstant.r_provider = (providerbtn7.titleLabel?.text)!
                Appconstant.r_accountNo = account_no7.text!
                
                Appconstant.r_amount = amounttxtField7.text!
                Appconstant.fromsegue = true
                Appconstant.segmentValueWhileClickingForgot = 6
                
                var alertController:UIAlertController?
                alertController?.view.tintColor = UIColor.blackColor()
                alertController = UIAlertController(title: "PASSWORD",
                                                    message: "Please enter your 4 digit secret password.This is necessary to initiate every transaction",
                                                    preferredStyle: .Alert)
                alertController!.addTextFieldWithConfigurationHandler(
                    {(textField: UITextField!) in
                        
                        textField.placeholder = "Enter 4 digit Password"
                        textField.delegate = self
                        textField.secureTextEntry  = true
                        textField.keyboardType = UIKeyboardType.NumberPad
                        textField.layer.borderColor = UIColor.clearColor().CGColor
                        textField.borderStyle = .None
                        
                        
                })
                let action = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    
                    
                    if let textFields = alertController?.textFields{
                        let theTextFields = textFields as [UITextField]
                        let enteredText = theTextFields[0].text
                        //                        theTextFields[0].borderStyle = .None
                        print("Entered Text==>")
                        print(enteredText!)
                        if(enteredText == Appconstant.pwd)
                        {
                            self?.payBillForLandline()
                        }
                        else
                        {
                            self!.presentViewController(Alert().alert("INVALID PASSWORD", message: ""),animated: true,completion: nil)
                            print("Wrong PWD")
                        }
                        
                        
                    }
                    })
                let action1 = UIAlertAction(title: "Forgot", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    print(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
                    self!.sendrequesttoserverForForgotPassword(Appconstant.WEB_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
                    
                    })
                let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak
                    self](paramAction:UIAlertAction!) in
                    
                    })
                
                alertController?.addAction(action)
                alertController?.addAction(action1)
                alertController?.addAction(action2)
                self.presentViewController(alertController!, animated: true, completion: nil)
                
            }
        }
        
}

    func sendrequesttoserverForForgotPassword(url : String)
        
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            
            let item = json["result"]
            if(item["success"].stringValue == "true"){
                dispatch_async(dispatch_get_main_queue()) {
                    Appconstant.otp = item["otp"].stringValue
                    self.performSegueWithIdentifier("recharge_otp", sender: self)
                }
            }
                
            else{
                dispatch_async(dispatch_get_main_queue()) {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
            }
        }
        task.resume()
        
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        
        self.selectedRow = buttonIndex
        if( self.contactalert == true)
        {
            print("Contact values==>>")
            print(buttonIndex)
            print(phonenumber.count)
            print(phonenumber[0])
            //            print(phonenumber[1])
            if buttonIndex == phonenumber.count{
                
            }
            else{
                if(segmentController.selectedSegmentIndex == 0)
                {
                    contactnotxtfield1.text = phonenumber[buttonIndex]
                    amounttxtField1.becomeFirstResponder()
                    amounttxtField1_1.becomeFirstResponder()
                    operatortxtfield1.userInteractionEnabled = true
                }
                else
                {
                    amounttxtField2_2.becomeFirstResponder()
                    contactnotxtfield2.text = phonenumber[buttonIndex]
                    amounttxtField2.becomeFirstResponder()
                    operatortxtfield1.userInteractionEnabled = true
                }
            }
            self.contactalert = false
        }
            
        else
        {
            if segmentController.selectedSegmentIndex == 0 {
                
                if buttonIndex == 0{
                    providername = "Select Provider"
                    providerbtn1.setTitle("Select Provider", forState: .Normal)
                    special = false
                }
                else{
                    
                    special = false
                    providername = Operators.pre_operatorName[index_prov]
                    providerbtn1.setTitle(Operators.pre_operatorName[index_prov], forState: .Normal)
                    amounttxtField1.text = ""
                    operatorcode = Operators.pre_operatorCode[index_prov]
                    if Operators.pre_special[index_prov]{
                        radioView.hidden = false
                    }
                    else{
                        radioView.hidden = true
                    }
                }
            }
            else if segmentController.selectedSegmentIndex == 1{
                if(buttonIndex == 0){
                    providerbtn2.setTitle("Select Provider", forState: .Normal)
                    providername = "Select Provider"
                    radioView.hidden = true
                    
                }
                else{
                    providername = Operators.post_operatorName[buttonIndex-1]
                    providerbtn2.setTitle(Operators.post_operatorName[buttonIndex-1], forState: .Normal)
                    
                    amounttxtField2.text = ""
                    operatorcode = Operators.post_operatorCode[buttonIndex-1]
                    
                    if Operators.post_special[buttonIndex-1]{
                        radioView.hidden = false
//                         radioView.frame.origin.y=210
                        
                    }
                    else{
                        radioView.hidden = true
                    }
                    
                }
            }
            else if segmentController.selectedSegmentIndex == 2 {
                if buttonIndex == 0{
                    providername = "Select Provider"
                    provider3.setTitle("Select Provider", forState: .Normal)
                    operatortxtfield3.userInteractionEnabled = false
                    special = false
                }
                else{
                    
                    special = false
                    providername = Operators.dth_operatorName[buttonIndex-1]
                    provider3.setTitle(Operators.dth_operatorName[buttonIndex-1], forState: .Normal)
                    
                    amounttxtField3.text = ""
                    operatorcode = Operators.dth_operatorCode[buttonIndex-1]
                    operatortxtfield3.userInteractionEnabled = true
                }
            }
                
                
            else if segmentController.selectedSegmentIndex == 3 {
                
            }
                
                
                
                //
                //            else if segmentController.selectedSegmentIndex == 3 {
                //
                //                if buttonIndex == 0{
                //                    providername = "Select Electricity board"
                //                    providerbtn4.setTitle("Select Electricity board", forState: .Normal)
                //                    special = false
                //                }
                //                else{
                //                    constraintsEle.hidden = false
                //                    special = false
                //                    print(Operators.elect_operatorName[buttonIndex-1])
                //                    providername = Operators.elect_operatorName[buttonIndex-1]
                //                    providerbtn4.setTitle(Operators.elect_operatorName[buttonIndex-1], forState: .Normal)
                //                    let provider = Operators.elect_operatorName[buttonIndex-1]
                //
                //
                //                    //                let provider = providerbtn4.titleLabel?.text
                //                    print(provider)
                //                    if(provider == "West Bengal State Electricity Distribution Company Limited")
                //                    {
                //                        amounttxtField4.hidden = true
                //                        amounttxtfield4_4.hidden = false
                //                        label4.hidden = false
                //                        phone_no4.hidden = false
                //                    }
                //                    else{
                //                        amounttxtField4.hidden = false
                //                        amounttxtfield4_4.hidden = true
                //                        label4.hidden = true
                //                        phone_no4.hidden = true
                //                    }
                //                    customer_no4.text = ""
                //                    amounttxtField4.text = ""
                //                    toastLabel.hidden = true
                //                    operatorcode = Operators.elect_operatorCode[buttonIndex-1]
                //                }
                //            }
            else if segmentController.selectedSegmentIndex == 4 {
                if buttonIndex == 0{
                    providername = "Select Gas Company"
                    providerbtn5.setTitle("Select Gas Company", forState: .Normal)
                    special = false
                }
                else{
                    constraintgas.hidden = false
                    special = false
                    providername = Operators.gas_operatorName[buttonIndex-1]
                    providerbtn5.setTitle(Operators.gas_operatorName[buttonIndex-1], forState: .Normal)
                    customeraccnoo5.text = ""
                    amounttxtField5.text = ""
                    operatorcode = Operators.gas_operatorCode[buttonIndex-1]
                }
            }
            else if segmentController.selectedSegmentIndex == 5 {
                if buttonIndex == 0{
                    providername = "Select Company"
                    providerbtn6.setTitle("Select Company", forState: .Normal)
                    special = false
                }
                else
                {
                    constraintins.hidden = false
                    special = false
                    providername = Operators.ins_operatorName[buttonIndex-1]
                    providerbtn6.setTitle(Operators.ins_operatorName[buttonIndex-1], forState: .Normal)
                    policyno6.text = ""
                    amounttxtField6_6.text = ""
                    amounttxtField6.text == ""
                    dobtxtField6.text = ""
                    operatorcode = Operators.ins_operatorCode[buttonIndex-1]
                    ins_provider = Operators.ins_operatorName[buttonIndex-1]
                    
                    if(ins_provider == "INDIA FIRST Life Insurance" || ins_provider == "PNB Metlife")
                    {
                        
                        dobtxtField6.hidden = true
                        amounttxtField6.hidden = false
                        amounttxtField6_6.hidden = true
                        label6_6.hidden = true
                        
                        
                    }
                    
                    
                }
            }
            else if segmentController.selectedSegmentIndex == 6 {
                if buttonIndex == 0{
                    providername = "Select Provider"
                    providerbtn7.setTitle("Select Provider", forState: .Normal)
                    special = false
                }
                else{
                    special = false
                    constraintlan.hidden = false
                    providername = Operators.lan_operatorName[buttonIndex-1]
                    providerbtn7.setTitle(Operators.lan_operatorName[buttonIndex-1], forState: .Normal)
                    self.activityIndicator.stopAnimating()
                    phonenotxtField7.text = ""
                    account_no7.text = ""
                    amounttxtField7_7.text = ""
                    amounttxtField7.text = ""
                    operatorcode = Operators.lan_operatorCode[buttonIndex-1]
                    lan_provider = Operators.lan_operatorName[buttonIndex-1]
                    
                    if(lan_provider != "BSNL landline" || lan_provider != "MTNL Delhi")
                    {
                        account_no7.hidden = true
                        constriantButtonAccount7.hidden = true
                        amounttxtField7.hidden = false
                        amounttxtField7_7.hidden = true
                        label7_7.hidden = true
                        amounttxtFieldAfterServiceType.hidden = true
                        serviceType7.hidden = true
                        constriantButtonServiceType7.hidden = true
                        labelLineForAmount3.hidden = true
                    }
                    else
                    {
                        
                    }
                    
                    
                }
            }
            
            
        }
    }
    
    @IBAction func sselectproviderdropdown(sender: AnyObject) {
        let alertView: UIAlertView = UIAlertView()
        alertView.delegate = self
        
        
        
        if segmentController.selectedSegmentIndex == 0{
            alertView.addButtonWithTitle("Select Provider")
            for(var i = 0; i<Operators.pre_operatorName.count; i++){
                alertView.addButtonWithTitle(Operators.pre_operatorName[i])
            }
        }
        else if segmentController.selectedSegmentIndex == 1 {
            alertView.addButtonWithTitle("Select Provider")
            for(var i = 0; i<Operators.post_operatorName.count; i++){
                alertView.addButtonWithTitle(Operators.post_operatorName[i])
            }
        }
        else if segmentController.selectedSegmentIndex == 2 {
            alertView.addButtonWithTitle("Select Provider")
            for(var i = 0; i<Operators.dth_operatorName.count; i++){
                alertView.addButtonWithTitle(Operators.dth_operatorName[i])
            }
        }
        else  if segmentController.selectedSegmentIndex == 3{
            print(Operators.elect_operatorName)
            print(Operators.elect_operatorName.count)
            alertView.addButtonWithTitle("Select Electricity Board")
            for(var i = 0; i<Operators.elect_operatorName.count; i++){
                alertView.addButtonWithTitle(Operators.elect_operatorName[i])
            }
        }
        else if segmentController.selectedSegmentIndex == 4 {
            print(Operators.gas_operatorName)
            alertView.addButtonWithTitle("Select Gas Company")
            for(var i = 0; i<Operators.gas_operatorName.count; i++){
                alertView.addButtonWithTitle(Operators.gas_operatorName[i])
            }
        }
        else if segmentController.selectedSegmentIndex == 5 {
            print(Operators.ins_operatorName)
            alertView.addButtonWithTitle("Select Company")
            for(var i = 0; i<Operators.ins_operatorName.count; i++){
                alertView.addButtonWithTitle(Operators.ins_operatorName[i])
            }
        }
        else if segmentController.selectedSegmentIndex == 6 {
            alertView.addButtonWithTitle("Select Provider")
            for(var i = 0; i<Operators.lan_operatorName.count; i++){
                alertView.addButtonWithTitle(Operators.lan_operatorName[i])
            }
        }
        
        
        alertView.show()
        
    }
    @IBAction func operatorbtnaction(sender: AnyObject) {
        isPrepaidClicked = true
        self.performSegueWithIdentifier("operatorSegue", sender: self)
    }
    
    @IBAction func operatorbtnacrion3(sender: AnyObject) {
        isPrepaidClicked = false
        self.performSegueWithIdentifier("operatorSegue", sender: self)
    }
    
    @IBAction func browse1btnAction(sender: AnyObject) {
        self.performSegueWithIdentifier("recharge_to_plan", sender: self)
    }
    
    @IBAction func browse3btnAction(sender: AnyObject) {
        self.performSegueWithIdentifier("recharge_to_plan", sender: self)
    }
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "operatorSegue") {
            let nextview = segue.destinationViewController as! operatorViewController
            nextview.IsPostpaid = IsPostpaid
            nextview.operatorNameFromRecharge = (operatortxtfield1.titleLabel?.text)!
            nextview.isClickedPrepaid = self.isPrepaidClicked
            if radioView.hidden{
                nextview.isradioviewhide = true
            }
            else{
                nextview.isradioviewhide = false
            }
            nextview.isradiospecial = specialselected
            if segmentController.selectedSegmentIndex == 0{
                nextview.amount = self.amounttxtField1.text!
                nextview.mobile = self.contactnotxtfield1.text!
                nextview.provider = (self.providerbtn1.titleLabel?.text)!
                nextview.browsebtn1_hide = browsePlanbtn1.hidden
                nextview.browsebtn3_hide = browsePlanbtn3.hidden
                nextview.selectedindex = 0
            }
            else if segmentController.selectedSegmentIndex == 2{
                nextview.amount = self.amounttxtField3.text!
                nextview.mobile = self.customerIdtxtfield3.text!
                nextview.provider = (self.provider3.titleLabel?.text)!
                nextview.browsebtn1_hide = browsePlanbtn1.hidden
                nextview.browsebtn3_hide = browsePlanbtn3.hidden
                nextview.selectedindex = 2
            }
        }
        else if(segue.identifier == "recharge_to_plan"){
            let nextview = segue.destinationViewController as! BrowsePlanViewController
            nextview.plan_selectedspecial = specialselected
            if radioView.hidden{
                nextview.isradioviewhide = true
            }
            else{
                nextview.isradioviewhide = false
            }
            nextview.isradiospecial = specialselected
            if segmentController.selectedSegmentIndex == 0{
                nextview.providername = (self.providerbtn1.titleLabel?.text)!
                nextview.segmentno = (self.segmentController.selectedSegmentIndex)
                nextview.servicename = "M"
                //                nextview.servicename = "M"
                nextview.locationname = (operatortxtfield1.titleLabel?.text)!
                nextview.plan_mobileno = self.contactnotxtfield1.text!
                nextview.plan_provider = (self.providerbtn1.titleLabel?.text)!
                nextview.plan_operator = (operatortxtfield1.titleLabel?.text)!
                nextview.plan_amt = self.amounttxtField1.text!
                nextview.plan_radioview = radioView.hidden
                nextview.plan_selectedindex = 0
            }
            else if segmentController.selectedSegmentIndex == 2{
                nextview.providername = (self.provider3.titleLabel?.text)!
                print(self.segmentController.selectedSegmentIndex)
                nextview.segmentno = (self.segmentController.selectedSegmentIndex)
                nextview.servicename = "D"
                nextview.locationname = (operatortxtfield3.titleLabel?.text)!
                nextview.plan_mobileno = self.customerIdtxtfield3.text!
                nextview.plan_provider = (self.provider3.titleLabel?.text)!
                nextview.plan_operator = (operatortxtfield3.titleLabel?.text)!
                nextview.plan_amt = self.amounttxtField3.text!
                nextview.plan_radioview = radioView.hidden
                nextview.plan_selectedindex = 2
            }
        }
        if(segue.identifier == "from_provider") {
            let nextview = segue.destinationViewController as!SelectProviderViewController
            if(segmentno_provider == 0)
            {
                nextview.mobileno = mobileno
                nextview.segmentNumberSelectedInProvider = segmentno_provider
                nextview.operatorName = Operators.pre_operatorName
            }
            else if(segmentno_provider == 1)
            {
                nextview.mobileno = mobileno
                nextview.segmentNumberSelectedInProvider = segmentno_provider
                nextview.operatorName = Operators.post_operatorName
            }
            else if(segmentno_provider == 2)
            {
                nextview.mobileno = mobileno
                nextview.segmentNumberSelectedInProvider = segmentno_provider
                nextview.operatorName = Operators.dth_operatorName
            }
            else if(segmentno_provider == 3)
            {
                nextview.segmentNumberSelectedInProvider = segmentno_provider
                nextview.operatorName = Operators.elect_operatorName
            }
            else if(segmentno_provider == 4)
            {
                nextview.segmentNumberSelectedInProvider = segmentno_provider
                nextview.operatorName = Operators.gas_operatorName
            }
            else if(segmentno_provider == 5)
            {
                nextview.segmentNumberSelectedInProvider = segmentno_provider
                nextview.operatorName = Operators.ins_operatorName
            }
            else if(segmentno_provider == 6)
            {
                nextview.segmentNumberSelectedInProvider = segmentno_provider
                nextview.operatorName = Operators.lan_operatorName
            }
        }
        
        else if segue.identifier == "web" {
            let nextview = segue.destinationViewController as! WebViewController
            nextview.fromRecharge = true
           
        }
    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            print("Selected item 0")
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("To_Home", sender: self)
            }
            
        }
        else if tabBarIndex == 2 {
            print("Selected item 2")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("logout")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            print("Selected item 1")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("MyAccount")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    func updatetransactionDB(){
        DBHelper().purzDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
        let databasePath = databaseURL.absoluteString
        let purzDB = FMDatabase(path: databasePath as String)
        
        if purzDB.open() {
            let delete = "DELETE FROM TRANSACTIONS"
            
            let result = purzDB.executeUpdate(delete,
                                              withArgumentsInArray: nil)
            
            if !result{
                //   status.text = "Failed to add contact"
                print("Error: \(purzDB.lastErrorMessage())")
            }
            
        }
        purzDB.close()
        
        let request = NSMutableURLRequest(URL: NSURL(string: Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=10")!)
        request.HTTPMethod = "GET"
        request.addValue("Basic NzY2NzE4OTI5MToxMjM0", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                self.view.userInteractionEnabled = true
                
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            DBHelper().purzDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("purz.db")
            let databasePath = databaseURL.absoluteString
            let purzDB = FMDatabase(path: databasePath as String)
            
            for item1 in json["result"].arrayValue{
                let item = item1["transaction"]
                
                var transactionamt = ""
                var ben_id = ""
                var trans_date = ""
                var descriptions = ""
                let balance_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                let amt = balance_two_decimal.componentsSeparatedByString(".")
                if(amt[1].characters.count == 1){
                    let finalamount = balance_two_decimal + "0"
                    transactionamt = finalamount
                }
                else{
                    transactionamt = balance_two_decimal
                }
                if(!item["beneficiaryId"].stringValue.isEmpty){
                    let benid = item["beneficiaryId"].stringValue.componentsSeparatedByString("+91")
                    print(benid)
                    var i = 0
                    for(i=0; i<benid.count; i++){
                        
                    }
                    ben_id = benid[i-1]
                }
                else{
                    ben_id = item["beneficiaryId"].stringValue
                }
                let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd MMM"
                dateFormatter.timeZone = NSTimeZone(name: "UTC")
                let dateString = dateFormatter.stringFromDate(date)
                if(!dateString.isEmpty){
                    let datearray = dateString.componentsSeparatedByString(" ")
                    if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                        let correctdate = datearray[0] + "st " + datearray[1]
                        trans_date = correctdate
                    }
                    else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                        let correctdate = datearray[0] + "nd " + datearray[1]
                        trans_date = correctdate
                    }
                    else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                        let correctdate = datearray[0] + "rd " + datearray[1]
                        trans_date = correctdate
                    }
                    else{
                        let correctdate = datearray[0] + "th " + datearray[1]
                        trans_date = correctdate
                    }
                    
                }
                let matches = self.matchesForRegexInText("[0-9]", text: item["description"].stringValue)
                let desc = matches.joinWithSeparator("")
                descriptions = desc
                
                if purzDB.open() {
                    
                    let insert = "INSERT INTO TRANSACTIONS (AMOUNT,BENEFICIARY_ID,TRANSACTION_TYPE,TYPE,TIME,TRANSACTION_STATUS,TX_REF,BENEFICIARY_NAME,DESCRIPTION,OTHER_PARTY_NAME,OTHER_PARTY_ID,TXN_ORIGIN,TRANSACTIONID) VALUES"
                    let value0 =  "('"+transactionamt+"','\(ben_id)','\(item["transactionType"].stringValue)','\(item["type"].stringValue)',"
                    let value1 = "'"+trans_date+"','\(item["transactionStatus"].stringValue)','\(item["txRef"].stringValue)','\(item["beneficiaryName"].stringValue)',"
                    let value2 = "'\(descriptions)','\(item["otherPartyName"].stringValue)','\(item["otherPartyId"].stringValue)','\(item["txnOrigin"].stringValue)','\(item["externalTransactionId"].stringValue)')"
                    let insertsql = insert+value0+value1+value2
                    let result = purzDB.executeUpdate(insertsql,
                                                      withArgumentsInArray: nil)
                    
                    if !result {
                        //   status.text = "Failed to add contact"
                        print("Error: \(purzDB.lastErrorMessage())")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                    purzDB.close()
                    
                }
            }
        }
        
        task.resume()
    }
    func matchesForRegexInText(regex: String, text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                                                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    
    

    

    
    func LoadMoneyFromCard(amount: String) {
    webchecktxtFieldAmt = amount
        let addmoneyviewmodel = AddMoneyViewModel.init(entityId: Appconstant.customerid, amount: Float(Double(amount)!-Double(Appconstant.mainbalance)!), pgType: "", bankName: "")!
        let serializedjson  = JSONSerializer.toJson(addmoneyviewmodel)
        print(serializedjson)
        self.sendrequesttoserverBillDeskIntegration(Appconstant.BASE_URL+Appconstant.URL_BILL_DESK_INTEGRATION, values: serializedjson)
    }
    
    func sendrequesttoserverBillDeskIntegration(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("Basic YWRtaW46YWRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                print(error)
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                let item = json["result"]
                print(json["result"])
                print(json["result"].stringValue)
                if json["result"].isEmpty{
                    let item1 = json["exception"]
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert(item1["detailMessage"].stringValue, message: ""),animated: true,completion: nil)
                    }
                }
                else{
                    
                    Appconstant.Url = item["targetUrl"].stringValue + "?msg=" + item["requestMessage"].stringValue
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.performSegueWithIdentifier("web", sender: self)
                    }
                }
            }
        }
        
        task.resume()
        
    }
    
}


